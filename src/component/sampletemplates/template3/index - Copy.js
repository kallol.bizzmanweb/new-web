import React , { Component} from 'react';

import Constants from '../common/Constants';

import { MDBContainer, MDBTabPane, MDBTabContent, MDBNav, MDBNavItem, MDBNavLink } from "mdbreact";

import { MDBBtn, MDBModal, MDBModalBody, MDBModalHeader, MDBModalFooter } from 'mdbreact';


import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
//import './style/react-tabs.css';

import './css/main.css';
//import './css/main.css.map';
//import './css/main.scss';

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCubes, faFileAlt } from "@fortawesome/free-solid-svg-icons";
import { faAppleAlt } from "@fortawesome/free-solid-svg-icons";
import { faCog } from "@fortawesome/free-solid-svg-icons";
import { faFile } from "@fortawesome/free-solid-svg-icons";
import { faClock } from "@fortawesome/free-solid-svg-icons";
import { faArrowDown } from "@fortawesome/free-solid-svg-icons";
import { faCrown } from "@fortawesome/free-solid-svg-icons";

import { faUpload } from "@fortawesome/free-solid-svg-icons";
import { faTextHeight } from "@fortawesome/free-solid-svg-icons";
import { faFolder } from "@fortawesome/free-solid-svg-icons";
import { faColumns } from "@fortawesome/free-solid-svg-icons";
import { faVideo } from "@fortawesome/free-solid-svg-icons";
import { faEnvelope } from "@fortawesome/free-solid-svg-icons";

import Userpics from './128.jpg';

//import Userpics1 from './images/500.jpg';

class Templates extends Component {
  
    state = {
      activeItem: "1"
    }

  constructor() {
      super();
      this.state = {
        showPopup: false,
        showDiv: true,
        show:true,
        isActive: true,
        isActive1: false,
        isActive2: false,
        isActive3: false,
        isActive4: false
      };
    }
    togglePopup() {
      this.setState({
        showPopup: !this.state.showPopup
      });
    }
    
    
    toggle = tab => () => {
      if (this.state.activeItem !== tab) {
      this.setState({
        activeItem: tab
      });
      }
    }

    handleShow = () => {
      this.setState({
        isActive: true,
        isActive1: false,
        isActive2: false,
        isActive3: false,
        isActive4: false,
        isActive5: false
      });
    };

    handleShows = () => {
      this.setState({
        isActive: false,
        isActive1: true,
        isActive2: false,
        isActive3: false,
        isActive5: false

      });
    };

    handleShows1 = () => {
      this.setState({
        isActive: false,
        isActive1: false,
        isActive2: true,
        isActive3: false,
        isActive4: false,
        isActive5: false
      });
    };

    handleShows2 = () => {
      this.setState({
        isActive: false,
        isActive1: false,
        isActive2: false,
        isActive3: true,
        isActive4: false,
        isActive5: false
      });
    };

    handleShows3 = () => {
      this.setState({
        isActive: false,
        isActive1: false,
        isActive2: false,
        isActive3: false,
        isActive4: true,
        isActive5: false
      });
    };
    handleShows4 = () => {
      this.setState({
        isActive: false,
        isActive1: false,
        isActive2: false,
        isActive3: false,
        isActive4: false,
        isActive5: true
      });
    };
  
    //state = { isOpen: false };
  
    handleShowDialog = () => {
      this.setState({ isOpen: !this.state.isOpen });
      console.log("clicked");
      
    };
  /*
    constructor(props) {
      super(props);
  
      this.state = { isOpen: false };
    }
  
    toggleModal = () => {
      this.setState({
        isOpen: !this.state.isOpen
      });
    }
    */
  
    render() {

      const imagesPath = Constants.imagespath;

      const { showDiv } = this.state.showDiv;

      const template = () => {
        window.location.href = "/Template2";
        console.log("Template Page"); 
       }
  
      return (
<html>
  <body>
        <div class="resume-wrapper">
        <section class="profile section-padding" style={{width:'40%'}}>
            <div class="container">
                <div class="picture-resume-wrapper">
            <div class="picture-resume">
            <span><img src={Userpics} alt="" /></span>
  
          </div>
             <div class="clearfix"></div>
     </div>
          <div class="name-wrapper">
            <h1>John <br/>Anderson</h1>
          </div>
          <div class="clearfix"></div>
          <div class="contact-info clearfix">
              <ul class="list-titles">
                  <li>Call</li>
                  <li>Mail</li>
                  <li>Web</li>
                  <li>Home</li>
              </ul>
            <ul class="list-content ">
                <li>+34 123 456 789</li>
                <li>j.anderson@gmail.com</li> 
                <li><a href="#">janderson.com</a></li> 
                <li>Los Angeles, CA</li> 
            </ul>
          </div>
          <div class="contact-presentation"> 
              <p><span class="bold">Lorem</span> ipsum dolor sit amet, consectetur adipiscing elit. Vivamus euismod congue nisi, nec consequat quam. In consectetur faucibus turpis eget laoreet. Sed nec imperdiet purus. </p>
          </div>
          <div class="contact-social clearfix">
              <ul class="list-titles">
                  <li>Twitter</li>
                  <li>Dribbble</li>
                  <li>Codepen</li>
              </ul>
            <ul class="list-content"> 
                  <li><a href="">@janderson</a></li> 
                  <li><a href="">janderson</a></li> 
                  <li><a href="">janderson</a></li> 
              </ul>
          </div>
            </div>
        </section>
      
      <section class="experience section-padding">
          <div class="container">
              <h3 class="experience-title">Experience</h3>
          
          <div class="experience-wrapper">
              <div class="company-wrapper clearfix">
                  <div class="experience-title">Company name</div> 
              <div class="time">Nov 2012 - Present</div> 
              </div>
            
            <div class="job-wrapper clearfix">
                <div class="experience-title">Front End Developer </div> 
              <div class="company-description">
                  <p style={{width:'50%'}}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce a elit facilisis, adipiscing leo in, dignissim magna.</p>  
              </div>
            </div>
            
            <div class="company-wrapper clearfix">
                  <div class="experience-title">Company name</div> 
              <div class="time">Nov 2010 - Present</div> 
              </div>
            
             <div class="job-wrapper clearfix">
                <div class="experience-title">Freelance, Web Designer / Web Developer</div> 
              <div class="company-description">
                  <p style={{width:'50%'}}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce a elit facilisis, adipiscing leo in, dignissim magna.</p>  
              </div>
            </div>
            
            <div class="company-wrapper clearfix">
                  <div class="experience-title">Company name</div> 
              <div class="time">Nov 2009 - Nov 2010</div> 
              </div> 
            
             <div class="job-wrapper clearfix">
                <div class="experience-title">Web Designer </div> 
              <div class="company-description">
                  <p style={{width:'50%'}}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce a elit facilisis, adipiscing leo in, dignissim magna.</p>   
              </div>
            </div>
            
          </div>
          
          <div class="section-wrapper clearfix">
              <h3 class="section-title">Skills</h3>  
                <ul>
                    <li class="skill-percentage">HTML / HTML5</li>
                    <li class="skill-percentage">CSS / CSS3 / SASS / LESS</li>
                    <li class="skill-percentage">Javascript</li>
                    <li class="skill-percentage">Jquery</li>
                    <li class="skill-percentage">Wordpress</li>
                    <li class="skill-percentage">Photoshop</li>
                
                </ul>
            
          </div>
          
          <div class="section-wrapper clearfix">
            <h3 class="section-title">Hobbies</h3>  
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce a elit facilisis, adipiscing leo in, dignissim magna.</p>
            
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce a elit facilisis, adipiscing leo in, dignissim magna.</p> 
          </div>
          
          </div>
      </section>
      
      <div class="clearfix"></div>
    </div>
    </body>
</html>
        
    
    
    );
  }
}

export default Templates;