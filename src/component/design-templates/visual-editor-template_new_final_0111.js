import React , { Component} from 'react';

import Constants from '../../common/Constants';

import { MDBContainer, MDBTabPane, MDBTabContent, MDBNav, MDBNavItem, MDBNavLink } from "mdbreact";

import { MDBBtn, MDBModal, MDBModalBody, MDBModalHeader, MDBModalFooter } from 'mdbreact';


import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import './style/react-tabs.css';

import DonutChart from "react-svg-donut-chart";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCubes, faFileAlt } from "@fortawesome/free-solid-svg-icons";
import { faAppleAlt } from "@fortawesome/free-solid-svg-icons";
import { faCog } from "@fortawesome/free-solid-svg-icons";
import { faFile } from "@fortawesome/free-solid-svg-icons";
import { faClock } from "@fortawesome/free-solid-svg-icons";
import { faArrowDown } from "@fortawesome/free-solid-svg-icons";
import { faCrown } from "@fortawesome/free-solid-svg-icons";

import { faUpload } from "@fortawesome/free-solid-svg-icons";
import { faTextHeight } from "@fortawesome/free-solid-svg-icons";
import { faFolder } from "@fortawesome/free-solid-svg-icons";
import { faColumns } from "@fortawesome/free-solid-svg-icons";
import { faVideo } from "@fortawesome/free-solid-svg-icons";
import { faEnvelope } from "@fortawesome/free-solid-svg-icons";

import Userpics from './images/300.png';

import Userpics1 from './images/500.jpg';

import Template1 from './images/image1.jpeg';
import Template2 from './images/image3.jpeg';
import Template3 from './images/image2.jpeg';

import pies from './images/piechart.png';
import bars from './images/barchart.jpg';
import lines from './images/linechart.png';

import scenery from './images/scenery.jpg';
import scenery2 from './images/scene2.jpg';

import image1 from './images/Icons/1.png';
import image2 from './images/Icons/2.png';
import image3 from './images/Icons/3.png';

import Chart from "chart.js";

import style from './css/main.scss';

////import CanvasImage from '@axetroy/react-canvas-image';

//import Draggable from 'react-draggable';

//import {Editor, Frame, Canvas, Selector} from "@craftjs/core";

import ResizeImage from 'react-resize-image';

import { DraggableEventHandler, default as DraggableRoot } from "react-draggable";
import { Enable, Resizable, ResizeDirection } from "re-resizable";

//import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import { Rnd } from "react-rnd";

import axios from 'axios';

import {Layer, Rect, Stage, Group} from 'react-konva';

import {Pie, Doughnut} from 'react-chartjs-2';

import A4 from './baseTemplate'
import { EditableText, List, RowTexts, HeaderPic } from './core'

import cs from 'classnames';

import RaisedButton from 'material-ui/RaisedButton'
////import cs from 'classnames'
import ph from './placeholder.jpg'
import phimg from './128.jpg'
import FileReaderInput from 'react-file-reader-input'

//import Userpics1 from './images/500.jpg';    

import phpie from './charts7.png';


import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

//import { Editor } from 'react-draft-wysiwyg';
//import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';


import { EditorState, ContentState, convertToRaw, convertFromRaw } from 'draft-js';
import htmlToDraft from 'html-to-draftjs';
import draftToHtml from 'draftjs-to-html';

import { Row, FormGroup, FormControl, ControlLabel, Button, HelpBlock } from 'react-bootstrap';

import styles from './css/style.scss'; 

import { Draggable, Droppable } from 'react-drag-and-drop';



import { PieChart } from 'react-minimal-pie-chart';


import ReactQuill, { Quill } from 'react-quill';
import quillEmoji from 'quill-emoji';
import 'react-quill/dist/quill.snow.css';
const { EmojiBlot, ShortNameEmoji, ToolbarEmoji, TextAreaEmoji } = quillEmoji;


Quill.register({
  'formats/emoji': EmojiBlot,
  'modules/emoji-shortname': ShortNameEmoji,
  'modules/emoji-toolbar': ToolbarEmoji,
  'modules/emoji-textarea': TextAreaEmoji
}, true);



const modulesQuill = {
  toolbar: {
    container: [
      [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
      [{ 'font': [] }],
      [{ 'align': [] }],
      ['bold', 'italic', 'underline', 'strike'],
      [{ 'list': 'ordered' }, { 'list': 'bullet' }, { 'script': 'sub'}, { 'script': 'super' }, { 'indent': '-1'}, { 'indent': '+1' }, { 'direction': 'rtl' }, { 'size': ['small', false, 'large', 'huge'] }, { 'color': ['#000000', '#e60000', '#ff9900', '#ffff00', '#008a00', '#0066cc', '#9933ff', '#ffffff', '#facccc', '#ffebcc', '#ffffcc', '#cce8cc', '#cce0f5', '#ebd6ff', '#bbbbbb', '#f06666', '#ffc266', '#ffff66', '#66b966', '#66a3e0', '#c285ff', '#888888', '#a10000', '#b26b00', '#b2b200', '#006100', '#0047b2', '#6b24b2', '#444444', '#5c0000', '#663d00', '#666600', '#003700', '#002966', '#3d1466', 'custom-color'] }, { 'background': [] }, 'link', 'emoji'],          // dropdown with defaults from theme
      ['clean']      
    ],
    handlers: {
      'color': function (value) {
        if (value == 'custom-color') value = window.prompt('Enter Hex Color Code');
        this.quill.format('color', value);
      }
    }
  },
  keyboard: {
    bindings: {
      tab: false,
      custom: {
        key: 13,
        shiftKey: true,
        handler: function () { /** do nothing */ }
      },
      handleEnter: {
        key: 13,
        handler: function () { /** do nothing */ }
      }
    }
  },
  'emoji-toolbar': true,
  'emoji-textarea': true,
  'emoji-shortname': true,
  
};

const formatsQuill = [
  'header', 'font', 'size',
  'bold', 'italic', 'underline', 'strike', 'blockquote',
  'list', 'bullet', 'indent', 'align',
  'link', 'image', 'background', 'color', 'emoji'
];

const dataPie = [
  {value: 100, stroke: "#22594e", strokeWidth: 6},
  {value: 60, stroke: "#2f7d6d"},
  {value: 30, stroke: "#3da18d"},
  {value: 20, stroke: "#69c2b0"},
  {value: 10, stroke: "#a1d9ce"},
]

class Templates extends Component {
  
    state = {
      activeItem: "1"
    }

  constructor() {
      super();
      ////this.state = {fields: {},emailerror:'', loginError: ''};

      const retrievedsess = sessionStorage.getItem("search");     

      this.myRef = React.createRef();

      this.ctx = React.createRef();

      this.ctxs = React.createRef();

      var html ='<div>';

      html = html + 'First Company, Inc., Los Angeles, CA January 1, 2007 - present';
      
      html = html + 'Customer Service Manager';
      html = html + '•	Oversee Customer Service Department';
      html = html + '•	Supervise Customer Service Representatives ';
      
      html = html + '•	Winner, First Company Excellence Award';
      
      html = html + 'Second Corporation, San Diego, CA';
      html = html + 'January 1, 2003 - December 31, 2006';
      
      html = html + 'Customer Service Representative';
      html = html + '•	Provide service to customers via telephone and email';
      html = html + '•	Respond to all inquiries within 24 hours';
      
      html = html + '•	Successfully reorganized call escalation protocol';

      html = html + '</div>';

      
     /* this.state = {
        editorState: EditorState.createEmpty(),
        hideToolbar: true,
        emailbody : "html here"
      };
      */

      this.state = { editorHtml :'' }

       

      

      if(retrievedsess=='true'){

      this.state = {
        showPopup: false,
        showDiv: true,
        show:true,
        isActiveCreate: false,
        isActive: false,
        isActive1: false,
        isActive2: false,
        isActive3: false,
        isActive4: false,
        isActive5: false,
        isActive6: false,
        isActive7: true,
        fields: {},emailerror:'', loginError: '',
        editorHtml : html
      };

    }else{

      this.state = {
        showPopup: false,
        showDiv: true,
        show:true,
        isActiveCreate: false,
        isActive: true,
        isActive1: false,
        isActive2: false,
        isActive3: false,
        isActive4: false,
        isActive5: false,
        isActive6: false,
        isActive7: false,
        isActive8: false,
        isActive9: true,
        fields: {},emailerror:'', loginError: '',
        editorHtml : html
      };
      
    }

    

    //const html = 'Technical lead and senior developer with 19+ years of experience working on full lifecycle projects involving component based architectures. Experience includes implementing B2B and B2C omni-channel applications (web-11+(Php,Java), mobile(android, ios, ionic – 6+ years, Hadoop and Big Data(Map Reduce, Hive, Pig, Hbase)), client server applications and enabling of back office applications.';

    

   /* const reactStringReplace = require('react-string-replace');
    const backEndResponseString = "content here";
    const emailBody = reactStringReplace(backEndResponseString, () => (
      <span>Hello</span>
    ));
*/

    const contentBlock = htmlToDraft(html);
    if (contentBlock) {
      const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks);
      const newEditorState = EditorState.createWithContent(contentState);
      this.state.editorState = newEditorState;
    }

    this.states1={
      backEndResponseString : html
    }   

  /*  this.states1={
      
    }     
  */

  
  }
    
  /*  
  handleChange = (value)=>{
    this.setState({ emailbody: value });
    }
  */  

    handleChange = (html) => {
      this.setState({
        editorHtml : html
      })
    } 


    onEditorStateChange = editorState => {
    this.setState({
    editorState
    });
    };
    
    onContentStateChange = contentState => {
    this.setState({
    contentState
    });
    };
  

    togglePopup() {
      this.setState({
        showPopup: !this.state.showPopup
      });
    };
    
    handleLoginFormData(e) {
      e.preventDefault();
      let fieldName = e.target.name, fieldValue = e.target.value, fields = this.state.fields;
  
      fields[fieldName] = fieldValue;

      //alert(fields);

      this.setState({fields: fields});
   
    }
    
    toggle = tab => () => {
      if (this.state.activeItem !== tab) {
      this.setState({
        activeItem: tab
      });
      }
    }

    handleShow = () => {
      this.setState({
        isActiveCreate: false, 
        isActive:  true,
        isActive1: false,
        isActive2: false,
        isActive3: false,
        isActive4: false,
        isActive5: false,
        isActive6: false,
        isActive7: false,
        isActive8: false,
        isActive9: true
      });
    };

    handleShows = () => {
      this.setState({
        isActiveCreate: false,
        isActive: false,
        isActive1: true,
        isActive2: false,
        isActive3: false,
        isActive5: false,
        isActive6: false,
        isActive7: false,
        isActive8: false,
        isActive9: true
      });
    };

    handleShowsCreate = () => {
      this.setState({
        isActiveCreate: true,
        isActive: false,
        isActive1: false,
        isActive2: true,
        isActive3: false,
        isActive5: false,
        isActive6: false,
        isActive7: false,
        isActive8: false,
        isActive9: false
      });
    };

    handleShows1 = () => {
      this.setState({
        isActiveCreate: false,
        isActive: false,
        isActive1: false,
        isActive2: true,
        isActive3: false,
        isActive4: false,
        isActive5: false,
        isActive6: false,
        isActive7: false,
        isActive8: true,
        isActive9: true
      });
    };

    handleShows2 = () => {
      this.setState({
        isActiveCreate: false,
        isActive: false,
        isActive1: false,
        isActive2: false,
        isActive3: true,
        isActive4: false,
        isActive5: false,
        isActive6: false,
        isActive7: false,
        isActive8: false,
        isActive9: true
      });
    };

    handleShows3 = () => {
      this.setState({
        isActiveCreate: false,
        isActive: false,
        isActive1: false,
        isActive2: false,
        isActive3: false,
        isActive4: true,
        isActive5: false,
        isActive6: false,
        isActive7: false,
        isActive8: false,
        isActive9: true
      });
    };
    handleShows4 = () => {
      this.setState({
        isActiveCreate: false,
        isActive: false,
        isActive1: false,
        isActive2: false,
        isActive3: false,
        isActive4: false,
        isActive5: true,
        isActive6: false,
        isActive7: false,
        isActive8: false,
        isActive9: true
      });
    };

    handleShows5 = () => {
      this.setState({
        isActiveCreate: false,
        isActive: false,
        isActive1: false,
        isActive2: false,
        isActive3: false,
        isActive4: false,
        isActive5: false,
        isActive6: true,
        isActive7: false,
        isActive8: false,
        isActive9: true
      });

      sessionStorage.removeItem("search");
    };

    handleShows6 = () => {
      this.setState({
        isActiveCreate: false,
        isActive: false,
        isActive1: false,
        isActive2: false,
        isActive3: false,
        isActive4: false,
        isActive5: false,
        isActive6: false,
        isActive7: true,
        isActive8: false,
        isActive9: true
      });
    }

    handleShows7 = () => {
      this.setState({
        isActiveCreate: false,
        isActive: true,
        isActive1: false,
        isActive2: false,
        isActive3: false,
        isActive4: false,
        isActive5: false,
        isActive6: false,
        isActive7: false,
        isActive8: true,
        isActive9: true
      });
    }

    
  
    handleLoginFormSubmit(e) {
      e.preventDefault();

     //// const err = this.handleValidation();
  
     let self = this, fields = this.state.fields;

     sessionStorage.setItem('text1', fields.text1);
     sessionStorage.setItem('text2', fields.text2);
     sessionStorage.setItem('text3', fields.text3);
     sessionStorage.setItem('text4', fields.text4);
     sessionStorage.setItem('text5', fields.text5);
     sessionStorage.setItem('text6', fields.text6);
     sessionStorage.setItem('search', "true");
  
     //console.log(fields.fullname);
     //console.log(fields.email);
     //console.log(fields.loginPassword);

     window.location.href="/visualeditortemplate"; 

     this.setState({
      isActive: false,
      isActive1: false,
      isActive2: false,
      isActive3: false,
      isActive4: false,
      isActive5: false,
      isActive6: false,
      isActive7: true,
      isActive8: false,
      isActive9: true,
      isActive14: true
    });

    this.forceUpdate();
    

    }

    //state = { isOpen: false };
  
    handleShowDialog = () => {
      this.setState({ isOpen: !this.state.isOpen });
      console.log("clicked");
      
    };
  /*
    constructor(props) {
      super(props);
  
      this.state = { isOpen: false };
    }
  
    toggleModal = () => {
      this.setState({
        isOpen: !this.state.isOpen
      });
    }
    */



   componentDidMount() {
    const ctx = this.ctx;
    new Chart(this.ctx, {
      type: "bar",
      data: {
        labels: ["Red", "Blue", "Yellow"],
        datasets: [
          {
            label: "# of Likes",
            data: [12, 19, 3],
            backgroundColor: [
              "rgb(255,0,0)",
              "rgb(0,0,255)",
              "rgb(128,0,0)"
            ]
          },
          {
            label: "# of Likes",
            data: [-12, -19, -3],
            backgroundColor: [
              "rgb(255,0,0)",
              "rgb(0,0,255)",
              "rgb(128,0,0)"
            ]
          }
        ]
      }
    });
  }

  
  ////};

  
    render() {

      const isMobile = window.innerWidth <= 500;

      const onEditorStateChange = (newEditorState) => {
        this.state.editorState = newEditorState;
      }
  
      const { editorState } = this.state;
  
      const handleLoginFormSubmits = (e) => {
  
        e.preventDefault();
      
        this.state.hideToolbar=!this.state.hideToolbar;
      
      
      };


    /* const ctxs = this.ctxs;
      const config = {
        type: 'line',
        data: {
          labels: [new Date('2019-08-20'), new Date('2019-08-25'), new Date('2019-08-30')],
          datasets: [{
            label: 'Line',
            data: [2, 5, 3],
            borderColor: '#D4213D',
            fill: false,
          }, ],
        },
        options: {
          scales: {
            xAxes: [{
              type: 'time',
            }, ],
          },
          pan: {
            enabled: true,
            mode: 'xy',
          },
          zoom: {
            enabled: true,
            mode: 'x', // or 'x' for "drag" version
          },
        },
      };
      
      ////window.onload = function() {
      new Chart(this.ctxs, config);

      const state = {
        labels: ['January', 'February', 'March',
                 'April', 'May'],
        datasets: [
          {
            label: 'Rainfall',
            backgroundColor: [
              '#B21F00',
              '#C9DE00',
              '#2FDE00',
              '#00A6B4',
              '#6800B4'
            ],
            hoverBackgroundColor: [
            '#501800',
            '#4B5000',
            '#175000',
            '#003350',
            '#35014F'
            ],
            data: [65, 59, 80, 81, 56]
          }
        ]
      }
      */


   const ctx = this.ctx;
   var charts = new Chart(this.ctx, {
      type: "doughnut",
      data: {
        labels: ["Red", "Blue", "Yellow"],
        datasets: [
          {
            label: "# of Likes",
            data: [12, 19, 3],
            backgroundColor: [
              "rgb(255,0,0)",
              "rgb(0,0,255)",
              "rgb(128,0,0)"
            ]
          }
        ]
      }
    });

    //const chart = charts;

     /* 
     
      alert(chart.toBase64Image(chart));

      alert(chart);

      const done = () => {

      let chart = this.ctx.current.charts;

      alert(chart.toBase64Image(chart));

      alert(chart);

      ////console.log(chart.toBase64Image(chart));
    }

    */


    ////const base64Image = this.refs.current.this.ctx.toBase64Image();

    ////alert(base64Image);

/*
    var chart = new Chart(ctx, {
      type: 'pie',
      data: {
         labels: ['Standing costs', 'Running costs'], // responsible for how many bars are gonna show on the chart
         // create 12 datasets, since we have 12 items
         // data[0] = labels[0] (data for first bar - 'Standing costs') | data[1] = labels[1] (data for second bar - 'Running costs')
         // put 0, if there is no data for the particular bar
         datasets: [{
            label: 'Washing and cleaning',
            data: [0, 8],
            backgroundColor: '#22aa99'
         }, {
            label: 'Traffic tickets',
            data: [0, 2],
            backgroundColor: '#994499'
         }, {
            label: 'Tolls',
            data: [0, 1],
            backgroundColor: '#316395'
         }, {
            label: 'Parking',
            data: [5, 2],
            backgroundColor: '#b82e2e'
         }, {
            label: 'Car tax',
            data: [0, 1],
            backgroundColor: '#66aa00'
         }, {
            label: 'Repairs and improvements',
            data: [0, 2],
            backgroundColor: '#dd4477'
         }, {
            label: 'Maintenance',
            data: [6, 1],
            backgroundColor: '#0099c6'
         }, {
            label: 'Inspection',
            data: [0, 2],
            backgroundColor: '#990099'
         }, {
            label: 'Loan interest',
            data: [0, 3],
            backgroundColor: '#109618'
         }, {
            label: 'Depreciation of the vehicle',
            data: [0, 2],
            backgroundColor: '#109618'
         }, {
            label: 'Fuel',
            data: [0, 1],
            backgroundColor: '#dc3912'
         }, {
            label: 'Insurance and Breakdown cover',
            data: [4, 0],
            backgroundColor: '#3366cc'
         }]
      },
      options: {
         responsive: false,
         legend: {
            position: 'right' // place legend on the right side of chart
         },
         scales: {
            xAxes: [{
               stacked: true // this should be set to make the bars stacked
            }],
            yAxes: [{
               stacked: true // this also..
            }]
         }  
      }
   });

   */
   
   ////var charts = chart!=null ? chart.toBase64Image():null;


   //alert( charts );

      const imagesPath = Constants.imagespath;

      const { showDiv } = this.state.showDiv;

      const template = () => {
        window.location.href = "/Template2";
        console.log("Template Page"); 
       }
  
       const template1 = () => {
        window.location.href = "/SampleTemplate2";
        console.log("Template Page"); 
       }

       const template2 = () => {
        window.location.href = "/SampleTemplate3";
        console.log("Template Page"); 
       }

       const template3 = () => {
        window.location.href = "/SampleTemplate4";
        console.log("Template Page"); 
       }

       const piecharts = () => {
        window.location.href = "/piechart";
        console.log("Template Page"); 
       }

       const barcharts = () => {
        window.location.href = "/barchart";
        console.log("Template Page"); 
       }

       const linecharts = () => {
        window.location.href = "/linechart";
        console.log("Template Page"); 
       }

    var title;   

    var banner_image;

    var banner_images55iour=[];

    const parsedTextimgour=[
    ];

    const iframe = '<iframe src="http://plnkr.co/" width="540" height="450"></iframe>'; 

    const apiUrl = 'http://162.213.248.35:3003/api/get-all-image';
 
    //alert("get all image .... : "); 
    
    axios(apiUrl, {
     method: 'GET',
     mode: 'no-cors',
     headers: {
       'Accept' : 'application/json',
       "Content-Type" : "application/json"
     },
    ////credentials: 'same-origin',
   }).then(response => {
     //return  response;
     //alert('products:'+JSON.stringify(response));
     
     const imagedata = JSON.stringify(response.data.result)
     var imagedatas = JSON.parse(imagedata);

      //alert("imagedatas : "+JSON.stringify(imagedatas));

      for (var i = 0; i < imagedatas.length; i++) {

        ////title = imagedatas[i].image;

        ////alert(title);

        //banner_image = "http://162.213.248.35:3004/images/uploads/" + imagedatas[i].image_slug;

        banner_image = imagedatas[i].image;
    
        banner_images55iour.push(banner_image);
        
      }

      localStorage.setItem('text5sarriour',  JSON.stringify(banner_images55iour));

      var retrievedDatas55our = localStorage.getItem("text5sarriour");
    
      var bannerimages12 = JSON.parse(retrievedDatas55our); 
      
      if(bannerimages12!=null){

      for (var i = 0; i < bannerimages12.length; i++) {
            
            const itemsimg = bannerimages12[i];

            //alert(itemsimg);
    
            parsedTextimgour.push({
                textimg: itemsimg
            });

            if(parsedTextimgour.length>1){
              localStorage.setItem('titlestxtimgour', JSON.stringify(parsedTextimgour));
  
              sessionStorage.setItem('titlestxtimgour', JSON.stringify(parsedTextimgour));
            }

      }  

    }

    })
     .catch((error) => {
         //return  error;
         //alert('ERROR1111111111:'+error);
    });

     var retrievedDatas59imgour = localStorage.getItem("titlestxtimgour");
    
      var bannertxt11imgour = JSON.parse(retrievedDatas59imgour);

      ////alert(bannertxt11imgour);

      return (

      <div class="site-inner-header">
      <div class="container" style={{minHeight:'50px'}}>
          <div class="row">
              <div class="col-md-6">
                  <div class="inner-nav">
                      <ul>
                          <li><a href="index.html" style={{color: '#fff'}}>Home</a></li>
                          <li>File</li>
                          <li><FontAwesomeIcon icon={faCrown} /> Resize</li>
                      </ul>
                  </div>
              </div>
              <div class="col-md-6">
                  <div class="left-inner-nav">
                      <ul>
                          <li><FontAwesomeIcon icon={faCrown} /> Upgrade</li>
                          <li>Share</li>&nbsp;&nbsp;&nbsp;&nbsp;
                          <li><FontAwesomeIcon icon={faArrowDown} /> Download</li>
                      </ul>
                  </div>
              </div>
          </div>
      </div>
    <div class="grey-bg mg55" style={{height:'900px'}}>    
    <div class="container-fluid">
        <div class="row">
             
            <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 bhoechie-tab-container">
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-3 bhoechie-tab-menu">
                    <div class="list-group">
                    {this.state.isActiveCreate ?(
                        <a  href="#" onClick={this.handleShowsCreate} class="list-group-item active text-center"> <FontAwesomeIcon icon={faColumns} />
                            <br />Create New </a>
                     ) : (    
                        <a  href="#" onClick={this.handleShowsCreate} class="list-group-item text-center"> <FontAwesomeIcon icon={faColumns} />
                            <br />Create New </a>      
                     )}     
                    {this.state.isActive ?(
                        <a  href="#" onClick={this.handleShow} class="list-group-item active text-center"> <FontAwesomeIcon icon={faColumns} />
                            <br />Template </a>
                     ) : (    
                      <a  href="#" onClick={this.handleShow} class="list-group-item text-center"> <FontAwesomeIcon icon={faColumns} />
                      <br />Template </a>
                     )}   
                     {this.state.isActive1 ?(
                        <a href="#" onClick={this.handleShows} class="list-group-item active text-center"> <FontAwesomeIcon icon={faVideo} />
                            <br />Photos </a>
                      ) : (    
                        <a href="#" onClick={this.handleShows} class="list-group-item text-center"> <FontAwesomeIcon icon={faVideo} />
                            <br />Photos </a>  
                      )} 
                      {this.state.isActive2 ?(       
                        <a href="#" onClick={this.handleShows1} class="list-group-item active text-center"> <FontAwesomeIcon icon={faEnvelope} />
                            <br />Elements </a>
                       ) : ( 
                        <a href="#" onClick={this.handleShows1} class="list-group-item text-center"> <FontAwesomeIcon icon={faEnvelope} />
                        <br />Elements </a>  
                      )}   
                      {this.state.isActive3 ?(  
                        <a href="#" onClick={this.handleShows2} class="list-group-item active text-center"> <FontAwesomeIcon icon={faTextHeight} />
                            <br /> Text </a>
                       ) : (
                        <a href="#" onClick={this.handleShows2} class="list-group-item text-center"> <FontAwesomeIcon icon={faTextHeight} />
                        <br /> Text </a>
                       )} 
                       {this.state.isActive4 ?(
                        <a href="#" onClick={this.handleShows3} class="list-group-item active text-center"> <FontAwesomeIcon icon={faUpload} />
                            <br /> Upload </a>
                        ) : (
                          <a href="#" onClick={this.handleShows3} class="list-group-item text-center"> <FontAwesomeIcon icon={faUpload} />
                          <br /> Upload </a>
                        )}  
                       {this.state.isActive5 ?(
                        <a href="#" onClick={this.handleShows4} class="list-group-item active text-center"> <FontAwesomeIcon icon={faFolder} />
                            <br /> Folder </a>
                        ) : (
                          <a href="#" onClick={this.handleShows4} class="list-group-item text-center"> <FontAwesomeIcon icon={faFolder} />
                          <br /> Folder </a>  
                       )}    
                       {this.state.isActive6 ?(
                        <a href="#" onClick={this.handleShows5} class="list-group-item active text-center"> <FontAwesomeIcon icon={faFolder} />
                            <br /> Chart </a>
                        ) : (
                          <a href="#" onClick={this.handleShows5} class="list-group-item text-center"> <FontAwesomeIcon icon={faFolder} />
                          <br /> Chart </a>  
                       )}    
                    </div>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-9 col-xs-9 bhoechie-tab">
                {this.state.isActive2 &&
                    <div class="bhoechie-tab-content active">
                        <div class="bhoechie-tab-content active">
                        <div class="inner-search">
                            <input type="search" placeholder="Search Your Content"></input> </div>
                        <div class="inner-template">

                          

                      <div class="row">

            <div class="col-md-3 col-sm-3 col-xs-3"> 

            <Draggable
            handle=".handle"
            defaultPosition={{x: 0, y: 0}}
            position={null}
            grid={[125, 125]}
            scale={1}
            onStart={this.handleStart}
            onDrag={this.handleDrag}
            onStop={this.handleStop}>
            <Resizable
              ref={this.refResizable}
              defaultSize={20}
            >

            <img style={{width:'5px', height:'5px'}} alt="" src={image1} class="img-responsive" style={{backgroundColor:'coral'}}></img>  
            </Resizable>
            </Draggable>   

            </div>

            <div class="col-md-3 col-sm-3 col-xs-3">

            <Draggable
            handle=".handle"
            defaultPosition={{x: 0, y: 0}}
            position={null}
            grid={[125, 125]}
            scale={1}
            onStart={this.handleStart}
            onDrag={this.handleDrag}
            onStop={this.handleStop}>
            <Resizable
              ref={this.refResizable}
              defaultSize={20}
            >

            <img style={{width:'5px', height:'5px'}} alt="" src={image2} class="img-responsive" style={{backgroundColor:'coral'}}></img>  
            </Resizable>
            </Draggable>   

            </div>

            <div class="col-md-3 col-sm-3 col-xs-3">

            <Draggable
            handle=".handle"
            defaultPosition={{x: 0, y: 0}}
            position={null}
            grid={[125, 125]}
            scale={1}
            onStart={this.handleStart}
            onDrag={this.handleDrag}
            onStop={this.handleStop}>
            <Resizable
              ref={this.refResizable}
              defaultSize={20}
            >

            <img style={{width:'5px', height:'5px'}} alt="" src={image3} class="img-responsive" style={{backgroundColor:'coral'}}></img>  
            </Resizable>
            </Draggable>   

            </div>

            <div class="col-md-3 col-sm-3 col-xs-3">

            <Draggable
            handle=".handle"
            defaultPosition={{x: 0, y: 0}}
            position={null}
            grid={[125, 125]}
            scale={1}
            onStart={this.handleStart}
            onDrag={this.handleDrag}
            onStop={this.handleStop}>
            <Resizable
              ref={this.refResizable}
              defaultSize={20}
            >

            <img style={{width:'15px', height:'15px'}} alt="" src={pies} class="img-responsive" style={{backgroundColor:'coral'}}></img>  
            </Resizable>
            </Draggable>   

            </div>
              
            <div>
            </div>   

            <br />
            <br />
            <br />


            <div class="col-md-12 col-sm-12 col-xs-12">      
            {bannertxt11imgour!=null && bannertxt11imgour.map((person, index) => 
            <div class="col-md-3 col-sm-3 col-xs-3">
            <img style={{width:'5px', height:'5px'}} alt="" src={person.textimg} class="img-responsive" style={{backgroundColor:'coral'}} />
                  </div>
            )}
            </div>
            </div>


                            
                    </div>
                            
                    </div>
                    </div>
                    
                }  
                {this.state.isActive6 &&
                    <div class="bhoechie-tab-content active">
                        <div class="inner-search">
                            <input type="search" placeholder="Search Your Content"></input> </div>
                        <div class="inner-template">
                            <h4>Charts</h4>
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-6"> <img onClick={this.handleShows6} src={pies} class="img-responsive" alt="" style={{backgroundColor:'coral'}}></img> </div>
                                <div class="col-md-6 col-sm-6 col-xs-6"> <img onClick={barcharts} src={bars} class="img-responsive" alt="" style={{backgroundColor:'coral'}}></img> </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-6"> <img onClick={linecharts} alt="" src={lines} class="img-responsive" style={{backgroundColor:'coral'}}></img>  </div>
                                
                            </div>
                            
                        </div>
                    </div>
                }                
                {this.state.isActive &&
                    <div class="bhoechie-tab-content active">
                        <div class="inner-search">
                            <input type="search" placeholder="Search Your Content"></input> </div>
                        <div class="inner-template">
                            <h4>Sample Templates</h4>
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-6"> <img onClick={this.handleShows7} src={Template1} class="img-responsive" alt="" /> </div>
                                <div class="col-md-6 col-sm-6 col-xs-6"> <img onClick={template2} src={Template2} class="img-responsive" alt="" /> </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-6"> <img onClick={template3} src={Template3} class="img-responsive" alt="" /> </div>
                                <div class="col-md-6 col-sm-6 col-xs-6"> <img src={Userpics} class="img-responsive" alt="" /> </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-6"> <img src={Userpics} class="img-responsive" alt="" /> </div>
                                <div class="col-md-6 col-sm-6 col-xs-6"> <img src={Userpics} class="img-responsive" alt="" /> </div>
                            </div>
                        </div>
                    </div>
                }    
                {this.state.isActive7 &&
                    <div class="bhoechie-tab-content active">
                      <form name="chartForm" className="loginForm" onSubmit={(e) => this.handleLoginFormSubmit(e)}>
                        <div class="inner-search">
                            <input type="text" name= "text1" placeholder="Text1" onChange={(e) => this.handleLoginFormData(e)} value={this.state.text1}></input> </div>
                        <br /><br />    
                        <div class="inner-search">
                            <input type="text" name= "text2" placeholder="Text2" onChange={(e) => this.handleLoginFormData(e)} value={this.state.text2}></input> </div> 
                        <br /><br />    
                        <div class="inner-search">
                            <input type="text" name= "text3" placeholder="Text3" onChange={(e) => this.handleLoginFormData(e)} value={this.state.text3}></input> </div>
                        <br /><br />    
                        <div class="inner-search">
                            <input type="text" name= "text4" placeholder="Text4" onChange={(e) => this.handleLoginFormData(e)} value={this.state.text4}></input> </div> 
                        <br /><br />    
                        <div class="inner-search">
                        <input type="text" name= "text5" placeholder="Text5" onChange={(e) => this.handleLoginFormData(e)} value={this.state.text5}></input> </div>   
                        <br /><br />  
                        <div class="inner-search">
                        <input type="text" name= "text6" placeholder="Text6" onChange={(e) => this.handleLoginFormData(e)} value={this.state.text6}></input> </div>   
                        
                        <Button type="submit" bsStyle="primary">SUBMIT</Button>
                    </form>                                       
                    </div>
                    }
                    {this.state.isActive1 &&
                    <div class="bhoechie-tab-content active">
                        <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32. The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32. </p>
                    </div>
                    }   
                   
                    {this.state.isActive3 &&
                    <div class="bhoechie-tab-content active">
                        <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32. The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32. </p>
                    </div>
                    }  
                    {this.state.isActive4 &&   
                    <div class="bhoechie-tab-content active">
                        <p> <div class="inner-sidepage"> <iframe src={"http://162.213.248.35:3005/editorupload"} width="300" height="300" frameborder="0" scrollbars='no' allowfullscreen></iframe></div>
                       </p>
                    </div>
                    }
                    {this.state.isActive5 &&  
                    <div class="bhoechie-tab-content active">
                        <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32. The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32. </p>
                    </div>
                    }
                </div>
            </div>
            { isMobile ?  
          (<div class="col-lg-2 col-md-2 col-sm-3 col-xs-3">
          {
          this.state.isActiveCreate === true ?
          (<div style={{width:'400px', height:'400px', marginRight:'auto'}}>          
        
          <form name="contactForm" className="comment-form contact-form" onSubmit={(e) => handleLoginFormSubmits(e)}>    
          <Button type="submit" style={{marginTop:'30px', width:'110px'}} class="thm-btn" bsStyle="primary"><span>Toolbar</span></Button>
          </form>
      
          <A4 style={{width:'400px', height:'400px'}}>
          <article className={cs({
              [style.cv]: true,
              exportRoot: true
          })}>
              <HeaderPic style={{
                padding: '0px 0px 0px 0px'
          }}>
          <header className={style.header}>
                      
          <div class="clearfix"></div>
              
          <div id="editor" style={{height:'5000px', width:'400px'}}>
                          
          <ReactQuill
            modules={modulesQuill}
            formats={formatsQuill}
            onChange={this.handleChange.bind(this)}
            value={this.state.editorHtml}
            onChange={this.handleChange}
          />

          </div>

          </header>
          </HeaderPic>
          </article>
          </A4>
      
          </div>)
            :
            this.state.isActive7 === true ?
            (<div class="inner-sidepage"> <iframe src={"http://localhost:3000/piechart"} width="800" height="600" marginLeft="-500px" marginTop="-500px" frameborder="0" allowfullscreen></iframe></div>)
            :
            this.state.isActive8 === true ?
            (<div class="inner-sidepage"> <iframe src={"http://localhost:3000/SampleTemplate2"} width="1200" height="600" frameborder="0" allowfullscreen></iframe></div>)
            :          
            (<div class="inner-sidepage"> <img src={Userpics1} class="img-responsive" /> </div>)}
            
                
            </div>) : (<div class="col-lg-5 col-md-5 col-sm-4 col-xs-3">
          {
          this.state.isActiveCreate === true ?
          (<div style={{width:'1200px', height:'1000px', marginRight:'400px'}}>          
        
          <form name="contactForm" className="comment-form contact-form" onSubmit={(e) => handleLoginFormSubmits(e)}>    
          <Button type="submit" style={{marginTop:'30px', width:'110px'}} class="thm-btn" bsStyle="primary"><span>Toolbar</span></Button>
          </form>
      
          <A4 style={{width:'1000px', height:'1000px'}}>
          <article className={cs({
              [style.cv]: true,
              exportRoot: true
          })}>
              <HeaderPic style={{
                padding: '0px 0px 0px 0px'
          }}>
          <header className={style.header}>
                      
          <div class="clearfix"></div>
              
          <div id="editor" style={{height:'5000px', width:'1100px'}}>
                          
          <ReactQuill
            modules={modulesQuill}
            formats={formatsQuill}
            onChange={this.handleChange.bind(this)}
            value={this.state.editorHtml}
            onChange={this.handleChange}
          />

          </div>

          </header>
          </HeaderPic>
          </article>
          </A4>
      
          </div>)
            :
            this.state.isActive7 === true ?
            (<div class="inner-sidepage"> <iframe src={"http://localhost:3000/piechart"} width="800" height="600" marginLeft="-500px" marginTop="-500px" frameborder="0" allowfullscreen></iframe></div>)
            :
            this.state.isActive8 === true ?
            (<div class="inner-sidepage"> <iframe src={"http://localhost:3000/SampleTemplate2"} width="1200" height="600" frameborder="0" allowfullscreen></iframe></div>)
            :          
            (<div class="inner-sidepage"> <img src={Userpics1} class="img-responsive" /> </div>)}
            
                
            </div>)}
            
        </div>
        </div>
        </div>
    </div>
    
    );
  }  onDrop(data) {

    //this.upload(data);

    //alert("data : "+data);

    console.log(data)
    // => banana 
}
}

export default Templates;