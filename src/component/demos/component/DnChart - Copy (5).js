import React, { useRef } from "react";

import {Pie, Doughnut} from 'react-chartjs-2';


var Image =  function App() {

  var retrievedDatas1 = sessionStorage.getItem("text1");


  //const text6 = JSON.parse(retrievedDatas6);
  
  if(retrievedDatas1!=null){
  
    const retrievedDatas1 = sessionStorage.getItem("text1");
  //const text1 = JSON.parse(retrievedDatas1);
  
  ////alert("data : "+retrievedDatas1);
  
  const retrievedsess = sessionStorage.getItem("search");
  
  ////alert("retrievedsess .."+retrievedsess);
  
  //var retrievedDatas1 = sessionStorage.getItem("text1");
  
  var retrievedDatas2 = sessionStorage.getItem("text2");
  //const text2 = JSON.parse(retrievedDatas2);
  
  var retrievedDatas3 = sessionStorage.getItem("text3");
  //const text3 = JSON.parse(retrievedDatas3);
  
  var retrievedDatas4 = sessionStorage.getItem("text4");
  //const text4 = JSON.parse(retrievedDatas4);
  
  var retrievedDatas5 = sessionStorage.getItem("text5");
  //const text5 = JSON.parse(retrievedDatas5);
  
  var retrievedDatas6 = sessionStorage.getItem("text6");
  //const text6 = JSON.parse(retrievedDatas6);
  
  }else{
  
   const retrievedDatas1 = "100";
  
    var retrievedDatas2 = "200";
  //const text2 = JSON.parse(retrievedDatas2);
  
  var retrievedDatas3 = "100";
  //const text3 = JSON.parse(retrievedDatas3);
  
  var retrievedDatas4 = "400";
  //const text4 = JSON.parse(retrievedDatas4);
  
  var retrievedDatas5 = "800";
  //const text5 = JSON.parse(retrievedDatas5);
  
  var retrievedDatas6 = "100";
  //const text6 = JSON.parse(retrievedDatas6);
  
  }
  
  
  
  var sum = retrievedDatas1 + retrievedDatas2 + retrievedDatas3 + retrievedDatas4 + retrievedDatas5 + retrievedDatas6;
  
  var retrievedDatas11 = (retrievedDatas1/sum) * 100;
  var retrievedDatas21 = (retrievedDatas2/sum) * 100;
  var retrievedDatas31 = (retrievedDatas3/sum) * 100;
  var retrievedDatas41 = (retrievedDatas4/sum) * 100;
  var retrievedDatas51 = (retrievedDatas5/sum) * 100;
  var retrievedDatas61 = (retrievedDatas6/sum) * 100;
  
  
  const state = {
  
    labels: ['Summary', 'Skills', 'Profile', 'Work Experience', 'Education', 'Text Section'],
    datasets: [
      {
        label: 'Rainfall',
        backgroundColor: [
          '#B21F00',
          '#C9DE00',
          '#2FDE00',
          '#00A6B4',
          '#6800B4',
          '#008080'
        ],
        hoverBackgroundColor: [
        '#501800',
        '#4B5000',
        '#175000',
        '#003350',
        '#35014F',
        '#FF00FF'
        ],
        data: [retrievedDatas1, retrievedDatas2, retrievedDatas3, retrievedDatas4, retrievedDatas5, retrievedDatas6]
      }
    ]
  }  

const createImage = (options) => {
  options = options || {};
  const img = document.createElement("img");
  if (options.src) {
    img.src = options.src;
  }
  return img;
};

const copyToClipboard = async (pngBlob) => {
  try {
    await navigator.clipboard.write([
      // eslint-disable-next-line no-undef
      new ClipboardItem({
        [pngBlob.type]: pngBlob
      })
    ]);
    console.log("Image copied");
  } catch (error) {
    console.error(error);
  }
};

const convertToPng = (imgBlob) => {
  const canvas = document.createElement("canvas");
  const ctx = canvas.getContext("2d");
  const imageEl = createImage({ src: window.URL.createObjectURL(imgBlob) });
  imageEl.onload = (e) => {
    canvas.width = e.target.width;
    canvas.height = e.target.height;
    ctx.drawImage(e.target, 0, 0, e.target.width, e.target.height);
    canvas.toBlob(copyToClipboard, "image/png", 1);
  };
};

const copyImg = async (src) => {
  const img = await fetch(src);
  const imgBlob = await img.blob();
  const extension = src.split(".").pop();
  const supportedToBeConverted = ["jpeg", "jpg", "gif"];
  if (supportedToBeConverted.indexOf(extension.toLowerCase())) {
    return convertToPng(imgBlob);
  } else if (extension.toLowerCase() === "png") {
    return copyToClipboard(imgBlob);
  }
  console.error("Format unsupported");
  return;
};


  const ref = useRef(null);
  return (
    <div>
      <img id="image" ref={ref} width="100" src="https://i.imgur.com/Oq3ie1b.jpg" alt="" />

      <Doughnut
          data={state}
          options={{
            title:{
              display:true,
              text:'% of different captions in Resume',
              fontSize:20
            },
            legend:{
              display:true,
              position:'right'
            }
          }}
          ref={ref}
        />
      <button onClick={() => copyImg(ref.current.src)}>copy img</button>
    </div>
  );
};

export default Image;