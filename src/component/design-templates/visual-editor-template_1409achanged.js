import React, { Component } from "react";
import { render } from "react-dom";
import { Resize, ResizeVertical, ResizeHorizon } from "react-resize-layout";

//import { Resizable, ResizableBox } from 'react-resizable';

import { Rnd } from "react-rnd";

import Chart from "chart.js";

import Draggable from 'react-draggable';

import { DraggableCore, DraggableEventHandler, default as DraggableRoot } from "react-draggable";
import { Enable, Resizable, ResizeDirection } from "re-resizable";


class Templates extends Component {

// horizon and vertical


render() {

  this.ctx = React.createRef();

      

    const ctx = this.ctx;
       new Chart(this.ctx, {
          type: "bar",
          data: {
            labels: ["Red", "Blue", "Yellow"],
            datasets: [
              {
                label: "# of Likes",
                data: [12, 19, 3],
                backgroundColor: [
                  "rgb(255,0,0)",
                  "rgb(0,0,255)",
                  "rgb(128,0,0)"
                ]
              },
              {
                label: "# of Likes",
                data: [-12, -19, -3],
                backgroundColor: [
                  "rgb(255,0,0)",
                  "rgb(0,0,255)",
                  "rgb(128,0,0)"
                ]
              }
            ]
          }
        });

  return(
    <div>
    <Draggable
    handle=".handle"
    defaultPosition={{x: 0, y: 0}}
    position={null}
    grid={[125, 125]}
    scale={1}
    onStart={this.handleStart}
    onDrag={this.handleDrag}
    onStop={this.handleStop}>
    <Resizable
      ref={this.refResizable}
      defaultSize={20}
    >
      
    <div>
      <div className="handle"> <canvas width='800' height='300' ref={ctx => (this.ctx = ctx)}/></div>
      <div>drag on</div>
    </div>
    </Resizable>
  </Draggable>
  
  <input type="text" style={{width:'300px', height:'80px'}}></input>


    <Rnd
    style={{display: "flex", alignItems: "center", justifyContent: "center", border: "solid 1px #ddd", background: "#f0f0f0"}}
    default={{
      x: 700,
      y: 300,
      width: 1200,
      height: 600
    }}
  >  
            <iframe src={"http://localhost:3000/SampleTemplate3"} width="1200" height="600" frameborder="0" allowfullscreen></iframe>
            </Rnd>

    </div>
);
};
}

export default Templates;