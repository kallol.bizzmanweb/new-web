import { Component, Fragment} from 'react';

import React, {useCallback} from 'react';
import {useDropzone} from 'react-dropzone';

import { render } from "react-dom";

import Dropzone from 'react-dropzone'

import Constants from '../../common/Constants';

import { MDBContainer, MDBTabPane, MDBTabContent, MDBNav, MDBNavItem, MDBNavLink } from "mdbreact";

import { MDBBtn, MDBModal, MDBModalBody, MDBModalHeader, MDBModalFooter } from 'mdbreact';

import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import './style/react-tabs.css';

import Image from 'react-image-resizer';

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCubes, faFileAlt } from "@fortawesome/free-solid-svg-icons";
import { faAppleAlt } from "@fortawesome/free-solid-svg-icons";
import { faCog } from "@fortawesome/free-solid-svg-icons";
import { faFile } from "@fortawesome/free-solid-svg-icons";
import { faClock } from "@fortawesome/free-solid-svg-icons";
import { faArrowDown } from "@fortawesome/free-solid-svg-icons";
import { faCrown } from "@fortawesome/free-solid-svg-icons";

import { faUpload } from "@fortawesome/free-solid-svg-icons";
import { faTextHeight } from "@fortawesome/free-solid-svg-icons";
import { faFolder } from "@fortawesome/free-solid-svg-icons";
import { faColumns } from "@fortawesome/free-solid-svg-icons";
import { faVideo } from "@fortawesome/free-solid-svg-icons";
import { faEnvelope } from "@fortawesome/free-solid-svg-icons";

import Userpics from './images/300.png';

import Userpics1 from './images/500.jpg';

import Template1 from './images/image1.jpeg';
import Template2 from './images/image3.jpeg';
import Template3 from './images/image2.jpeg';

import pies from './images/piechart.png';
import bars from './images/barchart.jpg';
import lines from './images/linechart.png';

import image1 from './images/Icons/1.png';
import image2 from './images/Icons/2.png';
import image3 from './images/Icons/3.png';

import { Button } from 'react-bootstrap';

import Chart from "chart.js";

//import Draggable from 'react-draggable';

import { Draggable, Droppable } from 'react-drag-and-drop';

import { DraggableEventHandler, default as DraggableRoot } from "react-draggable";
import { Enable, Resizable, ResizeDirection } from "re-resizable";

//import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import { Rnd } from "react-rnd";

import axios from 'axios';

import request from "superagent";

import { LayersManager, Layer } from 'react-layers-manager';
import { Modal } from 'react-bootstrap';
import { FinancialSeriesView } from 'igniteui-react-charts';  

class Templates extends Component {
  
    state = {
      activeItem: "1"
    }

  constructor() {
      super();
      ////this.state = {fields: {},emailerror:'', loginError: ''};

      const retrievedsess = sessionStorage.getItem("search");


      this.myRef = React.createRef();

      this.ctx = React.createRef();

      const retrievedDatas1 = sessionStorage.getItem("text8");
      const retrievedDatas2 = sessionStorage.getItem("text9");
      const retrievedDatas3 = sessionStorage.getItem("text10");

      const retrievedDataspage = sessionStorage.getItem("page");

      
      //alert("true..8"+retrievedDatas1);
      //alert("true..9"+retrievedDatas2);
      //alert("true..10"+retrievedDatas3);

      //alert("true..10a"+retrievedDataspage);

      if(retrievedDataspage=='template'){

        //alert("true..10a"+retrievedDataspage);
  
        if(retrievedDatas1=='true'){
  
          //alert("trues..81"+retrievedDatas1);
    
          this.state = {
            files: [],
            showPopup: false,
            showDiv: true,
            show:true,
            isActive: false,
            isActive1: false,
            isActive2: false,
            isActive3: false,
            isActive4: false,
            isActive5: false,
            isActive6: true,
            isActive8: true,
            isActive7: false,
            fields: {},emailerror:'', loginError: ''
          };
    
        }else if(retrievedDatas2=='true'){
    
          //alert("trues..91"+retrievedDatas1);
    
          this.state = {
            files: [],
            showPopup: false,
            showDiv: true,
            show:true,
            isActive: false,
            isActive1: false,
            isActive2: false,
            isActive3: false,
            isActive4: false,
            isActive5: false,
            isActive6: true,
            isActive10: true,
            isActive7: false,
            fields: {},emailerror:'', loginError: ''
          };
    
        }else if(retrievedDatas3=='true'){
    
          //alert("trues..101"+retrievedDatas1);
    
          this.state = {
            files: [],
            showPopup: false,
            showDiv: true,
            show:true,
            isActive: false,
            isActive1: false,
            isActive2: false,
            isActive3: false,
            isActive4: false,
            isActive5: false,
            isActive6: true,
            isActive11: true,
            isActive7: false,
            fields: {},emailerror:'', loginError: ''
          };
  
      }
  
      sessionStorage.removeItem("page");
  
    }else{
      

    if(retrievedsess=='true'){

    const retrievedDatas1 = sessionStorage.getItem("text8");
    const retrievedDatas2 = sessionStorage.getItem("text9");
    const retrievedDatas3 = sessionStorage.getItem("text10");

    const retrievedDataspage = sessionStorage.getItem("page");

    /*
    alert("true..8"+retrievedDatas1);
    alert("true..9"+retrievedDatas2);
    alert("true..10"+retrievedDatas3);

    alert("true..10a"+retrievedDataspage);
    */

    sessionStorage.removeItem("text8");
    sessionStorage.removeItem("text9");
    sessionStorage.removeItem("text10");

    if(retrievedDatas1=='true'){

      ////alert("trues..81"+retrievedDatas1);

      this.state = {
        files: [],
        showPopup: false,
        showDiv: true,
        show:true,
        isActive: false,
        isActive1: false,
        isActive2: false,
        isActive3: false,
        isActive4: false,
        isActive5: false,
        isActive6: false,
        isActive8: true,
        isActive7: true,
        fields: {},emailerror:'', loginError: ''
      };

    }else if(retrievedDatas2=='true'){

      ////alert("trues..91"+retrievedDatas1);

      this.state = {
        files: [],
        showPopup: false,
        showDiv: true,
        show:true,
        isActive: false,
        isActive1: false,
        isActive2: false,
        isActive3: false,
        isActive4: false,
        isActive5: false,
        isActive6: false,
        isActive10: true,
        isActive7: true,
        fields: {},emailerror:'', loginError: ''
      };

    }else if(retrievedDatas3=='true'){

      ////alert("trues..101"+retrievedDatas1);

      this.state = {
        files: [],
        showPopup: false,
        showDiv: true,
        show:true,
        isActive: false,
        isActive1: false,
        isActive2: false,
        isActive3: false,
        isActive4: false,
        isActive5: false,
        isActive6: false,
        isActive11: true,
        isActive7: true,
        fields: {},emailerror:'', loginError: ''
      };

    

    }else{

      this.state = {
        files: [],
        showPopup: false,
        showDiv: true,
        show:true,
        isActive: false,
        isActive1: false,
        isActive2: false,
        isActive3: false,
        isActive4: false,
        isActive5: false,
        isActive6: false,
        isActive8: false,
        isActive7: true,
        fields: {},emailerror:'', loginError: ''
      };
    }
  

    }else{

      this.state = {
        showPopup: false,
        showDiv: true,
        show:true,
        isActive: true,
        isActive1: false,
        isActive2: false,
        isActive3: false,
        isActive4: false,
        isActive5: false,
        isActive6: false,
        isActive7: false,
        isActive8: false,
        isActive9: true,
        fields: {},emailerror:'', loginError: ''
      };
      
    }

  }

    
    }

    onPreviewDrop = (files) => {
      this.setState({
        files: this.state.files.concat(files),
       });
    }

    togglePopup() {
      this.setState({
        showPopup: !this.state.showPopup
      });
    }
    
    handleLoginFormData(e) {
      e.preventDefault();
      let fieldName = e.target.name, fieldValue = e.target.value, fields = this.state.fields;
  
      fields[fieldName] = fieldValue;

      //alert(fields);

      this.setState({fields: fields});
   
    }
    
    toggle = tab => () => {
      if (this.state.activeItem !== tab) {
      this.setState({
        activeItem: tab
      });
      }
    }

    handleShow = () => {
      this.setState({
        isActive: true,
        isActive1: false,
        isActive2: false,
        isActive3: false,
        isActive4: false,
        isActive5: false,
        isActive6: false,
        isActive7: false,
        isActive8: false,
        isActive9: true,
        isActive10: false,
        isActive11: false,
      });
    };

    handleShows = () => {
      this.setState({
        isActive: false,
        isActive1: true,
        isActive2: false,
        isActive3: false,
        isActive5: false,
        isActive6: false,
        isActive7: false,
        isActive8: false,
        isActive9: true,
        isActive10: false,
        isActive11: false,
      });
    };

    handleShows1 = () => {
      this.setState({
        isActive: false,
        isActive1: false,
        isActive2: true,
        isActive3: false,
        isActive4: false,
        isActive5: false,
        isActive6: false,
        isActive7: false,
        isActive9: true,
        isActive10: false,
        isActive11: false,
      });
    };

    handleShows2 = () => {
      this.setState({
        isActive: false,
        isActive1: false,
        isActive2: false,
        isActive3: true,
        isActive4: false,
        isActive5: false,
        isActive6: false,
        isActive7: false,
        isActive8: false,
        isActive9: true,
        isActive10: false,
        isActive11: false,
      });
    };

    handleShows3 = () => {
      this.setState({
        isActive: false,
        isActive1: false,
        isActive2: false,
        isActive3: false,
        isActive4: true,
        isActive5: false,
        isActive6: false,
        isActive7: false,
        isActive8: false,
        isActive9: true,
        isActive10: false,
        isActive11: false,
      });
    };
    handleShows4 = () => {
      this.setState({
        isActive: false,
        isActive1: false,
        isActive2: false,
        isActive3: false,
        isActive4: false,
        isActive5: true,
        isActive6: false,
        isActive7: false,
        isActive8: false,
        isActive9: true,
        isActive10: false,
        isActive11: false,
      });
    };

    handleShows5 = () => {
      this.setState({
        isActive: false,
        isActive1: false,
        isActive2: false,
        isActive3: false,
        isActive4: false,
        isActive5: false,
        isActive6: true,
        isActive7: false,
        isActive9: true,
      });

      sessionStorage.removeItem("search");
    };

    handleShows6 = () => {
      this.setState({
        isActive: false,
        isActive1: false,
        isActive2: false,
        isActive3: false,
        isActive4: false,
        isActive5: false,
        isActive6: false,
        isActive7: true,
        isActive9: true,
      });
    }

    handleShows7 = () => {

      sessionStorage.setItem('text8', "true");

      this.setState({
        isActive: true,
        isActive1: false,
        isActive2: false,
        isActive3: false,
        isActive4: false,
        isActive5: false,
        isActive6: false,
        isActive7: false,
        isActive8: true,
        isActive9: true,
        isActive10: false,
      });
    }

    handleShows71 = () => {

      sessionStorage.setItem('text9', "true");

      this.setState({
        isActive: true,
        isActive1: false,
        isActive2: false,
        isActive3: false,
        isActive4: false,
        isActive5: false,
        isActive6: false,
        isActive7: false,
        isActive8: false,
        isActive9: true,
        isActive10: true,
      });
    }

    handleShows72 = () => {

      sessionStorage.setItem('text10', "true");

      this.setState({
        isActive: true,
        isActive1: false,
        isActive2: false,
        isActive3: false,
        isActive4: false,
        isActive5: false,
        isActive6: false,
        isActive7: false,
        isActive8: false,
        isActive9: false,
        isActive10: false,
        isActive11: true,
      });
    }
  
    handleLoginFormSubmit(e) {
      e.preventDefault();

     //// const err = this.handleValidation();
  
     let self = this, fields = this.state.fields;

     sessionStorage.setItem('text1', fields.text1);
     sessionStorage.setItem('text2', fields.text2);
     sessionStorage.setItem('text3', fields.text3);
     sessionStorage.setItem('text4', fields.text4);
     sessionStorage.setItem('text5', fields.text5);
     sessionStorage.setItem('text6', fields.text6);
     sessionStorage.setItem('search', "true");
  
     //console.log(fields.fullname);
     //console.log(fields.email);
     //console.log(fields.loginPassword);

     window.location.href="/visualeditortemplate"; 

     this.setState({
      isActive: false,
      isActive1: false,
      isActive2: false,
      isActive3: false,
      isActive4: false,
      isActive5: false,
      isActive6: false,
      isActive7: true,
      isActive8: true,
      isActive9: true,
      isActive14: true
    });

    const retrievedDatas1 = sessionStorage.getItem("text8");

    alert(retrievedDatas1);

    if(retrievedDatas1 === true){

      alert(retrievedDatas1);

      this.setState({
         isActive: true,
         isActive1: false,
         isActive2: false,
         isActive3: false,
         isActive4: false,
         isActive5: false,
         isActive6: false,
         isActive7: false,
         isActive8: true,
         isActive9: false,
         isActive10: false,
         isActive11: false,
       });

      }

    this.forceUpdate();
    

    }

    //state = { isOpen: false };
  
    handleShowDialog = () => {
      this.setState({ isOpen: !this.state.isOpen });
      console.log("clicked");
      
    };
  /*
    constructor(props) {
      super(props);
  
      this.state = { isOpen: false };
    }
  
    toggleModal = () => {
      this.setState({
        isOpen: !this.state.isOpen
      });
    }
    */

   componentDidMount() {
    const ctx = this.ctx;
    new Chart(this.ctx, {
      type: "bar",
      data: {
        labels: ["Red", "Blue", "Yellow"],
        datasets: [
          {
            label: "# of Likes",
            data: [12, 19, 3],
            backgroundColor: [
              "rgb(255,0,0)",
              "rgb(0,0,255)",
              "rgb(128,0,0)"
            ]
          },
          {
            label: "# of Likes",
            data: [-12, -19, -3],
            backgroundColor: [
              "rgb(255,0,0)",
              "rgb(0,0,255)",
              "rgb(128,0,0)"
            ]
          }
        ]
      }
    });
  }

  
    render() {

      const previewStyle = {
        display: 'inline',
        width: 100,
        height: 100,
      };

      const handleChangeStatus = ({ meta }, status) => {
        console.log(status, meta)
      }
    
      const handleSubmit = (files, allFiles) => {
        console.log(files.map(f => f.meta))
        allFiles.forEach(f => f.remove())
      }
          

      const imagesPath = Constants.imagespath;

      const { showDiv } = this.state.showDiv;

      const template = () => {
        window.location.href = "/Template2";
        console.log("Template Page"); 
       }
  
       const template1 = () => {
        window.location.href = "/SampleTemplate2";
        console.log("Template Page"); 
       }

       const template2 = () => {
        window.location.href = "/SampleTemplate3";
        console.log("Template Page"); 
       }

       const template3 = () => {
        window.location.href = "/SampleTemplate4";
        console.log("Template Page"); 
       }

       const piecharts = () => {
        window.location.href = "/piechart";
        console.log("Template Page"); 
       }

       const barcharts = () => {
        window.location.href = "/barchart";
        console.log("Template Page"); 
       }

       const linecharts = () => {
        window.location.href = "/linechart";
        console.log("Template Page"); 
       }

       const retrievedDatas1 = sessionStorage.getItem("text8");

    alert(retrievedDatas1);

    if(retrievedDatas1 === true){

      alert(retrievedDatas1);

      this.setState({
         isActive: true,
         isActive1: false,
         isActive2: false,
         isActive3: false,
         isActive4: false,
         isActive5: false,
         isActive6: false,
         isActive7: false,
         isActive8: true,
         isActive9: false,
         isActive10: false,
         isActive11: false,
       });

      }


    var title;   

    var banner_image;

    var banner_images55iour=[];

    const parsedTextimgour=[
    ];

    const iframe = '<iframe src="http://plnkr.co/" width="540" height="450"></iframe>'; 

    const apiUrl = 'http://162.213.248.35:3003/api/get-all-image';
 
    //alert("get all image .... : "); 
    
    axios(apiUrl, {
     method: 'GET',
     mode: 'no-cors',
     headers: {
       'Accept' : 'application/json',
       "Content-Type" : "application/json"
     },
    ////credentials: 'same-origin',
   }).then(response => {
     //return  response;
     //alert('products:'+JSON.stringify(response));
     
     const imagedata = JSON.stringify(response.data.result)
     var imagedatas = JSON.parse(imagedata);

      //alert("imagedatas : "+JSON.stringify(imagedatas));

      for (var i = 21; i < imagedatas.length; i++) {

        ////title = imagedatas[i].image;

        ////alert(title);

        //banner_image = "http://162.213.248.35:3004/images/uploads/" + imagedatas[i].image_slug;

        banner_image = imagedatas[i].image;
    
        banner_images55iour.push(banner_image);
        
      }

      localStorage.setItem('text5sarriour',  JSON.stringify(banner_images55iour));

      var retrievedDatas55our = localStorage.getItem("text5sarriour");
    
      var bannerimages12 = JSON.parse(retrievedDatas55our); 
      
      if(bannerimages12!=null){

      for (var i = 0; i < bannerimages12.length; i++) {
            
            const itemsimg = bannerimages12[i];

            alert(itemsimg);
    
            parsedTextimgour.push({
                textimg: itemsimg
            });

            if(parsedTextimgour.length>1){
              localStorage.setItem('titlestxtimgour', JSON.stringify(parsedTextimgour));
  
              sessionStorage.setItem('titlestxtimgour', JSON.stringify(parsedTextimgour));
            }

      }  

    }

     })
     .catch((error) => {
         //return  error;
         //alert('ERROR1111111111:'+error);
     });

    var retrievedDatas59imgour = localStorage.getItem("titlestxtimgour");
    
    var bannertxt11imgour = JSON.parse(retrievedDatas59imgour);

      ////alert(bannertxt11imgour);

      return (

      <div class="site-inner-header">
      <div class="container" style={{minHeight:'50px'}}>
          <div class="row">
              <div class="col-md-6">
                  <div class="inner-nav">
                      <ul>
                          <li><a href="index.html" style={{color: '#fff'}}>Home</a></li>
                          <li>File</li>
                          <li><FontAwesomeIcon icon={faCrown} /> Resize</li>
                      </ul>
                  </div>
              </div>
              <div class="col-md-6">
                  <div class="left-inner-nav">
                      <ul>
                          <li><FontAwesomeIcon icon={faCrown} /> Upgrade</li>
                          <li>Share</li>&nbsp;&nbsp;&nbsp;&nbsp;
                          <li><FontAwesomeIcon icon={faArrowDown} /> Download</li>
                      </ul>
                  </div>
              </div>
          </div>
      </div>
    <div class="grey-bg mg55" style={{height:'900px'}}>    
    <div class="container-fluid">
        <div class="row">
             
            <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 bhoechie-tab-container">
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-3 bhoechie-tab-menu">
                    <div class="list-group">
                    {this.state.isActive ?(
                        <a  href="#" onClick={this.handleShow} class="list-group-item active text-center"> <FontAwesomeIcon icon={faColumns} />
                            <br />Template </a>
                     ) : (    
                      <a  href="#" onClick={this.handleShow} class="list-group-item text-center"> <FontAwesomeIcon icon={faColumns} />
                      <br />Template </a>
                     )}   
                     {this.state.isActive1 ?(
                        <a href="#" onClick={this.handleShows} class="list-group-item active text-center"> <FontAwesomeIcon icon={faVideo} />
                            <br />Photos </a>
                      ) : (    
                        <a href="#" onClick={this.handleShows} class="list-group-item text-center"> <FontAwesomeIcon icon={faVideo} />
                            <br />Photos </a>  
                      )} 
                      {this.state.isActive2 ?(       
                        <a href="#" onClick={this.handleShows1} class="list-group-item active text-center"> <FontAwesomeIcon icon={faEnvelope} />
                            <br />Elements </a>
                       ) : ( 
                        <a href="#" onClick={this.handleShows1} class="list-group-item text-center"> <FontAwesomeIcon icon={faEnvelope} />
                        <br />Elements </a>  
                      )}   
                      {this.state.isActive3 ?(  
                        <a href="#" onClick={this.handleShows2} class="list-group-item active text-center"> <FontAwesomeIcon icon={faTextHeight} />
                            <br /> Text </a>
                       ) : (
                        <a href="#" onClick={this.handleShows2} class="list-group-item text-center"> <FontAwesomeIcon icon={faTextHeight} />
                        <br /> Text </a>
                       )} 
                       {this.state.isActive4 ?(
                        <a href="#" onClick={this.handleShows3} class="list-group-item active text-center"> <FontAwesomeIcon icon={faUpload} />
                            <br /> Upload </a>
                        ) : (
                          <a href="#" onClick={this.handleShows3} class="list-group-item text-center"> <FontAwesomeIcon icon={faUpload} />
                          <br /> Upload </a>
                        )}  
                       {this.state.isActive5 ?(
                        <a href="#" onClick={this.handleShows4} class="list-group-item active text-center"> <FontAwesomeIcon icon={faFolder} />
                            <br /> Folder </a>
                        ) : (
                          <a href="#" onClick={this.handleShows4} class="list-group-item text-center"> <FontAwesomeIcon icon={faFolder} />
                          <br /> Folder </a>  
                       )}    
                       {this.state.isActive6 ?(
                        <a href="#" onClick={this.handleShows5} class="list-group-item active text-center"> <FontAwesomeIcon icon={faFolder} />
                            <br /> Chart </a>
                        ) : (
                          <a href="#" onClick={this.handleShows5} class="list-group-item text-center"> <FontAwesomeIcon icon={faFolder} />
                          <br /> Chart </a>  
                       )}    
                    </div>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-9 col-xs-9 bhoechie-tab">
                {this.state.isActive2 &&
                    <div class="bhoechie-tab-content active">
                        <div class="bhoechie-tab-content active">
                        <div class="inner-search">
                            <input type="search" placeholder="Search Your Content"></input> </div>
                        <div class="inner-template">
                            
                             <div class="row">

                     <div>
                     <Image src={image1} style={{ width:'40px', height:'50px'}} />               
                     </div>
          

          <div class="row">

                <div class="col-md-3 col-sm-3 col-xs-3"> 

                <Draggable
                handle=".handle"
                defaultPosition={{x: 0, y: 0}}
                position={null}
                grid={[125, 125]}
                scale={1}
                onStart={this.handleStart}
                onDrag={this.handleDrag}
                onStop={this.handleStop}>
                <Resizable
                  ref={this.refResizable}
                  defaultSize={20}
                >

                <img style={{width:'5px', height:'5px'}} alt="" src={image1} class="img-responsive" style={{backgroundColor:'coral'}}></img>  
                </Resizable>
                </Draggable>   

                </div>

                <div class="col-md-3 col-sm-3 col-xs-3">

                <Draggable
                handle=".handle"
                defaultPosition={{x: 0, y: 0}}
                position={null}
                grid={[125, 125]}
                scale={1}
                onStart={this.handleStart}
                onDrag={this.handleDrag}
                onStop={this.handleStop}>
                <Resizable
                  ref={this.refResizable}
                  defaultSize={20}
                >

                <img style={{width:'5px', height:'5px'}} alt="" src={image2} class="img-responsive" style={{backgroundColor:'coral'}}></img>  
                </Resizable>
                </Draggable>   

                </div>

                <div class="col-md-3 col-sm-3 col-xs-3">

                <Draggable
                handle=".handle"
                defaultPosition={{x: 0, y: 0}}
                position={null}
                grid={[125, 125]}
                scale={1}
                onStart={this.handleStart}
                onDrag={this.handleDrag}
                onStop={this.handleStop}>
                <Resizable
                  ref={this.refResizable}
                  defaultSize={20}
                >

                <img style={{width:'5px', height:'5px'}} alt="" src={image3} class="img-responsive" style={{backgroundColor:'coral'}}></img>  
                </Resizable>
                </Draggable>   

                </div>

                <div class="col-md-3 col-sm-3 col-xs-3">

                <Draggable
                handle=".handle"
                defaultPosition={{x: 0, y: 0}}
                position={null}
                grid={[125, 125]}
                scale={1}
                onStart={this.handleStart}
                onDrag={this.handleDrag}
                onStop={this.handleStop}>
                <Resizable
                  ref={this.refResizable}
                  defaultSize={20}
                >

                <img style={{width:'15px', height:'15px'}} alt="" src={pies} class="img-responsive" style={{backgroundColor:'coral'}}></img>  
                </Resizable>
                </Draggable>   

                </div>
                  
                <div>
                </div>   

                <br />
                <br />
                <br />


                <div class="col-md-12 col-sm-12 col-xs-12">      
                {bannertxt11imgour!=null && bannertxt11imgour.map((person, index) => 
                <div class="col-md-3 col-sm-3 col-xs-3">
                <img style={{width:'5px', height:'5px'}} alt="" src={person.textimg} class="img-responsive" style={{backgroundColor:'coral'}} />
                      </div>
                )}
                </div>
                </div>

                         

                    </div>
                            
                        </div>
                      </div>
                    </div>
                }  
                {this.state.isActive6 &&
                    <div class="bhoechie-tab-content active">
                        <div class="inner-search">
                            <input type="search" placeholder="Search Your Content"></input> </div>
                        <div class="inner-template">
                            <h4>Charts</h4>
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-6"> <img onClick={this.handleShows6} src={pies} class="img-responsive" alt="" style={{backgroundColor:'coral'}}></img> </div>
                                <div class="col-md-6 col-sm-6 col-xs-6"> <img onClick={barcharts} src={bars} class="img-responsive" alt="" style={{backgroundColor:'coral'}}></img> </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-6"> <img onClick={linecharts} alt="" src={lines} class="img-responsive" style={{backgroundColor:'coral'}}></img>  </div>
                                
                            </div>
                            
                        </div>
                    </div>
                }                
                {this.state.isActive &&
                    <div class="bhoechie-tab-content active">
                        <div class="inner-search">
                            <input type="search" placeholder="Search Your Content"></input> </div>
                        <div class="inner-template">
                            <h4>Sample Templates</h4>
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-6"> <img onClick={this.handleShows7} src={Template1} class="img-responsive" alt="" /> </div>
                                <div class="col-md-6 col-sm-6 col-xs-6"> <img onClick={this.handleShows71} src={Template2} class="img-responsive" alt="" /> </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-6"> <img onClick={this.handleShows72} src={Template3} class="img-responsive" alt="" /> </div>
                                <div class="col-md-6 col-sm-6 col-xs-6"> <img src={Userpics} class="img-responsive" alt="" /> </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-6"> <img src={Userpics} class="img-responsive" alt="" /> </div>
                                <div class="col-md-6 col-sm-6 col-xs-6"> <img src={Userpics} class="img-responsive" alt="" /> </div>
                            </div>
                        </div>
                    </div>
                }    
                {this.state.isActive7 &&
                    <div class="bhoechie-tab-content active">
                      <form name="chartForm" className="loginForm" onSubmit={(e) => this.handleLoginFormSubmit(e)}>
                        <div class="inner-search">
                            <input type="text" name= "text1" placeholder="Text1" onChange={(e) => this.handleLoginFormData(e)} value={this.state.text1}></input> </div>
                        <br /><br />    
                        <div class="inner-search">
                            <input type="text" name= "text2" placeholder="Text2" onChange={(e) => this.handleLoginFormData(e)} value={this.state.text2}></input> </div> 
                        <br /><br />    
                        <div class="inner-search">
                            <input type="text" name= "text3" placeholder="Text3" onChange={(e) => this.handleLoginFormData(e)} value={this.state.text3}></input> </div>
                        <br /><br />    
                        <div class="inner-search">
                            <input type="text" name= "text4" placeholder="Text4" onChange={(e) => this.handleLoginFormData(e)} value={this.state.text4}></input> </div> 
                        <br /><br />    
                        <div class="inner-search">
                        <input type="text" name= "text5" placeholder="Text5" onChange={(e) => this.handleLoginFormData(e)} value={this.state.text5}></input> </div>   
                        <br /><br />  
                        <div class="inner-search">
                        <input type="text" name= "text6" placeholder="Text6" onChange={(e) => this.handleLoginFormData(e)} value={this.state.text6}></input> </div>
 
                        <Button type="submit" bsStyle="primary">SUBMIT</Button>
                        <div class="inner-sidepage"> <iframe src={"http://localhost:3000/piechart"} width="320" height="400" marginLeft="100px" marginTop="-500px" frameborder="0" allowfullscreen></iframe></div>
                    </form>
                                                           
                    </div>
                    }
                    {this.state.isActive1 &&
                    <div class="bhoechie-tab-content active">
                        <p></p>
                    </div>
                    }   
                   
                    {this.state.isActive3 &&
                    <div class="bhoechie-tab-content active">
                        <p></p>
                    </div>
                    }  
                    {this.state.isActive4 &&   
                    <div class="bhoechie-tab-content active">
                        <p> <div class="inner-sidepage"> <iframe src={"http://162.213.248.35:3005/editorupload"} width="300" height="300" frameborder="0" scrollbars='no' allowfullscreen></iframe></div>
                       </p>
                    </div>
                    }
                    {this.state.isActive5 &&  
                    <div class="bhoechie-tab-content active">
                        <p></p>
                    </div>
                    }
                </div>
            </div>
            
            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-3">
            { this.state.isActive7 === true && this.state.isActive8 === true ?
            (<LayersManager><div class="inner-sidepage"> <iframe src={"http://localhost:3000/SampleTemplate2"} width="1200" height="600" frameborder="0" allowfullscreen></iframe></div></LayersManager>)
            : this.state.isActive8 === true ?
            (<LayersManager><div class="inner-sidepage"> <iframe src={"http://localhost:3000/SampleTemplate2"} width="1200" height="600" frameborder="0" allowfullscreen></iframe></div></LayersManager>)
            :
            this.state.isActive10 === true ?
            (<LayersManager><div class="inner-sidepage"> <iframe src={"http://localhost:3000/SampleTemplate3"} width="1200" height="600" frameborder="0" allowfullscreen></iframe></div></LayersManager>)
            :
            this.state.isActive11 === true ?
            (<LayersManager><div class="inner-sidepage"> <iframe src={"http://localhost:3000/SampleTemplate4"} width="1200" height="600" frameborder="0" allowfullscreen></iframe></div></LayersManager>)
            :          
            (<div class="inner-sidepage"> <img src={Userpics1} class="img-responsive" /> </div>)}
            
                
            </div>
        </div>
        </div>
        </div>
    </div>
    
    );
  }
  onDrop(data) {

    this.upload(data);

    alert("data : "+data);

    console.log(data)
    // => banana 
}
}

export default Templates;