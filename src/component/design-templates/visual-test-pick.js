import React from 'react'
import reactCSS from 'reactcss'
import { SketchPicker } from 'react-color';
import { MDBCard, MDBCardBody, MDBBtn, MDBRow, MDBCol } from "mdbreact";

import { Editor } from 'react-draft-wysiwyg';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';

class SketchExample extends React.Component {
  state = {
    displayColorPicker: false,
    displayColor: false,
    color: {
      r: '241',
      g: '112',
      b: '19',
      a: '1',
    },
  };

  

  handleClick = () => {
    this.setState({ displayColorPicker: !this.state.displayColorPicker })
  };

  handleClose = () => {
    this.setState({ displayColorPicker: false })
  };

  handleChange = (color) => {
    this.setState({ color: color.rgb });
    this.setState({ displayPicker: true })
  };

  render() {

    const isBackgroundRed = true;

    const styles = reactCSS({
      'default': {
        color: {
          width: '36px',
          height: '14px',
          borderRadius: '2px',
          background: `rgba(${ this.state.color.r }, ${ this.state.color.g }, ${ this.state.color.b }, ${ this.state.color.a })`,
        },
        colors: {
          width: '1000px',
          height: '1000px',
          borderRadius: '2px',
          color: `rgba(${ this.state.color.r }, ${ this.state.color.g }, ${ this.state.color.b }, ${ this.state.color.a })`,
        },
        swatch: {
          padding: '5px',
          background: '#fff',
          borderRadius: '1px',
          boxShadow: '0 0 0 1px rgba(0,0,0,.1)',
          display: 'inline-block',
          cursor: 'pointer',
        },
        popover: {
          position: 'absolute',
          zIndex: '2',
        },
        cover: {
          position: 'fixed',
          top: '0px',
          right: '0px',
          bottom: '0px',
          left: '0px',
        },
      },
    });

    return (
      <div>
      <div>
        <div style={ styles.swatch } onClick={ this.handleClick }>
          <div style={ styles.color } />
        </div>
        { this.state.displayColorPicker ? <div style={ styles.popover }>
        <div style={ styles.cover } onClick={ this.handleClose }/>
        <SketchPicker color={ this.state.color } onChange={ this.handleChange } /> 
        </div> 
         : null }

      </div>

      <Editor
      toolbarClassName="toolbarClassName"
      wrapperClassName="wrapperClassName"
      editorClassName="editorClassName"
      onEditorStateChange={this.onEditorStateChange}
    />
     
      { this.state.displayPicker ? 
      (<MDBRow>
        <MDBCol md="6" className="mx-auto">
          <MDBCard style={ styles.color }>
            <MDBCardBody className="text-center d-flex justify-content-center align-items-center flex-column">
              <p>My background color will be changed</p>

            </MDBCardBody>
          </MDBCard>
        </MDBCol>
        <MDBCol md="6" className="mx-auto">
          <MDBCard>
            <MDBCardBody className="text-center d-flex justify-content-center align-items-center flex-column">
              <p style={ styles.color }>
              <label style={ styles.colors } className="e-float-text">Last Name</label>
              </p>

           </MDBCardBody>
          </MDBCard>
        </MDBCol>
      </MDBRow>) : null}
    </div>
    )
  }
}

export default SketchExample