import React , { Component} from 'react';

import Constants from '../../common/Constants';

import { MDBContainer, MDBTabPane, MDBTabContent, MDBNav, MDBNavItem, MDBNavLink } from "mdbreact";

import { MDBBtn, MDBModal, MDBModalBody, MDBModalHeader, MDBModalFooter } from 'mdbreact';

import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';

import './styles/style.css';

import { dom } from '@fortawesome/fontawesome-svg-core';

//import { faHome } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus } from "@fortawesome/free-solid-svg-icons";
import { faArrowDown } from "@fortawesome/free-solid-svg-icons";
import { faCog } from "@fortawesome/free-solid-svg-icons";
import { faFile } from "@fortawesome/free-solid-svg-icons";

import Userpic from './images/user-pic.jpg';

//dom.watch() // This will kick off the initial replacement of i to svg tags and configure a MutationObserver

class Popup extends React.Component {
  render() {
    return (
      <div className='popup'>
        <div className='popup_inner'>
          <h1>{this.props.text}</h1>
          <div>hello</div>
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <button onClick={this.props.closePopup}>close me</button>
        </div>
      </div>
    );
  }
}

class Templates extends Component {
  
  state = {
    activeItem: "1"
  }
  
 constructor() {
    super();
    this.sayBasic = this.sayBasic.bind(this)
    this.state = {
      showPopup: false
    };
  }
  togglePopup() {
    this.setState({
      showPopup: !this.state.showPopup
    });
  }
  
  
  toggle = tab => () => {
    if (this.state.activeItem !== tab) {
    this.setState({
      activeItem: tab
    });
    }
  }

  //state = { isOpen: false };

  handleShowDialog = () => {
    this.setState({ isOpen: !this.state.isOpen });
    console.log("clicked");
    
  };

  sayBasic() {
    window.location.href = "/basiceditor";
  }

/*
  constructor(props) {
    super(props);

    this.state = { isOpen: false };
  }

  toggleModal = () => {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }
  */

  render() {
    const imagesPath = Constants.imagespath;
    

    return (

    <div class="site-inner-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-8">
                      <div class="collapse navbar-collapse js-navbar-collapse">
                        <ul class="nav navbar-nav">
                              <li><a href="#"> Dashboard </a></li>
                        </ul>

    </div>
                </div>
                <div class="col-md-4">
                    <div class="left-inner-nav">
                        <ul>
                           <input type="search" class="form-control resume-search"></input>
                            <li>Share</li>
                            <li><FontAwesomeIcon icon={faArrowDown} /> Download</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    
    
    <div class="grey-bg mg55">
    <div class="container">
        <div class="row">
        <div class="col-md-6">
            <div class="login-user">
            <img src={Userpic} class="img-responsive" alt="" />
                <span>Jhon Doe</span>
            </div>
            </div>
            <div class="col-md-6">
            <div class="login-views text-right">
                <p>Weekly Views</p>
                <p><small>10</small></p>
                </div>
            </div>
        </div>
        <span class="line"></span>
        
        <div class="dashboard-btn">
        <ul>
            <li><button onClick={this.sayBasic} class="btn btn-primary btn-lg"><FontAwesomeIcon icon={faPlus} /> CV/Resume</button> </li>
            <li><button class="btn btn-primary btn-lg"><FontAwesomeIcon icon={faPlus} /> Cover letter</button> </li>
            <li><button class="btn btn-primary btn-lg"><FontAwesomeIcon icon={faPlus} /> Website</button> </li>
            </ul>
        </div>
        
        <div class="resume-edit mg55">
        <span><FontAwesomeIcon icon={faFile} /> Resume</span>
            
            <div class="row mg55">
            <div class="col-md-4 cv-edit bg-white">
                
                <h4>My CV Maker</h4>
                <span> <a href="#"><FontAwesomeIcon icon={faCog} /></a> </span>
                
                <br />
                <br />
                <div class="clearfix"></div>
                <h5>CREATED / UPDATED</h5>
                <span> Jun 15 , 2020</span>
                <br />
                <br />
                <h5>TEMPLATE</h5>
                <span> MONTE</span>
                <br />
                <br />
                <div class="row">
                <div class="col-md-6">
                    <h5>TYPE</h5>
                <span> Export Only</span>
                    </div>
                    <div class="col-md-6">
                    <h5>WEEKLY VIEWS</h5>
                <span> 0 Views - Share</span>
                </div>
                    </div>
                
                 <div class="edit-btn">
                <a href="/visualeditor">Edit</a>
                     
                </div>
                 <br />
                 <div class=" edit-btn share-btn">
                <a href="#">Share</a>
                     
                </div>
                </div>
                
            </div>
        </div>
        </div>
    </div>

  
    </div>
    
      );
  }
}

export default Templates;