import React from 'react';

import CopyToClipboard from 'react-copy-to-clipboard';

import {Pie, Doughnut, Chart} from 'react-chartjs-2';


//import {Pie, Doughnut, Chart} from 'react-chartjs';

////import {Chart as Doughnut} from 'react-chartjs';

import image3 from './images/piechart.png';

class App extends React.Component {
  

  constructor() {

    super();

    ////super(props);
  
    this.imgRef = React.createRef();
    this.state = {
      text: '',
      image: ''
    };

  
    ///super(props);
    this.chartRef = React.createRef();

    const parsedTextimg=[
    ];

    parsedTextimg.push({
      label: "A",
      value: 46,
      });
      parsedTextimg.push({
        label: "B",
        value: 30,
    });
    parsedTextimg.push({
      label: "C",
      value: 35,
    });
    parsedTextimg.push({
      label: "D",
      value: 66,
    });
    parsedTextimg.push({
      label: "E",
      value: 40,
    });

    if(parsedTextimg.length>1){
    
      localStorage.setItem('titlestxtrfp', JSON.stringify(parsedTextimg));
    
    }

    var retrievedDatasrfp = localStorage.getItem("titlestxtrfp");

    ////alert(retrievedDatasrfp);

    this.bannertxt11rfp = JSON.parse(retrievedDatasrfp);
  }


  componentDidUpdate() {
    this.myChart.data.labels = this.bannertxt11rfp.map(d => d.label);
    this.myChart.data.datasets[0].data = this.bannertxt11rfp.map(d => d.value);
    this.myChart.update();
  }

  componentDidMount() {
    this.myChart = new Chart(this.chartRef.current, {
      type: 'doughnut',
      data: {
        labels: this.bannertxt11rfp.map(d => d.label),
        datasets: [{
          data: this.bannertxt11rfp.map(d => d.value),
          backgroundColor: this.props.colors
        }]
      }
    });
  }
  
  


  render() {
    return (
      <div>
        
    <CopyToClipboard text={"text is OK"} onCopy={this.onCopy}>
          <button>Copy</button>
    </CopyToClipboard>

    
    <div ref={node => (this.imgRef = node)}>
    <canvas ref={this.chartRef} />
    </div>

    

    </div>
     );
  }
}


export default App;