import isEmpty from 'lodash/isEmpty';
import isEqual from 'lodash/isEqual';


export function loggedIn() { //logged in services
  let userDetails = localStorage.getItem('userDetails');
  return !isEmpty( userDetails ) ? userDetails : '';
}



export function setUserData( userData ) { //set user details
  localStorage.setItem('userDetails', JSON.stringify(userData));
}

