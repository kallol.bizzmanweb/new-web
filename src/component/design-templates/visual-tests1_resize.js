import React, { Component } from 'react'
import ResizeImage from 'react-resize-image'

import bars from './images/barchart.jpg';
 
class componentName extends Component {
  render () {
    return (
      <ResizeImage
        src={bars}
        alt="Tsunami bt hokusai"
        options={{ width: 900 }}
      />
    )
  }
}
 
export default componentName
