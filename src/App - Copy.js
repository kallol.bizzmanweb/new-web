import React from 'react';
import './App.css';
import LoginView from './component/LoginView.js';
import CVIndex from './component/CVIndex.js';
import SignUpView from './component/signUp/SignUpView.js';

import Templates from './component/design-templates/templates';

import cvdashboard from './component/design-templates/cv_dashboard';

import basiceditor from './component/design-templates/basic-editors';

import visualeditor from './component/design-templates/visual-editor';

import visualeditortemplate from './component/design-templates/visual-editor-template';

import SampleTemplate2 from './component/sampletemplates/template2/index.js';

import SampleTemplate3 from './component/sampletemplates/template3/index.js';

import SampleTemplate4 from './component/sampletemplates/template4/index.js';

//import piechart from './component/design-templates/piechart.js';

import charts from './component/demo/container/App.js';

import areachart from './component/demo/component/AreaChart.js';
import piechart from './component/demo/component/PieChart.js';


import linechart from './component/demo/component/LineChart.js';

import barchart from './component/demo/component/BarChart.js';




import {
  Route,
  BrowserRouter as Router,
  Switch
} from "react-router-dom";

function App() {
  return (
   
   <Router>

    <div>
     
      <Switch>
        <Route exact path="/" component={CVIndex} />
        <Route path="/login" component={LoginView} />
        <Route path="/signUp" component={SignUpView} />
        <Route path="/templates" component={Templates} />
        <Route path="/cvdashboard" component={cvdashboard} />
        <Route path="/basiceditor" component={basiceditor} />
        <Route path="/visualeditor" component={visualeditor} />
        <Route path="/visualeditortemplate" component={visualeditortemplate} />
        <Route path="/SampleTemplate2" component={SampleTemplate2} />
        <Route path="/SampleTemplate3" component={SampleTemplate3} />
        <Route path="/SampleTemplate4" component={SampleTemplate4} />
        <Route path="/charts" component={charts} />
        <Route path="/areachart" component={areachart} />
        <Route path="/piechart" component={piechart} />
        <Route path="/linechart" component={linechart} />
        <Route path="/barchart" component={barchart} />


      </Switch>
    </div>
  </Router>
     
    
  );
}

export default App;
