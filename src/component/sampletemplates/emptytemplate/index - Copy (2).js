import React , { Component} from 'react';

import Constants from '../common/Constants';

import { MDBContainer, MDBTabPane, MDBTabContent, MDBNav, MDBNavItem, MDBNavLink } from "mdbreact";

import { MDBBtn, MDBModal, MDBModalBody, MDBModalHeader, MDBModalFooter } from 'mdbreact';

import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';

//import './style/react-tabs.css';

import './css/main.css';

//import './css/main.css.map';
//import './css/main.scss';

import style from './css/main.scss';

import loremIpsum from 'react-lorem-ipsum';

import styles from './css1/style.scss';

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCubes, faFileAlt } from "@fortawesome/free-solid-svg-icons";
import { faAppleAlt } from "@fortawesome/free-solid-svg-icons";
import { faCog } from "@fortawesome/free-solid-svg-icons";
import { faFile } from "@fortawesome/free-solid-svg-icons";
import { faClock } from "@fortawesome/free-solid-svg-icons";
import { faArrowDown } from "@fortawesome/free-solid-svg-icons";
import { faCrown } from "@fortawesome/free-solid-svg-icons";

import { faUpload } from "@fortawesome/free-solid-svg-icons";
import { faTextHeight } from "@fortawesome/free-solid-svg-icons";
import { faFolder } from "@fortawesome/free-solid-svg-icons";
import { faColumns } from "@fortawesome/free-solid-svg-icons";
import { faVideo } from "@fortawesome/free-solid-svg-icons";
import { faEnvelope } from "@fortawesome/free-solid-svg-icons";

import A4 from '../baseTemplate'
import { EditableText, List, RowTexts, HeaderPic } from '../core'

import Userpics from './headshot.jpg';

import cs from 'classnames';

//import Userpics1 from './images/500.jpg';

class Templates extends Component {
  
    state = {
      activeItem: "1"
    }

  constructor() {
      super();
      this.state = {
        showPopup: false,
        showDiv: true,
        show:true,
        isActive: true,
        isActive1: false,
        isActive2: false,
        isActive3: false,
        isActive4: false
      };

      this.state = {
        cv: {
          name: 'Wee',
          nickname: 'fi3ework',
        }
      }
    }
    togglePopup() {
      this.setState({
        showPopup: !this.state.showPopup
      });
    }
    
    
    toggle = tab => () => {
      if (this.state.activeItem !== tab) {
      this.setState({
        activeItem: tab
      });
      }
    }

    handleShow = () => {
      this.setState({
        isActive: true,
        isActive1: false,
        isActive2: false,
        isActive3: false,
        isActive4: false,
        isActive5: false
      });
    };

    handleShows = () => {
      this.setState({
        isActive: false,
        isActive1: true,
        isActive2: false,
        isActive3: false,
        isActive5: false

      });
    };

    handleShows1 = () => {
      this.setState({
        isActive: false,
        isActive1: false,
        isActive2: true,
        isActive3: false,
        isActive4: false,
        isActive5: false
      });
    };

    handleShows2 = () => {
      this.setState({
        isActive: false,
        isActive1: false,
        isActive2: false,
        isActive3: true,
        isActive4: false,
        isActive5: false
      });
    };

    handleShows3 = () => {
      this.setState({
        isActive: false,
        isActive1: false,
        isActive2: false,
        isActive3: false,
        isActive4: true,
        isActive5: false
      });
    };
    handleShows4 = () => {
      this.setState({
        isActive: false,
        isActive1: false,
        isActive2: false,
        isActive3: false,
        isActive4: false,
        isActive5: true
      });
    };
  
    //state = { isOpen: false };
  
    handleShowDialog = () => {
      this.setState({ isOpen: !this.state.isOpen });
      console.log("clicked");
      
    };
  /*
    constructor(props) {
      super(props);
  
      this.state = { isOpen: false };
    }
  
    toggleModal = () => {
      this.setState({
        isOpen: !this.state.isOpen
      });
    }
    */
  
    render() {

      const imagesPath = Constants.imagespath;

      //const { showDiv } = this.state.showDiv;

      const template = () => {
        window.location.href = "/Template2";
        console.log("Template Page"); 
       }
  
      return (

        <A4>
        <article className={cs({
          [style.cv]: true,
         exportRoot: true
        })}>
          <HeaderPic style={{
            padding: '20px 20px 10px 0px'
          }}>
            <header className={style.header}>
            <div id="cv" class="instaFade">
	<div class="mainDetails">
		<div id="headshot" class="quickFade">
			<img src={Userpics} alt="Alan Smith" />
		</div>
		
		<div id="name">
			<h1 class="quickFade  headTitle"><EditableText
                    tagName="h6"
                    html='Joe Bloggs'
                    className={styles.headTitle}
                  /></h1>
			<h2 class="quickFade delayThree headTitles"><EditableText
                    tagName="h6"
                    html='Job Title'
                    className={styles.headTitles}
                  /></h2>
		</div>
		
		<div id="contactDetails" class="quickFade delayFour">
			<ul>
				<li><a href="mailto:joe@bloggs.com" target="_blank"><EditableText
                    tagName="h6"
                    html='<b>e: joe@bloggs.com</b>'
                    className={styles.name}
                  /></a></li>
				<li><a href="http://www.bloggs.com"><EditableText
                    tagName="h6"
                    html='<b>w: www.bloggs.com</b>'
                    className={styles.name}
                  /></a></li>
				<li><EditableText
                    tagName="h6"
                    html='<b>m: 01234567890</b>'
                    className={styles.name}
                  /></li>
			</ul>
		</div>
		<div class="clear"></div>
	</div>
	
	<div id="mainArea" class="quickFade delayFive">
		<section>
			<article>
				<div class="sectionTitles">
					<EditableText
                    tagName="h4"
                    html='Personal Profile'
                    className={styles.sectionTitles}
                  />
				</div>
				
				<div class="sectionContent">
        <EditableText
                    tagName="h4"
                    html='<p>Lorem</span> ipsum dolor sit amet, consectetur adipiscing elit. Vivamus euismod congue nisi, nec consequat quam. In consectetur faucibus turpis eget laoreet. Sed nec imperdiet purus. </p>'
                    className={styles.name}
                  /> 
				</div>
			</article>
			<div class="clear"></div>
		</section>
		
		
		<section>
			<div class="sectionTitles">
				<EditableText
                    tagName="h6"
                    html='Work Experience'
                    className={styles.sectionTitles}
                  />
			</div>
			
			<div class="sectionContent">
				<article>
        <EditableText
                    tagName="h2"
                    html='Job Title at Company'
                    className={styles.sectionTitles}
                  />
					<p class="subDetails"><EditableText
                    tagName="h1"
                    html='April 2011 - Present'
                    className={styles.subDetails}
                  /></p>
					<EditableText
                    tagName="h4"
                    html='<p>Lorem</span> ipsum dolor sit amet, consectetur adipiscing elit. Vivamus euismod congue nisi, nec consequat quam. In consectetur faucibus turpis eget laoreet. Sed nec imperdiet purus. </p>'
                    className={styles.name}
                  /> 
				</article>
				
				<article>
					<EditableText
                    tagName="h2"
                    html='Job Title at Company'
                    className={styles.sectionTitles}
                  />
					<p class="subDetails"><EditableText
                    tagName="h1"
                    html='Janruary 2007 - March 2011'
                    className={styles.subDetails}
                  /></p>
					<EditableText
                    tagName="h4"
                    html='<p>Lorem</span> ipsum dolor sit amet, consectetur adipiscing elit. Vivamus euismod congue nisi, nec consequat quam. In consectetur faucibus turpis eget laoreet. Sed nec imperdiet purus. </p>'
                    className={styles.name}
                  /> 
				</article>
				
				<article>
        <EditableText
                    tagName="h2"
                    html='Job Title at Company'
                    className={styles.sectionTitles}
                  />
					<p class="subDetails"><EditableText
                    tagName="h1"
                    html='October 2004 - December 2006'
                    className={styles.subDetails}
                  /></p>
					<EditableText
                    tagName="h4"
                    html='<p>Lorem</span> ipsum dolor sit amet, consectetur adipiscing elit. Vivamus euismod congue nisi, nec consequat quam. In consectetur faucibus turpis eget laoreet. Sed nec imperdiet purus. </p>'
                    className={styles.name}
                  /> 
				</article>
			</div>
			<div class="clear"></div>
		</section>
		
		
		<section>
			<div class="sectionTitles">
				<EditableText
                    tagName="h6"
                    html='Key Skills'
                    className={styles.sectionTitles}
                  />
			</div>
			
			<div class="sectionContent">
				<ul class="keySkills">
					<li>A Key Skill</li>
					<li>A Key Skill</li>
					<li>A Key Skill</li>
					<li>A Key Skill</li>
					<li>A Key Skill</li>
					<li>A Key Skill</li>
					<li>A Key Skill</li>
					<li>A Key Skill</li>
				</ul>
			</div>
			<div class="clear"></div>
		</section>
		
		
		<section>
			<div class="sectionTitles">
				<EditableText
                    tagName="h4"
                    html='Education'
                    className={styles.sectionTitles}
                  />
			</div>
			
			<div class="sectionContent">
				<article>
					<h2>College/University</h2>
					<p class="subDetails">Qualification</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ultricies massa et erat luctus hendrerit. Curabitur non consequat enim.</p>
				</article>
				
				<article>
					<h2>College/University</h2>
					<p class="subDetails">Qualification</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ultricies massa et erat luctus hendrerit. Curabitur non consequat enim.</p>
				</article>
			</div>
			<div class="clear"></div>
		</section>
		
	</div>
</div>
    </header>
    </HeaderPic>
    </article>
    </A4>
          
    
    );
  }
}

export default Templates;