import React, { Component } from "react";
import { render } from "react-dom";
import { Resize, ResizeVertical, ResizeHorizon } from "react-resize-layout";


class Templates extends Component {

// horizon and vertical
render() {
  return(
  <div>
    <Resize handleWidth="2px" handleColor="#000">
        <ResizeVertical height="120px" minHeight="50px">
            Vertical 1
        </ResizeVertical>
        <ResizeVertical height="160px" minHeight="20px">
            <Resize handleWidth="2px" handleColor="red">
                <ResizeHorizon width="90px">Horizon 1</ResizeHorizon>
                <ResizeHorizon width="120px">Horizon 2</ResizeHorizon>
                <ResizeHorizon minWidth="50px">Horizon 3</ResizeHorizon>
            </Resize>
        </ResizeVertical>
        <ResizeVertical>Vertical 2</ResizeVertical>
    </Resize>
    </div>
);
};
}

export default Templates;