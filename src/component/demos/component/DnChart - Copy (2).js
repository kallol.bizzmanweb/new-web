import React from 'react';

import {Pie, Doughnut, Chart} from 'react-chartjs-2';

import ClipboardItem from 'react-copy-to-clipboard';

//import {Pie, Doughnut, Chart} from 'react-chartjs';

////import {Chart as Doughnut} from 'react-chartjs';


export default class DoughnutChart extends React.Component {
  constructor(props) {
    super(props);
    this.chartRef = React.createRef();

    
  

    const parsedTextimg=[
    ];

    parsedTextimg.push({
      label: "A",
      value: 46,
      });
      parsedTextimg.push({
        label: "B",
        value: 30,
    });
    parsedTextimg.push({
      label: "C",
      value: 35,
    });
    parsedTextimg.push({
      label: "D",
      value: 66,
    });
    parsedTextimg.push({
      label: "E",
      value: 40,
    });

    if(parsedTextimg.length>1){
    
      localStorage.setItem('titlestxtrfp', JSON.stringify(parsedTextimg));
    
    }

    var retrievedDatasrfp = localStorage.getItem("titlestxtrfp");

    ////alert(retrievedDatasrfp);

    this.bannertxt11rfp = JSON.parse(retrievedDatasrfp);
  }

  copyCodeToClipboard = () => {
    const el = this.textArea
    el.select()
    document.execCommand("copy")
  }

  

  copyCodeToClipboards = () => {
    //const el = this.chartRef.current
    //el.select
    //document.execCommand("copy")

    ////this.chartRef.toBlob(blob => navigator.clipboard.write([new ClipboardItem({'image/png': blob})]));

    const item1 = new ClipboardItem({
      "text/plain;charset=utf-8": "data",
      "text/html": "<b>data</b>",
    }
  );
  const imageData = new ArrayBuffer(4096);
  // ... Fill |imageData| with image data.
  //  imageData = "iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mNk+P+/HgAFhAJ/wlseKgAAAABJRU5ErkJggg==";

  const item2 = new ClipboardItem({
      "image/png": new Blob(["iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mNk+P+/HgAFhAJ/wlseKgAAAABJRU5ErkJggg=="], {type: "image/png"}),
      "text/plain": "An image of a blob",
    }
  );

  {navigator.clipboard.write(item1, item2)};


  }

  componentDidUpdate() {
    this.myChart.data.labels = this.bannertxt11rfp.map(d => d.label);
    this.myChart.data.datasets[0].data = this.bannertxt11rfp.map(d => d.value);
    this.myChart.update();
  }

  componentDidMount() {

    this.myChart = new Chart(this.chartRef.current, {
      type: 'doughnut',
      data: {
        labels: this.bannertxt11rfp.map(d => d.label),
        datasets: [{
          data: this.bannertxt11rfp.map(d => d.value),
          backgroundColor: this.props.colors
        }]
      }
    });
  }

  render() {

    return (
<div>
        <div>
          <textarea
            ref={(textarea) => this.textArea = textarea}
            value="Example copy for the textarea."
          />
        </div>
        <div>
          <button onClick={() => {navigator.clipboard.writeText(this.textArea.value)}}> 
            Copy to Clipboard
          </button>
        </div>
       
        <canvas ref={this.chartRef} />;
        <div>
        <button onClick={() => {navigator.clipboard.write(this.myChart)}}> 
            Copy to Clipboard
        </button>
        </div>
      </div>


    )
  }
}