import React , { Component} from 'react';

import Constants from '../../common/Constants';

import { MDBContainer, MDBTabPane, MDBTabContent, MDBNav, MDBNavItem, MDBNavLink } from "mdbreact";

import { MDBBtn, MDBModal, MDBModalBody, MDBModalHeader, MDBModalFooter } from 'mdbreact';


import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import './style/react-tabs.css';


import { IgrDoughnutChartModule } from 'igniteui-react-charts';
import { IgrDoughnutChart } from 'igniteui-react-charts';
import { IgrRingSeriesModule } from 'igniteui-react-charts';
import { IgrRingSeries } from 'igniteui-react-charts';

import Chart from "chart.js";


import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCubes, faFileAlt } from "@fortawesome/free-solid-svg-icons";
import { faAppleAlt } from "@fortawesome/free-solid-svg-icons";
import { faCog } from "@fortawesome/free-solid-svg-icons";
import { faFile } from "@fortawesome/free-solid-svg-icons";
import { faClock } from "@fortawesome/free-solid-svg-icons";
import { faArrowDown } from "@fortawesome/free-solid-svg-icons";
import { faCrown } from "@fortawesome/free-solid-svg-icons";

import { faUpload } from "@fortawesome/free-solid-svg-icons";
import { faTextHeight } from "@fortawesome/free-solid-svg-icons";
import { faFolder } from "@fortawesome/free-solid-svg-icons";
import { faColumns } from "@fortawesome/free-solid-svg-icons";
import { faVideo } from "@fortawesome/free-solid-svg-icons";
import { faEnvelope } from "@fortawesome/free-solid-svg-icons";

import Userpics from './images/300.png';

import Userpics1 from './images/500.jpg';

import Template1 from './images/image1.jpeg';
import Template2 from './images/image3.jpeg';
import Template3 from './images/image2.jpeg';

import pies from './images/piechart.png';
import bars from './images/barchart.jpg';
import lines from './images/linechart.png';

import image1 from './images/Icons/1.png';
import image2 from './images/Icons/2.png';
import image3 from './images/Icons/3.png';

import { Button } from 'react-bootstrap';

import Draggable from 'react-draggable';

import { DraggableCore, DraggableEventHandler, default as DraggableRoot } from "react-draggable";
import { Enable, Resizable, ResizeDirection } from "re-resizable";

import './css/main.css';

import A4 from '../template/baseTemplate'
import { EditableText, List, RowTexts, HeaderPic } from '../template/core'

//import Userpics from './128.jpg';

import cs from 'classnames';

import RaisedButton from 'material-ui/RaisedButton'
////import cs from 'classnames'
import ph from './placeholder.jpg'
import phimg from './128.jpg'
import FileReaderInput from 'react-file-reader-input'

//import Userpics1 from './images/500.jpg';

import phpie from './charts7.png';

import style from './css/style.scss';

import styles from './css/style.scss';

import { Rnd } from "react-rnd";

import { Stage, Layer, Image } from 'react-konva';

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

IgrDoughnutChartModule.register();
IgrRingSeriesModule.register();



class Templates extends Component {
  
    state = {
      activeItem: "1"
    }

  constructor() {
      super();
      ////this.state = {fields: {},emailerror:'', loginError: ''};

      const retrievedsess = sessionStorage.getItem("search");

      this.ctx = React.createRef();

      this.ctxcan = React.createRef();

      this.state = {
        
        data: [
            { MarketShare: 30, Company: "Google",    },
            { MarketShare: 15, Company: "Microsoft", },
            { MarketShare: 30, Company: "Apple",     },
            { MarketShare: 15, Company: "Samsung",   },
            { MarketShare: 10, Company: "Other",     },
    ] };

    this.state = {
      isOnHonver: false,
      imgSrc: phimg
    }

    this.state = {
      cv: {
        name: 'Wee',
        nickname: 'fi3ework',
      }
    }
  

  
        
      

      if(retrievedsess=='true'){

      this.state = {
        showPopup: false,
        showDiv: true,
        show:true,
        isActive: false,
        isActive1: false,
        isActive2: false,
        isActive3: false,
        isActive4: false,
        isActive5: false,
        isActive6: false,
        isActive7: true,
        fields: {},emailerror:'', loginError: ''
      };

    }else{

      this.state = {
        showPopup: false,
        showDiv: true,
        show:true,
        isActive: true,
        isActive1: false,
        isActive2: false,
        isActive3: false,
        isActive4: false,
        isActive5: false,
        isActive6: false,
        isActive7: false,
        isActive8: false,
        isActive9: true,
        fields: {},emailerror:'', loginError: ''
      };
      
    }
    }


    _arrayBufferToBase64(buffer) {
      let binary = ''
      let bytes = new Uint8Array(buffer)
      let len = bytes.byteLength
      for (let i = 0; i < len; i++) {
        binary += String.fromCharCode(bytes[i])
      }
      return window.btoa(binary)
      }
  
      upload = (e, results) => {
        let reader = results[0][0].target
        let that = this
        let base64 = 'data:image/jpg;base64, ' + this._arrayBufferToBase64(reader.result)
        that.img.setAttribute('src', base64)
      }
  
      uploads = (e, results) => {
        let reader = results[0][0].target
        let that = this
        let base64 = 'data:image/jpg;base64, ' + this._arrayBufferToBase64(reader.result)
        that.imgs.setAttribute('src', base64)
      }
  
      togglePopup() {
        this.setState({
          showPopup: !this.state.showPopup
        });
      }
      
      toggle = tab => () => {
        if (this.state.activeItem !== tab) {
        this.setState({
          activeItem: tab
        });
        }
      }

    togglePopup() {
      this.setState({
        showPopup: !this.state.showPopup
      });
    }
    
    handleLoginFormData(e) {
      e.preventDefault();
      let fieldName = e.target.name, fieldValue = e.target.value, fields = this.state.fields;
  
      fields[fieldName] = fieldValue;

      //alert(fields);

      this.setState({fields: fields});
   
    }
    
    toggle = tab => () => {
      if (this.state.activeItem !== tab) {
      this.setState({
        activeItem: tab
      });
      }
    }

    handleShow = () => {
      this.setState({
        isActive: true,
        isActive1: false,
        isActive2: false,
        isActive3: false,
        isActive4: false,
        isActive5: false,
        isActive6: false,
        isActive7: true,
        isActive8: false,
        isActive9: true
      });
    };

    handleShows = () => {
      this.setState({
        isActive: false,
        isActive1: true,
        isActive2: false,
        isActive3: false,
        isActive5: false,
        isActive6: false,
        isActive7: false,
        isActive8: false,
        isActive9: true
      });
    };

    handleShows1 = () => {
      this.setState({
        isActive: false,
        isActive1: false,
        isActive2: true,
        isActive3: false,
        isActive4: false,
        isActive5: false,
        isActive6: false,
        isActive7: false,
        isActive8: true,
        isActive9: true
      });
    };

    handleShows2 = () => {
      this.setState({
        isActive: false,
        isActive1: false,
        isActive2: false,
        isActive3: true,
        isActive4: false,
        isActive5: false,
        isActive6: false,
        isActive7: false,
        isActive8: false,
        isActive9: true
      });
    };

    handleShows3 = () => {
      this.setState({
        isActive: false,
        isActive1: false,
        isActive2: false,
        isActive3: false,
        isActive4: true,
        isActive5: false,
        isActive6: false,
        isActive7: false,
        isActive8: false,
        isActive9: true
      });
    };
    handleShows4 = () => {
      this.setState({
        isActive: false,
        isActive1: false,
        isActive2: false,
        isActive3: false,
        isActive4: false,
        isActive5: true,
        isActive6: false,
        isActive7: false,
        isActive8: false,
        isActive9: true
      });
    };

    handleShows5 = () => {
      this.setState({
        isActive: false,
        isActive1: false,
        isActive2: false,
        isActive3: false,
        isActive4: false,
        isActive5: false,
        isActive6: true,
        isActive7: false,
        isActive8: false,
        isActive9: true
      });

      sessionStorage.removeItem("search");
    };

    handleShows6 = () => {
      this.setState({
        isActive: false,
        isActive1: false,
        isActive2: false,
        isActive3: false,
        isActive4: false,
        isActive5: false,
        isActive6: false,
        isActive7: true,
        isActive8: false,
        isActive9: true
      });
    }

    handleShows7 = () => {
      this.setState({
        isActive: true,
        isActive1: false,
        isActive2: false,
        isActive3: false,
        isActive4: false,
        isActive5: false,
        isActive6: false,
        isActive7: true,
        isActive8: true,
        isActive9: true
      });
    }
  
    handleLoginFormSubmit(e) {
      e.preventDefault();

     //// const err = this.handleValidation();
  
     let self = this, fields = this.state.fields;

     sessionStorage.setItem('text1', fields.text1);
     sessionStorage.setItem('text2', fields.text2);
     sessionStorage.setItem('text3', fields.text3);
     sessionStorage.setItem('text4', fields.text4);
     sessionStorage.setItem('text5', fields.text5);
     sessionStorage.setItem('text6', fields.text6);
     sessionStorage.setItem('search', "true");
  
     //console.log(fields.fullname);
     //console.log(fields.email);
     //console.log(fields.loginPassword);

     window.location.href="/visualeditortemplate"; 

     this.setState({
      isActive: false,
      isActive1: false,
      isActive2: false,
      isActive3: false,
      isActive4: false,
      isActive5: false,
      isActive6: false,
      isActive7: true,
      isActive8: false,
      isActive9: true,
      isActive14: true
    });

    this.forceUpdate();
    

    }

    //state = { isOpen: false };
  
    handleShowDialog = () => {
      this.setState({ isOpen: !this.state.isOpen });
      console.log("clicked");
      
    };
  /*
    constructor(props) {
      super(props);
  
      this.state = { isOpen: false };
    }
  
    toggleModal = () => {
      this.setState({
        isOpen: !this.state.isOpen
      });
    }
    */
  
    render() {

      

      const imagesPath = Constants.imagespath;

      const { showDiv } = this.state.showDiv;

      const template = () => {
        window.location.href = "/Template2";
        console.log("Template Page"); 
       }
  
       const template1 = () => {
        window.location.href = "/SampleTemplate2";
        console.log("Template Page"); 
       }

       const template2 = () => {
        window.location.href = "/SampleTemplate3";
        console.log("Template Page"); 
       }

       const template3 = () => {
        window.location.href = "/SampleTemplate4";
        console.log("Template Page"); 
       }

       const piecharts = () => {
        window.location.href = "/piechart";
        console.log("Template Page"); 
       }

       const barcharts = () => {
        window.location.href = "/barchart";
        console.log("Template Page"); 
       }

       const linecharts = () => {
        window.location.href = "/linechart";
        console.log("Template Page"); 
       }

       const ctx = this.ctx;
       var chart = JSON.stringify(new Chart(this.ctx, {
          type: "bar",
          data: {
            labels: ["Red", "Blue", "Yellow"],
            datasets: [
              {
                label: "# of Likes",
                data: [12, 19, 3],
                backgroundColor: [
                  "rgb(255,0,0)",
                  "rgb(0,0,255)",
                  "rgb(128,0,0)"
                ]
              },
              {
                label: "# of Likes",
                data: [-12, -19, -3],
                backgroundColor: [
                  "rgb(255,0,0)",
                  "rgb(0,0,255)",
                  "rgb(128,0,0)"
                ]
              }
            ]
          }
        }).toDataURL);


      const iframe = '<iframe src="http://plnkr.co/" width="540" height="450"></iframe>'; 


      return (

      <div class="site-inner-header">
      <div class="container" style={{minHeight:'50px'}}>
          <div class="row">
              <div class="col-md-6">
                  <div class="inner-nav">
                      <ul>
                          <li><a href="index.html" style={{color: '#fff'}}>Home</a></li>
                          <li>File</li>
                          <li><FontAwesomeIcon icon={faCrown} /> Resize</li>
                      </ul>
                  </div>
              </div>
              <div class="col-md-6">
                  <div class="left-inner-nav">
                      <ul>
                          <li><FontAwesomeIcon icon={faCrown} /> Upgrade</li>
                          <li>Share</li>&nbsp;&nbsp;&nbsp;&nbsp;
                          <li><FontAwesomeIcon icon={faArrowDown} /> Download</li>
                      </ul>
                  </div>
              </div>
          </div>
      </div>
    <div class="grey-bg mg55" style={{minHeight:'100%'}}>    
    <div class="container-fluid">
        <div class="row">
             
            <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 bhoechie-tab-container">
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-3 bhoechie-tab-menu">
                    <div class="list-group">
                    {this.state.isActive ?(
                        <a  href="#" onClick={this.handleShow} class="list-group-item active text-center"> <FontAwesomeIcon icon={faColumns} />
                            <br />Template </a>
                     ) : (    
                      <a  href="#" onClick={this.handleShow} class="list-group-item text-center"> <FontAwesomeIcon icon={faColumns} />
                      <br />Template </a>
                     )}   
                     {this.state.isActive1 ?(
                        <a href="#" onClick={this.handleShows} class="list-group-item active text-center"> <FontAwesomeIcon icon={faVideo} />
                            <br />Photos </a>
                      ) : (    
                        <a href="#" onClick={this.handleShows} class="list-group-item text-center"> <FontAwesomeIcon icon={faVideo} />
                            <br />Photos </a>  
                      )} 
                      {this.state.isActive2 ?(       
                        <a href="#" onClick={this.handleShows1} class="list-group-item active text-center"> <FontAwesomeIcon icon={faEnvelope} />
                            <br />Elements </a>
                       ) : ( 
                        <a href="#" onClick={this.handleShows1} class="list-group-item text-center"> <FontAwesomeIcon icon={faEnvelope} />
                        <br />Elements </a>  
                      )}   
                      {this.state.isActive3 ?(  
                        <a href="#" onClick={this.handleShows2} class="list-group-item active text-center"> <FontAwesomeIcon icon={faTextHeight} />
                            <br /> Text </a>
                       ) : (
                        <a href="#" onClick={this.handleShows2} class="list-group-item text-center"> <FontAwesomeIcon icon={faTextHeight} />
                        <br /> Text </a>
                       )} 
                       {this.state.isActive4 ?(
                        <a href="#" onClick={this.handleShows3} class="list-group-item active text-center"> <FontAwesomeIcon icon={faUpload} />
                            <br /> Upload </a>
                        ) : (
                          <a href="#" onClick={this.handleShows3} class="list-group-item text-center"> <FontAwesomeIcon icon={faUpload} />
                          <br /> Upload </a>
                        )}  
                       {this.state.isActive5 ?(
                        <a href="#" onClick={this.handleShows4} class="list-group-item active text-center"> <FontAwesomeIcon icon={faFolder} />
                            <br /> Folder </a>
                        ) : (
                          <a href="#" onClick={this.handleShows4} class="list-group-item text-center"> <FontAwesomeIcon icon={faFolder} />
                          <br /> Folder </a>  
                       )}    
                       {this.state.isActive6 ?(
                        <a href="#" onClick={this.handleShows5} class="list-group-item active text-center"> <FontAwesomeIcon icon={faFolder} />
                            <br /> Chart </a>
                        ) : (
                          <a href="#" onClick={this.handleShows5} class="list-group-item text-center"> <FontAwesomeIcon icon={faFolder} />
                          <br /> Chart </a>  
                       )}    
                    </div>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-9 col-xs-9 bhoechie-tab">
                {this.state.isActive2 &&
                    <div class="bhoechie-tab-content active">
                        <div class="bhoechie-tab-content active">
                        <div class="inner-search">
                            <input type="search" placeholder="Search Your Content"></input> </div>
                        <div class="inner-template">
                            
                             <div class="row">
                                <div class="col-md-3 col-sm-3 col-xs-3"> <img style={{width:'5px', height:'5px'}} alt="" src={image1} class="img-responsive" style={{backgroundColor:'coral'}}></img>  </div>
                                
                            
                                <div class="col-md-3 col-sm-3 col-xs-3"> <img style={{width:'5px', height:'5px'}} alt="" src={image2} class="img-responsive" style={{backgroundColor:'coral'}}></img>  </div>
                                
                            
                                <div class="col-md-3 col-sm-3 col-xs-3"> <img style={{width:'5px', height:'5px'}} alt="" src={image3} class="img-responsive" style={{backgroundColor:'coral'}}></img>  </div>
                            
                                <div class="col-md-3 col-sm-3 col-xs-3"> <img style={{width:'5px', height:'5px'}} alt="" src={image3} class="img-responsive" style={{backgroundColor:'coral'}}></img>  </div>
                                
                            </div>
                            
                        </div>
                      </div>
                    </div>
                }  
                {this.state.isActive6 &&
                    <div class="bhoechie-tab-content active">
                        <div class="inner-search">
                            <input type="search" placeholder="Search Your Content"></input> </div>
                        <div class="inner-template">
                            <h4>Charts</h4>
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-6"> <img onClick={this.handleShows6} src={pies} class="img-responsive" alt="" style={{backgroundColor:'coral'}}></img> </div>
                                <div class="col-md-6 col-sm-6 col-xs-6"> <img onClick={barcharts} src={bars} class="img-responsive" alt="" style={{backgroundColor:'coral'}}></img> </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-6"> <img onClick={linecharts} alt="" src={lines} class="img-responsive" style={{backgroundColor:'coral'}}></img>  </div>
                                
                            </div>
                            
                        </div>
                    </div>
                }                
                {this.state.isActive &&
                    <div class="bhoechie-tab-content active">
                        <div class="inner-search">
                            <input type="search" placeholder="Search Your Content"></input> </div>
                        <div class="inner-template">
                            <h4>Sample Templates</h4>
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-6"> <img onClick={this.handleShows7} src={Template1} class="img-responsive" alt="" /> </div>
                                <div class="col-md-6 col-sm-6 col-xs-6"> <img onClick={template2} src={Template2} class="img-responsive" alt="" /> </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-6"> <img onClick={template3} src={Template3} class="img-responsive" alt="" /> </div>
                                <div class="col-md-6 col-sm-6 col-xs-6"> <img src={Userpics} class="img-responsive" alt="" /> </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-6"> <img src={Userpics} class="img-responsive" alt="" /> </div>
                                <div class="col-md-6 col-sm-6 col-xs-6"> <img src={Userpics} class="img-responsive" alt="" /> </div>
                            </div>
                        </div>
                    </div>
                }    
                {this.state.isActive7 && !this.state.isActive &&
                    <div class="bhoechie-tab-content active">
                      <form name="chartForm" className="loginForm" onSubmit={(e) => this.handleLoginFormSubmit(e)}>
                        <div class="inner-search">
                            <input type="text" name= "text1" placeholder="Text1" onChange={(e) => this.handleLoginFormData(e)} value={this.state.text1}></input> </div>
                        <br /><br />    
                        <div class="inner-search">
                            <input type="text" name= "text2" placeholder="Text2" onChange={(e) => this.handleLoginFormData(e)} value={this.state.text2}></input> </div> 
                        <br /><br />    
                        <div class="inner-search">
                            <input type="text" name= "text3" placeholder="Text3" onChange={(e) => this.handleLoginFormData(e)} value={this.state.text3}></input> </div>
                        <br /><br />    
                        <div class="inner-search">
                            <input type="text" name= "text4" placeholder="Text4" onChange={(e) => this.handleLoginFormData(e)} value={this.state.text4}></input> </div> 
                        <br /><br />    
                        <div class="inner-search">
                        <input type="text" name= "text5" placeholder="Text5" onChange={(e) => this.handleLoginFormData(e)} value={this.state.text5}></input> </div>   
                        <br /><br />  
                        <div class="inner-search">
                        <input type="text" name= "text6" placeholder="Text6" onChange={(e) => this.handleLoginFormData(e)} value={this.state.text6}></input> </div>   
                        
                        <Button type="submit" bsStyle="primary">SUBMIT</Button>
                    </form>                                       
                    </div>
                    }
                    {this.state.isActive1 &&
                    <div class="bhoechie-tab-content active">
                        <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32. The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32. </p>
                    </div>
                    }   
                   
                    {this.state.isActive3 &&
                    <div class="bhoechie-tab-content active">
                        <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32. The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32. </p>
                    </div>
                    }  
                    {this.state.isActive4 &&   
                    <div class="bhoechie-tab-content active">
                        <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32. The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32. </p>
                    </div>
                    }
                    {this.state.isActive5 &&  
                    <div class="bhoechie-tab-content active">
                        <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32. The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32. </p>
                    </div>
                    }
                </div>
            </div>
            
            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-3">
            {this.state.isActive7 === true ?
            (<div> 
            <Draggable
        handle=".handle"
        defaultPosition={{x: 0, y: 0}}
        position={null}
        grid={[125, 125]}
        scale={1}
        onStart={this.handleStart}
        onDrag={this.handleDrag}
        onStop={this.handleStop}>
        <Resizable
          ref={this.refResizable}
          defaultSize={20}
        >
          
        <div>
          <div className="handle"> <canvas width='800' height='300' ref={ctx => (this.ctx = ctx)}/></div>
          <div>drag on</div>
        </div>
        </Resizable>
      </Draggable>
  </div>):null} 
  </div>
  
  
  <div class="col-lg-5 col-md-5 col-sm-4 col-xs-3">    
          {this.state.isActive7 === true && this.state.isActive8 === true ?
            (<div>
            <div> 
              <Draggable
          handle=".handle"
          defaultPosition={{x: 0, y: 0}}
          position={null}
          grid={[125, 125]}
          scale={1}
          onStart={this.handleStart}
          onDrag={this.handleDrag}
          onStop={this.handleStop}>
          <Resizable
            ref={this.refResizable}
            defaultSize={20}
          >
            
          <div>
            <div className="handle"><div class="inner-sidepage"> 
            
            <Rnd
    style={{display: "flex", alignItems: "center", justifyContent: "center", border: "solid 1px #ddd", background: "#f0f0f0"}}
    default={{
      x: 0,
      y: 0,
      width: 0,
      height: 0
    }}
  >  
            <sandbox src={"http://localhost:3000/SampleTemplate3"} width="1200" height="600" frameborder="0" allowfullscreen></sandbox>
            </Rnd>
            </div></div>
            <div>drag on</div>
          </div>
          </Resizable>
        </Draggable>
    </div>
    <div>)
    </div>
    </div>      
  
    ):null};
            
                
            </div>
            <DraggableCore
    bounds='parent'
    ref='draggable'
    onStop={this.handleDraggableStop}>
       
      <div className='draggable-child'>
      
            <div className="handle"><div class="inner-sidepage"> 
            <Rnd
    style={{display: "flex", alignItems: "center", justifyContent: "center", border: "solid 1px #ddd", background: "#f0f0f0"}}
    default={{
      x: 600,
      y: 600,
      width:2800,
      height: 500
    }}
  >  

       
          <div>
    </div>
  
             </Rnd>
            </div></div>
            <div>drag on</div>
          
      <Draggable
        handle=".handle"
        defaultPosition={{x: 0, y: 0}}
        position={null}
        grid={[125, 125]}
        scale={1}
        onStart={this.handleStart}
        onDrag={this.handleDrag}
        onStop={this.handleStop}>
        <Resizable
          ref={this.refResizable}
          defaultSize={20}
        >
          
        <div>
          <div className="handle"> <canvas width='0' height='0' ref={ctx => (this.ctx = ctx)}/></div>
          <div>drag on</div>
        </div>
        </Resizable>
      </Draggable>
      
      </div>
  </DraggableCore>



  
        </div>
        {this.state.isActive7 === true && this.state.isActive8 === true ?
      (<Draggable
        handle=".handle"
        defaultPosition={{x: 0, y: 0}}
        position={null}
        grid={[125, 125]}
        scale={1}
        onStart={this.handleStart}
        onDrag={this.handleDrag}
        onStop={this.handleStop}>
        <Resizable
          ref={this.refResizable}
          defaultSize={20}
        >
        <div className="handle">
        <div style={{height: '200px', width: '200px', background: 'white', position: 'relative'}}>
  <div style={{position: 'absolute', top: '20px', left: '30px'}}>
    <div style={{background: '#eee', height: '10px', width: '10px', borderRadius: '25px'}}>
    <MuiThemeProvider>
    <Rnd
    style={{display: "flex", alignItems: "center", justifyContent: "center", border: "solid 1px #ddd", background: "#f0f0f0"}}
    default={{
      x: 600,
      y: 600,
      width:2800,
      height: 500
    }}
      >  <Draggable
      handle=".handle"
      defaultPosition={{x: 0, y: 0}}
      position={null}
      grid={[125, 125]}
      scale={1}
      onStart={this.handleStart}
      onDrag={this.handleDrag}
      onStop={this.handleStop}>
      <Resizable
        ref={this.refResizable}
        defaultSize={20}
      >
        
      <div>
        <div className="handle">  

        <article className={cs({
          [style.cv]: true,
         exportRoot: true
        })}>
          <HeaderPic style={{
            padding: '0px 0px 0px 0px'
          }}>
        <header className={style.header}>
          
        <div class="resume-wrapper">
        <section class="profile section-padding" style={{width:'40%'}}>
            <div class="container">
            <div class="picture-resume-wrapper" style={{width:'15%'}}>
            <div class="picture-resume">
            <div
            className={style.headerPicWrapper}
            style={this.props.style}
            onMouseEnter={() => this.setState({ isOnHonver: true })}
            onMouseLeave={() => this.setState({ isOnHonver: false })}
        >   
        <img
          src={phimg}
          ref={(node) => { this.img = node }}
        />
        <FileReaderInput
          as="buffer"
          onChange={this.upload}>
          
        <RaisedButton
            label="CHANGE PIC"
            className={cs(
              {
                [style.changePic]: true,
                changePic: true
              }
            )}
            style={{
              position: 'absolute',
              top: 0,
              right: 10,
              display: this.state.isOnHonver ? 'block' : 'none',
              zIndex: 0,
            }}
          />
        </FileReaderInput>
        <div className={style.children}>
          {this.props.children}
        </div>
      </div>
        
      </div>
             <div class="clearfix"></div>
      </div>
          <div class="name-wrapper" style={{width:'100%'}}>
          <div> 
            
          <div> 
            <Draggable
        handle=".handle"
        defaultPosition={{x: 0, y: 0}}
        position={null}
        grid={[125, 125]}
        scale={1}
        onStart={this.handleStart}
        onDrag={this.handleDrag}
        onStop={this.handleStop}>
        <Resizable
          ref={this.refResizable}
          defaultSize={20}
        >
          
        <div>
          <div className="handle"> <canvas width='800' height='300' ref={ctx => (this.ctx = ctx)}/></div>
          <div>drag on</div>
        </div>
        </Resizable>
        </Draggable>
        </div>
          
      </div>
          </div>
            
          
          <div class="clearfix"></div>
          <div class="contact-info clearfix">
          <div class="picture-resume-wrapper" style={{width:'300px',height:'300px'}}>  
          <div class="picture-resume">
          <div style={{width:'300px',height:'300px'}}
            className={style.headerPicWrapper}
            style={this.props.style}
            onMouseEnter={() => this.setState({ isOnHonver: true })}
            onMouseLeave={() => this.setState({ isOnHonver: false })}
        >   
        <img style={{width:'300px',height:'300px', marginTop:'-100px', marginLeft:'-40px'}}
          src={phpie}
          ref={(node) => { this.imgs = node }}
        />
        <FileReaderInput
          as="buffer"
          onChange={this.uploads}>
          
        <RaisedButton
            label="Add Graph"
            className={cs(
              {
                [style.changePic]: true,
                changePic: true
              }
            )}
            style={{
              position: 'absolute',
              top: 0,
              right: 10,
              display: this.state.isOnHonver ? 'block' : 'none',
              zIndex: 0,
            }}
          />
        </FileReaderInput>
        <div className={style.children}>
          {this.props.children}
        </div>
      </div>
        
      </div>
    

          </div>    
          </div>  
          
          <div class="clearfix"></div>
          <div class="contact-info clearfix">
              <ul class="list-titles" style={{width:'15%', marginTop:'-100px'}}>
                  <li><EditableText
                    tagName="h4"
                    html='<b>Call</b>'
                    className={styles.name}
                  /></li>
                  <li><EditableText
                    tagName="h4"
                    html='<b>Mail</b>'
                    className={styles.name}
                  /></li>
                  <li><EditableText
                    tagName="h4"
                    html='<b>Web</b>'
                    className={styles.name}
                  /></li>
                  <li><EditableText
                    tagName="h4"
                    html='<b>Home</b>'
                    className={styles.name}
                  /></li>
              </ul>
            <ul class="list-content" style={{width:'15%', marginTop:'-100px'}}>
                <li><EditableText
                    tagName="h4"
                    html='<b>+34 123 456 789</b>'
                    className={styles.name}
                  /></li>
                <li><EditableText
                    tagName="h4"
                    html='<b>j.anderson@gmail.com</b>'
                    className={styles.name}
                  /></li> 
                <li><a href="#"><EditableText
                    tagName="h4"
                    html='<b>janderson.com</b>'
                    className={styles.name}
                  /></a></li> 
                <li><EditableText
                    tagName="h4"
                    html='<b>Los Angeles, CA</b>'
                    className={styles.name}
                  /></li> 
            </ul>
          </div>
          <div class="contact-presentation"> 
              <EditableText
                    tagName="h4"
                    html='<p>Lorem</span> ipsum dolor sit amet, consectetur adipiscing elit. Vivamus euismod congue nisi, nec consequat quam. In consectetur faucibus turpis eget laoreet. Sed nec imperdiet purus. </p>'
                    className={styles.name}
                  /> 
          </div>
          <div class="contact-social clearfix">
              <ul class="list-titles" style={{width:'15%'}}>
                  <li>Twitter</li>
                  <li>Dribbble</li>
                  <li>Codepen</li>
              </ul>
            <ul class="list-content"> 
                  <li><a href="">@janderson</a></li> 
                  <li><a href="">janderson</a></li> 
                  <li><a href="">janderson</a></li> 
              </ul>
          </div>
            </div>
        </section>
      
      <section class="experience section-padding">
          <div class="container">
              <h3 class="experience-title"><EditableText
                    tagName="h4"
                    html='Experience'
                    className={styles.name}
                  /></h3>
          
          <div class="experience-wrapper">
              <div class="company-wrapper clearfix">
                  <div class="experience-title"><EditableText
                    tagName="h4"
                    html='Company name'
                    className={styles.name}
                  /></div> 
              <div class="time"><EditableText
                    tagName="h4"
                    html='Nov 2012 - Present'
                    className={styles.name}
                  /></div> 
              </div>
            
            <div class="job-wrapper clearfix">
                <div class="experience-title"><EditableText
                    tagName="h4"
                    html='Front End Developer'
                    className={styles.name}
                  /> </div> 
              <div class="company-description">
                  <p style={{width:'45%', color:'#9099a0'}}>
                  <EditableText
                    tagName="h4"
                    html='Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce a elit facilisis, adipiscing leo in, dignissim magna.</b'
                    className={styles.name}
                  /></p>  
              </div>
            </div>
            
            <div class="company-wrapper clearfix">
                  <div class="experience-title"><EditableText
                    tagName="h4"
                    html='Company name'
                    className={styles.name}
                  /></div> 
              <div class="time"><EditableText
                    tagName="h4"
                    html='Nov 2010 - Present'
                    className={styles.name}
                  /></div> 
              </div>
            
             <div class="job-wrapper clearfix">
                <div class="experience-title"><EditableText
                    tagName="h4"
                    html='Freelance, Web Designer / Web Developer'
                    className={styles.name}
                  /></div> 
              <div class="company-description">
                  <p style={{width:'45%', color:'#9099a0'}}>
                  <EditableText
                    tagName="h4"
                    html='Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce a elit facilisis, adipiscing leo in, dignissim magna.'
                    className={styles.name}
                  /></p>  
              </div>
            </div>
            
            <div class="company-wrapper clearfix">
                  <div class="experience-title"><EditableText
                    tagName="h4"
                    html='Company name'
                    className={styles.name}
                  /></div> 
              <div class="time"><EditableText
                    tagName="h4"
                    html='Nov 2009 - Nov 2010'
                    className={styles.name}
                  /></div> 
              </div> 
            
             <div class="job-wrapper clearfix">
                <div class="experience-title">Web Designer </div> 
              <div class="company-description">
                  <p style={{width:'45%', color:'#9099a0'}}><EditableText
                    tagName="h4"
                    html='Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce a elit facilisis, adipiscing leo in, dignissim magna.'
                    className={styles.name}
                  /></p>   
              </div>
            </div>
            
          </div>
          
          <div style={{width:'30%'}} class="section-wrapper clearfix">
              <h3 class="section-title">Skills</h3>  
                <ul>
                    <li class="skill-percentage"><EditableText
                    tagName="h4"
                    html='HTML / HTML5'
                    className={styles.name}
                  /></li>
                    <li class="skill-percentage"><EditableText
                    tagName="h4"
                    html='CSS / CSS3 / SASS / LESS'
                    className={styles.name}
                  /></li>
                    <li class="skill-percentage"><EditableText
                    tagName="h4"
                    html='Javascript'
                    className={styles.name}
                  /></li>
                    <li class="skill-percentage"><EditableText
                    tagName="h4"
                    html='Jquery'
                    className={styles.name}
                  /></li>
                    <li class="skill-percentage"><EditableText
                    tagName="h4"
                    html='Wordpress'
                    className={styles.name}
                  /></li>
                    <li class="skill-percentage"><EditableText
                    tagName="h4"
                    html='Photoshop'
                    className={styles.name}
                  /></li>
                
                </ul>
            
          </div>
          
          <div class="section-wrapper clearfix">
            <h3 class="section-title">Hobbies</h3>  
            <p style={{width:'45%', color:'#9099a0'}}><EditableText
                    tagName="h4"
                    html='Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce a elit facilisis, adipiscing leo in, dignissim magna.'
                    className={styles.name}
                  /></p>
            
            <p style={{color:'#9099a0'}}><EditableText
                    tagName="h4"
                    html='Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce a elit facilisis, adipiscing leo in, dignissim magna.'
                    className={styles.name}
                  /></p> 
          </div>
          
          </div>
      </section>
      
      <div class="clearfix"></div>
    </div>

    
    </header>
    </HeaderPic>
    </article>
    </div>
    </div>
    </Resizable>
      </Draggable>
    
    </Rnd>
    </MuiThemeProvider>
    </div>
    </div>
    </div>
    
    
    </div>
    </Resizable>
      </Draggable>) : null}    
 
 </div>

        </div>
        </div>
        
   
    
    );
  }
}

export default Templates;