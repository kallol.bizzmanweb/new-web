import React , { Component} from 'react';

import Constants from '../../common/Constants';

import { MDBContainer, MDBTabPane, MDBTabContent, MDBNav, MDBNavItem, MDBNavLink } from "mdbreact";

import { MDBBtn, MDBModal, MDBModalBody, MDBModalHeader, MDBModalFooter } from 'mdbreact';

//import Modal from './Modal';

import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
//import './style/react-tabs.css';

import './styles/style.css';

import {Button, Collapse} from 'react-bootstrap'

import { dom } from '@fortawesome/fontawesome-svg-core';

//import { faHome } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus } from "@fortawesome/free-solid-svg-icons";
import { faArrowDown } from "@fortawesome/free-solid-svg-icons";
import { faCog } from "@fortawesome/free-solid-svg-icons";
import { faFile } from "@fortawesome/free-solid-svg-icons";
import { faChevronRight } from "@fortawesome/free-solid-svg-icons";

import Userpic from './images/user-pic.jpg';


//dom.watch() // This will kick off the initial replacement of i to svg tags and configure a MutationObserver

class Popup extends React.Component {
  render() {
    return (
      <div className='popup' style={{backgroundColor:'#000000',marginLeft:'100px'}}>
        <div className='popup_inner'>
          <h1>{this.props.text}</h1>
          <div style={{backgroundColor:'#000000'}}>hhhhhhhhhhhhhbbbbbbbbbbbbbbbb</div>
        <div style={{backgroundColor:'#000000'}}>hhhhhhhhhhhhhbbbbbbbbbbbbbbbb</div>
        <div style={{backgroundColor:'#000000'}}>hhhhhhhhhhhhhbbbbbbbbbbbbbbbb</div>
        <div style={{backgroundColor:'#000000'}}>hhhhhhhhhhhhhbbbbbbbbbbbbbbbb</div>
        <div style={{backgroundColor:'#000000'}}>hhhhhhhhhhhhhbbbbbbbbbbbbbbbb</div>
        <button onClick={this.props.closePopup}>close me</button>
        </div>
      </div>
    );
  }
}

class Popup1 extends React.Component {
  render() {
    return (
      <div className='popup' style={{backgroundColor:'#000000',marginLeft:'100px'}}>
        <div className='popup_inner'>
        <div class="modal-content basic-editor-modal">
                <div class="modal-header">
                    <button type="button" onClick={this.props.closePopup} class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add summary</h4> </div>
                <div class="modal-body">
                    <p>Description</p>
                    <textarea></textarea>
                </div>
                <div class="modal-footer">
                    <button type="button" onClick={this.props.closePopup} class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary">Save</button>
                </div>
            </div>
        
        </div>
      </div>
    );
  }
}

class Popup2 extends React.Component {
  render() {
    return (
      <div className='popup' style={{backgroundColor:'#000000',marginLeft:'100px'}}>
        <div className='popup_inner'>
        <div class="modal-content basic-editor-modal">
                <div class="modal-header">
                    <button type="button" onClick={this.props.closePopup} class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Skills</h4> </div>
                <div class="modal-body">
                    <p>Description</p>
                    <textarea></textarea>
                </div>
                <div class="modal-footer">
                    <button type="button" onClick={this.props.closePopup} class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary">Save</button>
                </div>
            </div>
        
        </div>
      </div>
    );
  }
}

class Popup3 extends React.Component {
  render() {
    return (
      <div className='popup' style={{backgroundColor:'#000000',marginLeft:'100px'}}>
        <div className='popup_inner'>
        <div class="modal-content basic-editor-modal">
                <div class="modal-header">
                    <button type="button" onClick={this.props.closePopup} class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Portfolio</h4> </div>
                <div class="modal-body">
                    <p>Description</p>
                    <textarea></textarea>
                </div>
                <div class="modal-footer">
                    <button type="button" onClick={this.props.closePopup} class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary">Save</button>
                </div>
            </div>
        
        </div>
      </div>
    );
  }
}

class Popup4 extends React.Component {
  render() {
    return (
      <div className='popup' style={{backgroundColor:'#000000',marginLeft:'100px'}}>
        <div className='popup_inner'>
        <div class="modal-content basic-editor-modal">
                <div class="modal-header">
                    <button type="button" onClick={this.props.closePopup} class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Test Section</h4> </div>
                <div class="modal-body">
                    <p>Description</p>
                    <textarea></textarea>
                </div>
                <div class="modal-footer">
                    <button type="button" onClick={this.props.closePopup} class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary">Save</button>
                </div>
            </div>
        
        </div>
      </div>
    );
  }
}

class Popup5 extends React.Component {
  render() {
    return (
      <div className='popup' style={{backgroundColor:'#000000',marginLeft:'100px'}}>
        <div className='popup_inner'>
        <div class="modal-content basic-editor-modal">
                <div class="modal-header">
                    <button type="button" onClick={this.props.closePopup} class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Custom Dated Section</h4> </div>
                <div class="modal-body">
                    <p>Description</p>
                    <textarea></textarea>
                </div>
                <div class="modal-footer">
                    <button type="button" onClick={this.props.closePopup} class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary">Save</button>
                </div>
            </div>
        
        </div>
      </div>
    );
  }
}

class Popup6 extends React.Component {
  render() {
    return (
      <div className='popup' style={{backgroundColor:'#000000',marginLeft:'100px'}}>
        <div className='popup_inner'>
        <div class="modal-content basic-editor-modal">
                <div class="modal-header">
                    <button type="button" onClick={this.props.closePopup} class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Charts</h4> </div>
                <div class="modal-body">
                    <p>Description</p>
                    <textarea></textarea>
                </div>
                <div class="modal-footer">
                    <button type="button" onClick={this.props.closePopup} class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary">Save</button>
                </div>
            </div>
        
        </div>
      </div>
    );
  }
}

class Popup7 extends React.Component {
  render() {
    return (
      <div className='popup' style={{backgroundColor:'#000000',marginLeft:'100px'}}>
        <div className='popup_inner'>
        <div class="modal-dialog">
            <div class="modal-content basic-editor-modal">
                <div class="modal-header">
                    <button type="button" onClick={this.props.closePopup} class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">
                    <img src={Userpic} class="img-responsive" alt="" />
                        <p><a href="#">Change Image</a> <br />
                            <a href="#">Reset</a></p>
                    </h4> </div>
                <div class="modal-body">
                    <input type="text" class="form-control" placeholder="Enter Name"></input>
                    <br />
                    <input type="text" class="form-control" placeholder="Enter Email"></input>
                    <br />
                    <input type="text" class="form-control" placeholder="Enter Phone"></input>
                    <br />
                    <input type="text" class="form-control" placeholder="Enter City/state"></input>
                    <br />
                    <input type="text" class="form-control" placeholder="Enter Headline"></input>
                    <br />
                    <input type="text" class="form-control" placeholder="Enter Your Website"></input> </div>
                <div class="modal-footer">
                    <button type="button" onClick={this.props.closePopup} class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
        </div>
        
        
      </div>
    );
  }
}

class Popup8 extends React.Component {
  render() {
    return (
      <div className='popup' style={{backgroundColor:'#000000',marginLeft:'100px'}}>
        <div className='popup_inner'>
        <div class="modal-content basic-editor-modal">
                <div class="modal-header">
                    <button type="button" onClick={this.props.closePopup} class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Work Experience</h4> </div>
                    <div class="modal-body">
                <input type="text" class="form-control" placeholder="Your most recent position here"></input>
                    <br />
                    <input type="text" class="form-control" placeholder="Most recent company here"></input>
                    <br />
                    <div class="modal-body">
                    <p>DESCRIPTION</p>
                      <textarea></textarea>
                    </div>
                    <br />
                    <p>START DATE</p>
                    <input type="text" class="form-control" placeholder="Start Date"></input>
                    <br />
                    <p>END DATE</p>
                    <input type="text" class="form-control" placeholder="End Date"></input>
               
                </div>
                   <div class="modal-footer">
                    <button type="button" onClick={this.props.closePopup} class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary">Save</button>
                </div>
            </div>
        
        </div>
      </div>
    );
  }
}

class Popup9 extends React.Component {
  render() {
    return (
      <div className='popup' style={{backgroundColor:'#000000',marginLeft:'100px'}}>
        <div className='popup_inner'>
        <div class="modal-content basic-editor-modal">
                <div class="modal-header">
                    <button type="button" onClick={this.props.closePopup} class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Work Experience</h4> </div>
                <div class="modal-body">
                <input type="text" class="form-control" placeholder="Your most recent position here"></input>
                    <br />
                    <input type="text" class="form-control" placeholder="Most recent company here"></input>
                    <br />
                    <div class="modal-body">
                    <p>DESCRIPTION</p>
                      <textarea></textarea>
                    </div>
                    <br />
                    <p>START DATE</p>
                    <input type="text" class="form-control" placeholder="Start Date"></input>
                    <br />
                    <p>END DATE</p>
                    <input type="text" class="form-control" placeholder="End Date"></input>
               
                </div>
                <div class="modal-footer">
                    <button type="button" onClick={this.props.closePopup} class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary">Save</button>
                </div>
            </div>
        
        </div>
      </div>
    );
  }
}

class Popup10 extends React.Component {
  render() {
    return (
      <div className='popup' style={{backgroundColor:'#000000',marginLeft:'100px'}}>
        <div className='popup_inner'>
        <div class="modal-content basic-editor-modal">
                <div class="modal-header">
                    <button type="button" onClick={this.props.closePopup} class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Work Experience</h4> </div>
                <div class="modal-body">
                <input type="text" class="form-control" placeholder="Your most recent position here"></input>
                    <br />
                    <input type="text" class="form-control" placeholder="Most recent company here"></input>
                    <br />
                    <div class="modal-body">
                    <p>DESCRIPTION</p>
                      <textarea></textarea>
                    </div>
                    <br />
                    <p>START DATE</p>
                    <input type="text" class="form-control" placeholder="Start Date"></input>
                    <br />
                    <p>END DATE</p>
                    <input type="text" class="form-control" placeholder="End Date"></input>
               
                </div>
                <div class="modal-footer">
                    <button type="button" onClick={this.props.closePopup} class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary">Save</button>
                </div>
            </div>
        
        </div>
      </div>
    );
  }
}

class Popup11 extends React.Component {
  render() {
    return (
      <div className='popup' style={{backgroundColor:'#000000',marginLeft:'100px'}}>
        <div className='popup_inner'>
        <div class="modal-content basic-editor-modal">
                <div class="modal-header">
                    <button type="button" onClick={this.props.closePopup} class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Education</h4> </div>
                <div class="modal-body">
                <input type="text" class="form-control" placeholder="Your most recent position here"></input>
                    <br />
                    <input type="text" class="form-control" placeholder="Most recent company here"></input>
                    <br />
                    <div class="modal-body">
                    <p>DESCRIPTION</p>
                      <textarea></textarea>
                    </div>
                    <br />
                    <p>START DATE</p>
                    <input type="text" class="form-control" placeholder="Start Date"></input>
                    <br />
                    <p>END DATE</p>
                    <input type="text" class="form-control" placeholder="End Date"></input>
               
                </div>
                <div class="modal-footer">
                    <button type="button" onClick={this.props.closePopup} class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary">Save</button>
                </div>
            </div>
        
        </div>
      </div>
    );
  }
}

class Popup12 extends React.Component {
  render() {
    return (
      <div className='popup' style={{backgroundColor:'#000000',marginLeft:'100px'}}>
        <div className='popup_inner'>
        <div class="modal-content basic-editor-modal">
                <div class="modal-header">
                    <button type="button" onClick={this.props.closePopup} class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Education</h4> </div>
                <div class="modal-body">
                <input type="text" class="form-control" placeholder="Your most recent position here"></input>
                    <br />
                    <input type="text" class="form-control" placeholder="Most recent company here"></input>
                    <br />
                    <div class="modal-body">
                    <p>DESCRIPTION</p>
                      <textarea></textarea>
                    </div>
                    <br />
                    <p>START DATE</p>
                    <input type="text" class="form-control" placeholder="Start Date"></input>
                    <br />
                    <p>END DATE</p>
                    <input type="text" class="form-control" placeholder="End Date"></input>
               
                </div>
                <div class="modal-footer">
                    <button type="button" onClick={this.props.closePopup} class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary">Save</button>
                </div>
            </div>
        
        </div>
      </div>
    );
  }
}

class Popup13 extends React.Component {
  render() {
    return (
      <div className='popup' style={{backgroundColor:'#000000',marginLeft:'100px'}}>
        <div className='popup_inner'>
        <div class="modal-content basic-editor-modal">
                <div class="modal-header">
                    <button type="button" onClick={this.props.closePopup} class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Education</h4> </div>
                <div class="modal-body">
                <input type="text" class="form-control" placeholder="Your most recent position here"></input>
                    <br />
                    <input type="text" class="form-control" placeholder="Most recent company here"></input>
                    <br />
                    <div class="modal-body">
                    <p>DESCRIPTION</p>
                      <textarea></textarea>
                    </div>
                    <br />
                    <p>START DATE</p>
                    <input type="text" class="form-control" placeholder="Start Date"></input>
                    <br />
                    <p>END DATE</p>
                    <input type="text" class="form-control" placeholder="End Date"></input>
               
                </div>
                <div class="modal-footer">
                    <button type="button" onClick={this.props.closePopup} class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary">Save</button>
                </div>
            </div>
        
        </div>
      </div>
    );
  }
}


class Templates extends Component {
  
  state = {
    activeItem: "1"
  }
  
 constructor() {
    super();
    this.state = {
      showPopup: false,
      showPopup2: false,
      showPopup3: false,
      showPopup4: false,
      showPopup5: false,
      showPopup6: false,
      showPopup7: false,
      showPopup8: false,
      showPopup9: false,
      showPopup10: false,
      showPopup11: false,
      showPopup12: false,
      showPopup13: false,
      showPopup14: false,
      show:false,
      show1:false,
      show2:false,
      show3:false
    };
  }
  togglePopup() {
    this.setState({
      showPopup2: !this.state.showPopup2
    });
  }
  togglePopup1() {
    this.setState({
      showPopup3: !this.state.showPopup3
    });
  }
  togglePopup2() {
    this.setState({
      showPopup4: !this.state.showPopup4
    });
  }
  togglePopup3() {
    this.setState({
      showPopup5: !this.state.showPopup5
    });
  }
  togglePopup4() {
    this.setState({
      showPopup6: !this.state.showPopup6
    });
  }
  togglePopup5() {
    this.setState({
      showPopup7: !this.state.showPopup7
    });
  }
  togglePopup6() {
    this.setState({
      showPopup8: !this.state.showPopup8
    });
  }
  togglePopup7() {
    this.setState({
      showPopup9: !this.state.showPopup9
    });
  }
  togglePopup8() {
    this.setState({
      showPopup10: !this.state.showPopup10
    });
  }
  togglePopup9() {
    this.setState({
      showPopup11: !this.state.showPopup11
    });
  }
  togglePopup10() {
    this.setState({
      showPopup12: !this.state.showPopup12
    });
  }
  togglePopup11() {
    this.setState({
      showPopup13: !this.state.showPopup13
    });
  }
  togglePopup12() {
    this.setState({
      showPopup14: !this.state.showPopup14
    });
  }


  toggle = tab => () => {
    if (this.state.activeItem !== tab) {
    this.setState({
      activeItem: tab
    });
    }
  }

  //state = { isOpen: false };

  handleShowDialog = () => {
    this.setState({ isOpen: !this.state.isOpen });
    console.log("cliked");
    
  };
/*
  constructor(props) {
    super(props);

    this.state = { isOpen: false };
  }

  toggleModal = () => {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }
  */

  

  render() {
    const imagesPath = Constants.imagespath;
    

    return (

    <div class="site-inner-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-8">
                    <div class="collapse navbar-collapse js-navbar-collapse">
                        <ul class="nav navbar-nav">
                            <li><a href="#">Dashboard</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="left-inner-nav">
                        <ul>
                            <input type="search" class="form-control resume-search"></input>
                            <li>Share</li>
                            <li><FontAwesomeIcon icon={faArrowDown} /> Download</li>
                        </ul>
                    </div>
                </div>
                
            </div>
        </div>
    
    
        <div class="grey-bg basic-editor mg55">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="wrapper center-block">
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                            <div class="panel panel-default">
                                <div class="panel-heading" active role="tab" id="headingOne">
                                    <h4 class="panel-title">
                                    <div>
              
              <div onClick={()=>{this.setState({show:!this.state.show})}}>{ this.state.show? '' : ''} 
              <div class="panel-heading" role="tab" id="headingOne">
                                    <h4 class="panel-title">                        
                                        <a aria-expanded="true" aria-controls="collapseOne">
                                            Add Section
                                        </a></h4></div></div>
                                        {
                  this.state.show? <div><h4>
                    
                    <div id="collapseOne" class="panel-collapse" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel-body">
                    
                                            <div onClick={this.togglePopup.bind(this)} style={{fontColor: 'black', fontSize: '14px', backgroundColor: '#ebebeb', border: '7px solid #fff', padding: '5px', width: '100%', justifyContent: "center", alignItems: "center"}}> <a style={{color: 'black'}} href="#" data-toggle="modal" data-target="#myModal"><FontAwesomeIcon icon={faPlus} /> Add Summery</a> </div>
                                            {this.state.showPopup2 ? 
                                            <Popup1
                                              text='Close Me'
                                              closePopup={this.togglePopup.bind(this)}
                                            />
                                            : null
                                          }   
                                            <div onClick={this.togglePopup1.bind(this)} style={{fontColor: 'black', fontSize: '14px', backgroundColor: '#ebebeb', border: '7px solid #fff', padding: '5px', width: '100%', justifyContent: "center", alignItems: "center"}}> <a style={{color: 'black'}} href="#"><FontAwesomeIcon icon={faPlus} /> Add Skills</a> </div>
                                            {this.state.showPopup3 ? 
                                            <Popup2
                                              text='Close Me'
                                              closePopup={this.togglePopup1.bind(this)}
                                            />
                                            : null
                                          }   
                                            <div onClick={this.togglePopup2.bind(this)} style={{fontColor: 'black', fontSize: '14px', backgroundColor: '#ebebeb', border: '7px solid #fff', padding: '5px', width: '100%', justifyContent: "center", alignItems: "center"}}> <a style={{color: 'black'}} href="#"><FontAwesomeIcon icon={faPlus} /> Add Portfolio</a> </div>
                                            {this.state.showPopup4 ? 
                                            <Popup3
                                              text='Close Me'
                                              closePopup={this.togglePopup2.bind(this)}
                                            />
                                            : null
                                          }   
                                            <div onClick={this.togglePopup3.bind(this)} style={{fontColor: 'black', fontSize: '14px', backgroundColor: '#ebebeb', border: '7px solid #fff', padding: '5px', width: '100%', justifyContent: "center", alignItems: "center"}}> <a style={{color: 'black'}} href="#"><FontAwesomeIcon icon={faPlus} /> Add Text Section</a> </div>
                                            {this.state.showPopup5 ? 
                                            <Popup4
                                              text='Close Me'
                                              closePopup={this.togglePopup3.bind(this)}
                                            />
                                            : null
                                          }   
                                            <div onClick={this.togglePopup4.bind(this)} style={{fontColor: 'black', fontSize: '14px', backgroundColor: '#ebebeb', border: '7px solid #fff', padding: '5px', width: '100%', justifyContent: "center", alignItems: "center"}}> <a style={{color: 'black'}} href="#"><FontAwesomeIcon icon={faPlus} /> Add Custom Dated Section</a> </div>
                                            {this.state.showPopup6 ? 
                                            <Popup5
                                              text='Close Me'
                                              closePopup={this.togglePopup4.bind(this)}
                                            />
                                            : null
                                          }   
                                          <div onClick={this.togglePopup5.bind(this)} style={{fontColor: 'black', fontSize: '14px', backgroundColor: '#ebebeb', border: '7px solid #fff', padding: '5px', width: '100%', justifyContent: "center", alignItems: "center"}}> <a style={{color: 'black'}} href="#"><FontAwesomeIcon icon={faPlus} /> Add Charts</a> </div>
                                            {this.state.showPopup7 ? 
                                            <Popup6
                                              text='Close Me'
                                              closePopup={this.togglePopup5.bind(this)}
                                            />
                                            : null
                                          }   
                                            
                                    </div>
                                    </div>
                    </h4></div> : null
              }                                        
          </div>
                                       
                                    </h4> </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingTwo">
                                <h4 class="panel-title">
                                    <div>
              
              <div onClick={()=>{this.setState({show1:!this.state.show1})}}>{ this.state.show1? '' : ''} 
              <div class="panel-heading" role="tab" id="headingOne">
                                    <h4 class="panel-title">
              <a aria-expanded="true" aria-controls="collapseOne">
                                            Profile
                                        </a></h4></div></div>
                                        {
                  this.state.show1? <div><h4>
                    
                    <div id="collapseTwo">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6"> <img src={Userpic} class="img-responsive" alt="" />
                                <p style={{display: 'inline-block;'}} src={`${imagesPath}/design-1.webp`}> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Jack Sparrow</p>
                            </div>
                            <div class="col-md-6">
                                <br />
                                <br /> <a onClick={this.togglePopup6.bind(this)} style={{fontColor: 'black', fontSize: '14px', border: '0px solid #fff', padding: '5px', width: '100%', justifyContent: "center", alignItems: "center"}} href="#" data-toggle="modal" data-target="#myModal1"></a> </div>
                                {this.state.showPopup8 ? 
                                            <Popup7
                                              text='Close Me'
                                              closePopup={this.togglePopup6.bind(this)}
                                            />
                                            : null
                                          }          
                        </div>
                    </div>
                </div>                
                    </h4></div> : null
              }                                        
          </div>
                                       
                                    </h4> 
                                    </div>
                                
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingThree">
                                    
                                    <h4 class="panel-title">
                                    <div>
              
                                    <div onClick={()=>{this.setState({show2:!this.state.show2})}}>{ this.state.show2? '' : ''} 
                                    <div class="panel-heading" role="tab" id="headingOne">
                                                          <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    Work Experience


                                      <ul class="work-exp" style={{marginBottom:'15px'}}>
                                          <li> <button> <div style={{color: 'black', fontSize: '14px', backgroundColor: '#ebebeb', border: '0px solid #fff', paddingLeft: '5px', paddingRight: '5px', height: 'auto', width: '100%', justifyContent: "center", alignItems: "center"}}> Edit</div></button> </li>
                                          <li> <button> <div style={{color: 'black', fontSize: '14px', backgroundColor: '#ebebeb', border: '0px solid #fff', paddingLeft: '5px', paddingRight: '5px', height: 'auto', width: '100%', justifyContent: "center", alignItems: "center"}}> Add</div></button> </li>


                                      </ul>
                                      </a></h4></div></div>
                                                              {
                                        this.state.show2? <div><h4>
                                          
                                          <div id="collapseOne" class="panel-collapse" role="tabpanel" aria-labelledby="headingOne">
                                          <div class="panel-body">
                                           
                                            <div onClick={this.togglePopup7.bind(this)} style={{fontColor: 'black', fontSize: '14px', backgroundColor: '#ebebeb', border: '7px solid #fff', padding: '5px', width: '100%', justifyContent: "center", alignItems: "center"}}> <a style={{color: 'black'}} href="#"><FontAwesomeIcon icon={faPlus} /> ....</a> 
                                            {this.state.showPopup9 ? 
                                            <Popup8
                                              text='Close Me'
                                              closePopup={this.togglePopup7.bind(this)}
                                            />
                                            : null
                                          } 
                                              </div>
                                            <div onClick={this.togglePopup8.bind(this)} style={{fontColor: 'black', fontSize: '14px', backgroundColor: '#ebebeb', border: '7px solid #fff', padding: '5px', width: '100%', justifyContent: "center", alignItems: "center"}}> <a style={{color: 'black'}} href="#"><FontAwesomeIcon icon={faPlus} /> .... </a> </div>
                                            {this.state.showPopup10 ? 
                                            <Popup9
                                              text='Close Me'
                                              closePopup={this.togglePopup8.bind(this)}
                                            />
                                            : null
                                          } 
                                            <div onClick={this.togglePopup9.bind(this)} style={{fontColor: 'black', fontSize: '14px', backgroundColor: '#ebebeb', border: '7px solid #fff', padding: '5px', width: '100%', justifyContent: "center", alignItems: "center"}}> <a style={{color: 'black'}} href="#"><FontAwesomeIcon icon={faPlus} /> .... </a> </div>
                                            {this.state.showPopup11 ? 
                                            <Popup10
                                              text='Close Me'
                                              closePopup={this.togglePopup9.bind(this)}
                                            />
                                            : null
                                          }   
                                                          </div>
                                                          </div>
                                          </h4></div> : null
                                    }                                        
                                </div>
                                       
                                    </h4>
                                    
                                    </div>
                                
                            </div>
                            
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingFour">
                                <h4 class="panel-title">
                                    <div>
              
                                    <div onClick={()=>{this.setState({show3:!this.state.show3})}}>{ this.state.show3? '' : ''} 
                                    <div class="panel-heading" role="tab" id="headingOne">
                                                          <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    Education


                                      <ul class="work-exp">
                                      <li> <button> <div style={{color: 'black', fontSize: '14px', backgroundColor: '#ebebeb', border: '0px solid #fff', paddingLeft: '5px', paddingRight: '5px', height: 'auto', width: '100%', justifyContent: "center", alignItems: "center"}}> Edit</div></button> </li>
                                      <li> <button> <div style={{color: 'black', fontSize: '14px', backgroundColor: '#ebebeb', border: '0px solid #fff', paddingLeft: '5px', paddingRight: '5px', height: 'auto', width: '100%', justifyContent: "center", alignItems: "center"}}> Add</div></button> </li>


                                      </ul>
                                      </a></h4></div></div>
                                                              {
                                        this.state.show3? <div><h4>
                                          
                                          <div id="collapseOne" class="panel-collapse" role="tabpanel" aria-labelledby="headingOne">
                                          <div class="panel-body">
                                           
                                            <div onClick={this.togglePopup10.bind(this)} style={{fontColor: 'black', fontSize: '14px', backgroundColor: '#ebebeb', border: '7px solid #fff', padding: '5px', width: '100%', justifyContent: "center", alignItems: "center"}}> <a style={{color: 'black'}} href="#"><FontAwesomeIcon icon={faPlus} /> ....</a> </div>
                                            {this.state.showPopup12 ? 
                                            <Popup11
                                              text='Close Me'
                                              closePopup={this.togglePopup10.bind(this)}
                                            />
                                            : null
                                          }   


                                            <div onClick={this.togglePopup11.bind(this)} style={{fontColor: 'black', fontSize: '14px', backgroundColor: '#ebebeb', border: '7px solid #fff', padding: '5px', width: '100%', justifyContent: "center", alignItems: "center"}}> <a style={{color: 'black'}} href="#"><FontAwesomeIcon icon={faPlus} /> .... </a> </div>
                                            {this.state.showPopup13 ? 
                                            <Popup12
                                              text='Close Me'
                                              closePopup={this.togglePopup11.bind(this)}
                                            />
                                            : null
                                          }   
                                            <div onClick={this.togglePopup12.bind(this)} style={{fontColor: 'black', fontSize: '14px', backgroundColor: '#ebebeb', border: '7px solid #fff', padding: '5px', width: '100%', justifyContent: "center", alignItems: "center"}}> <a style={{color: 'black'}} href="#"><FontAwesomeIcon icon={faPlus} /> .... </a> </div>
                                            {this.state.showPopup14 ? 
                                            <Popup13
                                              text='Close Me'
                                              closePopup={this.togglePopup12.bind(this)}
                                            />
                                            : null
                                          }  
                                                          </div>
                                                          </div>
                                          </h4></div> : null
                                    }                                        
                                </div>
                                       
                                    </h4>
                                     </div>
                                
                            </div>

                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content basic-editor-modal">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add summary</h4> </div>
                <div class="modal-body">
                    <p>Description</p>
                    <textarea></textarea>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </div>
    <div id="myModal1" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content basic-editor-modal">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">
                        <img src="images/user-pic.jpg" class="img-responsive img-rounded" />
                        <p><a href="#">Change Image</a> <br />
                            <a href="#">Reset</a></p>
                    </h4> </div>
                <div class="modal-body">
                    <input type="text" class="form-control" placeholder="Enter Name"></input>
                    <br />
                    <input type="text" class="form-control" placeholder="Enter Email"></input>
                    <br />
                    <input type="text" class="form-control" placeholder="Enter Phone"></input>
                    <br />
                    <input type="text" class="form-control" placeholder="Enter City/state"></input>
                    <br />
                    <input type="text" class="form-control" placeholder="Enter Headline"></input>
                    <br />
                    <input type="text" class="form-control" placeholder="Enter Your Website"></input> </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
        </div>
        </div>
    
      );
  }
}

export default Templates;