import React,{Component} from 'react';
import Chart from "chart.js";

class GraphChart extends Component {
    constructor(props) {
        super(props);
        this.myRef = React.createRef();

      }

      componentDidMount() {
        const ctx = this.ctx;

        new Chart(ctx, {
          type: "doughnut",
          data: {
            labels: ["Red", "Blue", "Yellow"],
            datasets: [
              {
                label: "# of Likes",
                data: [12, 19, 3],
                backgroundColor: [
                  "rgba(255, 99, 132, 0.2)",
                  "rgba(54, 162, 235, 0.2)",
                  "rgba(255, 206, 86, 0.2)"
                ]
              },
              {
                label: "# of Likes",
                data: [-12, -19, -3],
                backgroundColor: [
                  "rgba(255, 99, 132, 0.2)",
                  "rgba(54, 162, 235, 0.2)",
                  "rgba(255, 206, 86, 0.2)"
                ]
              }
            ]
          }
        });
      }

      render() {

        return (
            <div>
                <canvas width='800' height='300' ref={ctx => (this.ctx = ctx)}/>
            </div>
        )
    }
}
export default GraphChart;