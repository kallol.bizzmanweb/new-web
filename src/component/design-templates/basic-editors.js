import React , { Component} from 'react';

import Constants from '../../common/Constants';

import { MDBContainer, MDBTabPane, MDBTabContent, MDBNav, MDBNavItem, MDBNavLink } from "mdbreact";

import { MDBBtn, MDBModal, MDBModalBody, MDBModalHeader, MDBModalFooter } from 'mdbreact';

import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';

import './styles/style.css';

import axios from 'axios';

import {Button, Collapse} from 'react-bootstrap';

import { dom } from '@fortawesome/fontawesome-svg-core';

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus } from "@fortawesome/free-solid-svg-icons";
import { faArrowDown } from "@fortawesome/free-solid-svg-icons";
import { faCog } from "@fortawesome/free-solid-svg-icons";
import { faFile } from "@fortawesome/free-solid-svg-icons";
import { faChevronRight } from "@fortawesome/free-solid-svg-icons";

import Userpic from './images/user-pic.jpg';

class Popup extends React.Component {
  render() {
    return (
      <div className='popup' style={{backgroundColor:'#000000',marginLeft:'100px'}}>
        <div className='popup_inner'>
        <h1>{this.props.text}</h1>
        <div style={{backgroundColor:'#000000'}}>hhhhhhhhhhhhhbbbbbbbbbbbbbbbb</div>
        <div style={{backgroundColor:'#000000'}}>hhhhhhhhhhhhhbbbbbbbbbbbbbbbb</div>
        <div style={{backgroundColor:'#000000'}}>hhhhhhhhhhhhhbbbbbbbbbbbbbbbb</div>
        <div style={{backgroundColor:'#000000'}}>hhhhhhhhhhhhhbbbbbbbbbbbbbbbb</div>
        <div style={{backgroundColor:'#000000'}}>hhhhhhhhhhhhhbbbbbbbbbbbbbbbb</div>
        <button onClick={this.props.closePopup}>close me</button>
        </div>
      </div>
    );
  }
}

class Popup1 extends Component {

  constructor() {
    super()


  this.onChangeDescription = this.onChangeDescription.bind(this);
  this.onChangeEmail = this.onChangeEmail.bind(this);
  this.onSubmit = this.onSubmit.bind(this);

    this.state = {
    name: '',
    email: '',
    description: ""
  }
  
  } 

    onChangeEmail(e) {
      this.setState({ email: e.target.value })
    }

    onChangeDescription(e) {
          this.setState({ description: e.target.value })
    }

    onSubmit(e) {
        e.preventDefault();

        alert("Add Summary");

        const userObject = {
          email: this.state.email,
          description: this.state.description
        };

        const summaryjson = JSON.stringify({
          email: this.state.email,
          description: this.state.description
        });

        console.log("userObject",summaryjson);

        //alert(summaryjson);

        axios.post('http://162.213.248.35:3003/api/add-text', summaryjson)
                .then((res) => {

              //alert(res.data);

              alert("Summary Successfully Added");  

              console.log(res.data);

              window.location.href="/basiceditor"; 

        }).catch((error) => {
                    console.log(error)
        });
              
      ////this.setState({ name: '', email: '' })
  }

  render() {
    return (
      <div className='popup' style={{backgroundColor:'#000000',marginLeft:'100px'}}>
        <div className='popup_inner'>
        <div class="modal-dialog">
        <div class="modal-content basic-editor-modal">
        <form onSubmit={this.onSubmit}>
                <div class="modal-header">
                    <button type="button" onClick={this.props.closePopup} class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add summary</h4> </div>
                <div class="modal-body">
                <input type="text" onChange={this.onChangeEmail} class="form-control" placeholder="Enter Email"></input>
                <br />
                </div>    
                <div class="modal-body">
                    <p>Description</p>
                    <textarea onChange={this.onChangeDescription} />
                </div>
                <div class="modal-footer">
                    <button type="button" onClick={this.props.closePopup} class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <input type="submit" value="Save" className="btn btn-primary" />
                </div>
                </form>
            </div>
        </div>
        </div>
      </div>
    );
  }
}

class Popup2 extends React.Component {

    constructor() {
    super()

    this.onChangeDescription = this.onChangeDescription.bind(this);
    this.onChangeEmail = this.onChangeEmail.bind(this);
    this.onChangeSkills = this.onChangeSkills.bind(this);
    this.onChangeStrength = this.onChangeStrength.bind(this);
    this.onSubmit = this.onSubmit.bind(this);

      this.state = {
      name: '',
      email: '',
      description: "",
      skills: '',
      strength: ""
    }
    
    } 

    onChangeEmail(e) {
      this.setState({ email: e.target.value })
    }

    onChangeDescription(e) {
          this.setState({ description: e.target.value })
    }

    onChangeSkills(e) {
      this.setState({ skills: e.target.value })
    }

    onChangeStrength(e) {
          this.setState({ strength: e.target.value })
    }

    onSubmit(e) {
        e.preventDefault();

        alert("Add Skills");

        const userObject = {
          email: this.state.email,
          description: this.state.description,
          skills: this.state.skills,
          strength: this.state.strength
        };

        const summaryjson = JSON.stringify({
          email: this.state.email,
          description: this.state.description,
          skills: this.state.skills,
          strength: this.state.strength
        });

        console.log("userObject",summaryjson);

        //alert(summaryjson);

        axios.post('http://162.213.248.35:3003/api/add-skills', summaryjson)
                .then((res) => {

                    //alert(res.data);

                    alert("Skills Successfully Added");  

                    console.log(res.data);

                    window.location.href="/basiceditor"; 

                }).catch((error) => {
                    console.log(error)
                });
              
      ////this.setState({ name: '', email: '' })
  }
  
  render() {
    return (
      <div className='popup' style={{backgroundColor:'#000000',marginLeft:'100px'}}>
        <div className='popup_inner'>
        <div class="modal-dialog">
        <div class="modal-content basic-editor-modal">
        <form onSubmit={this.onSubmit}>
                <div class="modal-header">
                    <button type="button" onClick={this.props.closePopup} class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Skills</h4> </div>
                    <div class="modal-body">
                    <input type="text" onChange={this.onChangeEmail} class="form-control" placeholder="Enter Email"></input>

                </div>
                <div class="modal-body">
                    <p>Description</p>
                    <textarea onChange={this.onChangeDescription} />
                </div>    
                <div class="modal-body">
                    <p>Skills</p>
                    <textarea onChange={this.onChangeSkills} />
                </div>
                <div class="modal-body">
                    <p>Strength</p>
                    <textarea onChange={this.onChangeStrength} />
                </div>
                <div class="modal-footer">
                    <button type="button" onClick={this.props.closePopup} class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <input type="submit" value="Save" className="btn btn-primary" />
                </div>
                </form>
            </div>
        </div>
        </div>
      </div>
    );
  }
}

class Popup3 extends React.Component {

  
  render() {
    return (
      <div className='popup' style={{backgroundColor:'#000000',marginLeft:'100px'}}>
        <div className='popup_inner'>
        <div class="modal-dialog"> 
        <div class="modal-content basic-editor-modal">
                <div class="modal-header">
                    <button type="button" onClick={this.props.closePopup} class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Portfolio</h4> </div>
                <div class="modal-body">
                    <p>Description</p>
                    <textarea></textarea>
                </div>
                <div class="modal-footer">
                    <button type="button" onClick={this.props.closePopup} class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
        </div>
      </div>
    );
  }
}

class Popup4 extends React.Component {

        constructor() {
        super()

        this.onChangeDescription = this.onChangeDescription.bind(this);
        this.onChangeEmail = this.onChangeEmail.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

          this.state = {
          name: '',
          email: '',
          description: ""
        }
        
        } 

        onChangeEmail(e) {
            this.setState({ email: e.target.value })
        }

        onChangeDescription(e) {
                this.setState({ description: e.target.value })
        }

        onSubmit(e) {
              e.preventDefault();

              alert("Add Text");

              const userObject = {
                email: this.state.email,
                description: this.state.description
              };

              const summaryjson = JSON.stringify({
                email: this.state.email,
                description: this.state.description
              });

              console.log("userObject",summaryjson);

              //alert(summaryjson);

              axios.post('http://162.213.248.35:3003/api/add-text', summaryjson)
                      .then((res) => {

                          //alert(res.data);

                          alert("Text Successfully Added");  

                          console.log(res.data);

                          window.location.href="/basiceditor"; 

                      }).catch((error) => {
                          console.log(error)
                      });
                    
            ////this.setState({ name: '', email: '' })
        }

  render() {
    return (
      <div className='popup' style={{backgroundColor:'#000000',marginLeft:'100px'}}>
        <div className='popup_inner'>
        <div class="modal-dialog">
        <div class="modal-content basic-editor-modal">
                <div class="modal-header">
                    <button type="button" onClick={this.props.closePopup} class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Test Section</h4> </div>                   
                <div class="modal-body">
                <input type="text" onChange={this.onChangeEmail} class="form-control" placeholder="Enter Email"></input>
                <br />
                    <p>Description</p>
                    <textarea onChange={this.onChangeDescription} />
                </div>
                <div class="modal-footer">
                    <button type="button" onClick={this.props.closePopup} class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
        </div>
      </div>
    );
  }
}

class Popup5 extends React.Component {

  constructor() {
    super()

    this.onChangeEmail = this.onChangeEmail.bind(this);
    this.onChangeExperience = this.onChangeExperience.bind(this);
    this.onChangeSchool = this.onChangeSchool.bind(this);
    this.onChangeDescription = this.onChangeDescription.bind(this);
    this.onChangeWebsite = this.onChangeWebsite.bind(this);
    this.onChangeStartdate = this.onChangeStartdate.bind(this);
    this.onChangeEnddate = this.onChangeEnddate.bind(this);
    this.onSubmit = this.onSubmit.bind(this);

    this.state = {
    email: '',
    experience: '',
    school: '',
    description: '',
    headline: '',
    website: '',
    startdate: '',
    enddate: ''
    }
    
    } 

    onChangeEmail(e) {
      this.setState({ email: e.target.value })
    }

    onChangeExperience(e) {
      this.setState({ experience: e.target.value })
    }

    onChangeSchool(e) {
      this.setState({ school: e.target.value })
    }

    onChangeWebsite(e) {
      this.setState({ headline: e.target.value })
    }

    onChangeDescription(e) {
          this.setState({ description: e.target.value })
    }

    onChangeStartdate(e) {
      this.setState({ startdate: e.target.value })
    }

    onChangeEnddate(e) {
      this.setState({ enddate: e.target.value })
    }

    onSubmit(e) {
        e.preventDefault();

        alert("Add Custom Data ");

        const customObject = {
          email: this.state.email,
          experience: this.state.experience,
          school: this.state.school,
          description: this.state.description,
          headline: this.state.headline,
          website: this.state.website,
          startdate: this.state.startdate,
          enddate: this.state.enddate
        };

        const customjson = JSON.stringify({
          email: this.state.email,
          experience: this.state.experience,
          school: this.state.school,
          description: this.state.description,
          headline: this.state.headline,
          website: this.state.website,
          startdate: this.state.startdate,
          enddate: this.state.enddate
        });

        console.log("customObject",customObject);

        //alert(customjson);

        axios.post('http://162.213.248.35:3003/api/update-profile', customjson)
                .then((res) => {

                    //alert(res.data);

                    alert("Custom Data Successfully Added");  

                    console.log(res.data);

                    window.location.href="/basiceditor"; 

                }).catch((error) => {
                    console.log(error)
                });
              
      ////this.setState({ name: '', email: '' })
    }

  render() {
    return (
      <div className='popup' style={{backgroundColor:'#000000',marginLeft:'100px'}}>
        <div className='popup_inner'>
        <div class="modal-content basic-editor-modal">
                <div className='popup' style={{backgroundColor:'#000000',marginLeft:'100px'}}>
                <div className='popup_inner'>
                <div class="modal-dialog">
                <div class="modal-content basic-editor-modal">
                <form onSubmit={this.onSubmit}>
                    <div class="modal-header">
                    <button type="button" onClick={this.props.closePopup} class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title">Add Custom Dated Section</h4> </div>
                      <div class="modal-body"> 
                      <input type="text" onChange={this.onChangeEmail} class="form-control" placeholder="Enter Email"></input>
                      <br />
                      <input type="text" onChange={this.onChangeExperience} class="form-control" placeholder="Your additional experience here"></input>
                      <br />
                      <input type="text" onChange={this.onChangeSchool} class="form-control" placeholder="School here"></input>
                      <br />
                      <input type="text" onChange={this.onChangeWebsite} class="form-control" placeholder="Website"></input>
                      <br />
                      <p>Description</p>
                      <textarea onChange={this.onChangeDescription} />
                      <br />
                      <br />
                      <p>START DATE</p>
                      <input type="text" onChange={this.onChangeStartdate} class="form-control" placeholder="Start Date"></input>
                      <br />
                      <p>END DATE</p>
                      <input type="text" onChange={this.onChangeEnddate} class="form-control" placeholder="End Date"></input>
                    <div class="modal-footer">
                      <button type="button" onClick={this.props.closePopup} class="btn btn-default" data-dismiss="modal">Cancel</button>
                      <input type="submit" value="Save" className="btn btn-primary" />
                  </div>
                    </div>
                    </form>
                </div>
                </div>
                </div>
                </div>


            </div>
        
        </div>
      </div>
    );
  }
}

class Popup6 extends React.Component {

  constructor() {
    super()

    this.onChangeDescription = this.onChangeDescription.bind(this);
    this.onChangeEmail = this.onChangeEmail.bind(this);
    this.onChangeType = this.onChangeType.bind(this);
    this.onSubmit = this.onSubmit.bind(this);

    this.state = {
      name: '',
      email: '',
      description: "",
      chattype: ""
    }
    
    } 

    onChangeEmail(e) {
        this.setState({ email: e.target.value })
    }

    onChangeDescription(e) {
            this.setState({ description: e.target.value })
    }

    onChangeType(e) {
      this.setState({ chattype: e.target.value })
    }

    onSubmit(e) {
          e.preventDefault();

          alert("Add Charts");

          const chartObject = {
            email: this.state.email,
            description: this.state.description,
            chart_type: this.state.chattype
          };

          const chartjson = JSON.stringify({
            email: this.state.email,
            description: this.state.description,
            chart_type: this.state.chattype
          });

          console.log("chartObject",chartObject);

          //alert(chartjson);

          axios.post('http://162.213.248.35:3003/api/add-charts', chartjson)
                  .then((res) => {

                      //alert(res.data);

                      alert("Charts Successfully Added");  

                      console.log(res.data);

                      window.location.href="/basiceditor"; 

                  }).catch((error) => {
                      console.log(error)
                  });
                
        ////this.setState({ name: '', email: '' })
    }


  render() {
    return (
      <div className='popup' style={{backgroundColor:'#000000',marginLeft:'100px'}}>
        <div className='popup_inner'>
        <div class="modal-dialog">
        <div class="modal-content basic-editor-modal">
                <div class="modal-header">
                    <button type="button" onClick={this.props.closePopup} class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Charts</h4> </div>
                    
                <div class="modal-body">
                <form onSubmit={this.onSubmit}>                   
                    <input type="text" onChange={this.onChangeEmail} class="form-control" placeholder="Enter Email"></input>
                    <br />
                    <select onChange={this.onChangeType} name="chattype">
                      <option value="charttype">Chart Type</option>
                      <option value="line">line</option>
                      <option value="bar">bar</option>
                      <option value="area">area</option>
                      <option value="pie">pie</option>
                    </select>
                    <br />
                    <br />
                    <p>Description</p>
                    <textarea onChange={this.onChangeDescription} />
                    <br />
                    
                <div class="modal-footer">
                    <button type="button" onClick={this.props.closePopup} class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <input type="submit" value="Save" className="btn btn-primary" />
                </div>
                </form>
            </div>
            </div>
        </div>
        </div>
      </div>
    );
  }
}

class Popup7 extends React.Component {

  constructor() {
    super()

  this.onChangeName = this.onChangeName.bind(this);
  this.onChangeEmail = this.onChangeEmail.bind(this);
  this.onChangePhone = this.onChangePhone.bind(this);
  this.onChangeCity = this.onChangeCity.bind(this);
  this.onChangeHeadline = this.onChangeHeadline.bind(this);
  this.onChangeWebsite = this.onChangeWebsite.bind(this);
  this.onSubmit = this.onSubmit.bind(this);

    this.state = {
    name: '',
    email: '',
    phone: '',
    city: '',
    description: '',
    headline: '',
    website: ''
  }
  
  } 

    onChangeName(e) {
      this.setState({ name: e.target.value })
    }

    onChangeEmail(e) {
      this.setState({ email: e.target.value })
    }

    onChangePhone(e) {
      this.setState({ phone: e.target.value })
    }

    onChangeCity(e) {
      this.setState({ city: e.target.value })
    }

    onChangeHeadline(e) {
      this.setState({ headline: e.target.value })
    }

    onChangeWebsite(e) {
          this.setState({ website: e.target.value })
    }

    onSubmit(e) {
        e.preventDefault();

        alert("Add Profile");

        const profileObject = {
          name: this.state.name,
          email: this.state.email,
          phone: this.state.phone,
          city: this.state.city,
          headline: this.state.headline,
          website: this.state.website
        };

        const profilejson = JSON.stringify({
          name: this.state.name,
          email: this.state.email,
          phone: this.state.phone,
          city: this.state.city,
          headline: this.state.headline,
          website: this.state.website
        });

        console.log("userObject",profileObject);

        //alert(profilejson);

        axios.post('http://162.213.248.35:3003/api/update-profile', profilejson)
                .then((res) => {

                    //alert(res.data);

                    alert("Profile Successfully Added");  

                    console.log(res.data);

                    window.location.href="/basiceditor"; 

                }).catch((error) => {
                    console.log(error)
                });
              
      ////this.setState({ name: '', email: '' })
  }



  render() {
    return (
      <div className='popup' style={{backgroundColor:'#000000',marginLeft:'100px'}}>
        <div className='popup_inner'>
        <div class="modal-dialog">
            <div class="modal-content basic-editor-modal">
                <div class="modal-header">
                    <button type="button" onClick={this.props.closePopup} class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">
                    <img src={Userpic} class="img-responsive" alt="" />
                        <p><a href="#">Change Image</a> <br />
                            <a href="#">Reset</a></p>
                    </h4> </div>
                <div class="modal-body">
                <form onSubmit={this.onSubmit}>
                    <input type="text" onChange={this.onChangeName} class="form-control" placeholder="Enter Name"></input>
                    <br />
                    <input type="text" onChange={this.onChangeEmail} class="form-control" placeholder="Enter Email"></input>
                    <br />
                    <input type="text" onChange={this.onChangePhone} class="form-control" placeholder="Enter Phone"></input>
                    <br />
                    <input type="text" onChange={this.onChangeCity} class="form-control" placeholder="Enter City/state"></input>
                    <br />
                    <input type="text" onChange={this.onChangeHeadline} class="form-control" placeholder="Enter Headline"></input>
                    <br />
                    <input type="text" onChange={this.onChangeWebsite} class="form-control" placeholder="Enter Your Website"></input> 
                <div class="modal-footer">
                    <button type="button" onClick={this.props.closePopup} class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <input type="submit" value="Save" className="btn btn-primary" />
                </div>
                </form>
                </div>
            </div>
        </div>
        </div>
      </div>
      
    );
  }
}

class Popup8 extends React.Component {

  constructor() {
    super()

      this.onChangeEmail = this.onChangeEmail.bind(this);
      this.onChangePosition = this.onChangePosition.bind(this);
      this.onChangeCompany = this.onChangeCompany.bind(this);
      this.onChangeDescription = this.onChangeDescription.bind(this);
      this.onChangeStartdate = this.onChangeStartdate.bind(this);
      this.onChangeEnddate = this.onChangeEnddate.bind(this);
      
      this.onSubmit = this.onSubmit.bind(this);

      this.state = {
      position: '',
      organization: '',
      description: '',
      startdate: '',
      enddate: '',
      email: ''
    }
    
    } 

    onChangeEmail(e) {
      this.setState({ email: e.target.value })
    }

    onChangePosition(e) {
      this.setState({ organization: e.target.value })
    }

    onChangeCompany(e) {
      this.setState({ company: e.target.value })
    }

    onChangeDescription(e) {
      this.setState({ description: e.target.value })
    }

    onChangeStartdate(e) {
      this.setState({ startdate: e.target.value })
    }

    onChangeEnddate(e) {
      this.setState({ enddate: e.target.value })
    }

    

    onSubmit(e) {
        e.preventDefault();

        alert("Add Work Experience");

        const experienceObject = {
          position: this.state.position,
          organization: this.state.organization,
          description: this.state.description,
          startdate: this.state.startdate,
          enddate: this.state.enddate,
          email: this.state.email
        };

        const experiencejson = JSON.stringify({
          position: this.state.position,
          organization: this.state.organization,
          description: this.state.description,
          startdate: this.state.startdate,
          enddate: this.state.enddate,
          email: this.state.email
        });

        console.log("experienceObject",experienceObject);

        //alert(experiencejson);

        axios.post('http://162.213.248.35:3003/api/add-work-experience', experiencejson)
                .then((res) => {

                    //alert(res.data);

                    alert("Work Experience Successfully Added");  

                    console.log(res.data);

                    window.location.href="/basiceditor"; 

                }).catch((error) => {
                    console.log(error)
                });
              
                ////this.setState({ name: '', email: '' })
  }


  render() {
    return (
      <div className='popup' style={{backgroundColor:'#000000',marginLeft:'100px'}}>
          <div className='popup_inner'>
          <div class="modal-dialog">
          <div class="modal-content basic-editor-modal">
          <form onSubmit={this.onSubmit}>
                  <div class="modal-header">
                  <button type="button" onClick={this.props.closePopup} class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Work Experience</h4> </div>
                    <div class="modal-body"> 
                    <input type="text" onChange={this.onChangeEmail} class="form-control" placeholder="Enter Email"></input>
                    <br />
                    <input type="text" onChange={this.onChangePosition} class="form-control" placeholder="Your most recent position here"></input>
                    <br />
                    <input type="text" onChange={this.onChangeCompany} class="form-control" placeholder="Most recent company here"></input>
                    <br />
                    <p>Description</p>
                    <textarea onChange={this.onChangeDescription} />
                    <br />
                    <br />
                    <p>START DATE</p>
                    <input type="text" onChange={this.onChangeStartdate} class="form-control" placeholder="Start Date"></input>
                    <br />
                    <p>END DATE</p>
                    <input type="text" onChange={this.onChangeEnddate} class="form-control" placeholder="End Date"></input>
                   <div class="modal-footer">
                    <button type="button" onClick={this.props.closePopup} class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <input type="submit" value="Save" className="btn btn-primary" />
                </div>
              </div>
              </form>
          </div>
          </div>
          </div>
          </div>

    );
  }
}

class Popup9 extends React.Component {

  constructor() {
    super()

      this.onChangeEmail = this.onChangeEmail.bind(this);
      this.onChangePosition = this.onChangePosition.bind(this);
      this.onChangeCompany = this.onChangeCompany.bind(this);
      this.onChangeDescription = this.onChangeDescription.bind(this);
      this.onChangeStartdate = this.onChangeStartdate.bind(this);
      this.onChangeEnddate = this.onChangeEnddate.bind(this);
      
      this.onSubmit = this.onSubmit.bind(this);

      this.state = {
      position: '',
      organization: '',
      description: '',
      startdate: '',
      enddate: '',
      email: ''
    }
    
    } 

    onChangeEmail(e) {
      this.setState({ email: e.target.value })
    }

    onChangePosition(e) {
      this.setState({ position: e.target.value })
    }

    onChangeCompany(e) {
      this.setState({ organization: e.target.value })
    }

    onChangeDescription(e) {
      this.setState({ description: e.target.value })
    }

    onChangeStartdate(e) {
      this.setState({ startdate: e.target.value })
    }

    onChangeEnddate(e) {
      this.setState({ enddate: e.target.value })
    }

    

    onSubmit(e) {
        e.preventDefault();

        alert("Add Work Experience");

        const experienceObject = {
          position: this.state.position,
          organization: this.state.company,
          description: this.state.description,
          startdate: this.state.startdate,
          enddate: this.state.enddate,
          email: this.state.email
        };

        const experiencejson = JSON.stringify({
          position: this.state.position,
          organization: this.state.company,
          description: this.state.description,
          startdate: this.state.startdate,
          enddate: this.state.enddate,
          email: this.state.email
        });

        console.log("experienceObject",experienceObject);

        //alert(experiencejson);

        axios.post('http://162.213.248.35:3003/api/add-work-experience', experiencejson)
                .then((res) => {

                    //alert(res.data);

                    alert("Work Experience Successfully Added");  

                    console.log(res.data);

                    window.location.href="/basiceditor"; 

                }).catch((error) => {
                    console.log(error)
                });
              
                ////this.setState({ name: '', email: '' })
  }

  render() {
    return (
      <div className='popup' style={{backgroundColor:'#000000',marginLeft:'100px'}}>
      <div className='popup_inner'>
      <div class="modal-dialog">
      <div class="modal-content basic-editor-modal">
      <form onSubmit={this.onSubmit}>
              <div class="modal-header">
              <button type="button" onClick={this.props.closePopup} class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Work Experience</h4> </div>
                <div class="modal-body"> 
                <input type="text" onChange={this.onChangeEmail} class="form-control" placeholder="Enter Email"></input>
                <br />
                <input type="text" onChange={this.onChangePosition} class="form-control" placeholder="Your most recent position here"></input>
                <br />
                <input type="text" onChange={this.onChangeCompany} class="form-control" placeholder="Most recent company here"></input>
                <br />
                <p>Description</p>
                <textarea onChange={this.onChangeDescription} />
                <br />
                <br />
                <p>START DATE</p>
                <input type="text" onChange={this.onChangeStartdate} class="form-control" placeholder="Start Date"></input>
                <br />
                <p>END DATE</p>
                <input type="text" onChange={this.onChangeEnddate} class="form-control" placeholder="End Date"></input>
               <div class="modal-footer">
                <button type="button" onClick={this.props.closePopup} class="btn btn-default" data-dismiss="modal">Cancel</button>
                <input type="submit" value="Save" className="btn btn-primary" />
            </div>
          </div>
          </form>
      </div>
      </div>
      </div>
      </div>

    );
  }
}

class Popup10 extends React.Component {

  constructor() {
    super()

      this.onChangeEmail = this.onChangeEmail.bind(this);
      this.onChangePosition = this.onChangePosition.bind(this);
      this.onChangeCompany = this.onChangeCompany.bind(this);
      this.onChangeDescription = this.onChangeDescription.bind(this);
      this.onChangeStartdate = this.onChangeStartdate.bind(this);
      this.onChangeEnddate = this.onChangeEnddate.bind(this);
      
      this.onSubmit = this.onSubmit.bind(this);

      this.state = {
      position: '',
      organization: '',
      description: '',
      startdate: '',
      enddate: '',
      email: ''
    }
    
    } 

    onChangeEmail(e) {
      this.setState({ email: e.target.value })
    }

    onChangePosition(e) {
      this.setState({ position: e.target.value })
    }

    onChangeCompany(e) {
      this.setState({ organization: e.target.value })
    }

    onChangeDescription(e) {
      this.setState({ description: e.target.value })
    }

    onChangeStartdate(e) {
      this.setState({ startdate: e.target.value })
    }

    onChangeEnddate(e) {
      this.setState({ enddate: e.target.value })
    }

    

    onSubmit(e) {
        e.preventDefault();

        alert("Add Work Experience");

        const experienceObject = {
          position: this.state.position,
          organization: this.state.organization,
          description: this.state.description,
          startdate: this.state.startdate,
          enddate: this.state.enddate,
          email: this.state.email
        };

        const experiencejson = JSON.stringify({
          position: this.state.position,
          organization: this.state.organization,
          description: this.state.description,
          startdate: this.state.startdate,
          enddate: this.state.enddate,
          email: this.state.email
        });

        console.log("experienceObject",experienceObject);

        //alert(experiencejson);

        axios.post('http://162.213.248.35:3003/api/add-work-experience', experiencejson)
                .then((res) => {

                    //alert(res.data);

                    alert("Work Experience Successfully Added");  

                    console.log(res.data);

                    window.location.href="/basiceditor"; 

                }).catch((error) => {
                    console.log(error)
                });
              
                ////this.setState({ name: '', email: '' })
  }

  render() {
    return (
      <div className='popup' style={{backgroundColor:'#000000',marginLeft:'100px'}}>
      <div className='popup_inner'>
      <div class="modal-dialog">
      <div class="modal-content basic-editor-modal">
      <form onSubmit={this.onSubmit}>
              <div class="modal-header">
              <button type="button" onClick={this.props.closePopup} class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Work Experience</h4> </div>
                <div class="modal-body"> 
                <input type="text" onChange={this.onChangeEmail} class="form-control" placeholder="Enter Email"></input>
                <br />
                <input type="text" onChange={this.onChangePosition} class="form-control" placeholder="Your most recent position here"></input>
                <br />
                <input type="text" onChange={this.onChangeCompany} class="form-control" placeholder="Most recent company here"></input>
                <br />
                <p>Description</p>
                <textarea onChange={this.onChangeDescription} />
                <br />
                <br />
                <p>START DATE</p>
                <input type="text" onChange={this.onChangeStartdate} class="form-control" placeholder="Start Date"></input>
                <br />
                <p>END DATE</p>
                <input type="text" onChange={this.onChangeEnddate} class="form-control" placeholder="End Date"></input>
               <div class="modal-footer">
                <button type="button" onClick={this.props.closePopup} class="btn btn-default" data-dismiss="modal">Cancel</button>
                <input type="submit" value="Save" className="btn btn-primary" />
            </div>
          </div>
          </form>
      </div>
      </div>
      </div>
      </div>
    );
  }
}

class Popup11 extends React.Component {

  constructor() {
    super()

      this.onChangeEmail = this.onChangeEmail.bind(this);
      this.onChangeDegree = this.onChangeDegree.bind(this);
      this.onChangeOrganisation = this.onChangeOrganisation.bind(this);
      this.onChangeDescription = this.onChangeDescription.bind(this);
      this.onChangeStartdate = this.onChangeStartdate.bind(this);
      this.onChangeEnddate = this.onChangeEnddate.bind(this);
      
      this.onSubmit = this.onSubmit.bind(this);

      this.state = {
      position: '',
      organization: '',
      description: '',
      startdate: '',
      enddate: '',
      email: ''
    }
    
    } 

    onChangeEmail(e) {
      this.setState({ email: e.target.value })
    }

    onChangeDegree(e) {
      this.setState({ position: e.target.value })
    }

    onChangeOrganisation(e) {
      this.setState({ organization: e.target.value })
    }

    onChangeDescription(e) {
      this.setState({ description: e.target.value })
    }

    onChangeStartdate(e) {
      this.setState({ startdate: e.target.value })
    }

    onChangeEnddate(e) {
      this.setState({ enddate: e.target.value })
    }

    onSubmit(e) {
        e.preventDefault();

        alert("Add Education");

        const experienceObject = {
          position: this.state.position,
          organization: this.state.organization,
          description: this.state.description,
          startdate: this.state.startdate,
          enddate: this.state.enddate,
          email: this.state.email
        };

        const experiencejson = JSON.stringify({
          position: this.state.position,
          organization: this.state.organization,
          description: this.state.description,
          startdate: this.state.startdate,
          enddate: this.state.enddate,
          email: this.state.email
        });

        console.log("experienceObject",experienceObject);

        //alert(experiencejson);

        axios.post('http://162.213.248.35:3003/api/education', experiencejson)
                .then((res) => {

                    //alert(res.data);

                    alert("Education Successfully Added");  

                    console.log(res.data);

                    window.location.href="/basiceditor"; 

                }).catch((error) => {
                    console.log(error)
                });
              
                ////this.setState({ name: '', email: '' })
  }

  render() {
    return (
      <div className='popup' style={{backgroundColor:'#000000',marginLeft:'100px'}}>
        <div className='popup_inner'>
        <div class="modal-dialog">
        <div class="modal-content basic-editor-modal">
          <form onSubmit={this.onSubmit}>
                <div class="modal-header">
                    <button type="button" onClick={this.props.closePopup} class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Education</h4> </div>
                <div class="modal-body">
                <input type="text" onChange={this.onChangeEmail} class="form-control" placeholder="Enter Email"></input>
                <br />
                <input type="text" onChange={this.onChangeDegree} class="form-control" placeholder="Your most recent degree here"></input>
                    <br />
                    <input type="text" onChange={this.onChangeOrganisation} class="form-control" placeholder="Most recent company here"></input>
                    <br />
                    <p>DESCRIPTION</p>
                    <textarea onChange={this.onChangeDescription} />
                    <br />
                    <br />
                    <p>START DATE</p>
                    <input type="text" onChange={this.onChangeStartdate} class="form-control" placeholder="Start Date"></input>
                    <br />
                    <p>END DATE</p>
                    <input type="text" onChange={this.onChangeEnddate} class="form-control" placeholder="End Date"></input>
               
                </div>
                <div class="modal-footer">
                    <button type="button" onClick={this.props.closePopup} class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <input type="submit" value="Save" className="btn btn-primary" />
                </div>
            </form>
            </div>
            </div>
        </div>
      </div>
    );
  }
}

class Popup12 extends React.Component {

  constructor() {
    super()

      this.onChangeEmail = this.onChangeEmail.bind(this);
      this.onChangeDegree = this.onChangeDegree.bind(this);
      this.onChangeOrganisation = this.onChangeOrganisation.bind(this);
      this.onChangeDescription = this.onChangeDescription.bind(this);
      this.onChangeStartdate = this.onChangeStartdate.bind(this);
      this.onChangeEnddate = this.onChangeEnddate.bind(this);
      
      this.onSubmit = this.onSubmit.bind(this);

      this.state = {
      position: '',
      organization: '',
      description: '',
      startdate: '',
      enddate: '',
      email: ''
    }
    
    } 

    onChangeEmail(e) {
      this.setState({ email: e.target.value })
    }

    onChangeDegree(e) {
      this.setState({ position: e.target.value })
    }

    onChangeOrganisation(e) {
      this.setState({ organization: e.target.value })
    }

    onChangeDescription(e) {
      this.setState({ description: e.target.value })
    }

    onChangeStartdate(e) {
      this.setState({ startdate: e.target.value })
    }

    onChangeEnddate(e) {
      this.setState({ enddate: e.target.value })
    }

    onSubmit(e) {
        e.preventDefault();

        alert("Add Education");

        const experienceObject = {
          position: this.state.position,
          organization: this.state.organization,
          description: this.state.description,
          startdate: this.state.startdate,
          enddate: this.state.enddate,
          email: this.state.email
        };

        const experiencejson = JSON.stringify({
          position: this.state.position,
          organization: this.state.organization,
          description: this.state.description,
          startdate: this.state.startdate,
          enddate: this.state.enddate,
          email: this.state.email
        });

        console.log("experienceObject",experienceObject);

        //alert(experiencejson);

        axios.post('http://162.213.248.35:3003/api/education', experiencejson)
                .then((res) => {

                    //alert(res.data);

                    alert("Education Successfully Added");  

                    console.log(res.data);

                    window.location.href="/basiceditor"; 

                }).catch((error) => {
                    console.log(error)
                });
              
                ////this.setState({ name: '', email: '' })
  }

  render() {
    return (
      <div className='popup' style={{backgroundColor:'#000000',marginLeft:'100px'}}>
        <div className='popup_inner'>
        <div class="modal-dialog">
        <div class="modal-content basic-editor-modal">
          <form onSubmit={this.onSubmit}>
                <div class="modal-header">
                    <button type="button" onClick={this.props.closePopup} class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Education</h4> </div>
                <div class="modal-body">
                <input type="text" onChange={this.onChangeEmail} class="form-control" placeholder="Enter Email"></input>
                <br />
                <input type="text" onChange={this.onChangeDegree} class="form-control" placeholder="Your most recent degree here"></input>
                    <br />
                    <input type="text" onChange={this.onChangeOrganisation} class="form-control" placeholder="Most recent company here"></input>
                    <br />
                    <p>DESCRIPTION</p>
                    <textarea onChange={this.onChangeDescription} />
                    <br />
                    <br />
                    <p>START DATE</p>
                    <input type="text" onChange={this.onChangeStartdate} class="form-control" placeholder="Start Date"></input>
                    <br />
                    <p>END DATE</p>
                    <input type="text" onChange={this.onChangeEnddate} class="form-control" placeholder="End Date"></input>
               
                </div>
                <div class="modal-footer">
                    <button type="button" onClick={this.props.closePopup} class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <input type="submit" value="Save" className="btn btn-primary" />
                </div>
            </form>
            </div>
            </div>
        </div>
      </div>
    );
  }
}

class Popup13 extends React.Component {

  constructor() {
    super()

      this.onChangeEmail = this.onChangeEmail.bind(this);
      this.onChangeDegree = this.onChangeDegree.bind(this);
      this.onChangeOrganisation = this.onChangeOrganisation.bind(this);
      this.onChangeDescription = this.onChangeDescription.bind(this);
      this.onChangeStartdate = this.onChangeStartdate.bind(this);
      this.onChangeEnddate = this.onChangeEnddate.bind(this);
      
      this.onSubmit = this.onSubmit.bind(this);

      this.state = {
      position: '',
      organization: '',
      description: '',
      startdate: '',
      enddate: '',
      email: ''
    }
    
    } 

    onChangeEmail(e) {
      this.setState({ email: e.target.value })
    }

    onChangeDegree(e) {
      this.setState({ position: e.target.value })
    }

    onChangeOrganisation(e) {
      this.setState({ organization: e.target.value })
    }

    onChangeDescription(e) {
      this.setState({ description: e.target.value })
    }

    onChangeStartdate(e) {
      this.setState({ startdate: e.target.value })
    }

    onChangeEnddate(e) {
      this.setState({ enddate: e.target.value })
    }

    onSubmit(e) {
        e.preventDefault();

        alert("Add Education");

        const experienceObject = {
          position: this.state.position,
          organization: this.state.organization,
          description: this.state.description,
          startdate: this.state.startdate,
          enddate: this.state.enddate,
          email: this.state.email
        };

        const experiencejson = JSON.stringify({
          position: this.state.position,
          organization: this.state.organization,
          description: this.state.description,
          startdate: this.state.startdate,
          enddate: this.state.enddate,
          email: this.state.email
        });

        console.log("experienceObject",experienceObject);

        //alert(experiencejson);

        axios.post('http://162.213.248.35:3003/api/education', experiencejson)
                .then((res) => {

                    //alert(res.data);

                    alert("Education Successfully Added");  

                    console.log(res.data);

                    window.location.href="/basiceditor"; 

                }).catch((error) => {
                    console.log(error)
                });
              
                ////this.setState({ name: '', email: '' })
  }


  render() {
    return (
      <div className='popup' style={{backgroundColor:'#000000',marginLeft:'100px'}}>
        <div className='popup_inner'>
        <div class="modal-dialog">
        <div class="modal-content basic-editor-modal">
          <form onSubmit={this.onSubmit}>
                <div class="modal-header">
                    <button type="button" onClick={this.props.closePopup} class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Education</h4> </div>
                <div class="modal-body">
                <input type="text" onChange={this.onChangeEmail} class="form-control" placeholder="Enter Email"></input>
                <br />
                <input type="text" onChange={this.onChangeDegree} class="form-control" placeholder="Your most recent degree here"></input>
                    <br />
                    <input type="text" onChange={this.onChangeOrganisation} class="form-control" placeholder="Most recent company here"></input>
                    <br />
                    <p>DESCRIPTION</p>
                    <textarea onChange={this.onChangeDescription} />
                    <br />
                    <br />
                    <p>START DATE</p>
                    <input type="text" onChange={this.onChangeStartdate} class="form-control" placeholder="Start Date"></input>
                    <br />
                    <p>END DATE</p>
                    <input type="text" onChange={this.onChangeEnddate} class="form-control" placeholder="End Date"></input>
               
                </div>
                <div class="modal-footer">
                    <button type="button" onClick={this.props.closePopup} class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <input type="submit" value="Save" className="btn btn-primary" />
                </div>
            </form>
            </div>
            </div>
        </div>
      </div>
    );
  }
}


class Templates extends Component {


  constructor(props) {
    super(props)

    //this.onChangeUserName = this.onChangeUserName.bind(this);
    //this.onChangeUserEmail = this.onChangeUserEmail.bind(this);
    this.onChangeDescription = this.onChangeDescription.bind(this);
    this.onChangeEmail = this.onChangeEmail.bind(this);

    this.state = {
    showPopup: false,
    showPopup2: false,
    showPopup3: false,
    showPopup4: false,
    showPopup5: false,
    showPopup6: false,
    showPopup7: false,
    showPopup8: false,
    showPopup9: false,
    showPopup10: false,
    showPopup11: false,
    showPopup12: false,
    showPopup13: false,
    showPopup14: false,
    show:false,
    show1:false,
    show2:false,
    show3:false,
    name: '',
    email: '',
    description: ""
    };
  
    //this.onSubmit = this.onSubmit.bind(this);

    /* this.state = {
        name: '',
        email: '',
        description: ""
    } */
    } 

      onChangeEmail(e) {
        this.setState({ email: e.target.value })
      }

      onChangeDescription(e) {
            this.setState({ description: e.target.value })
      }

      onChangeUserName(e) {
          this.setState({ name: e.target.value })
      }

      onChangeUserEmail(e) {
          this.setState({ email: e.target.value })
      }

      onSubmit(e) {
          e.preventDefault();

          alert("Hello");

          const userObject = {
            email: this.state.email,
            description: this.state.description
          };

          const summaryjson = JSON.stringify({
            email: this.state.email,
            description: this.state.description
          });

          console.log("userObject",summaryjson);

          alert(summaryjson);

          axios.post('http://162.213.248.35:3003/api/add-text', summaryjson)
                  .then((res) => {
                      console.log(res.data)
                  }).catch((error) => {
                      console.log(error)
                  });
                
        ////this.setState({ name: '', email: '' })
    }

  
        state = {
          activeItem: "1"
        }
        
        togglePopup() {
          this.setState({
            showPopup2: !this.state.showPopup2
          });
        }
        togglePopup1() {
          this.setState({
            showPopup3: !this.state.showPopup3
          });
        }
        togglePopup2() {
          this.setState({
            showPopup4: !this.state.showPopup4
          });
        }
        togglePopup3() {
          this.setState({
            showPopup5: !this.state.showPopup5
          });
        }
        togglePopup4() {
          this.setState({
            showPopup6: !this.state.showPopup6
          });
        }
        togglePopup5() {
          this.setState({
            showPopup7: !this.state.showPopup7
          });
        }
        togglePopup6() {
          this.setState({
            showPopup8: !this.state.showPopup8
          });
        }
        togglePopup7() {
          this.setState({
            showPopup9: !this.state.showPopup9
          });
        }
        togglePopup8() {
          this.setState({
            showPopup10: !this.state.showPopup10
          });
        }
        togglePopup9() {
          this.setState({
            showPopup11: !this.state.showPopup11
          });
        }
        togglePopup10() {
          this.setState({
            showPopup12: !this.state.showPopup12
          });
        }
        togglePopup11() {
          this.setState({
            showPopup13: !this.state.showPopup13
          });
        }
        togglePopup12() {
          this.setState({
            showPopup14: !this.state.showPopup14
          });
        }


  toggle = tab => () => {
    if (this.state.activeItem !== tab) {
    this.setState({
      activeItem: tab
    });
    }
  }

  //state = { isOpen: false };

  handleShowDialog = () => {
    this.setState({ isOpen: !this.state.isOpen });
    console.log("cliked");
    
  };
  
  /*
  constructor(props) {
    super(props);

    this.state = { isOpen: false };
  }

  toggleModal = () => {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }
  */

  

  render() {
    const imagesPath = Constants.imagespath;
    

    return (

    <div class="site-inner-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-8">
                    <div class="collapse navbar-collapse js-navbar-collapse">
                        <ul class="nav navbar-nav">
                        <li><a href="/cvdashboard">Dashboard</a></li>
                            <li><a href="/basiceditor">Basic Editor</a></li>
                            <li><a href="/visualseditor">Visual Editor</a></li>
                            <li><a href="/visualeditortemplate">Visual Editor Template</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="left-inner-nav">
                        <ul>
                            <input type="search" class="form-control resume-search"></input>
                            <li>Share</li>
                            <li><FontAwesomeIcon icon={faArrowDown} /> Download</li>
                        </ul>
                    </div>
                </div>
                
            </div>
        </div>
    
    
        <div class="grey-bg basic-editor mg55">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="wrapper center-block">
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                            <div class="panel panel-default">
                                <div class="panel-heading" active role="tab" id="headingOne">
                                    <h4 class="panel-title">
                                    <div>
              
              <div onClick={()=>{this.setState({show:!this.state.show})}}>{ this.state.show? '' : ''} 
              <div class="panel-heading" role="tab" id="headingOne">
                                    <h4 class="panel-title">                        
                                        <a aria-expanded="true" aria-controls="collapseOne">
                                            Add Section
                                        </a></h4></div></div>
                                        {
                  this.state.show? <div><h4>
                    
                    <div id="collapseOne" class="panel-collapse" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel-body">
                    
                                            <div onClick={this.togglePopup.bind(this)} style={{fontColor: 'black', fontSize: '14px', backgroundColor: '#ebebeb', border: '7px solid #fff', padding: '5px', width: '100%', justifyContent: "center", alignItems: "center"}}> <a style={{color: 'black'}} href="#" data-toggle="modal" data-target="#myModal"><FontAwesomeIcon icon={faPlus} /> Add Summery</a> </div>
                                            {this.state.showPopup2 ? 
                                            <Popup1
                                              text='Close Me'
                                              closePopup={this.togglePopup.bind(this)}
                                            />
                                            : null
                                          }   
                                            <div onClick={this.togglePopup1.bind(this)} style={{fontColor: 'black', fontSize: '14px', backgroundColor: '#ebebeb', border: '7px solid #fff', padding: '5px', width: '100%', justifyContent: "center", alignItems: "center"}}> <a style={{color: 'black'}} href="#"><FontAwesomeIcon icon={faPlus} /> Add Skills</a> </div>
                                            {this.state.showPopup3 ? 
                                            <Popup2
                                              text='Close Me'
                                              closePopup={this.togglePopup1.bind(this)}
                                            />
                                            : null
                                          }   
                                            <div onClick={this.togglePopup2.bind(this)} style={{fontColor: 'black', fontSize: '14px', backgroundColor: '#ebebeb', border: '7px solid #fff', padding: '5px', width: '100%', justifyContent: "center", alignItems: "center"}}> <a style={{color: 'black'}} href="#"><FontAwesomeIcon icon={faPlus} /> Add Portfolio</a> </div>
                                            {this.state.showPopup4 ? 
                                            <Popup3
                                              text='Close Me'
                                              closePopup={this.togglePopup2.bind(this)}
                                            />
                                            : null
                                          }   
                                            <div onClick={this.togglePopup3.bind(this)} style={{fontColor: 'black', fontSize: '14px', backgroundColor: '#ebebeb', border: '7px solid #fff', padding: '5px', width: '100%', justifyContent: "center", alignItems: "center"}}> <a style={{color: 'black'}} href="#"><FontAwesomeIcon icon={faPlus} /> Add Text Section</a> </div>
                                            {this.state.showPopup5 ? 
                                            <Popup4
                                              text='Close Me'
                                              closePopup={this.togglePopup3.bind(this)}
                                            />
                                            : null
                                          }   
                                            <div onClick={this.togglePopup4.bind(this)} style={{fontColor: 'black', fontSize: '14px', backgroundColor: '#ebebeb', border: '7px solid #fff', padding: '5px', width: '100%', justifyContent: "center", alignItems: "center"}}> <a style={{color: 'black'}} href="#"><FontAwesomeIcon icon={faPlus} /> Add Custom Dated Section</a> </div>
                                            {this.state.showPopup6 ? 
                                            <Popup5
                                              text='Close Me'
                                              closePopup={this.togglePopup4.bind(this)}
                                            />
                                            : null
                                          }   
                                          <div onClick={this.togglePopup5.bind(this)} style={{fontColor: 'black', fontSize: '14px', backgroundColor: '#ebebeb', border: '7px solid #fff', padding: '5px', width: '100%', justifyContent: "center", alignItems: "center"}}> <a style={{color: 'black'}} href="#"><FontAwesomeIcon icon={faPlus} /> Add Charts</a> </div>
                                            {this.state.showPopup7 ? 
                                            <Popup6
                                              text='Close Me'
                                              closePopup={this.togglePopup5.bind(this)}
                                            />
                                            : null
                                          }   
                                            
                                    </div>
                                    </div>
                    </h4></div> : null
              }                                        
          </div>
                                       
                                    </h4> </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingTwo">
                                <h4 class="panel-title">
                                    <div>
              
              <div onClick={()=>{this.setState({show1:!this.state.show1})}}>{ this.state.show1? '' : ''} 
              <div class="panel-heading" role="tab" id="headingOne">
                                    <h4 class="panel-title">
              <a aria-expanded="true" aria-controls="collapseOne">
                                            Profile
                                        </a></h4></div></div>
                                        {
                  this.state.show1? <div><h4>
                    
                    <div id="collapseTwo">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6"> <img src={Userpic} class="img-responsive" alt="" />
                                <p style={{display: 'inline-block;'}} src={`${imagesPath}/design-1.webp`}> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Jack Sparrow</p>
                            </div>
                            <div class="col-md-6">
                                <br />
                                <br /> <a onClick={this.togglePopup6.bind(this)} style={{fontColor: 'black', fontSize: '14px', border: '0px solid #fff', padding: '5px', width: '100%', justifyContent: "center", alignItems: "center"}} href="#" data-toggle="modal" data-target="#myModal1"></a> </div>
                                {this.state.showPopup8 ? 
                                            <Popup7
                                              text='Close Me'
                                              closePopup={this.togglePopup6.bind(this)}
                                            />
                                            : null
                                          }          
                        </div>
                    </div>
                </div>                
                    </h4></div> : null
              }                                        
          </div>
                                       
                                    </h4> 
                                    </div>
                                
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingThree">
                                    
                                    <h4 class="panel-title">
                                    <div>
              
                                    <div onClick={()=>{this.setState({show2:!this.state.show2})}}>{ this.state.show2? '' : ''} 
                                    <div class="panel-heading" role="tab" id="headingOne">
                                                          <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    Work Experience


                                      
                                      </a></h4></div></div>
                                                              {
                                        this.state.show2? <div><h4>
                                          
                                          <div id="collapseOne" class="panel-collapse" role="tabpanel" aria-labelledby="headingOne">
                                          <div class="panel-body">
                                           
                                            <div onClick={this.togglePopup7.bind(this)} style={{fontColor: 'black', fontSize: '14px', backgroundColor: '#ebebeb', border: '7px solid #fff', padding: '5px', width: '100%', justifyContent: "center", alignItems: "center"}}> <a style={{color: 'black'}} href="#"><FontAwesomeIcon icon={faPlus} /> ....</a> </div>
                                            {this.state.showPopup9 ? 
                                            <Popup8
                                              text='Close Me'
                                              closePopup={this.togglePopup7.bind(this)}
                                            />
                                            : null
                                          } 
                                            <div onClick={this.togglePopup8.bind(this)} style={{fontColor: 'black', fontSize: '14px', backgroundColor: '#ebebeb', border: '7px solid #fff', padding: '5px', width: '100%', justifyContent: "center", alignItems: "center"}}> <a style={{color: 'black'}} href="#"><FontAwesomeIcon icon={faPlus} /> .... </a> </div>
                                            {this.state.showPopup10 ? 
                                            <Popup9
                                              text='Close Me'
                                              closePopup={this.togglePopup8.bind(this)}
                                            />
                                            : null
                                          } 
                                            <div onClick={this.togglePopup9.bind(this)} style={{fontColor: 'black', fontSize: '14px', backgroundColor: '#ebebeb', border: '7px solid #fff', padding: '5px', width: '100%', justifyContent: "center", alignItems: "center"}}> <a style={{color: 'black'}} href="#"><FontAwesomeIcon icon={faPlus} /> .... </a> </div>
                                            {this.state.showPopup11 ? 
                                            <Popup10
                                              text='Close Me'
                                              closePopup={this.togglePopup9.bind(this)}
                                            />
                                            : null
                                          }   
                                          </div>
                                          </div>
                                          </h4></div> : null
                                    }                                        
                                </div>
                                       
                                    </h4>
                                    
                                    </div>
                                
                            </div>
                            
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingFour">
                                <h4 class="panel-title">
                                    <div>
              
                                    <div onClick={()=>{this.setState({show3:!this.state.show3})}}>{ this.state.show3? '' : ''} 
                                    <div class="panel-heading" role="tab" id="headingOne">
                                                          <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    Education


                                      
                                      </a></h4></div></div>
                                                              {
                                        this.state.show3? <div><h4>
                                          
                                          <div id="collapseOne" class="panel-collapse" role="tabpanel" aria-labelledby="headingOne">
                                          <div class="panel-body">
                                           
                                            <div onClick={this.togglePopup10.bind(this)} style={{fontColor: 'black', fontSize: '14px', backgroundColor: '#ebebeb', border: '7px solid #fff', padding: '5px', width: '100%', justifyContent: "center", alignItems: "center"}}> <a style={{color: 'black'}} href="#"><FontAwesomeIcon icon={faPlus} /> ....</a> </div>
                                            {this.state.showPopup12 ? 
                                            <Popup11
                                              text='Close Me'
                                              closePopup={this.togglePopup10.bind(this)}
                                            />
                                            : null
                                          }   


                                            <div onClick={this.togglePopup11.bind(this)} style={{fontColor: 'black', fontSize: '14px', backgroundColor: '#ebebeb', border: '7px solid #fff', padding: '5px', width: '100%', justifyContent: "center", alignItems: "center"}}> <a style={{color: 'black'}} href="#"><FontAwesomeIcon icon={faPlus} /> .... </a> </div>
                                            {this.state.showPopup13 ? 
                                            <Popup12
                                              text='Close Me'
                                              closePopup={this.togglePopup11.bind(this)}
                                            />
                                            : null
                                          }   
                                            <div onClick={this.togglePopup12.bind(this)} style={{fontColor: 'black', fontSize: '14px', backgroundColor: '#ebebeb', border: '7px solid #fff', padding: '5px', width: '100%', justifyContent: "center", alignItems: "center"}}> <a style={{color: 'black'}} href="#"><FontAwesomeIcon icon={faPlus} /> .... </a> </div>
                                            {this.state.showPopup14 ? 
                                            <Popup13
                                              text='Close Me'
                                              closePopup={this.togglePopup12.bind(this)}
                                            />
                                            : null
                                          }  
                                                          </div>
                                                          </div>
                                          </h4></div> : null
                                    }                                        
                                </div>
                                       
                                    </h4>
                                     </div>
                                
                            </div>

                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content basic-editor-modal">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add summary</h4> </div>
                <div class="modal-body">
                    <p>Description</p>
                    <textarea></textarea>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </div>
    <div id="myModal1" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content basic-editor-modal">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">
                        <img src="images/user-pic.jpg" class="img-responsive img-rounded" />
                        <p><a href="#">Change Image</a> <br />
                            <a href="#">Reset</a></p>
                    </h4> </div>
                <div class="modal-body">
                    <input type="text" class="form-control" placeholder="Enter Name"></input>
                    <br />
                    <input type="text" class="form-control" placeholder="Enter Email"></input>
                    <br />
                    <input type="text" class="form-control" placeholder="Enter Phone"></input>
                    <br />
                    <input type="text" class="form-control" placeholder="Enter City/state"></input>
                    <br />
                    <input type="text" class="form-control" placeholder="Enter Headline"></input>
                    <br />
                    <input type="text" class="form-control" placeholder="Enter Your Website"></input> </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
        </div>
        </div>
    
      );
  }
}

export default Templates;