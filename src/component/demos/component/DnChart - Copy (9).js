import React, {Component} from 'react';
import Chart from "chart.js";

///import {Pie, Doughnut, Chart} from 'react-chartjs-2';

import CopyToClipboard from 'react-copy-to-clipboard';

import ClipboardItem from 'react-copy-to-clipboard';

import ReactDOM from "react-dom";
import { ContextMenu, MenuItem, ContextMenuTrigger } from "react-contextmenu";

import Draggable from 'react-draggable';

import { DraggableEventHandler, default as DraggableRoot } from "react-draggable";
import { Enable, Resizable, ResizeDirection } from "re-resizable";


class GraphChart extends Component {
    constructor(props) {
        super(props);
        this.myRef = React.createRef();
      }

      handleClickss(e, data) {
        alert(data.foo);

        const img =  data.foo;

        alert(img);

        const imgBlob =  img.blob();

        alert(imgBlob);


        try {
            navigator.clipboard.write([
              new ClipboardItem({
                  'image/png': imgBlob, // change image type accordingly
              })
            ]);
          } catch (error) {
              console.error(error);

              alert(error);
          }

      }

      handleClick(e, element) {

      alert(element)    
      alert(element)  

      //alert(element.navigator.clipboard.write);

      /*
      alert(element.getSelection());
      
      alert(element.getSelection.toBase64Image());  

      const selection = window.getSelection();
      const range = document.createRange();
      const img = element ;

      alert(img);

      alert(selection);

      */

      // Preserve alternate text
      //const altText = img.alt;
      //img.setAttribute('alt', img.canvas);

     /* range.selectNodeContents(element);
      selection.removeAllRanges();
      selection.addRange(range);
      */

      try {
        // Security exception may be thrown by some browsers.
        return document.execCommand('copy');
      } catch (ex) {
        console.warn('Copy to clipboard failed.', ex);

        alert('error');

        return false;
      } finally {
        //img.setAttribute('alt', altText);
      }

    }

      componentDidMount() {
        const ctx = this.ctx;
        new Chart(ctx, {
          type: "doughnut",
          data: {
            labels: ["Red", "Blue", "Yellow"],
            datasets: [
              {
                label: "# of Likes",
                data: [12, 19, 3],
                backgroundColor: [
                  "rgb(255,0,0)",
                  "rgb(0,0,255)",
                  "rgb(128,0,0)"
                ]
              },
              {
                label: "# of Likes",
                data: [-12, -19, -3],
                backgroundColor: [
                  "rgb(255,0,0)",
                  "rgb(0,0,255)",
                  "rgb(128,0,0)"
                ]
              }
            ]
          }
        });
      }
      render() {
        return (
          <div>
            <div mRef={ctx => (this.ctx = ctx)}>
                <canvas width='800' height='300' ref={ctx => (this.ctx = ctx)}/>
             </div>
             
             <div>
             <Draggable
        handle=".handle"
        defaultPosition={{x: 0, y: 0}}
        position={null}
        grid={[125, 125]}
        scale={1}
        onStart={this.handleStart}
        onDrag={this.handleDrag}
        onStop={this.handleStop}>
        <Resizable
          ref={this.refResizable}
          defaultSize={20}
        >
        <div>
          <div className="handle"> <canvas width='800' height='300' marginTop="-600px" ref={ctx => (this.ctx = ctx)}/></div>
          <div>This readme is really dragging on...</div>
        </div>
        </Resizable>
    </Draggable>


    <CopyToClipboard mRef={ctx => (this.ctx = ctx)} onCopy={this.onCopy}>
          <button>Copy</button>
    </CopyToClipboard>

            </div>
            <div>
      {/* NOTICE: id must be unique between EVERY <ContextMenuTrigger> and <ContextMenu> pair */}
      {/* NOTICE: inside the pair, <ContextMenuTrigger> and <ContextMenu> must have the same id */}
 
      <ContextMenuTrigger id="same_unique_identifier">
        <div className="well">
        
        </div>
      </ContextMenuTrigger>
 
      <ContextMenu id="same_unique_identifier">
        <MenuItem data={{foo: this.ctx}} onClick={this.handleClick}>
        <CopyToClipboard text={"text ok"} onCopy={this.onCopy}>
          <button>Copy</button>
        </CopyToClipboard>
        </MenuItem>
        <MenuItem data={{foo: 'Save'}} onClick={this.handleClick}>
        <CopyToClipboard mRef={ctx => (this.ctx = ctx)} onCopy={navigator.clipboard.write}>
          <button>Copy</button>
        </CopyToClipboard>
        </MenuItem>
        <MenuItem divider />
        <MenuItem data={{foo: 'Copy Link'}} onClick={this.handleClick}>
        Copy Link Address
        </MenuItem>
      </ContextMenu>
 
    </div>
            </div>
        )
    }
}
export default GraphChart;


