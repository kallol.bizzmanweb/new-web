import React, { useState, useEffect } from 'react';
import { EditorState, ContentState, convertToRaw } from 'draft-js';
import htmlToDraft from 'html-to-draftjs';
import draftToHtml from 'draftjs-to-html';
import { Editor } from 'react-draft-wysiwyg-johgeocoder';
import 'react-draft-wysiwyg-johgeocoder/dist/react-draft-wysiwyg.css';

const App = () => {

  const [editorState, setEditorState] = useState(null)

  useEffect(() => {
    const html = '<p>-- -- <br><strong>Lunes Test</strong>  |  Sales Executive<br>+1 (888) 888-8888</p><img src="https://s3.amazonaws.com/exceedbot-webchat/monday.gif" alt="undefined" style="float:left;height: auto;width: auto"/><p></p>';
    const contentBlock = htmlToDraft(html);
    if (contentBlock) {
      const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks);
      const newEditorState = EditorState.createWithContent(contentState);
      setEditorState(newEditorState)
    }
  }, []) //run once on initial render
  
  useEffect(() => {
    if(editorState){
      var html = draftToHtml(convertToRaw(editorState.getCurrentContent()))
      console.log(html)
    }
  }) //Run every state change

  var onEditorStateChange = (newEditorState) => {
    setEditorState(newEditorState)
  }

  return <>
  <div style={{width: '600px', margin: '0 auto', border:'1px solid black'}}>
    <Editor
        editorState={editorState}
        hasHtmlEditorOption={true}
        toolbarClassName="toolbarClassName"
        wrapperClassName="wrapperClassName"
        editorClassName="editorClassName"
        onEditorStateChange={(newEditorState) => onEditorStateChange(newEditorState)}
      />
  </div>
  </>
}

export default App;