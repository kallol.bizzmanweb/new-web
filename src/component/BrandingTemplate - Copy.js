import React,{Component} from 'react';
import brandingTemp1 from '../images/brandingTemp1.png';
import brandingTemp2 from '../images/brandingTemp2.png';
import brandingTemp3 from '../images/brandingTemp3.png';

import brandingTemp4 from './images/image1.jpeg';
import brandingTemp5 from './images/image2.jpeg';
import brandingTemp6 from './images/image3.jpeg';

//import CanvasApp from '../css/CanvasApp.css';

class BrandingTemplate extends Component
{


  render(){


    return(

            <section className="branding-template">
        <div className="container">
            <h2 className="template-header">
              Branding Template
          </h2>
            <center><span className="underline"></span></center>
            <div className = "row">
            <div className="owl-carousel owl-theme">
            <div class="owl-stage-outer">
              <div className="col-md-4">
                <div className="owl-item active" style={{"width": "380px"}}> 
                <div>
                <img src={brandingTemp4} alt= "branding template 1" style={{"width": "100%" , "height": "400px","object-fit": "contain"}}/> 
                </div>
                </div>
                </div>
                 <div className="col-md-4">
                <div className="owl-item active" style={{"width": "380px"}}> 
                <div>
               <img src={brandingTemp5} alt="branding template 3" className="img-responsive"/> 
               </div>
               </div>
                </div>
                 <div className="col-md-4">
               <div className="owl-item active" style={{"width": "380px"}}> 
                <div>
              <img src={brandingTemp6} alt="branding template 2" style={{"width": "100%" , "height": "400px" ,"object-fit": "contain"}}/> </div>
              </div>
              </div>
              </div>
              <div className = "row">
            </div>
            <div className="owl-load"> <a href="/BrandingTemplate">LOAD MORE</a> </div>
            </div>
            </div>

        </div>
    </section>


    	);

  }


}

export default BrandingTemplate;