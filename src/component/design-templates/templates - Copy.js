import React , { Component} from 'react';

import Constants from '../../common/Constants';

//import CanvasApp from '../../css/CanvasApp.css';

import { MDBContainer, MDBTabPane, MDBTabContent } from "mdbreact";
import { MDBNav, MDBNavItem, MDBNavLink } from "mdbreact";

class Templates extends Component {
    state = {
    activeItem: "1"
  }
  
  toggle = tab => () => {
    if (this.state.activeItem !== tab) {
    this.setState({
      activeItem: tab
    });
    }
  }

    
  render() {
    const imagesPath = Constants.imagespath;

    const isMobile = window.innerWidth <= 500;

    return (
      <div className="container-fluid">

    { isMobile ?  
     (<div class="col-md-2"><ul class="nav navbar-nav">
     <li><a href="#">Home</a></li>
     <li class="dropdown mega-dropdown">
       <a href="#" class="dropdown-toggle" data-toggle="dropdown">Templates &nbsp; <span class="glyphicon glyphicon-chevron-down pull-right"></span></a>

       <ul class="dropdown-menu mega-dropdown-menu row">
         <li class="col-sm-3">
           <ul>
               <li><img src="images/education-templates@2x-1.23e5d190.jpg" class="img-responsive"></img></li>
               <li class="dropdown-header">Social Media</li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
            
           </ul>
         </li>

          <li class="col-sm-3">
           <ul>
               <li><img src="images/education-templates@2x-1.23e5d190.jpg" class="img-responsive"></img></li>
               <li class="dropdown-header">Personal</li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
            
           </ul>
         </li>
           <li class="col-sm-3">
           <ul>
               <li><img src="images/education-templates@2x-1.23e5d190.jpg" class="img-responsive"></img></li>
               <li class="dropdown-header">Business</li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
            
           </ul>
         </li>
          <li class="col-sm-3">
           <ul>
               <li><img src="images/education-templates@2x-1.23e5d190.jpg" class="img-responsive"></img></li>
               <li class="dropdown-header">Education</li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
            
           </ul>
         </li>
               <li class="col-sm-3">
           <ul>
               <li><img src="images/education-templates@2x-1.23e5d190.jpg" class="img-responsive"></img></li>
               <li class="dropdown-header">Social Media</li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
            
           </ul>
         </li>
               <li class="col-sm-3">
           <ul>
               <li><img src="images/education-templates@2x-1.23e5d190.jpg" class="img-responsive"></img></li>
               <li class="dropdown-header">Social Media</li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
            
           </ul>
         </li>
       </ul>
         
     </li>
     <li class="dropdown mega-dropdown">
       <a href="#" class="dropdown-toggle" data-toggle="dropdown">Discover &nbsp; <span class="glyphicon glyphicon-chevron-down pull-right"></span></a>

       <ul class="dropdown-menu mega-dropdown-menu row">
         <li class="col-sm-3">
           <ul>
               <li><img src="images/education-templates@2x-1.23e5d190.jpg" class="img-responsive"></img></li>
               <li class="dropdown-header">Social Media</li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
            
           </ul>
         </li>

          <li class="col-sm-3">
           <ul>
               <li><img src="images/education-templates@2x-1.23e5d190.jpg" class="img-responsive"></img></li>
               <li class="dropdown-header">Personal</li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
            
           </ul>
         </li>
           <li class="col-sm-3">
           <ul>
               <li><img src="images/education-templates@2x-1.23e5d190.jpg" class="img-responsive"></img></li>
               <li class="dropdown-header">Business</li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
           </ul>
         </li>
          <li class="col-sm-3">
           <ul>
               <li><img src="images/education-templates@2x-1.23e5d190.jpg" class="img-responsive"></img></li>
               <li class="dropdown-header">Education</li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
           </ul>
         </li>
         
          
               <li class="col-sm-3">
           <ul>
               <li><img src="images/education-templates@2x-1.23e5d190.jpg" class="img-responsive"></img></li>
               <li class="dropdown-header">Social Media</li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
            
           </ul>
         </li>
               <li class="col-sm-3">
           <ul>
               <li><img src="images/education-templates@2x-1.23e5d190.jpg" class="img-responsive"></img></li>
               <li class="dropdown-header">Social Media</li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
            
           </ul>
         </li>
       </ul>
         
     </li>
     <li class="dropdown mega-dropdown">
       <a href="#" class="dropdown-toggle" data-toggle="dropdown">Learn &nbsp; <span class="glyphicon glyphicon-chevron-down pull-right"></span></a>

       <ul class="dropdown-menu mega-dropdown-menu row">
         <li class="col-sm-3">
           <ul>
               <li><img src="images/education-templates@2x-1.23e5d190.jpg" class="img-responsive"></img></li>
               <li class="dropdown-header">Social Media</li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
            
           </ul>
         </li>

          <li class="col-sm-3">
           <ul>
               <li><img src="images/education-templates@2x-1.23e5d190.jpg" class="img-responsive"></img></li>
             <li class="dropdown-header">Personal</li>
             <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
            
           </ul>
         </li>
           <li class="col-sm-3">
           <ul>
               <li><img src="images/education-templates@2x-1.23e5d190.jpg" class="img-responsive"></img></li>
             <li class="dropdown-header">Business</li>
             <li><a href="#">Menu 1</a></li>cre
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
            
           </ul>
         </li>
          <li class="col-sm-3">
           <ul>
               <li><img src="images/education-templates@2x-1.23e5d190.jpg" class="img-responsive"></img></li>
               <li class="dropdown-header">Education</li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
            
           </ul>
         </li>
         
          
               <li class="col-sm-3">
           <ul>
               <li><img src="images/education-templates@2x-1.23e5d190.jpg" class="img-responsive"></img></li>
               <li class="dropdown-header">Social Media</li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
            
           </ul>
         </li>
               <li class="col-sm-3">
           <ul>
               <li><img src="images/education-templates@2x-1.23e5d190.jpg" class="img-responsive"></img></li>
               <li class="dropdown-header">Social Media</li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
            
           </ul>
         </li>
       </ul>
         
     </li>
     <li class="dropdown mega-dropdown">
       <a href="#" class="dropdown-toggle" data-toggle="dropdown">Pricing &nbsp; <span class="glyphicon glyphicon-chevron-down pull-right"></span></a>

       <ul class="dropdown-menu mega-dropdown-menu row">
         <li class="col-sm-3">
           <ul>
               <li><img src="images/education-templates@2x-1.23e5d190.jpg" class="img-responsive"></img></li>
               <li class="dropdown-header">Social Media</li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
            
           </ul>
         </li>

          <li class="col-sm-3">
           <ul>
               <li><img src="images/education-templates@2x-1.23e5d190.jpg" class="img-responsive"></img></li>
               <li class="dropdown-header">Personal</li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
            
           </ul>
         </li>
           <li class="col-sm-3">
           <ul>
               <li><img src="images/education-templates@2x-1.23e5d190.jpg" class="img-responsive"></img></li>
               <li class="dropdown-header">Business</li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
            
           </ul>
         </li>
          <li class="col-sm-3">
           <ul>
               <li><img src="images/education-templates@2x-1.23e5d190.jpg" class="img-responsive"></img></li>
               <li class="dropdown-header">Education</li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
            
           </ul>
         </li>
         
          
               <li class="col-sm-3">
           <ul>
               <li><img src="images/education-templates@2x-1.23e5d190.jpg" class="img-responsive"></img></li>
               <li class="dropdown-header">Social Media</li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
            
           </ul>
         </li>
               <li class="col-sm-3">
           <ul>
               <li><img src="images/education-templates@2x-1.23e5d190.jpg" class="img-responsive"></img></li>
             <li class="dropdown-header">Social Media</li>
             <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
               <li><a href="#">Menu 1</a></li>
            
           </ul>
         </li>
       </ul>
         
     </li>     
     </ul>
     <a style={{width:'300px', height:'200px', textAlign: 'right', marginTop: '20px', color:'#000000'}} href="/">Sign out</a></div>) : (<header class="site-inner-header">
        <div class="row">
        <div class="col-md-12">
        <div class="collapse navbar-collapse js-navbar-collapse">
        <ul class="nav navbar-nav">
        <li><a href="#">Home</a></li>
        <li class="dropdown mega-dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Templates &nbsp; <span class="glyphicon glyphicon-chevron-down pull-right"></span></a>

          <ul class="dropdown-menu mega-dropdown-menu row">
            <li class="col-sm-3">
              <ul>
                  <li><img src="images/education-templates@2x-1.23e5d190.jpg" class="img-responsive"></img></li>
                  <li class="dropdown-header">Social Media</li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
               
              </ul>
            </li>

             <li class="col-sm-3">
              <ul>
                  <li><img src="images/education-templates@2x-1.23e5d190.jpg" class="img-responsive"></img></li>
                  <li class="dropdown-header">Personal</li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
               
              </ul>
            </li>
              <li class="col-sm-3">
              <ul>
                  <li><img src="images/education-templates@2x-1.23e5d190.jpg" class="img-responsive"></img></li>
                  <li class="dropdown-header">Business</li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
               
              </ul>
            </li>
             <li class="col-sm-3">
              <ul>
                  <li><img src="images/education-templates@2x-1.23e5d190.jpg" class="img-responsive"></img></li>
                  <li class="dropdown-header">Education</li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
               
              </ul>
            </li>
                  <li class="col-sm-3">
              <ul>
                  <li><img src="images/education-templates@2x-1.23e5d190.jpg" class="img-responsive"></img></li>
                  <li class="dropdown-header">Social Media</li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
               
              </ul>
            </li>
                  <li class="col-sm-3">
              <ul>
                  <li><img src="images/education-templates@2x-1.23e5d190.jpg" class="img-responsive"></img></li>
                  <li class="dropdown-header">Social Media</li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
               
              </ul>
            </li>
          </ul>
            
        </li>
        <li class="dropdown mega-dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Discover &nbsp; <span class="glyphicon glyphicon-chevron-down pull-right"></span></a>

          <ul class="dropdown-menu mega-dropdown-menu row">
            <li class="col-sm-3">
              <ul>
                  <li><img src="images/education-templates@2x-1.23e5d190.jpg" class="img-responsive"></img></li>
                  <li class="dropdown-header">Social Media</li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
               
              </ul>
            </li>

             <li class="col-sm-3">
              <ul>
                  <li><img src="images/education-templates@2x-1.23e5d190.jpg" class="img-responsive"></img></li>
                  <li class="dropdown-header">Personal</li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
               
              </ul>
            </li>
              <li class="col-sm-3">
              <ul>
                  <li><img src="images/education-templates@2x-1.23e5d190.jpg" class="img-responsive"></img></li>
                  <li class="dropdown-header">Business</li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
              </ul>
            </li>
             <li class="col-sm-3">
              <ul>
                  <li><img src="images/education-templates@2x-1.23e5d190.jpg" class="img-responsive"></img></li>
                  <li class="dropdown-header">Education</li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
              </ul>
            </li>
            
             
                  <li class="col-sm-3">
              <ul>
                  <li><img src="images/education-templates@2x-1.23e5d190.jpg" class="img-responsive"></img></li>
                  <li class="dropdown-header">Social Media</li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
               
              </ul>
            </li>
                  <li class="col-sm-3">
              <ul>
                  <li><img src="images/education-templates@2x-1.23e5d190.jpg" class="img-responsive"></img></li>
                  <li class="dropdown-header">Social Media</li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
               
              </ul>
            </li>
          </ul>
            
        </li>
        <li class="dropdown mega-dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Learn &nbsp; <span class="glyphicon glyphicon-chevron-down pull-right"></span></a>

          <ul class="dropdown-menu mega-dropdown-menu row">
            <li class="col-sm-3">
              <ul>
                  <li><img src="images/education-templates@2x-1.23e5d190.jpg" class="img-responsive"></img></li>
                  <li class="dropdown-header">Social Media</li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
               
              </ul>
            </li>

             <li class="col-sm-3">
              <ul>
                  <li><img src="images/education-templates@2x-1.23e5d190.jpg" class="img-responsive"></img></li>
                <li class="dropdown-header">Personal</li>
                <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
               
              </ul>
            </li>
              <li class="col-sm-3">
              <ul>
                  <li><img src="images/education-templates@2x-1.23e5d190.jpg" class="img-responsive"></img></li>
                <li class="dropdown-header">Business</li>
                <li><a href="#">Menu 1</a></li>cre
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
               
              </ul>
            </li>
             <li class="col-sm-3">
              <ul>
                  <li><img src="images/education-templates@2x-1.23e5d190.jpg" class="img-responsive"></img></li>
                  <li class="dropdown-header">Education</li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
               
              </ul>
            </li>
            
             
                  <li class="col-sm-3">
              <ul>
                  <li><img src="images/education-templates@2x-1.23e5d190.jpg" class="img-responsive"></img></li>
                  <li class="dropdown-header">Social Media</li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
               
              </ul>
            </li>
                  <li class="col-sm-3">
              <ul>
                  <li><img src="images/education-templates@2x-1.23e5d190.jpg" class="img-responsive"></img></li>
                  <li class="dropdown-header">Social Media</li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
               
              </ul>
            </li>
          </ul>
            
        </li>
        <li class="dropdown mega-dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Pricing &nbsp; <span class="glyphicon glyphicon-chevron-down pull-right"></span></a>

          <ul class="dropdown-menu mega-dropdown-menu row">
            <li class="col-sm-3">
              <ul>
                  <li><img src="images/education-templates@2x-1.23e5d190.jpg" class="img-responsive"></img></li>
                  <li class="dropdown-header">Social Media</li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
               
              </ul>
            </li>

             <li class="col-sm-3">
              <ul>
                  <li><img src="images/education-templates@2x-1.23e5d190.jpg" class="img-responsive"></img></li>
                  <li class="dropdown-header">Personal</li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
               
              </ul>
            </li>
              <li class="col-sm-3">
              <ul>
                  <li><img src="images/education-templates@2x-1.23e5d190.jpg" class="img-responsive"></img></li>
                  <li class="dropdown-header">Business</li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
               
              </ul>
            </li>
             <li class="col-sm-3">
              <ul>
                  <li><img src="images/education-templates@2x-1.23e5d190.jpg" class="img-responsive"></img></li>
                  <li class="dropdown-header">Education</li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
               
              </ul>
            </li>
            
             
                  <li class="col-sm-3">
              <ul>
                  <li><img src="images/education-templates@2x-1.23e5d190.jpg" class="img-responsive"></img></li>
                  <li class="dropdown-header">Social Media</li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
               
              </ul>
            </li>
                  <li class="col-sm-3">
              <ul>
                  <li><img src="images/education-templates@2x-1.23e5d190.jpg" class="img-responsive"></img></li>
                <li class="dropdown-header">Social Media</li>
                <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 1</a></li>
               
              </ul>
            </li>
          </ul>
            
        </li>
           
      </ul>

      <a style={{width:'1200px', height:'70px', textAlign: 'right', marginTop: '20px', color:'#000000'}} href="/">Sign out</a>
    </div>
                </div>
                </div>
                </header>)}
        <div className="row">
          <div className="col-lg-2 col-md-2 col-sm-12 col-xs-12 bhoechie-tab-container">
            <div className="col-lg-12 col-md-12 col-sm-3 col-xs-3 bhoechie-tab-menu">
              <div className="site-user"> <span className="user">TB</span> <span className="user-name">
                  Lorem Ipusm
                </span>
                <br />
                <div className="pic-btn"> <a href="#">Add Your Picture</a> </div>
              </div>
              <div className="create-btn"> <a href="/cvdashboard">Create CV</a> </div>
              <ul className="site-inner-menu">
                
                <li> <a href="#">Recomended For You</a></li>
                        <li> <a href="#">All Your design </a></li>
                        <li> <a href="#">Brand Kit</a></li>
                        <li> <a href="#">Create Team</a></li>
                        <li> <a href="#">Folder</a></li>
                        <li> <a href="#">Trash</a></li>
              </ul>
            </div>
          </div>
          <div className="col-lg-10 col-md-10 col-sm-12 col-xs-12">
            <div className="inner-banner">
            { isMobile ? 
            (<div><h2> Design Anything </h2>
              <input className="inner-search1" type="search" style={{width:'200px'}}  placeholder="Enter your Keyword" />
              <p> <a href="#">"Creativity is the natural order of life." - Julia Cameron</a> </p></div>) : 
                (<div><h2> Design Anything </h2>
                  <input className="inner-search1" type="search" placeholder="Enter your Keyword" />
                  <p> <a href="#">"Creativity is the natural order of life." - Julia Cameron</a> </p></div>)}
            </div>
            <div className="create-design">
              <h3><a href="/cvdashboard">Create CV</a> <i className="fas fa-chevron-right" /> </h3>
            </div> 

            <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#home">Recommended</a></li>
                    <li><a data-toggle="tab" href="#menu1">Social Media</a></li>
                    <li><a data-toggle="tab" href="#menu2">Events </a></li>
                    <li><a data-toggle="tab" href="#menu3">Marketing </a></li>
                    <li><a data-toggle="tab" href="#menu4">Documents</a></li>
                    <li><a data-toggle="tab" href="#menu5">Prints</a></li>
                    <li><a data-toggle="tab" href="#menu6">Video</a></li>
                    <li><a data-toggle="tab" href="#menu7">School</a></li>
                    <li><a data-toggle="tab" href="#menu8">Personal</a></li>
                    <li><a data-toggle="tab" href="#menu9">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></li>
                    <li><a data-toggle="tab" href="#menu10">Custom Dimensions</a></li>
                </ul>
                <div class="tab-content">
                    <div id="home" class="tab-pane fade in active">
                        <div class="owl-carousel owl-theme">
                        <div className="row row-gap">
              <div className="col-md-2">
                <div className="demo-design">
                  <a href="/design"><img src={`${imagesPath}/design-1.webp`} className="img-responsive" alt="" /></a>
                </div>
              </div>
              <div className="col-md-2">
                <div className="demo-design">
                  <a href="/design"><img src={`${imagesPath}/design-2.jpg`} className="img-responsive" alt="" /></a>
                </div>
              </div>
              <div className="col-md-2">
                <div className="demo-design">
                  <a href="/design"><img src={`${imagesPath}/design-3.jpg`} className="img-responsive" alt="" /></a>
                </div>
              </div>
              <div className="col-md-2">
                <div className="demo-design">
                  <a href="/design"><img src={`${imagesPath}/design-4.jpg`} className="img-responsive" alt="" /></a>
                </div>
              </div>
              <div className="col-md-2">
                <div className="demo-design">
                  <a href="/design"><img src={`${imagesPath}/design-5.jpg`} className="img-responsive" alt="" /></a>
                </div>
              </div>
              <div className="col-md-2">
                <div className="demo-design">
                  <a href="/design"><img src={`${imagesPath}/design-3.jpg`} className="img-responsive" alt="" /></a>
                </div>
              </div>
            </div>  </div>
                    </div>
                    
                    <div id="menu1" class="tab-pane fade">
                         <div class="row row-gap">
                        <div class="col-md-2 col-xs-6">
                            <div class="demo-design"> <img src="images/design-2.jpg" class="img-responsive" /> </div>
                        </div>
                        <div class="col-md-2 col-xs-6">
                            <div class="demo-design"> <img src="images/design-5.jpg" class="img-responsive" /> </div>
                        </div>
                       <div class="col-md-2 col-xs-6">
                            <div class="demo-design"> <img src="images/design-1.webp" class="img-responsive" /> </div>
                        </div>
                       <div class="col-md-2 col-xs-6">
                            <div class="demo-design"> <img src="images/design-3.jpg" class="img-responsive" /> </div>
                        </div>
                    </div>
                    </div>
                    
                    <div id="menu2" class="tab-pane fade">
                         <div class="row row-gap">
                        <div class="col-md-2 col-xs-6">
                            <div class="demo-design"> <img src="images/design-2.jpg" class="img-responsive" /> </div>
                        </div>
                        <div class="col-md-2 col-xs-6">
                            <div class="demo-design"> <img src="images/design-5.jpg" class="img-responsive" /> </div>
                        </div>
                        <div class="col-md-2 col-xs-6">
                            <div class="demo-design"> <img src="images/design-1.webp" class="img-responsive" /> </div>
                        </div>
                       <div class="col-md-2 col-xs-6">
                            <div class="demo-design"> <img src="images/design-3.jpg" class="img-responsive" /> </div>
                        </div>
                    </div>
                    </div>
                    
                      <div id="menu3" class="tab-pane fade">
                         <div class="row row-gap">
                        <div class="col-md-2">
                            <div class="demo-design"> <img src="images/design-2.jpg" class="img-responsive" /> </div>
                        </div>
                        <div class="col-md-2">
                            <div class="demo-design"> <img src="images/design-5.jpg" class="img-responsive" /> </div>
                        </div>
                        <div class="col-md-2">
                            <div class="demo-design"> <img src="images/design-1.webp" class="img-responsive" /> </div>
                        </div>
                        <div class="col-md-2">
                            <div class="demo-design"> <img src="images/design-3.jpg" class="img-responsive" /> </div>
                        </div>
                    </div>
                    </div>
                </div>
              
            <br />
            <div className="create-design">
              <h3>Instagram Posts</h3>
              <div className="row row-gap">
                <div className="col-md-2">
                  <div className="demo-design">
                    <a href="/design"><img src={`${imagesPath}/design-2.jpg`} className="img-responsive" alt="" /></a>
                  </div>
                </div>
                <div className="col-md-2">
                  <div className="demo-design">
                    <a href="/design"><img src={`${imagesPath}/design-5.jpg`} className="img-responsive" alt="" /></a>
                  </div>
                </div>
                <div className="col-md-2">
                  <div className="demo-design">
                    <a href="/design"><img src={`${imagesPath}/design-1.webp`} className="img-responsive" alt="" /></a>
                  </div>
                </div>
                <div className="col-md-2">
                  <div className="demo-design">
                    <a href="/design"><img src={`${imagesPath}/design-3.jpg`} className="img-responsive" alt="" /></a>
                  </div>
                </div>
              </div>
            </div> 
            <br />
            <div className="create-design">
              <h3>Logos</h3>
              <div className="row row-gap">
                <div className="col-md-2">
                  <div className="demo-design">
                    <a href="/design"><img src={`${imagesPath}/design-2.jpg`} className="img-responsive" alt="" /></a>
                  </div>
                </div>
                <div className="col-md-2">
                  <div className="demo-design">
                    <a href="/design"><img src={`${imagesPath}/design-5.jpg`} className="img-responsive" alt="" /></a>
                  </div>
                </div>
                <div className="col-md-2">
                  <div className="demo-design">
                    <a href="/design"><img src={`${imagesPath}/design-1.webp`} className="img-responsive" alt="" /></a>
                  </div>
                </div>
                <div className="col-md-2">
                  <div className="demo-design">
                    <a href="/design"><img src={`${imagesPath}/design-3.jpg`} className="img-responsive" alt="" /></a>
                  </div>
                </div>
              </div>
            </div> 
            <br />
            <div className="create-design">
              <h3>Posters</h3>
              <div className="row row-gap">
                <div className="col-md-2">
                  <div className="demo-design">
                    <a href="/design"><img src={`${imagesPath}/design-2.jpg`} className="img-responsive" alt="" /></a>
                  </div>
                </div>
                <div className="col-md-2">
                  <div className="demo-design">
                    <a href="/design"><img src={`${imagesPath}/design-5.jpg`} className="img-responsive" alt="" /></a>
                  </div>
                </div>
                <div className="col-md-2">
                  <div className="demo-design">
                    <a href="/design"><img src={`${imagesPath}/design-1.webp`} className="img-responsive" alt="" /></a>
                  </div>
                </div>
                <div className="col-md-2">
                  <div className="demo-design">
                    <a href="/design"><img src={`${imagesPath}/design-3.jpg`} className="img-responsive" alt="" /></a>
                  </div>
                </div>
              </div>
            </div> 
            <br />
            <div className="create-design">
              <h3>You Tube Thumbnails</h3>
              <div className="row row-gap">
                <div className="col-md-2">
                  <div className="demo-design">
                    <a href="/design"><img src={`${imagesPath}/design-2.jpg`} className="img-responsive" alt="" /></a>
                  </div>
                </div>
                <div className="col-md-2">
                  <div className="demo-design">
                    <a href="/design"><img src={`${imagesPath}/design-5.jpg`} className="img-responsive" alt="" /></a>
                  </div>
                </div>
                <div className="col-md-2">
                  <div className="demo-design">
                    <a href="/design"><img src={`${imagesPath}/design-1.webp`} className="img-responsive" alt="" /></a>
                  </div>
                </div>
                <div className="col-md-2">
                  <div className="demo-design">
                    <a href="/design"><img src={`${imagesPath}/design-3.jpg`} className="img-responsive" alt="" /></a>
                  </div>
                </div>
              </div>
            </div> 

            <br />
            <div className="create-design">
              <h3>Presentations</h3>
              <div className="row row-gap">
                <div className="col-md-2">
                  <div className="demo-design">
                    <a href="/design"><img src={`${imagesPath}/design-2.jpg`} className="img-responsive" alt="" /></a>
                  </div>
                </div>
                <div className="col-md-2">
                  <div className="demo-design">
                    <a href="/design"><img src={`${imagesPath}/design-5.jpg`} className="img-responsive" alt="" /></a>
                  </div>
                </div>
                <div className="col-md-2">
                  <div className="demo-design">
                    <a href="/design"><img src={`${imagesPath}/design-1.webp`} className="img-responsive" alt="" /></a>
                  </div>
                </div>
                <div className="col-md-2">
                  <div className="demo-design">
                    <a href="/design"><img src={`${imagesPath}/design-3.jpg`} className="img-responsive" alt="" /></a>
                  </div>
                </div>
              </div>
            </div> 

            <br />  
            <div className="create-design">
              <h3>Facebook Posts</h3>
              <div className="row row-gap">
                <div className="col-md-2">
                  <div className="demo-design">
                    <a href="/design"><img src={`${imagesPath}/design-2.jpg`} className="img-responsive" alt="" /></a>
                  </div>
                </div>
                <div className="col-md-2">
                  <div className="demo-design">
                    <a href="/design"><img src={`${imagesPath}/design-5.jpg`} className="img-responsive" alt="" /></a>
                  </div>
                </div>
                <div className="col-md-2">
                  <div className="demo-design">
                    <a href="/design"><img src={`${imagesPath}/design-1.webp`} className="img-responsive" alt="" /></a>
                  </div>
                </div>
                <div className="col-md-2">
                  <div className="demo-design">
                    <a href="/design"><img src={`${imagesPath}/design-3.jpg`} className="img-responsive" alt="" /></a>
                  </div>
                </div>
              </div>
            </div> 

            <br />
            <div className="create-design">
              <h3>Facebook Covers</h3>
              <div className="row row-gap">
                <div className="col-md-2">
                  <div className="demo-design">
                    <a href="/design"><img src={`${imagesPath}/design-2.jpg`} className="img-responsive" alt="" /></a>
                  </div>
                </div>
                <div className="col-md-2">
                  <div className="demo-design">
                    <a href="/design"><img src={`${imagesPath}/design-5.jpg`} className="img-responsive" alt="" /></a>
                  </div>
                </div>
                <div className="col-md-2">
                  <div className="demo-design">
                    <a href="/design"><img src={`${imagesPath}/design-1.webp`} className="img-responsive" alt="" /></a>
                  </div>
                </div>
                <div className="col-md-2">
                  <div className="demo-design">
                    <a href="/design"><img src={`${imagesPath}/design-3.jpg`} className="img-responsive" alt="" /></a>
                  </div>
                </div>
              </div>
            </div> 

            <br />
            <div className="create-design">
              <h3>Your Channel Art</h3>
              <div className="row row-gap">
                <div className="col-md-2">
                  <div className="demo-design">
                    <a href="/design"><img src={`${imagesPath}/design-2.jpg`} className="img-responsive" alt="" /></a>
                  </div>
                </div>
                <div className="col-md-2">
                  <div className="demo-design">
                    <a href="/design"><img src={`${imagesPath}/design-5.jpg`} className="img-responsive" alt="" /></a>
                  </div>
                </div>
                <div className="col-md-2">
                  <div className="demo-design">
                    <a href="/design"><img src={`${imagesPath}/design-1.webp`} className="img-responsive" alt="" /></a>
                  </div>
                </div>
                <div className="col-md-2">
                  <div className="demo-design">
                    <a href="/design"><img src={`${imagesPath}/design-3.jpg`} className="img-responsive" alt="" /></a>
                  </div>
                </div>
              </div>
            </div> 
            <br />
            <div className="create-design">
              <h3>Videos</h3>
              <div className="row row-gap">
                <div className="col-md-2">
                  <div className="demo-design">
                    <a href="/design"><img src={`${imagesPath}/design-2.jpg`} className="img-responsive" alt="" /></a>
                  </div>
                </div>
                <div className="col-md-2">
                  <div className="demo-design">
                    <a href="/design"><img src={`${imagesPath}/design-5.jpg`} className="img-responsive" alt="" /></a>
                  </div>
                </div>
                <div className="col-md-2">
                  <div className="demo-design">
                    <a href="/design"><img src={`${imagesPath}/design-1.webp`} className="img-responsive" alt="" /></a>
                  </div>
                </div>
                <div className="col-md-2">
                  <div className="demo-design">
                    <a href="/design"><img src={`${imagesPath}/design-3.jpg`} className="img-responsive" alt="" /></a>
                  </div>
                </div>
              </div>
            </div> 

          </div>
        </div>
      </div>
    );
  }
}

export default Templates;