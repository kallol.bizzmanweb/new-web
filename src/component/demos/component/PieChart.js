import React, { Component } from 'react';
import { PieChart, Pie, Legend, Cell, Tooltip, ResponsiveContainer, Sector,
  Label, LabelList } from 'recharts';
import { scaleOrdinal } from 'd3-scale';
import { schemeCategory10 } from 'd3-scale-chromatic';
import { changeNumberOfData } from './utils';

import _ from 'lodash';

const colors = scaleOrdinal(schemeCategory10).range();

var retrievedDatas1 = sessionStorage.getItem("text1");


//const text6 = JSON.parse(retrievedDatas6);

if(retrievedDatas1!=null){

  const retrievedDatas1 = sessionStorage.getItem("text1");
//const text1 = JSON.parse(retrievedDatas1);

////alert("data : "+retrievedDatas1);

const retrievedsess = sessionStorage.getItem("search");

////alert("retrievedsess .."+retrievedsess);

//var retrievedDatas1 = sessionStorage.getItem("text1");

var retrievedDatas2 = sessionStorage.getItem("text2");
//const text2 = JSON.parse(retrievedDatas2);

var retrievedDatas3 = sessionStorage.getItem("text3");
//const text3 = JSON.parse(retrievedDatas3);

var retrievedDatas4 = sessionStorage.getItem("text4");
//const text4 = JSON.parse(retrievedDatas4);

var retrievedDatas5 = sessionStorage.getItem("text5");
//const text5 = JSON.parse(retrievedDatas5);

var retrievedDatas6 = sessionStorage.getItem("text6");
//const text6 = JSON.parse(retrievedDatas6);

}else{

 const retrievedDatas1 = "100";

  var retrievedDatas2 = "200";
//const text2 = JSON.parse(retrievedDatas2);

var retrievedDatas3 = "100";
//const text3 = JSON.parse(retrievedDatas3);

var retrievedDatas4 = "400";
//const text4 = JSON.parse(retrievedDatas4);

var retrievedDatas5 = "800";
//const text5 = JSON.parse(retrievedDatas5);

var retrievedDatas6 = "100";
//const text6 = JSON.parse(retrievedDatas6);

}




const data01 = [
  { name: 'Summary', value: +retrievedDatas1, v: 89 },
  { name: 'Skills', value: +retrievedDatas2, v: 100 },
  { name: 'Profile', value: +retrievedDatas3, v: 200 },
  { name: 'Work Experience', value: +retrievedDatas4, v: 20 },
  { name: 'Education', value: +retrievedDatas5, v: 40 },
  { name: 'Text Section', value: +retrievedDatas6, v: 60 },
];

const data02 = [
  { name: 'Summary', value: +retrievedDatas1 },
  { name: 'Skills', value: +retrievedDatas2 },
  { name: 'Profile', value: +retrievedDatas3 },
  { name: 'Work Experience', value: +retrievedDatas4 },
  { name: 'Education', value: +retrievedDatas5 },
  { name: 'Text Section', value: +retrievedDatas6 },
];

const data03 = [
  { name: 'A1', value: 100 },
  { name: 'A2', value: 300 },
  { name: 'B1', value: 100 },
  { name: 'B2', value: 80 },
  { name: 'B3', value: 40 },
  { name: 'B4', value: 30 },
  { name: 'B5', value: 50 },
  { name: 'C1', value: 100 },
  { name: 'C2', value: 200 },
  { name: 'D1', value: 150 },
  { name: 'D2', value: 50 },
  { name: 'E1', value: 200 },
  { name: 'E2', value: 34 },
  { name: 'E3', value: 44 },
  { name: 'F1', value: 89 },
  { name: 'F2', value: 49 },
  { name: 'F3', value: 51 },
];

const initialState = { data01, data02, data03 };

const renderLabelContent = (props) => {
  const { value, percent, x, y, midAngle } = props;

  return (
    <g transform={`translate(${x}, ${y})`} textAnchor={ (midAngle < -90 || midAngle >= 90) ? 'end' : 'start'}>
      <text x={0} y={0}>{`Count: ${value}`}</text>
      <text x={0} y={20}>{`(Percent: ${(percent * 100).toFixed(2)}%)`}</text>
    </g>
  );
};
const renderActiveShape = (props) => {
  const RADIAN = Math.PI / 180;
  const { cx, cy, midAngle, innerRadius, outerRadius, startAngle, endAngle,
    fill, payload, percent } = props;
  const sin = Math.sin(-RADIAN * midAngle);
  const cos = Math.cos(-RADIAN * midAngle);
  const sx = cx + (outerRadius + 10) * cos;
  const sy = cy + (outerRadius + 10) * sin;
  const mx = cx + (outerRadius + 30) * cos;
  const my = cy + (outerRadius + 30) * sin;
  const ex = mx + (cos >= 0 ? 1 : -1) * 22;
  const ey = my;
  const textAnchor = cos >= 0 ? 'start' : 'end';

  return (
    <g>
      <text x={cx} y={cy} dy={8} textAnchor="middle" fill={fill}>{payload.name}</text>
      <Sector
        cx={cx}
        cy={cy}
        innerRadius={innerRadius}
        outerRadius={outerRadius}
        startAngle={startAngle}
        endAngle={endAngle}
        fill={fill}
      />
      <Sector
        cx={cx}
        cy={cy}
        startAngle={startAngle}
        endAngle={endAngle}
        innerRadius={outerRadius + 6}
        outerRadius={outerRadius + 10}
        fill={fill}
      />
      <path d={`M${sx},${sy}L${mx},${my}L${ex},${ey}`} stroke={fill} fill="none"/>
      <circle cx={ex} cy={ey} r={2} fill={fill} stroke="none"/>
      <text x={ex + (cos >= 0 ? 1 : -1) * 12} y={ey} textAnchor={textAnchor} fill="#333">
        {`Count ${payload.value}`}
      </text>
      <text x={ex + (cos >= 0 ? 1 : -1) * 12} y={ey} dy={18} textAnchor={textAnchor} fill="#999">
        {`(percent: ${(percent * 100).toFixed(2)}%)`}
      </text>
    </g>
  );
};

export default class Demo extends Component {

  static displayName = 'PieChartDemo';

  onPieEnter = (data, index, e) => {
    this.setState({
      activeIndex: index,
    });
  };

  state = {
    ...initialState,
    activeIndex: 0,
    animation: false,
  };

  handleChangeData = () => {
    this.setState(() => _.mapValues(initialState, changeNumberOfData));
  };

  handleChangeAnimation = () => {
    this.setState({
      animation: !this.state.animation,
    });
  };

  handlePieChartEnter = (a, b, c) => {
    console.log(a, b, c);
  };

  handleEnter = (e, activeIndex) => this.setState({ activeIndex });
  handleLeave = () => this.setState({ activeIndex: -1 });

  render () {
    const { data01, data02, data03 } = this.state;

    return (
      <div className="pie-charts">
        <a
          href="javascript: void(0);"
          className="btn update"
          onClick={this.handleChangeData}
        >
          change data
        </a>
        <br/>
        <p> PieChart</p>
        <div className="pie-chart-wrapper">
          <button onClick={this.handleChangeAnimation}></button>
          <PieChart width={800} height={400}>
            <Legend paylodUniqBy />
            
            <Pie
              data={data02}
              dataKey="value"
              cx={300}
              cy={200}
              startAngle={180}
              endAngle={-180}
              innerRadius={40}
              outerRadius={80}
              label={renderLabelContent}
              paddingAngle={0}
              isAnimationActive={this.state.animation}
            >
              {
                data02.map((entry, index) => (
                  <Cell key={`slice-${index}`} fill={colors[index % 10]}/>
                ))
              }
              <Label width={50} position="center">
                Resume
              </Label>
            </Pie>
          </PieChart>
        </div>


        {/* <p>PieChart has bug about tooltip</p>
        <div className="pie-chart-wrapper" style={{ width: '50%', height: '100%', backgroundColor: '#f5f5f5' }}>
          <ResponsiveContainer>
            <PieChart>
              <Pie
                data={data01}
                dataKey="value"
                innerRadius="25%"
                outerRadius="40%"
                onMouseEnter={this.handleEnter}
                onMouseLeave={this.handleLeave}
              >
                {
                  data01.map((entry, index) => (
                    <Cell
                      key={`slice-${index}`}
                      fill={colors[index % 10]}
                      fillOpacity={this.state.activeIndex === index ? 1 : 0.25}
                    />
                  ))
                }
                <Label value="test" />
              </Pie>
            </PieChart>
          </ResponsiveContainer>
        </div> */}
      </div>
    );
  }
}

