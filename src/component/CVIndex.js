import React,{Component} from 'react';
import IndexHeader from './IndexHeader.js';
import SiteBanner from './SiteBanner.js';
import SiteTemplate from './SiteTemplate';
import BrandingTemplate from './BrandingTemplate.js';
import Reviews from './Reviews.js';
import DesignTemplate from './DesignTemplate.js';
import Footer from './Footer.js';

import { MDBContainer } from 'mdbreact';


class CVIndex extends Component 
{

  render (){

     return(
        <div>
       
        <IndexHeader/>
        <SiteBanner/>
        <SiteTemplate/>
        <BrandingTemplate/>
        <Reviews/>
        <DesignTemplate/>
        <Footer/>
         
    </div>
     	);

  }

}

export default CVIndex;