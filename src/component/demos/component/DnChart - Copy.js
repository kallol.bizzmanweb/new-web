import React from 'react';

import {Pie, Doughnut, Chart} from 'react-chartjs-2';

//import {Pie, Doughnut, Chart} from 'react-chartjs';

////import {Chart as Doughnut} from 'react-chartjs';


export default class DoughnutChart extends React.Component {
  constructor(props) {
    super(props);
    this.chartRef = React.createRef();

    const parsedTextimg=[
    ];

    parsedTextimg.push({
      label: "A",
      value: 46,
      });
      parsedTextimg.push({
        label: "B",
        value: 30,
    });
    parsedTextimg.push({
      label: "C",
      value: 35,
    });
    parsedTextimg.push({
      label: "D",
      value: 66,
    });
    parsedTextimg.push({
      label: "E",
      value: 40,
    });

    if(parsedTextimg.length>1){
    
      localStorage.setItem('titlestxtrfp', JSON.stringify(parsedTextimg));
    
    }

    var retrievedDatasrfp = localStorage.getItem("titlestxtrfp");

    ////alert(retrievedDatasrfp);

    this.bannertxt11rfp = JSON.parse(retrievedDatasrfp);
  }


  componentDidUpdate() {
    this.myChart.data.labels = this.bannertxt11rfp.map(d => d.label);
    this.myChart.data.datasets[0].data = this.bannertxt11rfp.map(d => d.value);
    this.myChart.update();
  }

  componentDidMount() {
    this.myChart = new Chart(this.chartRef.current, {
      type: 'doughnut',
      data: {
        labels: this.bannertxt11rfp.map(d => d.label),
        datasets: [{
          data: this.bannertxt11rfp.map(d => d.value),
          backgroundColor: this.props.colors
        }]
      }
    });
  }

  render() {

    



    return <canvas ref={this.chartRef} />;
  }
}