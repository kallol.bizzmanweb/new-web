import React,{Component} from 'react';

import Constants from '../common/Constants';

import logo from './images/cvlogo1.png';


class IndexHeader extends Component{
  render() {
    const imagesPath = Constants.imagespath;

  	return (

  /*     <div>
        <nav className="navbar navbar-inverse">
          <div className="container-fluid">
              <div className="navbar-header">
                  <button type="button" className="navbar-toggle" data-toggle="collapse" data-target="#myNavbar"> <span className="icon-bar"></span> <span class="icon-bar"></span> <span className="icon-bar"></span> </button>
                  <a className="navbar-brand" href="/login"> <img src={`${imagesPath}/loginLogo.png`} alt ="login" className="img-responsive" /> </a>
              </div>
              <div className="collapse navbar-collapse" id="myNavbar">
                  <ul className="nav navbar-nav  navbar-right">
                      <li className="border-down"><a href="/resume">Resume</a></li>
                      <li className="dropdown"> <a className="dropdown-toggle" data-toggle="dropdown" href="/branding">Branding <span className="caret"></span></a>
                          <ul className="dropdown-menu">
                              <li><a href="/page1">Page 1</a></li>
                              <li><a href="page2">Page 1</a></li>
                              <li><a href="page3">Page 1</a></li>
                          </ul>
                      </li>
                      <li><a href="/resources">Resources</a></li>
                      <li className="rounded-border"><a href="/signUp" data-toggle="modal" data-target="#myModal"><span className="glyphicon glyphicon-log-in"></span> Sign Up</a></li>
                      <li class="rounded-border"><a href="/login"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
                  </ul>
              </div>
          </div>
        </nav>
      </div> */

      <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                <a class="navbar-brand" href="#"> <img style={{width:'30px', height:'30px'}} src={logo} class="img-responsive" /> </a>
            </div>
            <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="nav navbar-nav  navbar-right">                    
                    
                         <li className="rounded-border"><a href="/signUp" data-toggle="modal" data-target="#myModal"><span className="glyphicon glyphicon-log-in" style={{color:'#ffffff'}}></span><span style={{color:'#ffffff'}}>  Sign Up</span></a></li>
                         <li class="rounded-border"><a href="/login"><span class="glyphicon glyphicon-log-in" style={{color:'#ffffff'}}></span><span style={{color:'#ffffff'}}> Login</span></a></li>
             </ul>
            </div>
        </div>
    </nav>
    );
  }
}

export default IndexHeader;