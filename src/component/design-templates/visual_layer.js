import React, { Component } from "react";
import ReactQuill, { Quill } from 'react-quill';
import quillEmoji from 'quill-emoji';
import 'react-quill/dist/quill.snow.css';
const { EmojiBlot, ShortNameEmoji, ToolbarEmoji, TextAreaEmoji } = quillEmoji;


Quill.register({
  'formats/emoji': EmojiBlot,
  'modules/emoji-shortname': ShortNameEmoji,
  'modules/emoji-toolbar': ToolbarEmoji,
  'modules/emoji-textarea': TextAreaEmoji
}, true);


const modulesQuill = {
  toolbar: {
    container: [
      [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
      [{ 'font': [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15] }],
      [{ 'align': [] }],
      ['bold', 'italic', 'underline', 'strike'],
      [{ 'list': 'ordered' }, { 'list': 'bullet' }, { 'script': 'sub'}, { 'script': 'super' }, { 'indent': '-1'}, { 'indent': '+1' }, { 'direction': 'rtl' }, { 'size': ['small', false, 'large', 'huge'] }, { 'color': ['#000000', '#e60000', '#ff9900', '#ffff00', '#008a00', '#0066cc', '#9933ff', '#ffffff', '#facccc', '#ffebcc', '#ffffcc', '#cce8cc', '#cce0f5', '#ebd6ff', '#bbbbbb', '#f06666', '#ffc266', '#ffff66', '#66b966', '#66a3e0', '#c285ff', '#888888', '#a10000', '#b26b00', '#b2b200', '#006100', '#0047b2', '#6b24b2', '#444444', '#5c0000', '#663d00', '#666600', '#003700', '#002966', '#3d1466', 'custom-color'] }, { 'background': [] }, 'link', 'emoji'],          // dropdown with defaults from theme
      ['clean']      
    ],
    handlers: {
      'color': function (value) {
        if (value == 'custom-color') value = window.prompt('Enter Hex Color Code');
        this.quill.format('color', value);
      }
    }
  },
  keyboard: {
    bindings: {
      tab: false,
      custom: {
        key: 13,
        shiftKey: true,
        handler: function () { /** do nothing */ }
      },
      handleEnter: {
        key: 13,
        handler: function () { /** do nothing */ }
      }
    }
  },
  'emoji-toolbar': true,
  'emoji-textarea': true,
  'emoji-shortname': true,
};

const formatsQuill = [
  'header', 'font', 'size',
  'bold', 'italic', 'underline', 'strike', 'blockquote',
  'list', 'bullet', 'indent', 'align',
  'link', 'image', 'background', 'color', 'emoji'
];

class Test extends Component {
  constructor(props) {
    super(props)
    this.state = { editorHtml :'' } // You can also pass a Quill Delta here

    
    //this.handleChange = this.handleChange.bind(this)

    this.handleChange = (html) => {
      this.setState({
        editorHtml : html
      })
    } 
  }

  render() {
 
    return (

<ReactQuill
  modules={modulesQuill}
  formats={formatsQuill}
  onChange={this.handleChange.bind(this)}
  value={this.state.editorHtml}
  onChange={this.handleChange}
/>

);
  };

  
}


export default Test;