import React , { Component} from 'react';

import Constants from '../common/Constants';

import { MDBContainer, MDBTabPane, MDBTabContent, MDBNav, MDBNavItem, MDBNavLink } from "mdbreact";

import { MDBBtn, MDBModal, MDBModalBody, MDBModalHeader, MDBModalFooter } from 'mdbreact';

import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';

//import './style/react-tabs.css';

import './main.scss';

//import './css/main.css.map';
//import './css/main.scss';

import style from './main.scss';

import loremIpsum from 'react-lorem-ipsum';

import styles from './css1/style.scss';

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCubes, faFileAlt } from "@fortawesome/free-solid-svg-icons";
import { faAppleAlt } from "@fortawesome/free-solid-svg-icons";
import { faCog } from "@fortawesome/free-solid-svg-icons";
import { faFile } from "@fortawesome/free-solid-svg-icons";
import { faClock } from "@fortawesome/free-solid-svg-icons";
import { faArrowDown } from "@fortawesome/free-solid-svg-icons";
import { faCrown } from "@fortawesome/free-solid-svg-icons";

import { faUpload } from "@fortawesome/free-solid-svg-icons";
import { faTextHeight } from "@fortawesome/free-solid-svg-icons";
import { faFolder } from "@fortawesome/free-solid-svg-icons";
import { faColumns } from "@fortawesome/free-solid-svg-icons";
import { faVideo } from "@fortawesome/free-solid-svg-icons";

import { faRocket } from "@fortawesome/free-solid-svg-icons";
import { faEnvelope } from "@fortawesome/free-solid-svg-icons";
import { faPhone } from "@fortawesome/free-solid-svg-icons";
import { faMapMarker } from "@fortawesome/free-solid-svg-icons";
import { faHome } from "@fortawesome/free-solid-svg-icons";


import { faFacebook } from "@fortawesome/free-brands-svg-icons"
import { faInstagram } from "@fortawesome/free-brands-svg-icons";
import { faPinterest } from "@fortawesome/free-brands-svg-icons";
import { faLinkedin } from "@fortawesome/free-brands-svg-icons";
import { faCodepen } from "@fortawesome/free-brands-svg-icons";
import { faBehance } from "@fortawesome/free-brands-svg-icons";


import { faPalfed } from "@fortawesome/free-brands-svg-icons"
import { faBimobject } from "@fortawesome/free-brands-svg-icons";
import { faFirstdraft } from "@fortawesome/free-brands-svg-icons";
import { faHackerrank } from "@fortawesome/free-brands-svg-icons";
import { faGithubAlt } from "@fortawesome/free-brands-svg-icons";

//import { faBehance } from "@fortawesome/free-brands-svg-icons";

import { faStrava } from "@fortawesome/free-brands-svg-icons";

import { fas } from "@fortawesome/free-solid-svg-icons";
import { fab } from "@fortawesome/free-brands-svg-icons";


/*
  <div class="art"><i class="fas fa-palette"></i><span>Art</span></div>
  <div class="art"><i class="fas fa-book"></i><span>Books</span></div>
  <div class="movies"><i class="fas fa-film"></i><span>Movies</span></div>
  <div class="music"><i class="fas fa-headphones"></i><span>Music</span></div>
  <div class="games"><i class="fas fa-gamepad"></i><span>Games</span></div>


  <a href="#" target="_blank"><i class="fab fa-instagram "></i></a>
              <a href="#" target="_blank"><i class="fab fa-pinterest"></i></a>
              <a href="#" target="_blank"><i class="fab fa-linkedin"></i></a>
              <a href="#" target="_blank"><i class="fab fa-codepen"></i></a>
              <a href="#" target="_blank"><i class="fab fa-behance"></i></a>

*/

import A4 from '../baseTemplate'
import { EditableText, List, RowTexts, HeaderPic } from '../core'

//import Userpics from './128.jpg';

import cs from 'classnames';

import RaisedButton from 'material-ui/RaisedButton'
////import cs from 'classnames'

//import phimg from './headshot.jpg'
import FileReaderInput from 'react-file-reader-input'

//import Userpics1 from './images/500.jpg';

class Templates extends Component {
  
    state = {
      activeItem: "1"
    }

  constructor(props) {
      super(props);
      this.state = {
        showPopup: false,
        showDiv: true,
        show:true,
        isActive: true,
        isActive1: false,
        isActive2: false,
        isActive3: false,
        isActive4: false
      };

      this.state = {
        //isOnHonver: false,
        //imgSrc: phimg
      }

      this.state = {
        cv: {
          name: 'Wee',
          nickname: 'fi3ework',
        }
      }
    }
    togglePopup() {
      this.setState({
        showPopup: !this.state.showPopup
      });
    }
    
    
    toggle = tab => () => {
      if (this.state.activeItem !== tab) {
      this.setState({
        activeItem: tab
      });
      }
    }

    handleShow = () => {
      this.setState({
        isActive: true,
        isActive1: false,
        isActive2: false,
        isActive3: false,
        isActive4: false,
        isActive5: false
      });
    };

    handleShows = () => {
      this.setState({
        isActive: false,
        isActive1: true,
        isActive2: false,
        isActive3: false,
        isActive5: false

      });
    };

    handleShows1 = () => {
      this.setState({
        isActive: false,
        isActive1: false,
        isActive2: true,
        isActive3: false,
        isActive4: false,
        isActive5: false
      });
    };

    handleShows2 = () => {
      this.setState({
        isActive: false,
        isActive1: false,
        isActive2: false,
        isActive3: true,
        isActive4: false,
        isActive5: false
      });
    };

    handleShows3 = () => {
      this.setState({
        isActive: false,
        isActive1: false,
        isActive2: false,
        isActive3: false,
        isActive4: true,
        isActive5: false
      });
    };
    handleShows4 = () => {
      this.setState({
        isActive: false,
        isActive1: false,
        isActive2: false,
        isActive3: false,
        isActive4: false,
        isActive5: true
      });
    };
  
    //state = { isOpen: false };
  
    handleShowDialog = () => {
      this.setState({ isOpen: !this.state.isOpen });
      console.log("clicked");
      
    };

    _arrayBufferToBase64(buffer) {
      let binary = ''
      let bytes = new Uint8Array(buffer)
      let len = bytes.byteLength
      for (let i = 0; i < len; i++) {
        binary += String.fromCharCode(bytes[i])
      }
      return window.btoa(binary)
    }
  
    upload = (e, results) => {
      let reader = results[0][0].target
      let that = this
      let base64 = 'data:image/jpg;base64, ' + this._arrayBufferToBase64(reader.result)
      that.img.setAttribute('src', base64)
    }

    /*
      constructor(props) {
        super(props);
    
        this.state = { isOpen: false };
      }
    
      toggleModal = () => {
        this.setState({
          isOpen: !this.state.isOpen
        });
      }
      */
  
    render() {

      const imagesPath = Constants.imagespath;

      //const { showDiv } = this.state.showDiv;

      const template = () => {
        window.location.href = "/Template2";
        console.log("Template Page"); 
       }
  
      return (
        <A4>
        <article className={cs({
          [style.cv]: true,
         exportRoot: true
        })}>
          <HeaderPic style={{
            padding: '20px 40px 80px 80px'
          }}>
        <header className={style.header}>
       <div class="resume">
        <div class="base">
          <div class="profile" style={{width:'114%'}}>
            <div class="photo" style={{marginTop:'40px'}}>
              <i class="fas"><FontAwesomeIcon icon={faRocket} /></i>
            </div>
            <div class="info" style={{fontSize:'18px',textAlign:'center'}}>
              <h1 class="name"><EditableText
                    html='naomi weatherford'
                  /></h1>
              <h2 class="job"><EditableText
                    html='frontend web designer'
                  /></h2>
            </div>
            
          </div>
          <div class="about">
          <h3><EditableText
                    html='About Me'
                  /></h3>
          <br />
          <EditableText
                    html="I'm a web designer for Fiserv, specializing in web design, graphic design, and UX. Experienced with the Adobe Creative Suite, responsive design, social media management, and prototyping."
                  />
          </div>
      
          <div class="contact">
            <h3><EditableText
                    html='Contact Me'
                  /></h3>
            <div class="call" style={{textAlign:'left'}}><a href="#"><i class="fas"><FontAwesomeIcon icon={faPhone} /></i><span><EditableText
                    html='123-456-7890'
                  /></span></a></div>
            <div class="address" style={{textAlign:'left'}}><a href="#"><i class="fas"><FontAwesomeIcon icon={faMapMarker} /></i><span>
            <EditableText
                    html='Provo, Utah'
                  /></span></a></div>
            <div class="email"><a href="#"><i class="fas"><FontAwesomeIcon icon={faEnvelope} /></i><span>
            <EditableText
                    html='astronaomical'
                  /></span></a></div>
            <div class="website"><a href="#" target="_blank"> <i class="fas"><FontAwesomeIcon icon={faHome} /></i><span>
            <EditableText
                    html='astronaomical.com'
                  /></span></a></div>
          </div>
            <br/>
            <br/>
          <div class="follow">
            <h3><EditableText
                    html='Follow Me'
                  /></h3>
            <br />
            <br/>
            <div class="box">

              <a href="#" target="_blank"><i class="fab"><FontAwesomeIcon icon={faInstagram} /></i></a>
              <a href="#" target="_blank"><i class="fab"><FontAwesomeIcon icon={faFacebook} /></i></a>
              <a href="#" target="_blank"><i class="fab"><FontAwesomeIcon icon={faInstagram} /></i></a>
              <a href="#" target="_blank"><i class="fab"><FontAwesomeIcon icon={faPinterest} /></i></a>
              <a href="#" target="_blank"><i class="fab"><FontAwesomeIcon icon={faLinkedin} /></i></a>
              <a href="#" target="_blank"><i class="fab"><FontAwesomeIcon icon={faCodepen} /></i></a>
              <a href="#" target="_blank"><i class="fab"><FontAwesomeIcon icon={faBehance} /></i></a>
            </div>
          </div>
        </div>
        <div class="func">
          <div class="work">
            <h3><i class="fa fa-briefcase"></i>Experience</h3>
    <ul>
      <li><span><EditableText html='Technical Consultant -' /><br /><EditableText html='Web Design' /></span><small><EditableText html='Fiserv' /></small><small><EditableText html='Apr 2018 - Now' /></small></li>
      <li><span><EditableText html='Web Designer' /></span><small><EditableText html='Lynden' /></small><small><EditableText html='Jan 2018 - Apr 2018' /></small></li>
      <li><span><EditableText html='Intern - Web Design' /></span><small><EditableText html='Lynden' /></small><small><EditableText html='Aug 2017 - Dec 2017' /></small></li>
    </ul>
          </div>
          <div class="edu">
            <h3><i class="fa fa-graduation-cap"></i><EditableText html='Education' /></h3>
            <ul>
              <li><span><EditableText html='Bachelor of Science' /><br /><EditableText html='Web Design and Development' /></span><small><EditableText html='BYU-Idaho' /></small><small><EditableText html='Jan. 2016 - Apr. 2018' /></small></li>
              <li><span><EditableText html='Computer Science' /></span><small><EditableText html='Edmonds Community College' /></small><small><EditableText html='Sept. 2014 - Dec. 2015' /></small></li>
              <li><span><EditableText html='High School' /></span><small><EditableText html='Henry M. Jackson High School' /></small><small><EditableText html='Jan. 2013 - Jun. 2015' /></small></li>
            </ul>
          </div>
          <div class="skills-prog">
            <h3><i class="fas fa-code"></i><EditableText
                    html='Programming Skills'
                  /></h3>
            <ul>
              <li data-percent="95"><span><EditableText
                    html='HTML5'
                  /></span>
                <div class="skills-bar">
                  <div class="bar"><EditableText
                    html=''
                  /></div>
                </div>
              </li>
              <li data-percent="90"><span><EditableText
                    html='CSS3 & SCSS'
                  /></span>
                <div class="skills-bar">
                  <div class="bar"><EditableText
                    html=''
                  /></div>
                </div>
              </li>
              <li data-percent="60"><span><EditableText
                    html='JavaScript'
                  /></span>
                <div class="skills-bar">
                  <div class="bar"><EditableText
                    html=''
                  /></div>
                </div>
              </li>
              <li data-percent="50"><span>
              <EditableText
                    html='jQuery'
                  /></span>
                <div class="skills-bar">
                  <div class="bar"><EditableText
                    html=''
                  /></div>
                </div>
              </li>
              <li data-percent="40"><span><EditableText
                    html='JSON'
                  /></span>
                <div class="skills-bar">
                  <div class="bar">
                  <EditableText
                    html=''
                  />
                  </div>
                </div>
              </li>
              <li data-percent="55"><span><EditableText
                    html='PHP'
                  /></span>
                <div class="skills-bar">
                  <div class="bar"><EditableText
                    html=''
                  /></div>
                </div>
              </li>
              <li data-percent="40"><span><EditableText
                    html='MySQL'
                  /></span>
                <div class="skills-bar">
                  <div class="bar"><EditableText
                    html=''
                  /></div>
                </div>
              </li>
            </ul>
          </div>
          <div class="skills-soft">
            <h3><i class="fas"><FontAwesomeIcon icon={faCubes} /></i>Software Skills</h3>
            <ul>
              <li data-percent="90">
                <svg viewbox="0 0 100 100">
                  <circle cx="50" cy="50" r="45"></circle>
                  <circle class="cbar" cx="50" cy="50" r="45"></circle>
                </svg><span><EditableText
                    html='Illustrator'
                  /></span><small></small>
              </li>
              <li data-percent="75">
                <svg viewbox="0 0 100 100">
                  <circle cx="50" cy="50" r="45"></circle>
                  <circle class="cbar" cx="50" cy="50" r="45"></circle>
                </svg><span><EditableText
                    html='Photoshop'
                  /></span><small></small>
              </li>
              <li data-percent="85">
                <svg viewbox="0 0 100 100">
                  <circle cx="50" cy="50" r="45"></circle>
                  <circle class="cbar" cx="50" cy="50" r="45"></circle>
                </svg><span><EditableText
                    html='InDesign'
                  /></span><small></small>
              </li>
              <li data-percent="65">
                <svg viewbox="0 0 100 100">
                  <circle cx="50" cy="50" r="45"></circle>
                  <circle class="cbar" cx="50" cy="50" r="45"></circle>
                </svg><span><EditableText
                    html='Dreamweaver'
                  /></span><small></small>
              </li>
            </ul>
          </div>
          <div class="interests">
            <h3><i class="fa fa-briefcase"></i><span><EditableText
                    html='Interests'
                  /></span></h3>
            <div class="interests-items">
              <div class="art"><i class="fas"><FontAwesomeIcon icon={faPalfed} /></i><span><EditableText
                    html='Art'
                  /></span></div>
              <div class="art"><i class="fas"><FontAwesomeIcon icon={faBimobject} /></i><span><EditableText
                    html='Books'
                  /></span></div>
              <div class="movies"><i class="fas"><FontAwesomeIcon icon={faFirstdraft} /></i><span><EditableText
                    html='Movies'
                  /></span></div>
              <div class="music"><i class="fas"><FontAwesomeIcon icon={faHackerrank} /></i><span>
              <EditableText
                    html='Music'
                  /></span></div>
              <div class="games"><i class="fas"><FontAwesomeIcon icon={faBehance} /></i><span>
              <EditableText
                    html='Games'
                  /></span></div>

          </div>
          </div>
        </div>
      </div>
      </header>
      </HeaderPic>
    </article>
    </A4>
    );
  }
}

export default Templates;