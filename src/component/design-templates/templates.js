import React , { Component} from 'react';

import Constants from '../../common/Constants';

//import CanvasApp from '../../css/CanvasApp.css';

import { MDBContainer, MDBTabPane, MDBTabContent } from "mdbreact";
import { MDBNav, MDBNavItem, MDBNavLink } from "mdbreact";

class Templates extends Component {
    state = {
    activeItem: "1"
  }
  
  toggle = tab => () => {
    if (this.state.activeItem !== tab) {
    this.setState({
      activeItem: tab
    });
    }
  }

    
  render() {
    const imagesPath = Constants.imagespath;

    const isMobile = window.innerWidth <= 500;

    return (
      <div className="container-fluid">

    { isMobile ?  
     (<div class="col-md-2">
     <a style={{width:'300px', height:'200px', textAlign: 'right', marginTop: '20px', color:'#000000'}} href="/">Sign out</a></div>) : (<header class="site-inner-header">
        <div class="row">
        <div class="col-md-12">
        <div class="collapse navbar-collapse js-navbar-collapse">
   
      <a style={{width:'1200px', height:'70px', textAlign: 'right', marginTop: '20px', color:'#000000'}} href="/">Sign out</a>
    </div>
                </div>
                </div>
                </header>)}
        <div className="row">
          <div className="col-lg-2 col-md-2 col-sm-12 col-xs-12 bhoechie-tab-container">
            <div className="col-lg-12 col-md-12 col-sm-3 col-xs-3 bhoechie-tab-menu">
              <div className="site-user"> <span className="user">TB</span> <span className="user-name">
                  Lorem Ipusm
                </span>
                <br />
                <div className="pic-btn"> <a href="#">Add Your Picture</a> </div>
              </div>
              <div className="create-btn"> <a href="/cvdashboard">Create CV</a> </div>
              <ul className="site-inner-menu">
                
                <li> <a href="#">Recomended For You</a></li>
                        <li> <a href="#">All Your design </a></li>
                        <li> <a href="#">Brand Kit</a></li>
                        <li> <a href="#">Create Team</a></li>
                        <li> <a href="#">Folder</a></li>
                        <li> <a href="#">Trash</a></li>
              </ul>
            </div>
          </div>
          <div className="col-lg-10 col-md-10 col-sm-12 col-xs-12">
            <div className="inner-banner">
            { isMobile ? 
            (<div><h2> Design Anything </h2>
              <input className="inner-search1" type="search" style={{width:'200px'}}  placeholder="Enter your Keyword" />
              <p> <a href="#">"Creativity is the natural order of life." - Julia Cameron</a> </p></div>) : 
                (<div><h2> Design Anything </h2>
                  <input className="inner-search1" type="search" placeholder="Enter your Keyword" />
                  <p> <a href="#">"Creativity is the natural order of life." - Julia Cameron</a> </p></div>)}
            </div>
            

          </div>
        </div>
      </div>
    );
  }
}

export default Templates;