import React , { Component} from 'react';

import Constants from '../../common/Constants';

import { MDBContainer, MDBTabPane, MDBTabContent, MDBNav, MDBNavItem, MDBNavLink } from "mdbreact";

import { MDBBtn, MDBModal, MDBModalBody, MDBModalHeader, MDBModalFooter } from 'mdbreact';


import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import './style/react-tabs.css';


import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCubes, faFileAlt } from "@fortawesome/free-solid-svg-icons";
import { faAppleAlt } from "@fortawesome/free-solid-svg-icons";
import { faCog } from "@fortawesome/free-solid-svg-icons";
import { faFile } from "@fortawesome/free-solid-svg-icons";
import { faClock } from "@fortawesome/free-solid-svg-icons";
import { faArrowDown } from "@fortawesome/free-solid-svg-icons";
import { faCrown } from "@fortawesome/free-solid-svg-icons";

import { faUpload } from "@fortawesome/free-solid-svg-icons";
import { faTextHeight } from "@fortawesome/free-solid-svg-icons";
import { faFolder } from "@fortawesome/free-solid-svg-icons";
import { faColumns } from "@fortawesome/free-solid-svg-icons";
import { faVideo } from "@fortawesome/free-solid-svg-icons";
import { faEnvelope } from "@fortawesome/free-solid-svg-icons";

import Userpics from './images/300.png';

import Userpics1 from './images/500.jpg';

import Template1 from './images/image1.jpeg';
import Template2 from './images/image3.jpeg';
import Template3 from './images/image2.jpeg';

import pies from './images/piechart.png';
import bars from './images/barchart.jpg';
import lines from './images/linechart.png';

class Templates extends Component {
  
    state = {
      activeItem: "1"
    }

  constructor() {
      super();
      this.state = {
        showPopup: false,
        showDiv: true,
        show:true,
        isActive: true,
        isActive1: false,
        isActive2: false,
        isActive3: false,
        isActive4: false
      };
    }

    Iframe = React.createClass({     
      render: function() {
        return(         
          <div>          
            <iframe src={this.props.src} height={this.props.height} width={this.props.width}/>         
          </div>
        )
      }
    });

    togglePopup() {
      this.setState({
        showPopup: !this.state.showPopup
      });
    }
    
    
    toggle = tab => () => {
      if (this.state.activeItem !== tab) {
      this.setState({
        activeItem: tab
      });
      }
    }

    handleShow = () => {
      this.setState({
        isActive: true,
        isActive1: false,
        isActive2: false,
        isActive3: false,
        isActive4: false,
        isActive5: false,
        isActive6: false,
        isActive7: false
      });
    };

    handleShows = () => {
      this.setState({
        isActive: false,
        isActive1: true,
        isActive2: false,
        isActive3: false,
        isActive5: false,
        isActive6: false,
        isActive7: false

      });
    };

    handleShows1 = () => {
      this.setState({
        isActive: false,
        isActive1: false,
        isActive2: true,
        isActive3: false,
        isActive4: false,
        isActive5: false,
        isActive6: false,
        isActive7: false
      });
    };

    handleShows2 = () => {
      this.setState({
        isActive: false,
        isActive1: false,
        isActive2: false,
        isActive3: true,
        isActive4: false,
        isActive5: false,
        isActive6: false,
        isActive7: false
      });
    };

    handleShows3 = () => {
      this.setState({
        isActive: false,
        isActive1: false,
        isActive2: false,
        isActive3: false,
        isActive4: true,
        isActive5: false,
        isActive6: false,
        isActive7: false
      });
    };
    handleShows4 = () => {
      this.setState({
        isActive: false,
        isActive1: false,
        isActive2: false,
        isActive3: false,
        isActive4: false,
        isActive5: true,
        isActive6: false,
        isActive7: false
      });
    };

    handleShows5 = () => {
      this.setState({
        isActive: false,
        isActive1: false,
        isActive2: false,
        isActive3: false,
        isActive4: false,
        isActive5: false,
        isActive6: true,
        isActive7: false
      });
    };

    handleShows6 = () => {
      this.setState({
        isActive: false,
        isActive1: false,
        isActive2: false,
        isActive3: false,
        isActive4: false,
        isActive5: false,
        isActive6: false,
        isActive7: true
      });
    }
  
    //state = { isOpen: false };
  
    handleShowDialog = () => {
      this.setState({ isOpen: !this.state.isOpen });
      console.log("clicked");
      
    };
  /*
    constructor(props) {
      super(props);
  
      this.state = { isOpen: false };
    }
  
    toggleModal = () => {
      this.setState({
        isOpen: !this.state.isOpen
      });
    }
    */
  
    render() {

      const imagesPath = Constants.imagespath;

      const { showDiv } = this.state.showDiv;

      const template = () => {
        window.location.href = "/Template2";
        console.log("Template Page"); 
       }
  
       const template1 = () => {
        window.location.href = "/SampleTemplate2";
        console.log("Template Page"); 
       }

       const template2 = () => {
        window.location.href = "/SampleTemplate3";
        console.log("Template Page"); 
       }

       const template3 = () => {
        window.location.href = "/SampleTemplate4";
        console.log("Template Page"); 
       }

       const piecharts = () => {
        window.location.href = "/piechart";
        console.log("Template Page"); 
       }

       const barcharts = () => {
        window.location.href = "/barchart";
        console.log("Template Page"); 
       }

       const linecharts = () => {
        window.location.href = "/linechart";
        console.log("Template Page"); 
       }

      return (

      <div class="site-inner-header">
      <div class="container" style={{minHeight:'50px'}}>
          <div class="row">
              <div class="col-md-6">
                  <div class="inner-nav">
                      <ul>
                          <li><a href="index.html" style={{color: '#fff'}}>Home</a></li>
                          <li>File</li>
                          <li><FontAwesomeIcon icon={faCrown} /> Resize</li>
                      </ul>
                  </div>
              </div>
              <div class="col-md-6">
                  <div class="left-inner-nav">
                      <ul>
                          <li><FontAwesomeIcon icon={faCrown} /> Upgrade</li>
                          <li>Share</li>&nbsp;&nbsp;&nbsp;&nbsp;
                          <li><FontAwesomeIcon icon={faArrowDown} /> Download</li>
                      </ul>
                  </div>
              </div>
          </div>
      </div>
    <div class="grey-bg mg55" style={{minHeight:'100%'}}>    
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 bhoechie-tab-container">
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-3 bhoechie-tab-menu">
                    <div class="list-group">
                    {this.state.isActive ?(
                        <a  href="#" onClick={this.handleShow} class="list-group-item active text-center"> <FontAwesomeIcon icon={faColumns} />
                            <br />Template </a>
                     ) : (    
                      <a  href="#" onClick={this.handleShow} class="list-group-item text-center"> <FontAwesomeIcon icon={faColumns} />
                      <br />Template </a>
                     )}   
                     {this.state.isActive1 ?(
                        <a href="#" onClick={this.handleShows} class="list-group-item active text-center"> <FontAwesomeIcon icon={faVideo} />
                            <br />Photos </a>
                      ) : (    
                        <a href="#" onClick={this.handleShows} class="list-group-item text-center"> <FontAwesomeIcon icon={faVideo} />
                            <br />Photos </a>  
                      )} 
                      {this.state.isActive2 ?(       
                        <a href="#" onClick={this.handleShows1} class="list-group-item active text-center"> <FontAwesomeIcon icon={faEnvelope} />
                            <br />Elements </a>
                       ) : ( 
                        <a href="#" onClick={this.handleShows1} class="list-group-item text-center"> <FontAwesomeIcon icon={faEnvelope} />
                        <br />Elements </a>  
                      )}   
                      {this.state.isActive3 ?(  
                        <a href="#" onClick={this.handleShows2} class="list-group-item active text-center"> <FontAwesomeIcon icon={faTextHeight} />
                            <br /> Text </a>
                       ) : (
                        <a href="#" onClick={this.handleShows2} class="list-group-item text-center"> <FontAwesomeIcon icon={faTextHeight} />
                        <br /> Text </a>
                       )} 
                       {this.state.isActive4 ?(
                        <a href="#" onClick={this.handleShows3} class="list-group-item active text-center"> <FontAwesomeIcon icon={faUpload} />
                            <br /> Upload </a>
                        ) : (
                          <a href="#" onClick={this.handleShows3} class="list-group-item text-center"> <FontAwesomeIcon icon={faUpload} />
                          <br /> Upload </a>
                        )}  
                       {this.state.isActive5 ?(
                        <a href="#" onClick={this.handleShows4} class="list-group-item active text-center"> <FontAwesomeIcon icon={faFolder} />
                            <br /> Folder </a>
                        ) : (
                          <a href="#" onClick={this.handleShows4} class="list-group-item text-center"> <FontAwesomeIcon icon={faFolder} />
                          <br /> Folder </a>  
                       )}    
                       {this.state.isActive6 ?(
                        <a href="#" onClick={this.handleShows5} class="list-group-item active text-center"> <FontAwesomeIcon icon={faFolder} />
                            <br /> Chart </a>
                        ) : (
                          <a href="#" onClick={this.handleShows5} class="list-group-item text-center"> <FontAwesomeIcon icon={faFolder} />
                          <br /> Chart </a>  
                       )}    
                    </div>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-9 col-xs-9 bhoechie-tab">
                {this.state.isActive6 &&
                    <div class="bhoechie-tab-content active">
                        <div class="inner-search">
                            <input type="search" placeholder="Search Your Content"></input> </div>
                        <div class="inner-template">
                            <h4>Charts</h4>
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-6"> <img onClick={piecharts} src={pies} class="img-responsive" alt="" style={{backgroundColor:'coral'}}></img> </div>
                                <div class="col-md-6 col-sm-6 col-xs-6"> <img onClick={barcharts} src={bars} class="img-responsive" alt="" style={{backgroundColor:'coral'}}></img> </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-6"> <img onClick={linecharts} alt="" src={lines} class="img-responsive" style={{backgroundColor:'coral'}}></img>  </div>
                                <div class="col-md-6 col-sm-6 col-xs-6"> <img onClick={this.handleShows6} src={bars} class="img-responsive" alt="" style={{backgroundColor:'coral'}}></img> </div>
                            </div>
                            
                        </div>
                    </div>
                }  
                {this.state.isActive &&
                    <div class="bhoechie-tab-content active">
                        <div class="inner-search">
                            <input type="search" placeholder="Search Your Content"></input> </div>
                        <div class="inner-template">
                            <h4>Sample Templates</h4>
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-6"> <img onClick={template1} src={Template1} class="img-responsive" alt="" /> </div>
                                <div class="col-md-6 col-sm-6 col-xs-6"> <img onClick={template2} src={Template2} class="img-responsive" alt="" /> </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-6"> <img onClick={template3} src={Template3} class="img-responsive" alt="" /> </div>
                                <div class="col-md-6 col-sm-6 col-xs-6"> <img src={Userpics} class="img-responsive" alt="" /> </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-6"> <img src={Userpics} class="img-responsive" alt="" /> </div>
                                <div class="col-md-6 col-sm-6 col-xs-6"> <img src={Userpics} class="img-responsive" alt="" /> </div>
                            </div>
                        </div>
                    </div>
                }    
                {this.state.isActive7 &&
                    <div class="bhoechie-tab-content active">
                        <div class="inner-search">
                            <input type="search" placeholder="Search Your Content"></input> </div>
                        <div class="inner-template">
                            <h4>Sample Templates</h4>
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-6"> <img onClick={template1} src={Template1} class="img-responsive" alt="" /> </div>
                                <div class="col-md-6 col-sm-6 col-xs-6"> <img onClick={template2} src={Template2} class="img-responsive" alt="" /> </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-6"> <img onClick={template3} src={Template3} class="img-responsive" alt="" /> </div>
                                <div class="col-md-6 col-sm-6 col-xs-6"> <img src={Userpics} class="img-responsive" alt="" /> </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-6"> <img src={Userpics} class="img-responsive" alt="" /> </div>
                                <div class="col-md-6 col-sm-6 col-xs-6"> <img src={Userpics} class="img-responsive" alt="" /> </div>
                            </div>
                        </div>
                    </div>
                    }
                    {this.state.isActive1 &&
                    <div class="bhoechie-tab-content active">
                        <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32. The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32. </p>
                    </div>
                    }   
                    {this.state.isActive2 &&
                    <div class="bhoechie-tab-content active">
                        <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32. The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32. </p>
                    </div>
                    }
                    {this.state.isActive3 &&
                    <div class="bhoechie-tab-content active">
                        <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32. The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32. </p>
                    </div>
                    }  
                    {this.state.isActive4 &&   
                    <div class="bhoechie-tab-content active">
                        <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32. The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32. </p>
                    </div>
                    }
                    {this.state.isActive5 &&  
                    <div class="bhoechie-tab-content active">
                        <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32. The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32. </p>
                    </div>
                    }
                </div>
            </div>
            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-3">
                <div class="inner-sidepage"> <img src={Userpics1} class="img-responsive" /> </div>
                
            </div>
        </div>
        </div>
        </div>
    </div>
    
    );
  }
}

export default Templates;