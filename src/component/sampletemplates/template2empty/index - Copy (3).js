import React , { Component} from 'react';

import Constants from '../common/Constants';

import { MDBContainer, MDBTabPane, MDBTabContent, MDBNav, MDBNavItem, MDBNavLink } from "mdbreact";

import { MDBBtn, MDBModal, MDBModalBody, MDBModalHeader, MDBModalFooter } from 'mdbreact';

import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';

//import './style/react-tabs.css';

import './css/main.css';

//import './css/main.css.map';
//import './css/main.scss';

import style from './css/main.scss';

import loremIpsum from 'react-lorem-ipsum';

import styles from './css/style.scss';

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCubes, faFileAlt } from "@fortawesome/free-solid-svg-icons";
import { faAppleAlt } from "@fortawesome/free-solid-svg-icons";
import { faCog } from "@fortawesome/free-solid-svg-icons";
import { faFile } from "@fortawesome/free-solid-svg-icons";
import { faClock } from "@fortawesome/free-solid-svg-icons";
import { faArrowDown } from "@fortawesome/free-solid-svg-icons";
import { faCrown } from "@fortawesome/free-solid-svg-icons";

import { faUpload } from "@fortawesome/free-solid-svg-icons";
import { faTextHeight } from "@fortawesome/free-solid-svg-icons";
import { faFolder } from "@fortawesome/free-solid-svg-icons";
import { faColumns } from "@fortawesome/free-solid-svg-icons";
import { faVideo } from "@fortawesome/free-solid-svg-icons";
import { faEnvelope } from "@fortawesome/free-solid-svg-icons";

import A4 from '../baseTemplate'
import { EditableText, List, RowTexts, HeaderPic } from '../core'

import Userpics from './128.jpg';

import cs from 'classnames';

import RaisedButton from 'material-ui/RaisedButton'
////import cs from 'classnames'
import ph from './placeholder.jpg'
import phimg from './128.jpg'
import FileReaderInput from 'react-file-reader-input'

//import Userpics1 from './images/500.jpg';

import phpie from './charts7.png';

import Draggable from 'react-draggable';


import { Rnd } from "react-rnd";


import { DraggableEventHandler, default as DraggableRoot } from "react-draggable";
import { Enable, Resizable, ResizeDirection } from "re-resizable";

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

class Templates extends Component {
  
    state = {
      activeItem: "1"
    }

  constructor(props) {
      super(props);
      this.state = {
        showPopup: false,
        showDiv: true,
        show:true,
        isActive: true,
        isActive1: false,
        isActive2: false,
        isActive3: false,
        isActive4: false
      };

      this.state = {
        isOnHonver: false,
        imgSrc: phimg
      }

      this.state = {
        cv: {
          name: 'Wee',
          nickname: 'fi3ework',
        }
      }
    }
  
    _arrayBufferToBase64(buffer) {
      let binary = ''
      let bytes = new Uint8Array(buffer)
      let len = bytes.byteLength
      for (let i = 0; i < len; i++) {
        binary += String.fromCharCode(bytes[i])
      }
      return window.btoa(binary)
    }
  
    upload = (e, results) => {
      let reader = results[0][0].target
      let that = this
      let base64 = 'data:image/jpg;base64, ' + this._arrayBufferToBase64(reader.result)
      that.img.setAttribute('src', base64)
    }

    uploads = (e, results) => {
      let reader = results[0][0].target
      let that = this
      let base64 = 'data:image/jpg;base64, ' + this._arrayBufferToBase64(reader.result)
      that.imgs.setAttribute('src', base64)
    }

    togglePopup() {
      this.setState({
        showPopup: !this.state.showPopup
      });
    }
    
    toggle = tab => () => {
      if (this.state.activeItem !== tab) {
      this.setState({
        activeItem: tab
      });
      }
    }

    handleShow = () => {
      this.setState({
        isActive: true,
        isActive1: false,
        isActive2: false,
        isActive3: false,
        isActive4: false,
        isActive5: false
      });
    };

    handleShows = () => {
      this.setState({
        isActive: false,
        isActive1: true,
        isActive2: false,
        isActive3: false,
        isActive5: false

      });
    };

    handleShows1 = () => {
      this.setState({
        isActive: false,
        isActive1: false,
        isActive2: true,
        isActive3: false,
        isActive4: false,
        isActive5: false
      });
    };

    handleShows2 = () => {
      this.setState({
        isActive: false,
        isActive1: false,
        isActive2: false,
        isActive3: true,
        isActive4: false,
        isActive5: false
      });
    };

    handleShows3 = () => {
      this.setState({
        isActive: false,
        isActive1: false,
        isActive2: false,
        isActive3: false,
        isActive4: true,
        isActive5: false
      });
    };
    handleShows4 = () => {
      this.setState({
        isActive: false,
        isActive1: false,
        isActive2: false,
        isActive3: false,
        isActive4: false,
        isActive5: true
      });
    };
  
    //state = { isOpen: false };
  
    handleShowDialog = () => {
      this.setState({ isOpen: !this.state.isOpen });
      console.log("clicked");
      
    };
  /*
    constructor(props) {
      super(props);
  
      this.state = { isOpen: false };
    }
  
    toggleModal = () => {
      this.setState({
        isOpen: !this.state.isOpen
      });
    }
    */

   state = {
    drag: false
  }
  dropRef = React.createRef()
  handleDrag = (e) => {
    e.preventDefault()
    e.stopPropagation()
  }
  handleDragIn = (e) => {
    e.preventDefault()
    e.stopPropagation()
    this.dragCounter++
    if (e.dataTransfer.items && e.dataTransfer.items.length > 0) {
      this.setState({drag: true})
    }
  }
  handleDragOut = (e) => {
    e.preventDefault()
    e.stopPropagation()
    this.dragCounter--
    if (this.dragCounter === 0) {
      this.setState({drag: false})
    }
  }
  handleDrop = (e) => {
    e.preventDefault()
    e.stopPropagation()
    this.setState({drag: false})
    if (e.dataTransfer.files && e.dataTransfer.files.length > 0) {
      this.props.handleDrop(e.dataTransfer.files)
      e.dataTransfer.clearData()
      this.dragCounter = 0    
      //let reader = results[0][0].target
      let that = this
      let base64 = 'data:image/jpg;base64, ' + this._arrayBufferToBase64(e.dataTransfer.files)
      that.img.setAttribute('src', base64)
    }
  }
  componentDidMount() {
    let div = this.dropRef.current
    div.addEventListener('dragenter', this.handleDragIn)
    div.addEventListener('dragleave', this.handleDragOut)
    div.addEventListener('dragover', this.handleDrag)
    div.addEventListener('drop', this.handleDrop)
  }
  componentWillUnmount() {
    let div = this.dropRef.current
    div.removeEventListener('dragenter', this.handleDragIn)
    div.removeEventListener('dragleave', this.handleDragOut)
    div.removeEventListener('dragover', this.handleDrag)
    div.removeEventListener('drop', this.handleDrop)
  }
  
    render() {

      const imagesPath = Constants.imagespath;

      //const { showDiv } = this.state.showDiv;

      const template = () => {
        window.location.href = "/Template2";
        console.log("Template Page"); 
       }
  
      return (

        <A4>
        <article className={cs({
          [style.cv]: true,
         exportRoot: true
        })}>
          <HeaderPic style={{
            padding: '0px 0px 0px 0px'
          }}>
        <header className={style.header}>
          
        <div class="resume-wrapper">
        
        <section class="profile section-padding" style={{width:'40%'}}>
        
            <div class="container">
            <div class="picture-resume-wrapper" style={{width:'15%'}}>
            <div class="picture-resume">
            <div
            className={style.headerPicWrapper}
            style={this.props.style}
            onMouseEnter={() => this.setState({ isOnHonver: true })}
            onMouseLeave={() => this.setState({ isOnHonver: false })}
        >   
        <img
          src={phimg}
          ref={(node) => { this.img = node }}
        />
        <FileReaderInput
          as="buffer"
          onChange={this.upload}>
          
        <RaisedButton
            label="CHANGE PIC"
            className={cs(
              {
                [style.changePic]: true,
                changePic: true
              }
            )}
            style={{
              position: 'absolute',
              top: 0,
              right: 10,
              display: this.state.isOnHonver ? 'block' : 'none',
              zIndex: 0,
            }}
          />
        </FileReaderInput>
        <div className={style.children}>
          {this.props.children}
        </div>
      </div>
        
      </div>
             <div class="clearfix"></div>
      </div>
          <div class="name-wrapper" style={{width:'15%'}}>
            <EditableText
                    tagName="h1"
                    html='John <br/> Anderson'
                    className={styles.sectionTitles}
                  />
          </div>

          <div class="clearfix"></div>
          <div class="contact-info clearfix">
          <div class="picture-resume-wrapper" style={{width:'300px',height:'300px'}}>  
          <div class="picture-resume">
          <div style={{width:'300px',height:'300px'}}
            className={style.headerPicWrapper}
            style={this.props.style}
            onMouseEnter={() => this.setState({ isOnHonver: true })}
            onMouseLeave={() => this.setState({ isOnHonver: false })}
        >   
        <img style={{width:'300px',height:'300px', marginTop:'-100px', marginLeft:'-40px'}}
          src={phpie}
          ref={(node) => { this.imgs = node }}
        />
        <FileReaderInput
          as="buffer"
          onChange={this.uploads}>
          
        <RaisedButton
            label="Add Graph"
            className={cs(
              {
                [style.changePic]: true,
                changePic: true
              }
            )}
            style={{
              position: 'absolute',
              top: 0,
              right: 10,
              display: this.state.isOnHonver ? 'block' : 'none',
              zIndex: 0,
            }}
          />
        </FileReaderInput>
        <div className={style.children}>
          {this.props.children}
        </div>
      </div>
        
      </div>
    

          </div>    
          </div>  
          <div
        style={{display: 'inline-block', position: 'relative'}}
        ref={this.dropRef}
      >
        {this.state.dragging &&
          <div 
            style={{
              border: 'dashed grey 4px',
              backgroundColor: 'rgba(255,255,255,.8)',
              position: 'absolute',
              top: 0,
              bottom: 0,
              left: 0, 
              right: 0,
              zIndex: 9999
            }}
          >
            <div 
              style={{
                position: 'absolute',
                top: '0%',
                right: 0,
                left: 0,
                textAlign: 'center',
                color: 'grey',
                fontSize: 36
              }}
            >
              <Draggable
        handle=".handle"
        defaultPosition={{x: 0, y: 0}}
        position={null}
        grid={[15, 15]}
        scale={1}
        onStart={this.handleStart}
        onDrag={this.handleDrag}
        onStop={this.handleStop}>
        <Resizable
          ref={this.refResizable}
          defaultSize={20}
        > <div>drop here :)</div>
      </Resizable>
      </Draggable>
            </div>
          </div>
        }
        {this.props.children}
      </div>
          <div class="clearfix"></div>
          <div class="contact-info clearfix">
              <ul class="list-titles" style={{width:'15%', marginTop:'-100px'}}>
                  <li><EditableText
                    tagName="h4"
                    html='<b>Call</b>'
                    className={styles.name}
                  /></li>
                  <li><EditableText
                    tagName="h4"
                    html='<b>Mail</b>'
                    className={styles.name}
                  /></li>
                  <li><EditableText
                    tagName="h4"
                    html='<b>Web</b>'
                    className={styles.name}
                  /></li>
                  <li><EditableText
                    tagName="h4"
                    html='<b>Home</b>'
                    className={styles.name}
                  /></li>
              </ul>
            <ul class="list-content" style={{width:'15%', marginTop:'-100px'}}>
                <li><EditableText
                    tagName="h4"
                    html='<b>+34 123 456 789</b>'
                    className={styles.name}
                  /></li>
                <li><EditableText
                    tagName="h4"
                    html='<b>j.anderson@gmail.com</b>'
                    className={styles.name}
                  /></li> 
                <li><a href="#"><EditableText
                    tagName="h4"
                    html='<b>janderson.com</b>'
                    className={styles.name}
                  /></a></li> 
                <li><EditableText
                    tagName="h4"
                    html='<b>Los Angeles, CA</b>'
                    className={styles.name}
                  /></li> 
            </ul>
          </div>
          <div class="contact-presentation"> 
          <div
        style={{display: 'inline-block', position: 'relative'}}
        ref={this.dropRef}
      >
        {this.state.dragging &&
          <div 
            style={{
              border: 'dashed grey 4px',
              backgroundColor: 'rgba(255,255,255,.8)',
              position: 'absolute',
              top: 0,
              bottom: 0,
              left: 0, 
              right: 0,
              zIndex: 9999
            }}
          >
            <div 
              style={{
                position: 'absolute',
                top: '00%',
                right: 0,
                left: 0,
                textAlign: 'center',
                color: 'grey',
                fontSize: 36
              }}
            >
              <Draggable
        handle=".handle"
        defaultPosition={{x: 0, y: 0}}
        position={null}
        grid={[15, 15]}
        scale={1}
        onStart={this.handleStart}
        onDrag={this.handleDrag}
        onStop={this.handleStop}>
        <Resizable
          ref={this.refResizable}
          defaultSize={20}
        > <div>drop here :)</div>
      </Resizable>
      </Draggable>
            </div>
          </div>
        }
        {this.props.children}
      </div>
              <EditableText
                    tagName="h4"
                    html='<p>Lorem</span> ipsum dolor sit amet, consectetur adipiscing elit. Vivamus euismod congue nisi, nec consequat quam. In consectetur faucibus turpis eget laoreet. Sed nec imperdiet purus. </p>'
                    className={styles.name}
                  /> 
          </div>
          <div class="contact-social clearfix">
              <ul class="list-titles" style={{width:'15%'}}>
                  <li>Twitter</li>
                  <li>Dribbble</li>
                  <li>Codepen</li>
              </ul>
            <ul class="list-content"> 
                  <li><a href="">@janderson</a></li> 
                  <li><a href="">janderson</a></li> 
                  <li><a href="">janderson</a></li> 
              </ul>
          </div>
            </div>
        </section>
         
      <section class="experience section-padding">
      
      <div
        style={{display: 'inline-block', position: 'relative'}}
        ref={this.dropRef}
      >
        {this.state.dragging &&
          <div 
            style={{
              border: 'dashed grey 4px',
              backgroundColor: 'rgba(255,255,255,.8)',
              position: 'absolute',
              top: 0,
              bottom: 0,
              left: 0, 
              right: 0,
              zIndex: 9999
            }}
          >
            <div 
              style={{
                position: 'absolute',
                top: '00%',
                right: 0,
                left: 0,
                textAlign: 'center',
                color: 'grey',
                fontSize: 36
              }}
            >
              <Draggable
        handle=".handle"
        defaultPosition={{x: 0, y: 0}}
        position={null}
        grid={[15, 15]}
        scale={1}
        onStart={this.handleStart}
        onDrag={this.handleDrag}
        onStop={this.handleStop}>
        <Resizable
          ref={this.refResizable}
          defaultSize={20}
        > <div>drop here :)</div>
      </Resizable>
      </Draggable>
            </div>
          </div>
        }
        {this.props.children}
      </div>
      
          <div class="container">
              <h3 class="experience-title"><EditableText
                    tagName="h4"
                    html='Experience'
                    className={styles.name}
                  /></h3>
          
          <div class="experience-wrapper">
              <div class="company-wrapper clearfix">
                  <div class="experience-title"><Draggable
        handle=".handle"
        defaultPosition={{x: 0, y: 0}}
        position={null}
        grid={[15, 15]}
        scale={1}
        onStart={this.handleStart}
        onDrag={this.handleDrag}
        onStop={this.handleStop}>
        <Resizable
          ref={this.refResizable}
          defaultSize={20}
        ><EditableText
                    tagName="h4"
                    html='Company name'
                    className={styles.name}
                  /><Draggable
                  handle=".handle"
                  defaultPosition={{x: 0, y: 0}}
                  position={null}
                  grid={[15, 15]}
                  scale={1}
                  onStart={this.handleStart}
                  onDrag={this.handleDrag}
                  onStop={this.handleStop}>
                  <Resizable
                    ref={this.refResizable}
                    defaultSize={20}
                  >
                  <canvas
          src={phimg} style={{width:'auto', height:'auto'}}
          ref={(node) => { this.img = node }}
        />
        </Resizable>
      </Draggable>
                  </Resizable>
      </Draggable>
         
        
              <Draggable
        handle=".handle"
        defaultPosition={{x: 0, y: 0}}
        position={null}
        grid={[15, 15]}
        scale={1}
        onStart={this.handleStart}
        onDrag={this.handleDrag}
        onStop={this.handleStop}>
        <Resizable
          ref={this.refResizable}
          defaultSize={20}
        >   <div
        style={{display: 'inline-block', position: 'relative'}}
        ref={this.dropRef}
      >
        {this.state.dragging &&
          <div 
            style={{
              border: 'dashed grey 4px',
              backgroundColor: 'rgba(255,255,255,.8)',
              position: 'absolute',
              top: 0,
              bottom: 0,
              left: 0, 
              right: 0,
              zIndex: 9999
            }}
          >
            <div 
              style={{
                position: 'absolute',
                top: '00%',
                right: 0,
                left: 0,
                textAlign: 'center',
                color: 'grey',
                fontSize: 36
              }}
            >
              <Draggable
        handle=".handle"
        defaultPosition={{x: 0, y: 0}}
        position={null}
        grid={[15, 15]}
        scale={1}
        onStart={this.handleStart}
        onDrag={this.handleDrag}
        onStop={this.handleStop}>
        <Resizable
          ref={this.refResizable}
          defaultSize={20}
        > <div>drop here :)</div>
      </Resizable>
      </Draggable>
            </div>
          </div>
        }
        {this.props.children}
      </div>
     
      
      </Resizable>
      </Draggable>
      </div> 
      
     
                  
              <div class="time"><EditableText
                    tagName="h4"
                    html='Nov 2012 - Present'
                    className={styles.name}
                  /></div> 
              </div>
            
            <div class="job-wrapper clearfix">
                <div class="experience-title"><EditableText
                    tagName="h4"
                    html='Front End Developer'
                    className={styles.name}
                  /> </div> 
              <div class="company-description">
                  <p style={{width:'45%', color:'#9099a0'}}>
                  <EditableText
                    tagName="h4"
                    html='Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce a elit facilisis, adipiscing leo in, dignissim magna.</b'
                    className={styles.name}
                  /></p>  
              </div>
            </div>
            
            <div class="company-wrapper clearfix">
                  <div class="experience-title"><EditableText
                    tagName="h4"
                    html='Company name'
                    className={styles.name}
                  /></div> 
              <div class="time"><EditableText
                    tagName="h4"
                    html='Nov 2010 - Present'
                    className={styles.name}
                  /></div> 
              </div>
            
             <div class="job-wrapper clearfix">
                <div class="experience-title"><EditableText
                    tagName="h4"
                    html='Freelance, Web Designer / Web Developer'
                    className={styles.name}
                  /></div> 
              <div class="company-description">
                  <p style={{width:'45%', color:'#9099a0'}}>
                  <EditableText
                    tagName="h4"
                    html='Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce a elit facilisis, adipiscing leo in, dignissim magna.'
                    className={styles.name}
                  /></p>  
              </div>
            </div>
            
            <div class="company-wrapper clearfix">
                  <div class="experience-title"><EditableText
                    tagName="h4"
                    html='Company name'
                    className={styles.name}
                  /></div> 
              <div class="time"><EditableText
                    tagName="h4"
                    html='Nov 2009 - Nov 2010'
                    className={styles.name}
                  /></div> 
              </div> 
            
             <div class="job-wrapper clearfix">
                <div class="experience-title">Web Designer </div> 
              <div class="company-description">
                  <p style={{width:'45%', color:'#9099a0'}}><EditableText
                    tagName="h4"
                    html='Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce a elit facilisis, adipiscing leo in, dignissim magna.'
                    className={styles.name}
                  /></p>   
              </div>
            </div>
            
          </div>
          
          <div style={{width:'30%'}} class="section-wrapper clearfix">
              <h3 class="section-title">Skills</h3>  
                <ul>
                    <li class="skill-percentage"><EditableText
                    tagName="h4"
                    html='HTML / HTML5'
                    className={styles.name}
                  /></li>
                    <li class="skill-percentage"><EditableText
                    tagName="h4"
                    html='CSS / CSS3 / SASS / LESS'
                    className={styles.name}
                  /></li>
                    <li class="skill-percentage"><EditableText
                    tagName="h4"
                    html='Javascript'
                    className={styles.name}
                  /></li>
                    <li class="skill-percentage"><EditableText
                    tagName="h4"
                    html='Jquery'
                    className={styles.name}
                  /></li>
                    <li class="skill-percentage"><EditableText
                    tagName="h4"
                    html='Wordpress'
                    className={styles.name}
                  /></li>
                    <li class="skill-percentage"><EditableText
                    tagName="h4"
                    html='Photoshop'
                    className={styles.name}
                  /></li>
                
                </ul>
            
          </div>
          
          <div class="section-wrapper clearfix">
            <h3 class="section-title">Hobbies</h3>  
            <p style={{width:'45%', color:'#9099a0'}}><EditableText
                    tagName="h4"
                    html='Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce a elit facilisis, adipiscing leo in, dignissim magna.'
                    className={styles.name}
                  /></p>
            
            <p style={{color:'#9099a0'}}><EditableText
                    tagName="h4"
                    html='Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce a elit facilisis, adipiscing leo in, dignissim magna.'
                    className={styles.name}
                  /></p> 
          </div>
          
          </div>
      </section>
      
      <div class="clearfix"></div>
    </div>

    
    </header>
    </HeaderPic>
    </article>
    </A4>
          
    
    );
  }
}

export default Templates;