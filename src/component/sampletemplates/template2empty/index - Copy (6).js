import React , { Component} from 'react';

import Constants from '../common/Constants';

import { MDBContainer, MDBTabPane, MDBTabContent, MDBNav, MDBNavItem, MDBNavLink } from "mdbreact";

import { MDBBtn, MDBModal, MDBModalBody, MDBModalHeader, MDBModalFooter } from 'mdbreact';

import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';

import Dropzone from 'react-dropzone';

//import './style/react-tabs.css';

import './css/main.css';

//import './css/main.css.map';
//import './css/main.scss';

import style from './css/main.scss';

import loremIpsum from 'react-lorem-ipsum';

import styles from './css/style.scss';

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCubes, faFileAlt } from "@fortawesome/free-solid-svg-icons";
import { faAppleAlt } from "@fortawesome/free-solid-svg-icons";
import { faCog } from "@fortawesome/free-solid-svg-icons";
import { faFile } from "@fortawesome/free-solid-svg-icons";
import { faClock } from "@fortawesome/free-solid-svg-icons";
import { faArrowDown } from "@fortawesome/free-solid-svg-icons";
import { faCrown } from "@fortawesome/free-solid-svg-icons";

import { faUpload } from "@fortawesome/free-solid-svg-icons";
import { faTextHeight } from "@fortawesome/free-solid-svg-icons";
import { faFolder } from "@fortawesome/free-solid-svg-icons";
import { faColumns } from "@fortawesome/free-solid-svg-icons";
import { faVideo } from "@fortawesome/free-solid-svg-icons";
import { faEnvelope } from "@fortawesome/free-solid-svg-icons";

import A4 from '../baseTemplate'
import { EditableText, List, RowTexts, HeaderPic } from '../core'

import Userpics from './128.jpg';

import cs from 'classnames';

import RaisedButton from 'material-ui/RaisedButton'
////import cs from 'classnames'
import ph from './placeholder.jpg'
import phimg from './128.jpg'
import FileReaderInput from 'react-file-reader-input'

//import Userpics1 from './images/500.jpg';

import phpie from './charts7.png';

//import Draggable from 'react-draggable';

import { Draggable, Droppable } from 'react-drag-and-drop';


import { Rnd } from "react-rnd";


import { DraggableEventHandler, default as DraggableRoot } from "react-draggable";
import { Enable, Resizable, ResizeDirection } from "re-resizable";

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

class Templates extends Component {
  
    state = {
      activeItem: "1"
    }

  constructor(props) {
      super(props);
      this.state = {
        showPopup: false,
        showDiv: true,
        show:true,
        isActive: true,
        isActive1: false,
        isActive2: false,
        isActive3: false,
        isActive4: false
      };

      this.state = {
        isOnHonver: false,
        imgSrc: phimg
      }

      this.state = {
        cv: {
          name: 'Wee',
          nickname: 'fi3ework',
        }
      }
    }
  
    _arrayBufferToBase64(buffer) {
      let binary = ''
      let bytes = new Uint8Array(buffer)
      let len = bytes.byteLength
      for (let i = 0; i < len; i++) {
        binary += String.fromCharCode(bytes[i])
      }
      return window.btoa(binary)
    }
  
    upload = (e, results) => {
      let reader = results[0][0].target
      let that = this
      let base64 = 'data:image/jpg;base64, ' + this._arrayBufferToBase64(reader.result)
      that.img.setAttribute('src', base64)
    }

    uploads = (e, results) => {
      let reader = results[0][0].target
      let that = this
      let base64 = 'data:image/jpg;base64, ' + this._arrayBufferToBase64(reader.result)
      that.imgs.setAttribute('src', base64)
    }

    togglePopup() {
      this.setState({
        showPopup: !this.state.showPopup
      });
    }
    
    toggle = tab => () => {
      if (this.state.activeItem !== tab) {
      this.setState({
        activeItem: tab
      });
      }
    }

    handleShow = () => {
      this.setState({
        isActive: true,
        isActive1: false,
        isActive2: false,
        isActive3: false,
        isActive4: false,
        isActive5: false
      });
    };

    handleShows = () => {
      this.setState({
        isActive: false,
        isActive1: true,
        isActive2: false,
        isActive3: false,
        isActive5: false

      });
    };

    handleShows1 = () => {
      this.setState({
        isActive: false,
        isActive1: false,
        isActive2: true,
        isActive3: false,
        isActive4: false,
        isActive5: false
      });
    };

    handleShows2 = () => {
      this.setState({
        isActive: false,
        isActive1: false,
        isActive2: false,
        isActive3: true,
        isActive4: false,
        isActive5: false
      });
    };

    handleShows3 = () => {
      this.setState({
        isActive: false,
        isActive1: false,
        isActive2: false,
        isActive3: false,
        isActive4: true,
        isActive5: false
      });
    };
    handleShows4 = () => {
      this.setState({
        isActive: false,
        isActive1: false,
        isActive2: false,
        isActive3: false,
        isActive4: false,
        isActive5: true
      });
    };
  
    //state = { isOpen: false };
  
    handleShowDialog = () => {
      this.setState({ isOpen: !this.state.isOpen });
      console.log("clicked");
      
    };
  /*
    constructor(props) {
      super(props);
  
      this.state = { isOpen: false };
    }
  
    toggleModal = () => {
      this.setState({
        isOpen: !this.state.isOpen
      });
    }
    */

   state = {
    drag: false
  }
  dropRef = React.createRef()
  handleDrag = (e) => {
    e.preventDefault()
    e.stopPropagation()
  }
  handleDragIn = (e) => {
    e.preventDefault()
    e.stopPropagation()
    this.dragCounter++
    if (e.dataTransfer.items && e.dataTransfer.items.length > 0) {
      this.setState({drag: true})
    }
  }
  handleDragOut = (e) => {
    e.preventDefault()
    e.stopPropagation()
    this.dragCounter--
    if (this.dragCounter === 0) {
      this.setState({drag: false})
    }
  }
  handleDrop = (e) => {
    e.preventDefault()
    e.stopPropagation()
    this.setState({drag: false})
    if (e.dataTransfer.files && e.dataTransfer.files.length > 0) {
      this.props.handleDrop(e.dataTransfer.files)
      e.dataTransfer.clearData()
      this.dragCounter = 0    
      //let reader = results[0][0].target
      let that = this
      let base64 = 'data:image/jpg;base64, ' + this._arrayBufferToBase64(e.dataTransfer.files)
      that.img.setAttribute('src', base64)
    }
  }
  componentDidMount() {
    let div = this.dropRef.current
    div.addEventListener('dragenter', this.handleDragIn)
    div.addEventListener('dragleave', this.handleDragOut)
    div.addEventListener('dragover', this.handleDrag)
    div.addEventListener('drop', this.handleDrop)
  }
  componentWillUnmount() {
    let div = this.dropRef.current
    div.removeEventListener('dragenter', this.handleDragIn)
    div.removeEventListener('dragleave', this.handleDragOut)
    div.removeEventListener('dragover', this.handleDrag)
    div.removeEventListener('drop', this.handleDrop)
  }
  
    render() {

      const getUploadParams = () => {
        return { url: 'https://httpbin.org/post' }
      }
    
      const handleChangeStatus = ({ meta }, status) => {
        console.log(status, meta)
      }
    
      const handleSubmit = (files, allFiles) => {
        console.log(files.map(f => f.meta))
        allFiles.forEach(f => f.remove())
      }

      const imagesPath = Constants.imagespath;

      //const { showDiv } = this.state.showDiv;

      const template = () => {
        window.location.href = "/Template2";
        console.log("Template Page"); 
       }
  
      return (

        <A4>
        <article className={cs({
          [style.cv]: true,
         exportRoot: true
        })}>
          <HeaderPic style={{
            padding: '0px 0px 0px 0px'
          }}>
        <header className={style.header}>
          
        <div class="resume-wrapper">
          
        
      <section class="experience section-padding">
      
      <div
        style={{display: 'inline-block', position: 'relative'}}
        ref={this.dropRef}
      >
        {this.state.dragging &&
          <div 
            style={{
              border: 'dashed grey 4px',
              backgroundColor: 'rgba(255,255,255,.8)',
              position: 'absolute',
              top: 0,
              bottom: 0,
              left: 0, 
              right: 0,
              zIndex: 9999
            }}
          >
            <div 
              style={{
                position: 'absolute',
                top: '00%',
                right: 0,
                left: 0,
                textAlign: 'center',
                color: 'grey',
                fontSize: 36
              }}
            >
              <Draggable
        handle=".handle"
        defaultPosition={{x: 0, y: 0}}
        position={null}
        grid={[15, 15]}
        scale={1}
        onStart={this.handleStart}
        onDrag={this.handleDrag}
        onStop={this.handleStop}>
        <Resizable
          ref={this.refResizable}
          defaultSize={20}
        > <div>drop here :)</div>
      </Resizable>
      </Draggable>
            </div>
          </div>
        }
        {this.props.children}
      </div>
      
          <div class="container">
              <h3 class="experience-title"><EditableText
                    tagName="h4"
                    html='Experience'
                    className={styles.name}
                  /></h3>
          
          <div class="experience-wrapper">
              <div class="company-wrapper clearfix">
                  <div class="experience-title"><Draggable
        handle=".handle"
        defaultPosition={{x: 0, y: 0}}
        position={null}
        grid={[15, 15]}
        scale={1}
        onStart={this.handleStart}
        onDrag={this.handleDrag}
        onStop={this.handleStop}>
        <Resizable
          ref={this.refResizable}
          defaultSize={20}
        ><EditableText
                    tagName="h4"
                    html='Company name'
                    className={styles.name}
                  />
                  </Resizable>
      </Draggable>
         
        
              
      </div> 
      
            <div style={{width:'100%', color:'#ebebeb'}}>
      
            <Droppable // <= allowed drop types
                onDrop={this.onDrop.bind(this)}>
                  <Draggable
                    handle=".handle"
                    defaultPosition={{x: 0, y: 0}}
                    position={null}
                    grid={[800, 800]}
                    scale={1}
                    onStart={this.handleStart}
                    onDrag={this.handleDrag}
                    onStop={this.handleStop}>
                    <Resizable
                      ref={this.refResizable}
                      defaultSize={800}
                    >
                <ul className="Smoothie"></ul>
                </Resizable>
                </Draggable>
            </Droppable>

            </div>
                  
              <div class="time"><EditableText
                    tagName="h4"
                    html='Nov 2012 - Present'
                    className={styles.name}
                  /></div> 
              </div>
            
            <div class="job-wrapper clearfix">
                <div class="experience-title"><EditableText
                    tagName="h4"
                    html='Front End Developer'
                    className={styles.name}
                  /> </div> 
              <div class="company-description">
                  <p style={{width:'45%', color:'#ebebeb'}}>
                  <EditableText
                    tagName="h4"
                    html='Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce a elit facilisis, adipiscing leo in, dignissim magna.</b'
                    className={styles.name}
                  /></p>  
              </div>
            </div>
           
          </div>
         
          </div>
      </section>
      
      <div class="clearfix"></div>
    </div>

    
    </header>
    </HeaderPic>
    </article>
    </A4>
          
    
    );

    
  }
  onDrop(data) {

    //this.upload(data);

    //alert("data : "+data);

    console.log(data)
    // => banana 
}
}

export default Templates;