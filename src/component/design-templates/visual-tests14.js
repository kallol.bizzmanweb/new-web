import React from "react";
import { render } from "react-dom";
import ReactQuill, { Quill } from "react-quill";
import "react-quill/dist/quill.snow.css";
////import "./styles.css";

const CustomHeart = () => <span>♥</span>;

function insertHeart() {
  const cursorPosition = this.quill.getSelection().index;
  this.quill.insertText(cursorPosition, "♥");
  this.quill.setSelection(cursorPosition + 1);
}

/*
 * Custom toolbar component including the custom heart button and dropdowns
 */
const CustomToolbar = () => (
  <div id="toolbar">
    <select className="ql-font">
      <option value="arial" selected>
        Arial
      </option>
      <option value="comic-sans">Comic Sans</option>
      <option value="courier-new">Courier New</option>
      <option value="georgia">Georgia</option>
      <option value="helvetica">Helvetica</option>
      <option value="lucida">Lucida</option>
    </select>
    <select className="ql-size">
      <option value="10px">Size 1</option>
      <option value="14px">Size 2</option>
      <option value="18px">Size 3</option>
      <option value="20px">Size 4</option>
      <option value="24px">Size 5</option>
      <option value="48px">Size 6</option>
      <option value="100px" selected>
        Size 7
      </option>
      <option value="200px">Size 8</option>
      <option value="300px">Size 9</option>
    </select>
    <button className="ql-bold" />
    <button className="ql-italic" />
    <button className="ql-strike" />
    <button className="ql-list" value="ordered" />
    <button className="ql-list" value="bullet" />
    <button className="ql-underline" />
    <select className="ql-align" />
    <select className="ql-color" />
    <select className="ql-background">      
    </select>
    <button className="ql-clean" />
    <button className="ql-insertHeart">
      <CustomHeart />
    </button>
  </div>
);

// Add sizes to whitelist and register them
/* const Size = Quill.import("formats/size");
Size.whitelist = ["extra-small", "small", "medium", "large","extra-large"];
Quill.register(Size, true);
*/

var Size = Quill.import('attributors/style/size');
Size.whitelist = ['10px','14px','18px','20px','24px', '48px', '100px', '200px', '300px'];
Quill.register(Size, true);

 const colorList = ['#001f3f', '#0074D9', '#7FDBFF', '#39CCCC', '#3D9970', '#2ECC40', '#01FF70', '#FFDC00', '#FF851B', '#FF4136', '#85144b', '#F012BE', '#B10DC9', '#111111', '#AAAAAA', '#DDDDDD', '#FFFFFF'];
   

// Add fonts to whitelist and register them
const Font = Quill.import("formats/font");
Font.whitelist = [
  "arial",
  "comic-sans",
  "courier-new",
  "georgia",
  "helvetica",
  "lucida"
];
Quill.register(Font, true);



/*
 * Editor component with custom toolbar and content containers
 */
class Editor extends React.Component {

  state = { editorHtml: "" };

  handleChange = html => {
    this.setState({ editorHtml: html });
  };

  static modules = {
    toolbar: {
      container: "#toolbar",
      handlers: {
        insertHeart: insertHeart
      }
    }
    
  };

  static formats = [
    "background",
    "header",
    "font",
    "size",
    "bold",
    "italic",
    "underline",
    "strike",
    "blockquote",
    "list",
    "bullet",
    "indent",
    "link",
    "image",
    "color"
  ];

  render() {
    const imgStyle = { height : this.state.height, width: this.state.width} 
    return (
      <div className="text-editor">
        <CustomToolbar />

        

        <div style={{width:'45%', height:'auto', marginLeft:'25%'}}>          
        <ReactQuill
          value={this.state.editorHtml}
          onChange={this.handleChange}
          placeholder={this.props.placeholder}
          modules={Editor.modules}
          formats={Editor.formats}
        />
        </div>
      </div>
    );
  }
}

export default Editor;
