import React, { Component } from "react"
import DonutChart from "react-svg-donut-chart"
 
const dataPie = [
    {value: 100, stroke: "#22594e", strokeWidth: 6},
    {value: 60, stroke: "#2f7d6d"},
    {value: 30, stroke: "#3da18d"},
    {value: 20, stroke: "#69c2b0"},
    {value: 10, stroke: "#a1d9ce"},
  ]

class componentName extends Component {
    render () {
      return (  
  
    <DonutChart data={dataPie} />

    );
    
  }
}

export default componentName;