import React, { Fragment, useState } from 'react';
//import Message from './Message';
//import Progress from './Progress';
import axios from 'axios';

const FileUpload = () => {
const [file, setFile] = useState('');
const [filename, setFilename] = useState('Choose File');
const [uploadedFile, setUploadedFile] = useState({});
const [message, setMessage] = useState('');
const [uploadPercentage, setUploadPercentage] = useState(0);

const onChange = e => {
setFile(e.target.files[0]);
setFilename(e.target.files[0].name);
};

const onSubmit = async e => {
e.preventDefault();
const formData = new FormData();
//formData.append('file', file);

const data = new FormData();
data.append('category_id', '33');
data.append('category_name', '33');
data.append('image_actual', '33');
data.append('image_path', 'http://162.213.248.35:3004/images/uploads/');
data.append('image_slug', '33');
data.append('description', '33');
data.append('userPhoto', file);
data.append('email', '33');

try {
  const res = await axios.post('http://162.213.248.35:3003/api/upload-image', data, {
    headers: {
      'Content-Type': 'multipart/form-data'
    },
    }).then(response => {
        
    alert('File successfully Uploaded:'+response.status);


    })
    .catch((error) => {
        //return  error;
        alert('ERROR1111111111:'+error);
    });;

  const { fileName, filePath } = res.data;
  ////setUploadedFile({ fileName, filePath });
  ////setMessage('File uploaded');
} catch(err) {
 /* if(err.response.status === 500) {
    setMessage('There was a problem witht he server');
  } else {
    setMessage(err.response.data.msg);
  }
  */
}
}

return (
<Fragment>
  
  <form onSubmit={onSubmit}>
    <div className="custom-file mb-4">
      <input
        type="file"
        className="custom-file-input"
        id="customFile"
        onChange={onChange}
      />
      <label className='custom-file-label' htmlFor='customFile'>
        {filename}
      </label>
    </div>

    

    <input
      type="submit"
      value="Upload"
      className="btn btn-primary btn-block mt-4"
    />
  </form>
  { uploadedFile ? <div className="row mt-5">
    <div className="col-md-6 m-auto"></div>
      <h3 classNAme="text-center">{ uploadedFile.fileName }</h3>
      <img style={{ width: '100%' }} src={uploadedFile.filePath} alt="" />
    </div> : null }
</Fragment>
);
};

export default FileUpload;

