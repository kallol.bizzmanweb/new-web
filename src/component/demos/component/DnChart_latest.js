import React, {Component} from 'react';
import Chart from "chart.js";

import CopyToClipboard from 'react-copy-to-clipboard';

class GraphChart extends Component {
    constructor(props) {
        super(props);
        this.myRef = React.createRef();
      }
      componentDidMount() {
        const ctx = this.ctx;
        new Chart(ctx, {
          type: "bar",
          data: {
            labels: ["Red", "Blue", "Yellow"],
            datasets: [
              {
                label: "# of Likes",
                data: [12, 19, 3],
                backgroundColor: [
                  "rgba(255, 99, 132, 0.2)",
                  "rgba(54, 162, 235, 0.2)",
                  "rgba(255, 206, 86, 0.2)"
                ]
              },
              {
                label: "# of Likes",
                data: [-12, -19, -3],
                backgroundColor: [
                  "rgba(255, 99, 132, 0.2)",
                  "rgba(54, 162, 235, 0.2)",
                  "rgba(255, 206, 86, 0.2)"
                ]
              }
            ]
          }
        });
      }
      render() {
        return (
          <div>
            <div mRef={ctx => (this.ctx = ctx)}>
                <canvas width='800' height='300' ref={ctx => (this.ctx = ctx)}/>
             </div>
             
             <div>

    <CopyToClipboard mRef={ctx => (this.ctx = ctx)} onCopy={this.onCopy}>
          <button>Copy</button>
    </CopyToClipboard>

            </div>
            </div>
        )
    }
}
export default GraphChart;


