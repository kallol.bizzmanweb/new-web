import React, { Component } from "react";
import Editor from "react-weblineindia-ck-editor";


class Test extends Component {
  constructor(props) {

    super(props);
    
    this.state = {
      editorHtml :''
    }
    this.handleChange = (html) => {
      this.setState({
        editorHtml : html
      })
    } 
  }
    render(){
      return(
        <div>
         <Editor
          onChange={this.handleChange.bind(this)}
          value={this.state.editorHtml}
          bounds={".app"}
          placeholder="Type Something"
        />
        </div>
      );
    };
}
export default Test;