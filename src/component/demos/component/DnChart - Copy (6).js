import * as React from 'react';
import { IgrDoughnutChartModule } from 'igniteui-react-charts';
import { IgrDoughnutChart } from 'igniteui-react-charts';
import { IgrRingSeriesModule } from 'igniteui-react-charts';
import { IgrRingSeries } from 'igniteui-react-charts';

IgrDoughnutChartModule.register();
IgrRingSeriesModule.register();


export default class DoughnutChart extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [
          { MarketShare: 30, Company: "Google",    },
          { MarketShare: 15, Company: "Microsoft", },
          { MarketShare: 30, Company: "Apple",     },
          { MarketShare: 15, Company: "Samsung",   },
          { MarketShare: 10, Company: "Other",     },
  ] };
    
  }

  render() {

    return (

      <div className="igContainer">
      <IgrDoughnutChart
           height="50px" width="50px">
              <IgrRingSeries name="ring1"
                  dataSource={this.state.data}
                  labelMemberPath="Company"
                  valueMemberPath="MarketShare"/>
      </IgrDoughnutChart>
  </div>

    )
  }
}



