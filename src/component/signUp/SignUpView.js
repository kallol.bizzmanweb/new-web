import React ,{Component} from 'react';
import SignUpForm from './SignUpForm.js';
import LoginHeader from '../LoginHeader.js';


class SignUpView extends Component{

 render(){

 	return(

         <div>
         <LoginHeader/>

         <SignUpForm/>
         
          
         </div>

 		);
 }

}

export default SignUpView;
