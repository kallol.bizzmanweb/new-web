
import axios from 'axios'; 
  
import React,{Component} from 'react'; 
  
class App extends Component { 
   
    state = { 
  
      // Initially, no file is selected 
      selectedFile: null
    }; 
     
    // On file select (from the pop up) 
    onFileChange = event => { 
     
      // Update the state 
      this.setState({ selectedFile: event.target.files[0] }); 
     
    }; 
     
    // On file upload (click the upload button) 
    onFileUpload = () => { 
     
      // Create an object of formData 

      alert(this.state.selectedFile.name);
      
      const data = new FormData();
      data.append('category_id', '33');
      data.append('category_name', '33');
      data.append('image_actual', '33');
      data.append('image_path', '33');
      data.append('image_slug', '33');
      data.append('description', '33');
      data.append('userPhoto', this.state.selectedFile, this.state.selectedFile.name);
      data.append('email', '33');
     
      // Update the formData object 
      //formData.append( 
       /* "myFile", 
        this.state.selectedFile, 
        this.state.selectedFile.name 
       */
      //); 
     
      // Details of the uploaded file 
      console.log(this.state.selectedFile); 

      alert(this.state.selectedFile);
      alert(this.state.selectedFile.name);
      alert(this.state.selectedFile.type);

      const filename = this.state.selectedFile.name;

      const apiUrl = 'http://162.213.248.35:3003/api/upload-image';

      axios(apiUrl, {
        method: 'POST',
        body: data,
        ///userPhoto: this.state.selectedFile, filename,
        headers: {
          'Content-Type': 'multipart/form-data',
        },
       
      }).then(response => {
        
        alert('File successfully Uploaded:'+response.status);
    
    
        })
        .catch((error) => {
            //return  error;
            alert('ERROR1111111111:'+error);
        });

      /*
      //axios.post('http://162.213.248.35:3003/api/login', {email: fields.email, password: fields.loginPassword}).then(function(res) {
      //  let resData = JSON.parse( JSON.stringify(res) );

        //alert(resData);

        //alert(resData.data);

        //alert(resData.status);

        let res = axios.post('http://localhost//webservice/user/uploadImage',
        {
          method: 'post',
          body: data,
          headers: {
            'Content-Type': 'multipart/form-data; ',
          },
        }  

        if(resData.status===200){

        alert("Successfully Login");

        window.location.href="/templates";  

        }else{

          alert("Wrong Login");

        }

        let res = axios.post(
          'http://localhost//webservice/user/uploadImage',
          {
            method: 'post',
            body: data,
            headers: {
              'Content-Type': 'multipart/form-data; ',
            },
          }
        );
        let responseJson = await res.json();
        if (responseJson.status == 1) {
          alert('Upload Successful');
        }


        ////services.setUserData( resData.data );
        ////window.location.href="/";  
      })
      .catch(function(error) {
        self.setState({loginError: 'Please provide correct login details.'});
      }); 

      */
     
      // Request made to the backend api 
      // Send formData object 
      //axios.post("api/uploadfile", formData); 
    }; 
     
    // File content to be displayed after 
    // file upload is complete 
    fileData = () => { 
     
      if (this.state.selectedFile) { 
          
        return ( 
          <div> 
            <h2>File Details:</h2> 
            <p>File Name: {this.state.selectedFile.name}</p> 
            <p>File Type: {this.state.selectedFile.type}</p> 
            <p> 
              Last Modified:{" "} 
              {this.state.selectedFile.lastModifiedDate.toDateString()} 
            </p> 
          </div> 
        ); 
      } else { 
        return ( 
          <div> 
            <br /> 
            <h4>Choose before Pressing the Upload button</h4> 
          </div> 
        ); 
      } 
    }; 
     
    render() { 
     
      return ( 
        <div> 
            <h1> 
              GeeksforGeeks 
            </h1> 
            <h3> 
              File Upload using React! 
            </h3> 
            <div> 
                <input type="file" onChange={this.onFileChange} /> 
                <button onClick={this.onFileUpload}> 
                  Upload! 
                </button> 
            </div> 
          {this.fileData()} 
        </div> 
      ); 
    } 
  } 
  
  export default App; 
