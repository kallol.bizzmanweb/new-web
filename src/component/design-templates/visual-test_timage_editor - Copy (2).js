import React from 'react';
import 'froala-editor/js/froala_editor.pkgd.min.js';
import 'froala-editor/css/froala_style.min.css';
import 'froala-editor/css/froala_editor.pkgd.min.css';
import 'font-awesome/css/font-awesome.css';
import FroalaEditor from 'react-froala-wysiwyg';

class RealTimeFroalaEditor extends React.Component {

  constructor () {
    super();
  }

  componentWillUnmount(){
    //leave the session
    if (this.props.codox) {
      this.props.codox.stop();
    }
  }
  
  componentDidUpdate(prevProps) {
    //if the editor needs to load content from a different doc
    if (this.props.docId!== prevProps.docId) {
      this.startCollaboration();
    }
  }

 startCollaboration = () => {
    const {codox, apiKey, docId, username} = this.props;

  /*  setTimeout(() =>  {
      codox.init({
        app      : 'froala',
        username :  username,
        docId    :  docId,
        apiKey   : apiKey,
        editor   : this.editor
      });
    }, 100);
    */
  }
  
  froalaInitialized = (editor) =>  {
    this.editor = editor;
    this.startCollaboration();
  }

  render () {
    var self = this;
    
    //add a froala initalized handler to bootstrap codox
   /* const config={
      events : {
        'initialized':  function() {
          self.froalaInitialized(this) 
        }
      }
    }
    */

   var config = {
      placeholderText: 'Your notes here....',
      heightMin: 250,
      heightMax: 400,
      colorsHEXInput: true,
      autoFocus: true,
      toolbarBottom: true,
      linkAlwaysBlank: true,
      fontFamilySelection: true,
      fontSizeSelection: true,
      paragraphFormatSelection: true,
      htmlExecuteScripts: true,
      iframe: true,
      tabSpaces: 4,
      pluginsEnabled: ['align', 'charCounter', 'codeBeautifier', 'codeView', 'colors', 'draggable', 'embedly', 'emoticons', 'entities', 'file', 'fontFamily', 'fontSize', 'fullscreen', 'image', 'imageManager', 'inlineStyle', 'lineBreaker', 'link', 'lists', 'paragraphFormat', 'paragraphStyle', 'quickInsert', 'quote', 'save', 'table', 'url', 'video', 'wordPaste', 'color'],
      toolbarButtons: ['bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', '|', 'fontFamily', 'fontSize', 'color', 'inlineStyle', 'paragraphStyle', '|', 'paragraphFormat', 'align', 'formatOL', 'formatUL', 'outdent', 'indent', 'quote', 'check', '|', 'insertLink', 'insertImage', 'insertVideo', 'embedly', 'insertFile', 'insertTable', '|', 'emoticons', 'specialCharacters', 'insertHR', 'selectAll', 'clearFormatting', '|', 'spellChecker', 'help', 'html', '|', 'undo', 'redo', 'textColor', 'backgroundColor'],
      toolbarButtonsMD: ['bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', '|', 'fontFamily', 'fontSize', 'color', 'inlineStyle', 'paragraphStyle', '|', 'paragraphFormat', 'align', 'formatOL', 'formatUL', 'outdent', 'indent', 'quote', 'check', '|', 'insertLink', 'insertImage', 'insertVideo', 'embedly', 'insertFile', 'insertTable', '|', 'emoticons', 'specialCharacters', 'insertHR', 'selectAll', 'clearFormatting', '|', 'spellChecker', 'help', 'html', '|', 'undo', 'redo'],
      toolbarButtonsSM: ['bold', 'italic', 'underline', 'strikeThrough', '|', 'fontFamily', 'fontSize', 'color', '|', 'align', 'formatOL', 'formatUL', 'outdent', 'indent', 'quote', 'check', '|', 'insertLink', 'insertImage', 'insertVideo', 'embedly', 'insertFile', 'insertTable', '|', 'emoticons', 'specialCharacters', 'insertHR', 'selectAll', 'clearFormatting', '|', 'spellChecker', 'help', 'html', '|', 'undo', 'redo'],
      toolbarButtonsXS: ['bold', 'italic', 'underline', '|', 'fontFamily', 'fontSize', 'color', '|', 'align', 'formatOL', 'formatUL', 'outdent', 'indent', 'check', '|', 'insertLink', 'insertImage', 'insertVideo', 'insertFile', 'insertTable', '|', 'insertHR', 'selectAll', 'clearFormatting', '|', 'spellChecker', '|', 'undo', 'redo'],
      //toolbarButtons: ['bold', 'italic', 'underline', 'strikeThrough', 'color', '|', 'paragraphFormat', 'align', 'undo', 'redo', 'html'],
      //Colors list.
      colorsBackground: [
        '#15E67F', '#E3DE8C', '#D8A076', '#D83762', '#76B6D8', 'REMOVE',
        '#1C7A90', '#249CB8', '#4ABED9', '#FBD75B', '#FBE571', '#FFFFFF'
      ],
      colorsDefaultTab: 'background',
      colorsStep: 6,
      colorsText: [
        '#15E67F', '#E3DE8C', '#D8A076', '#D83762', '#76B6D8', 'REMOVE',
        '#1C7A90', '#249CB8', '#4ABED9', '#FBD75B', '#FBE571', '#FFFFFF'
      ]
    };
  
      
    return (
      <FroalaEditor
        config={config}
        model={this.props.model}
        onChange={this.props.onChange}
      />
    )
  }
}

export default RealTimeFroalaEditor;