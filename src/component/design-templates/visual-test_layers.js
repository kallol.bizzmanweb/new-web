import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { LayersManager, Layer } from 'react-layers-manager';
import { Modal } from 'react-bootstrap';



class Apps extends Component {

  render() {

    const SampleModal = () => (
      <Layer>
        <Modal>We have updated our privacy policy :trollface:</Modal>
      </Layer>
    )
    
    const App = () => (
      <div>
      <h1>Hello folks</h1>
      <SampleModal />
      <SampleModal />
      </div>
    )
    return (<LayersManager>
    <App />
  </LayersManager>);
};
}

export default Apps;