import React,{Component} from 'react';

import download1 from '../images/download1.png';
import download from '../images/download.png';
import download3 from '../images/download3.png';
import siteTestimonial1 from '../images/siteTestimonial1.jpg';

class DesignTemplate extends Component{

render(){

 
  return (
   <div>
        <section class="site-bg-icon" style={{height:'0px'}}>
        <div class="container">
            <h2>Create Design In 2 Minutes</h2>
            <center><span class="divider"></span></center>
            <div class="row">
                <div class="col-md-1"></div>
                <div class="col-md-3">
                    <div class="dwnld-icon"> <img src={download1} alt ="download sample1" class="img-responsive" /> </div>
                </div>
                <div class="col-md-3">
                    <div class="dwnld-icon"> <img src={download} alt = "download sample2" class="img-responsive" /> </div>
                </div>
                <div class="col-md-3">
                    <div class="dwnld-icon"> <img src={download3} alt= "download sample3" class="img-responsive" /> </div>
                </div>
                <div class="col-md-1"></div>
            </div>
        </div>
    </section>
    <br/>
 
    <section class="new-tech">
        <div class="container">
            <h2 class="template-header" style={{marginBottom:'-50%', marginTop:'32px'}}>
                   Learn New Technology
                 </h2>
            <center style={{marginTop:'-80%'}}><span class="underline"></span></center> <img src={siteTestimonial1} alt = "site Template"  class="img-responsive" /> </div>
    </section>
    </div>
    
 );

}


}

export default DesignTemplate;