import React, { Component } from 'react';
import Dropzone from 'react-dropzone';
import Image from 'react-image-resizer';
//import * as cloudfront from '../../services/config'

import phimg from './128.jpg';

export default class ImageUpload extends Component {
  constructor(props) {
    super(props)
    
    this.state = { preview: null }
    this.handleDrop = this.handleDrop.bind(this)
  }
  
  handleDrop([{ preview }]) {
    this.setState({ preview })
  }
  
  render() {
    const { preview } = this.state
    
    return (    
      
      <Dropzone onDrop={acceptedFiles => console.log(acceptedFiles)}>
      {({getRootProps, getInputProps}) => (
        <section>
          <div {...getRootProps()}>
            <input {...getInputProps()} />
            <p>Drag 'n' drop some files here, or click to select files</p>
          </div>
        </section>
      )}
    </Dropzone>
      
    )
  }
}

//export default ImageUpload;