////import React from 'react'
import reactCSS from 'reactcss'
import { SketchPicker } from 'react-color';
import { MDBCard, MDBCardBody, MDBBtn, MDBRow, MDBCol } from "mdbreact";

import { Editor } from 'react-draft-wysiwyg';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';

import React, { Component } from "react";

import { EditorState } from "draft-js";

////import "../../../../node_modules/react-draft-wysiwyg/dist/react-draft-wysiwyg.css";

class TextEditor extends Component {
constructor(props) {
super(props);

this.state = {
  editorState: EditorState.createEmpty(),
  hideToolbar: true
};

}

onEditorStateChange = editorState => {
this.setState({
editorState
});
};

onContentStateChange = contentState => {
this.setState({
contentState
});
};

render() {
const { editorState } = this.state;

return (
  <Editor
    editorState={editorState}
    toolbarOnFocus={!this.state.hideToolbar}
    toolbarHidden={this.state.hideToolbar}
    wrapperClassName="demo-wrapper"
    editorClassName="demo-editor"
    toolbarClassName="toolbar-class"
    value={"Resume Ranadip "}
    onFocus={() => {
      this.setState({ hideToolbar: false });
    }}
    onBlur={() => {
      this.setState({ hideToolbar: true });
    }}
    onEditorStateChange={this.onEditorStateChange}
    onContentStateChange={this.onContentStateChange}
    placeholder={"  Write Here"}
    editorStyle={{
      backgroundColor: "#fff",
      padding: 0,
      borderWidth: 0,
      borderColor: "transparent",
      height: 60
    }}
  />
);

}
}

export default TextEditor;