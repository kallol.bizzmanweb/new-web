import React from 'react';
import './App.css';
import LoginView from './component/LoginView.js';
import CVIndex from './component/CVIndex.js';
import SignUpView from './component/signUp/SignUpView.js';

import Templates from './component/design-templates/templates';

import cvdashboard from './component/design-templates/cv_dashboard';

import basiceditor from './component/design-templates/basic-editors';

import visualeditor from './component/design-templates/visual-editor';

import visualeditors from './component/design-templates/visual-editors';

import visualeditortemplate from './component/design-templates/visual-editor-template';


import visualeditortemplate4 from './component/design-templates/visual-editor-template_ne';


import SampleTemplate2 from './component/sampletemplates/template2/index.js';

import SampleTemplate3 from './component/sampletemplates/template3/index.js';

import SampleTemplate4 from './component/sampletemplates/template4/index.js';

import SampleTemplate5 from './component/sampletemplates/template5/index.js';

import SampleTemplate6 from './component/sampletemplates/template6/index.js';

import emptytemplate from './component/sampletemplates/emptytemplate/index.js';

//import piechart from './component/design-templates/piechart.js';

import charts from './component/demo/container/App.js';

import areachart from './component/demos/component/AreaChart.js';
import piechart from './component/demos/component/DnChart.js';
import linechart from './component/demos/component/LineChart.js';

import barchart from './component/demos/component/BarChart.js';


import piecharts from './component/piechartact.js';

import dragdroppg from './component/design-templates/dragdrop.js';

import editortest from './component/design-templates/visual-test.js';

import editortests from './component/design-templates/visual-test-pick.js';

import colourtest from './component/design-templates/visual-test-picks.js';

import colourtests from './component/design-templates/visual-test-pick-toolb.js';


import colourtests2 from './component/design-templates/visual-test-pick-toolb2.js';


import editortests1 from './component/design-templates/visual-test-imageresize.js';

import editorupload from './component/design-templates/uploadpage.js';

import editoruploads from './component/design-templates/visual-tests.js';

import editortests2 from './component/design-templates/visual-tests1.js';

import editortests4 from './component/design-templates/visual-tests2_edit_layer.js';

import editortests5 from './component/design-templates/visual-tests_editor.js';

import visualeditortemplatenew from './component/design-templates/visual-editor-template_new.js';

import visualeditortemplatenews from './component/design-templates/visual-editor-template_news.js';

import SampleTemplateempty2 from './component/sampletemplates/template2empty/index.js';

import visualeditorshape from './component/design-templates/visual-tests1_shape.js';

import visualresize from './component/design-templates/visual-tests1_resize.js';

import visualdrop from './component/design-templates/visual-tests1_drag_crop.js';

import visualdrops from './component/design-templates/visual-tests1_drops.js';

import visualdropz from './component/design-templates/visual-tests1_dropzone.js';

import visualgraph from './component/design-templates/visual-editor-templategraph.js';

import visualgraph2 from './component/design-templates/visual-editor-template-graph2.js';

import visualgraphs from './component/design-templates/visual-editor-template_change_graph.js';

import visualeditorsnew from './component/design-templates/visual-test-pick-toolb4.js';

import editorextr from './component/design-templates/visual-tests2_image_editor.js';

import editorextrs from './component/design-templates/visual-tests2kndo_editor.js';

import editorextrs2 from './component/design-templates/visual-tests2kndo_editors.js';

import editorextrs1 from './component/design-templates/visual-test_timage_editor.js';

import visualseditor from './component/design-templates/visual-editor-template_new_final.js';

import visualseditor4 from './component/design-templates/visual-editor-template_new_final_ne.js';

//import visualseditor2 from './component/design-templates/fileName.js';

import grapheditor from './component/design-templates/grapheditor.js';

import graph2 from './component/design-templates/visual-tests_graph2.js';

import testlayer from './component/design-templates/visual-test_layers.js';

import visualtests from './component/design-templates/visual-tests.js';

import visualtests14 from './component/design-templates/visual-tests14.js';

import {
  Route,
  BrowserRouter as Router,
  Switch
} from "react-router-dom";

function App() {
  return (
   
   <Router>

    <div>
     
      <Switch>
        <Route exact path="/" component={CVIndex} />
        <Route path="/login" component={LoginView} />
        <Route path="/signUp" component={SignUpView} />
        <Route path="/templates" component={Templates} />
        <Route path="/cvdashboard" component={cvdashboard} />
        <Route path="/basiceditor" component={basiceditor} />
        <Route path="/visualeditor" component={visualeditor} />
        <Route path="/visualeditortemplate" component={visualeditortemplate} />

        
        <Route path="/visualeditortemplate4" component={visualeditortemplate4} />


        <Route path="/SampleTemplate2" component={SampleTemplate2} />
        <Route path="/SampleTemplate3" component={SampleTemplate3} />
        <Route path="/SampleTemplate4" component={SampleTemplate4} />

        <Route path="/SampleTemplate5" component={SampleTemplate5} />

        <Route path="/SampleTemplate6" component={SampleTemplate6} />

        <Route path="/charts" component={charts} />
        <Route path="/areachart" component={areachart} />
        <Route path="/piechart" component={piechart} />
        <Route path="/linechart" component={linechart} />
        <Route path="/barchart" component={barchart} />

        <Route path="/piechartact" component={piecharts} />

        <Route path="/dragdrop" component={dragdroppg} />

        <Route path="/editortest" component={editortest} />

        <Route path="/editortests" component={editortests} />

        <Route path="/editortests1" component={editortests1} />

        <Route path="/editorupload" component={editorupload} />

        <Route path="/editoruploads" component={editoruploads} />

        <Route path="/editortests2" component={editortests2} />

        <Route path="/editortests4" component={editortests4} />

        <Route path="/editortests5" component={editortests5} />

        <Route path="/visualeditortemplatenew" component={visualeditortemplatenew} />

        <Route path="/visualeditortemplatenews" component={visualeditortemplatenews} />

        <Route path="/emptytemplate" component={emptytemplate} />

        <Route path="/visualeditorshape" component={visualeditorshape} />

        <Route path="/visualresize" component={visualresize} />

        <Route path="/visualdrop" component={visualdrop} />

        <Route path="/visualdrops" component={visualdrops} />

        <Route path="/visualdropz" component={visualdropz} />

        <Route path="/visualgraph" component={visualgraph} />

        <Route path="/visualgraphs" component={visualgraphs} />

        <Route path="/visualgraph2" component={visualgraph2} />

        <Route path="/visualeditors" component={visualeditors} />

        <Route path="/SampleTemplateempty2" component={SampleTemplateempty2} />

        <Route path="/colourtest" component={colourtest} />

        <Route path="/colourtests" component={colourtests} />

        <Route path="/colourtests2" component={colourtests2} />

        <Route path="/visualeditorsnew" component={visualeditorsnew} />

        <Route path="/editorextr" component={editorextr} />

        <Route path="/editorextrs" component={editorextrs} />

        <Route path="/editorextrs2" component={editorextrs2} />

        <Route path="/editorextrs1" component={editorextrs1} />

        <Route path="/visualseditor" component={visualseditor} />

        <Route path="/visualseditor4" component={visualseditor4} />

        <Route path="/grapheditor" component={grapheditor} />

        <Route path="/graph2" component={graph2} />

        <Route path="/testlayer" component={testlayer} />

        <Route path="/visualtests" component={visualtests} />

        <Route path="/visualtests14" component={visualtests14} />
               

      </Switch>
    </div>
  </Router>
     
    
  );
}

export default App;
