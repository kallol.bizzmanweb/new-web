import React , { Component} from 'react';

import Constants from '../common/Constants';

import { MDBContainer, MDBTabPane, MDBTabContent, MDBNav, MDBNavItem, MDBNavLink } from "mdbreact";

import { MDBBtn, MDBModal, MDBModalBody, MDBModalHeader, MDBModalFooter } from 'mdbreact';

import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';

//import './style/react-tabs.css';

import './css/main.css';

//import './css/main.css.map';
//import './css/main.scss';

import style from './css/main.scss';

import loremIpsum from 'react-lorem-ipsum';

import styles from './css1/style.scss';

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCubes, faFileAlt } from "@fortawesome/free-solid-svg-icons";
import { faAppleAlt } from "@fortawesome/free-solid-svg-icons";
import { faCog } from "@fortawesome/free-solid-svg-icons";
import { faFile } from "@fortawesome/free-solid-svg-icons";
import { faClock } from "@fortawesome/free-solid-svg-icons";
import { faArrowDown } from "@fortawesome/free-solid-svg-icons";
import { faCrown } from "@fortawesome/free-solid-svg-icons";

import { faUpload } from "@fortawesome/free-solid-svg-icons";
import { faTextHeight } from "@fortawesome/free-solid-svg-icons";
import { faFolder } from "@fortawesome/free-solid-svg-icons";
import { faColumns } from "@fortawesome/free-solid-svg-icons";
import { faVideo } from "@fortawesome/free-solid-svg-icons";
import { faEnvelope } from "@fortawesome/free-solid-svg-icons";

import A4 from '../baseTemplate'
import { EditableText, List, RowTexts, HeaderPic } from '../core'

import Userpics from './128.jpg';

import cs from 'classnames';

import RaisedButton from 'material-ui/RaisedButton'
////import cs from 'classnames'

import phimg from './headshot.jpg'
import FileReaderInput from 'react-file-reader-input'

import { Draggable, Droppable } from 'react-drag-and-drop';

import { Rnd } from "react-rnd";

import { DraggableEventHandler, default as DraggableRoot } from "react-draggable";
import { Enable, Resizable, ResizeDirection } from "re-resizable";

//import Userpics1 from './images/500.jpg';

class Templates extends Component {
  
    state = {
      activeItem: "1"
    }

  constructor(props) {
      super(props);
      this.state = {
        showPopup: false,
        showDiv: true,
        show:true,
        isActive: true,
        isActive1: false,
        isActive2: false,
        isActive3: false,
        isActive4: false
      };

      this.state = {
        isOnHonver: false,
        imgSrc: phimg
      }

      this.state = {
        cv: {
          name: 'Wee',
          nickname: 'fi3ework',
        }
      }
    }
    togglePopup() {
      this.setState({
        showPopup: !this.state.showPopup
      });
    }
    
    
    toggle = tab => () => {
      if (this.state.activeItem !== tab) {
      this.setState({
        activeItem: tab
      });
      }
    }

    handleShow = () => {
      this.setState({
        isActive: true,
        isActive1: false,
        isActive2: false,
        isActive3: false,
        isActive4: false,
        isActive5: false
      });
    };

    handleShows = () => {
      this.setState({
        isActive: false,
        isActive1: true,
        isActive2: false,
        isActive3: false,
        isActive5: false

      });
    };

    handleShows1 = () => {
      this.setState({
        isActive: false,
        isActive1: false,
        isActive2: true,
        isActive3: false,
        isActive4: false,
        isActive5: false
      });
    };

    handleShows2 = () => {
      this.setState({
        isActive: false,
        isActive1: false,
        isActive2: false,
        isActive3: true,
        isActive4: false,
        isActive5: false
      });
    };

    handleShows3 = () => {
      this.setState({
        isActive: false,
        isActive1: false,
        isActive2: false,
        isActive3: false,
        isActive4: true,
        isActive5: false
      });
    };
    handleShows4 = () => {
      this.setState({
        isActive: false,
        isActive1: false,
        isActive2: false,
        isActive3: false,
        isActive4: false,
        isActive5: true
      });
    };
  
    //state = { isOpen: false };
  
    handleShowDialog = () => {
      this.setState({ isOpen: !this.state.isOpen });
      console.log("clicked");
      
    };

    _arrayBufferToBase64(buffer) {
      let binary = ''
      let bytes = new Uint8Array(buffer)
      let len = bytes.byteLength
      for (let i = 0; i < len; i++) {
        binary += String.fromCharCode(bytes[i])
      }
      return window.btoa(binary)
    }
  
    upload = (e, results) => {
      let reader = results[0][0].target
      let that = this
      let base64 = 'data:image/jpg;base64, ' + this._arrayBufferToBase64(reader.result)
      that.img.setAttribute('src', base64)
    }

    /*
      constructor(props) {
        super(props);
    
        this.state = { isOpen: false };
      }
    
      toggleModal = () => {
        this.setState({
          isOpen: !this.state.isOpen
        });
      }
      */
  
    render() {

      const imagesPath = Constants.imagespath;

      //const { showDiv } = this.state.showDiv;

      const template = () => {
        window.location.href = "/Template2";
        console.log("Template Page"); 
       }
  
      return (
        <A4>
        <article className={cs({
          [style.cv]: true,
         exportRoot: true
        })}>
          <HeaderPic style={{
            padding: '20px 40px 80px 80px'
          }}>
             <header className={style.header}>
            <div id="cv">
        <div class="mainDetails">
          <div id="headshot">
          <div
          className={style.headerPicWrapper}
          style={this.props.style}
          onMouseEnter={() => this.setState({ isOnHonver: true })}
          onMouseLeave={() => this.setState({ isOnHonver: false })}
          >   
          <img
          src={Userpics}
          ref={(node) => { this.img = node }}
        />
        <FileReaderInput
          as="buffer"
          onChange={this.upload}>
          
        <RaisedButton
            label="CHANGE PIC"
            className={cs(
              {
                [style.changePic]: true,
                changePic: true
              }
            )}
            style={{
              position: 'absolute',
              top: 10,
              right: 10,
              display: this.state.isOnHonver ? 'block' : 'none',
              zIndex: 999,
            }}
          />
        </FileReaderInput>
        <div className={style.children}>
          {this.props.children}
        </div>
      </div>
		</div>
		
		<div id="name">
			<h1 class="headTitle"><EditableText
                    tagName="h6"
                    html='Joe Bloggs'
                    className={styles.headTitle}
                  /></h1>
			<h2 class="delayThree headTitles"><EditableText
                    tagName="h6"
                    html='Job Title'
                    className={styles.headTitles}
                  /></h2>
		
    <h2 class="delayThree headTitles">  
    <div>    
    <Draggable
                    handle=".handle"
                    defaultPosition={{x: 0, y: 0}}
                    position={null}
                    grid={[15, 15]}
                    scale={1}
                    onStart={this.handleStart}
                    onDrag={this.handleDrag}
                    onStop={this.handleStop}>
                    <Resizable
                      ref={this.refResizable}
                      defaultSize={20}
                    ><EditableText
                    tagName="h6"
                    html='Job Title'
                    className={styles.headTitles}
                  />
                              </Resizable>
                  </Draggable>
              
              </div> 
      
            <Droppable // <= allowed drop types
                onDrop={this.onDrop.bind(this)}>
                  <Draggable
                    handle=".handle"
                    defaultPosition={{x: 0, y: 0}}
                    position={null}
                    grid={[45, 45]}
                    scale={1}
                    onStart={this.handleStart}
                    onDrag={this.handleDrag}
                    onStop={this.handleStop}>
                    <Resizable
                      ref={this.refResizable}
                      defaultSize={20}
                    >
                <ul className="Smoothie"></ul>
                </Resizable>
                </Draggable>
            </Droppable>
            </h2>
            </div>
		
		<div id="contactDetails" class="delayFour">
			<ul>
				<li><a href="mailto:joe@bloggs.com" target="_blank"><EditableText
                    tagName="h6"
                    html='<b>e: joe@bloggs.com</b>'
                    className={styles.name}
                  /></a></li>
				<li><a href="http://www.bloggs.com"><EditableText
                    tagName="h6"
                    html='<b>w: www.bloggs.com</b>'
                    className={styles.name}
                  /></a></li>
				<li><EditableText
                    tagName="h6"
                    html='<b>m: 01234567890</b>'
                    className={styles.name}
                  /></li>
			</ul>
		</div>
		<div class="clear"></div>
	</div>
	
	<div id="mainArea" class="delayFive">
		<section>
			<article>
				<div class="sectionTitles">
					<EditableText
                    tagName="h4"
                    html='Personal Profile'
                    className={styles.sectionTitles}
                  />
				</div>
				
				<div class="sectionContent">
        
        <div>    
                  <Draggable
                    handle=".handle"
                    defaultPosition={{x: 0, y: 0}}
                    position={null}
                    grid={[15, 15]}
                    scale={1}
                    onStart={this.handleStart}
                    onDrag={this.handleDrag}
                    onStop={this.handleStop}>
                    <Resizable
                      ref={this.refResizable}
                      defaultSize={20}
                    ><EditableText
                    tagName="h4"
                    html='<p>Lorem</span> ipsum dolor sit amet, consectetur adipiscing elit. Vivamus euismod congue nisi, nec consequat quam. In consectetur faucibus turpis eget laoreet. Sed nec imperdiet purus. </p>'
                    className={styles.name}
                  />
                              </Resizable>
                  </Draggable>
              
              </div> 
      
            <Droppable // <= allowed drop types
                onDrop={this.onDrop.bind(this)}>
                  <Draggable
                    handle=".handle"
                    defaultPosition={{x: 0, y: 0}}
                    position={null}
                    grid={[45, 45]}
                    scale={1}
                    onStart={this.handleStart}
                    onDrag={this.handleDrag}
                    onStop={this.handleStop}>
                    <Resizable
                      ref={this.refResizable}
                      defaultSize={20}
                    >
                <ul className="Smoothie"></ul>
                </Resizable>
                </Draggable>
            </Droppable>

				</div>
			</article>
			<div class="clear"></div>
		</section>
		
		
		<section>
			<div class="sectionTitles">
				<EditableText
                    tagName="h6"
                    html='Work Experience'
                    className={styles.sectionTitles}
                  />
			</div>
			
			<div class="sectionContent">
				<article>
        <EditableText
                    tagName="h2"
                    html='Job Title at Company'
                    className={styles.sectionTitles}
                  />
					<p class="subDetails"><EditableText
                    tagName="h1"
                    html='April 2011 - Present'
                    className={styles.subDetails}
                  /></p>
					

<div>    
                  <Draggable
                    handle=".handle"
                    defaultPosition={{x: 0, y: 0}}
                    position={null}
                    grid={[15, 15]}
                    scale={1}
                    onStart={this.handleStart}
                    onDrag={this.handleDrag}
                    onStop={this.handleStop}>
                    <Resizable
                      ref={this.refResizable}
                      defaultSize={20}
                    ><EditableText
                    tagName="h4"
                    html='<p>Lorem</span> ipsum dolor sit amet, consectetur adipiscing elit. Vivamus euismod congue nisi, nec consequat quam. In consectetur faucibus turpis eget laoreet. Sed nec imperdiet purus. </p>'
                    className={styles.name}
                  /> 
                              </Resizable>
                  </Draggable>
              
              </div> 
      
            <Droppable // <= allowed drop types
                onDrop={this.onDrop.bind(this)}>
                  <Draggable
                    handle=".handle"
                    defaultPosition={{x: 0, y: 0}}
                    position={null}
                    grid={[45, 45]}
                    scale={1}
                    onStart={this.handleStart}
                    onDrag={this.handleDrag}
                    onStop={this.handleStop}>
                    <Resizable
                      ref={this.refResizable}
                      defaultSize={20}
                    >
                <ul className="Smoothie"></ul>
                </Resizable>
                </Draggable>
            </Droppable>
				</article>
				
				<article>
					<EditableText
                    tagName="h2"
                    html='Job Title at Company'
                    className={styles.sectionTitles}
                  />
					<p class="subDetails"><EditableText
                    tagName="h1"
                    html='Janruary 2007 - March 2011'
                    className={styles.subDetails}
                  /></p>
				
                  <div>    
                  <Draggable
                    handle=".handle"
                    defaultPosition={{x: 0, y: 0}}
                    position={null}
                    grid={[15, 15]}
                    scale={1}
                    onStart={this.handleStart}
                    onDrag={this.handleDrag}
                    onStop={this.handleStop}>
                    <Resizable
                      ref={this.refResizable}
                      defaultSize={20}
                    >	<EditableText
                    tagName="h4"
                    html='<p>Lorem</span> ipsum dolor sit amet, consectetur adipiscing elit. Vivamus euismod congue nisi, nec consequat quam. In consectetur faucibus turpis eget laoreet. Sed nec imperdiet purus. </p>'
                    className={styles.name}
                  /> 
                              </Resizable>
                  </Draggable>
              
              </div> 
      
            <Droppable // <= allowed drop types
                onDrop={this.onDrop.bind(this)}>
                  <Draggable
                    handle=".handle"
                    defaultPosition={{x: 0, y: 0}}
                    position={null}
                    grid={[45, 45]}
                    scale={1}
                    onStart={this.handleStart}
                    onDrag={this.handleDrag}
                    onStop={this.handleStop}>
                    <Resizable
                      ref={this.refResizable}
                      defaultSize={20}
                    >
                <ul className="Smoothie"></ul>
                </Resizable>
                </Draggable>
            </Droppable>
				</article>
				
				<article>
        <EditableText
                    tagName="h2"
                    html='Job Title at Company'
                    className={styles.sectionTitles}
                  />
					<p class="subDetails"><EditableText
                    tagName="h1"
                    html='October 2004 - December 2006'
                    className={styles.subDetails}
                  /></p>
					
                  <div>    
                  <Draggable
                    handle=".handle"
                    defaultPosition={{x: 0, y: 0}}
                    position={null}
                    grid={[15, 15]}
                    scale={1}
                    onStart={this.handleStart}
                    onDrag={this.handleDrag}
                    onStop={this.handleStop}>
                    <Resizable
                      ref={this.refResizable}
                      defaultSize={20}
                    ><EditableText
                    tagName="h4"
                    html='<p>Lorem</span> ipsum dolor sit amet, consectetur adipiscing elit. Vivamus euismod congue nisi, nec consequat quam. In consectetur faucibus turpis eget laoreet. Sed nec imperdiet purus. </p>'
                    className={styles.name}
                  /> 
                              </Resizable>
                  </Draggable>
              
              </div> 
      
            <Droppable // <= allowed drop types
                onDrop={this.onDrop.bind(this)}>
                  <Draggable
                    handle=".handle"
                    defaultPosition={{x: 0, y: 0}}
                    position={null}
                    grid={[45, 45]}
                    scale={1}
                    onStart={this.handleStart}
                    onDrag={this.handleDrag}
                    onStop={this.handleStop}>
                    <Resizable
                      ref={this.refResizable}
                      defaultSize={20}
                    >
                <ul className="Smoothie"></ul>
                </Resizable>
                </Draggable>
            </Droppable>
				</article>
			</div>
			<div class="clear"></div>
		</section>
		
		
		<section>
			<div class="sectionTitles">
				<EditableText
                    tagName="h6"
                    html='Key Skills'
                    className={styles.sectionTitles}
                  />
			</div>
			
			<div class="sectionContent">
				<ul class="keySkills">
					<li><EditableText
                    tagName="h6"
                    html='A Key Skill'
                    className={styles.sectionTitles}
                  /></li>
					<li>A Key Skill</li>
					<li>A Key Skill</li>
					<li>A Key Skill</li>
					<li>A Key Skill</li>
					<li>A Key Skill</li>
					<li>A Key Skill</li>
					<li>A Key Skill</li>
				</ul>
			</div>
			<div class="clear"></div>
		</section>
		
		
		<section>
			<div class="sectionTitles">
				<EditableText
                    tagName="h4"
                    html='Education'
                    className={styles.sectionTitles}
                  />
			</div>
			
			<div class="sectionContent">
				<article>
					<h2>College/University</h2>
					<p class="subDetails">Qualification</p>
					
          <div>    
                  <Draggable
                    handle=".handle"
                    defaultPosition={{x: 0, y: 0}}
                    position={null}
                    grid={[15, 15]}
                    scale={1}
                    onStart={this.handleStart}
                    onDrag={this.handleDrag}
                    onStop={this.handleStop}>
                    <Resizable
                      ref={this.refResizable}
                      defaultSize={20}
                    ><EditableText
                    tagName="h4"
                    html='<p>Lorem</span> ipsum dolor sit amet, consectetur adipiscing elit. Vivamus euismod congue nisi, nec consequat quam. In consectetur faucibus turpis eget laoreet. Sed nec imperdiet purus. </p>'
                    className={styles.name}
                  /> 
                              </Resizable>
                  </Draggable>
              
              </div> 
      
            <Droppable // <= allowed drop types
                onDrop={this.onDrop.bind(this)}>
                  <Draggable
                    handle=".handle"
                    defaultPosition={{x: 0, y: 0}}
                    position={null}
                    grid={[45, 45]}
                    scale={1}
                    onStart={this.handleStart}
                    onDrag={this.handleDrag}
                    onStop={this.handleStop}>
                    <Resizable
                      ref={this.refResizable}
                      defaultSize={20}
                    >
                <ul className="Smoothie"></ul>
                </Resizable>
                </Draggable>
            </Droppable>  
        
        
        
        
        
        
        </article>



        
				
				<article>
					<h2>College/University</h2>
					<p class="subDetails">Qualification</p>

				
          <div>    
                  <Draggable
                    handle=".handle"
                    defaultPosition={{x: 0, y: 0}}
                    position={null}
                    grid={[15, 15]}
                    scale={1}
                    onStart={this.handleStart}
                    onDrag={this.handleDrag}
                    onStop={this.handleStop}>
                    <Resizable
                      ref={this.refResizable}
                      defaultSize={20}
                    ><EditableText
                    tagName="h4"
                    html='<p>Lorem</span> ipsum dolor sit amet, consectetur adipiscing elit. Vivamus euismod congue nisi, nec consequat quam. In consectetur faucibus turpis eget laoreet. Sed nec imperdiet purus. </p>'
                    className={styles.name}
                  /> 
                              </Resizable>
                  </Draggable>
              
              </div> 
      
            <Droppable // <= allowed drop types
                onDrop={this.onDrop.bind(this)}>
                  <Draggable
                    handle=".handle"
                    defaultPosition={{x: 0, y: 0}}
                    position={null}
                    grid={[45, 45]}
                    scale={1}
                    onStart={this.handleStart}
                    onDrag={this.handleDrag}
                    onStop={this.handleStop}>
                    <Resizable
                      ref={this.refResizable}
                      defaultSize={20}
                    >
                <ul className="Smoothie"></ul>
                </Resizable>
                </Draggable>
            </Droppable>
        
        
        </article>
			</div>
			<div class="clear"></div>
		</section>
		
	</div>
</div>
</header>
      </HeaderPic>
    </article>
    </A4>
        
    );
  }
  onDrop(data) {

    //this.upload(data);

    //alert("data : "+data);

    console.log(data)
    // => banana 
}
}

export default Templates;