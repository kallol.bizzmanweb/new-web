import React , { Component} from 'react';

import Constants from '../../common/Constants';

import { MDBContainer, MDBTabPane, MDBTabContent, MDBNav, MDBNavItem, MDBNavLink } from "mdbreact";

import { MDBBtn, MDBModal, MDBModalBody, MDBModalHeader, MDBModalFooter } from 'mdbreact';


import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import './style/react-tabs.css';


import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCubes, faFileAlt } from "@fortawesome/free-solid-svg-icons";
import { faAppleAlt } from "@fortawesome/free-solid-svg-icons";
import { faCog } from "@fortawesome/free-solid-svg-icons";
import { faFile } from "@fortawesome/free-solid-svg-icons";
import { faClock } from "@fortawesome/free-solid-svg-icons";
import { faArrowDown } from "@fortawesome/free-solid-svg-icons";

import Userpic from './images/user-pic.jpg';


class Templates extends Component {
  
    state = {
      activeItem: "1"
    }
    
   constructor() {
      super();
      this.state = {
        showPopup: false
      };
    }
    togglePopup() {
      this.setState({
        showPopup: !this.state.showPopup
      });
    }
    
    
    toggle = tab => () => {
      if (this.state.activeItem !== tab) {
      this.setState({
        activeItem: tab
      });
      }
    }
  
    //state = { isOpen: false };
  
    handleShowDialog = () => {
      this.setState({ isOpen: !this.state.isOpen });
      console.log("cliked");
      
    };
  /*
    constructor(props) {
      super(props);
  
      this.state = { isOpen: false };
    }
  
    toggleModal = () => {
      this.setState({
        isOpen: !this.state.isOpen
      });
    }
    */
  
    render() {
      const imagesPath = Constants.imagespath;
      
  
      return (

    <div class="site-inner-header pos-fixed">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-8">
                    <div class="collapse navbar-collapse js-navbar-collapse">
                        <ul class="nav navbar-nav">
                            <li><a href="/cvdashboard">Dashboard</a></li>
                            <li><a href="/basiceditor">Basic Editor</a></li>
                            <li><a href="/visualeditor">Visual Editor</a></li>
                            <li><a href="/visualeditortemplate">Visual Editor Template</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="left-inner-nav">
                        <ul>
                            <input type="search" class="form-control resume-search"></input>
                            <li>Share</li>
                            <li><FontAwesomeIcon icon={faArrowDown} /> Download</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    
    
    <div class="grey-bg">
        <div class="visual-menu">
        <ul>
            <div style={{fontSize:'18px',color:'black'}}><FontAwesomeIcon icon={faCubes} /></div>
            <br />
            <br />
            
            <div style={{fontSize:'18px',color:'black'}}><FontAwesomeIcon icon={faAppleAlt} /></div>
            <br />
            <br />
            
            <div style={{fontSize:'18px',color:'black'}}><FontAwesomeIcon icon={faFileAlt} /></div>
            <br />
            <br />
            
            <div style={{fontSize:'18px',color:'black'}}><FontAwesomeIcon icon={faCog} /></div>
            <br />
            <br />
            <div style={{fontSize:'18px',color:'black'}}><FontAwesomeIcon icon={faClock} /></div>
            </ul>
        </div>
        
    <div class="container">
        <div class="your-cv">
    <span class="line"></span>
            <h1 class="text-center" contenteditable="">Your Name</h1>
            <span class="double-border"></span>
            <ul>
            <li> <a href="#" contenteditable="">City/State</a> </li>
                <li> <a href="#" contenteditable="">Phone Number</a> </li>
                <li> <a href="#" contenteditable="">Email</a> </li>
            </ul>
            <img src={Userpic} class="img-responsive" alt=""/>
            <h4 class="text-center" contenteditable="">Your Headline /  Current Title</h4>
            
            <h3 class="text-center">Work Experience</h3>
            <span class="line"></span>
            <div class="container">
              <div class="row pad25">
            <div class="col-md-4">
                <div class="row">
                <div class="col-md-6">
                    <input type="text" placeholder="Start Date" class="form-control"></input>
                    </div>
                    <div class="col-md-6">
                    <input type="text" placeholder="End Date" class="form-control"></input>
                    </div>
                </div>
                </div>
                  <div class="col-md-8">
                  <p contenteditable="">Your Most current postion Here</p>
                      <p contenteditable="">Your Most Recent Company</p>
                      <p contenteditable="">Your Most Impressive Achivement here</p>
                  </div>
            </div>
                       <div class="row pad25">
            <div class="col-md-4">
                <div class="row">
                <div class="col-md-6">
                    <input type="text" placeholder="Start Date" class="form-control"></input>
                    </div>
                    <div class="col-md-6">
                    <input type="text" placeholder="End Date" class="form-control"></input>
                    </div>
                </div>
                </div>
                  <div class="col-md-8">
                  <p contenteditable="">Your Most current postion Here</p>
                      <p contenteditable="">Your Most Recent Company</p>
                      <p contenteditable="">Your Most Impressive Achivement here</p>
                  </div>
            </div>
                       <div class="row pad25">
            <div class="col-md-4">
                <div class="row">
                <div class="col-md-6">
                    <input type="text" placeholder="Start Date" class="form-control"></input>
                    </div>
                    <div class="col-md-6">
                    <input type="text" placeholder="End Date" class="form-control"></input>
                    </div>
                </div>
                </div>
                  <div class="col-md-8">
                  <p contenteditable="">Your Most current postion Here</p>
                      <p contenteditable="">Your Most Recent Company</p>
                      <p contenteditable="">Your Most Impressive Achivement here</p>
                  </div>
            </div>
              
            </div>
              <h3 class="text-center">Education</h3>
            <span class="line"></span>
            <div class="container">
                     <div class="row pad25">
            <div class="col-md-4">
                <div class="row">
                <div class="col-md-6">
                    <input type="text" placeholder="Start Date" class="form-control"></input>
                    </div>
                    <div class="col-md-6">
                    <input type="text" placeholder="End Date" class="form-control"></input>
                    </div>
                </div>
                </div>
                  <div class="col-md-8">
                  <p contenteditable="">Your Most current postion Here</p>
                      <p contenteditable="">Your Most Recent Company</p>
                      <p contenteditable="">Your Most Impressive Achivement here</p>
                  </div>
            </div>
                         <div class="row pad25">
            <div class="col-md-4">
                <div class="row">
                <div class="col-md-6">
                    <input type="text" placeholder="Start Date" class="form-control"></input>
                    </div>
                    <div class="col-md-6">
                    <input type="text" placeholder="End Date" class="form-control"></input>
                    </div>
                </div>
                </div>
                  <div class="col-md-8">
                  <p contenteditable="">Your Most current postion Here</p>
                      <p contenteditable="">Your Most Recent Company</p>
                      <p contenteditable="">Your Most Impressive Achivement here</p>
                  </div>
            </div>
            </div>
                 <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingOne">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            Add Section
                                        </a>
                                    </h4> </div>
                                <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                    <div class="panel-body">
                                        <ul>
                                            <li> <a href="#" data-toggle="modal" data-target="#myModal"><i class="fas fa-plus"></i> Add Summery</a> </li>
                                            <li> <a href="#"><i class="fas fa-plus"></i> Add Skills</a> </li>
                                            <li> <a href="#"><i class="fas fa-plus"></i> Add Portfolio</a> </li>
                                            <li> <a href="#"><i class="fas fa-plus"></i> Add Text Section</a> </li>
                                            <li> <a href="#"><i class="fas fa-plus"></i> Add Custom Dated Section</a> </li>
                                            <li> <a href="#"><i class="fas fa-plus"></i> Add Charts</a> </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
          
        </div>
        </div>
    </div>

    </div>
    
    );
  }
}

export default Templates;