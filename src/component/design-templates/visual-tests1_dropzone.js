import React, { Component, Fragment } from "react";
import { render } from "react-dom";

import ReactDropzone from "react-dropzone";

import phimg from './128.jpg';

export default class Apps extends Component {
  constructor(props) {
    super(props);

    this.state = {
      files: []
    };
  }

  onPreviewDrop = (file1) => {

    alert(file1);


    this.setState({
      files: this.state.files.concat(file1),
     });
  }

  render() {

    const filesimage = this.state.files || null;

    const previewStyle = {
      display: 'inline',
      width: 100,
      height: 100,
    };

    return (
      <div className="app">
        <ReactDropzone accept="image/*"
          onDrop={this.onPreviewDrop}>
          {({getRootProps, getInputProps}) => (
            <div {...getRootProps()}>
              <input {...getInputProps()} />
              <p>Drag 'n' drop some files here, or click to select files</p>
            </div>
          )}
        </ReactDropzone>

        {filesimage.length > 0 &&
          <Fragment>
            <h3>Previews</h3>
            {filesimage.map((file) => (
              <div>
                {file.path}
                <img
                alt="Preview"
                key={file.path}
                src={file.preview}
                style={previewStyle}
                />
                </div>
            ))}
          </Fragment>
        }
      </div>
    );
  }
}
