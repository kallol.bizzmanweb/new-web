import React, { Component } from "react";
//import MDBColorPicker from "mdb-react-color-picker";
import { MDBCard, MDBCardBody, MDBBtn, MDBRow, MDBCol } from "mdbreact";

class App extends Component {
  state = {
    colorPicker3: false,
    colorPicker4: false,
    colorPickerValue3: "rgba(252, 253, 255, 1)",
    colorPickerValue4: "rgba(48, 48, 48, 1)",
    colorValue: ""
  };

  close = id => () => {
    const colorPickerId = `colorPicker${id}`;
    this.setState({ [colorPickerId]: false });
  };

  toggle = id => () => {
    const colorPickerId = `colorPicker${id}`;
    this.setState({ [colorPickerId]: !this.state[colorPickerId] });
  };

  saveColorValue = id => value => {
    const colorPickerValueId = `colorPickerValue${id}`;
    this.setState({ [colorPickerValueId]: value.rgba });
  };

  render() {
    return (
      <MDBRow>
        <MDBCol md="6" className="mx-auto">
          <MDBCard style={{ backgroundColor: this.state.colorPickerValue3 }}>
            <MDBCardBody className="text-center d-flex justify-content-center align-items-center flex-column">
              <p>My background color will be changed</p>

              <MDBColorPicker
                isOpen={this.state.colorPicker3}
                close={this.close(3)}
                toggle={this.toggleColorPicker}
                getValue={this.saveColorValue(3)}
              >
                <MDBBtn
                  outline
                  size="sm"
                  color="primary"
                  onClick={this.toggle(3)}
                >
                  Open picker
                </MDBBtn>
              </MDBColorPicker>
            </MDBCardBody>
          </MDBCard>
        </MDBCol>
        <MDBCol md="6" className="mx-auto">
          <MDBCard>
            <MDBCardBody className="text-center d-flex justify-content-center align-items-center flex-column">
              <p style={{ color: this.state.colorPickerValue4 }}>
                My text color will be changed
              </p>

              <MDBColorPicker
                isOpen={this.state.colorPicker4}
                close={this.close(4)}
                toggle={this.toggleColorPicker}
                getValue={this.saveColorValue(4)}
              >
                <MDBBtn
                  outline
                  size="sm"
                  color="primary"
                  onClick={this.toggle(4)}
                >
                  Open picker
                </MDBBtn>
              </MDBColorPicker>
            </MDBCardBody>
          </MDBCard>
        </MDBCol>
      </MDBRow>
    );
  }
}

export default App;