import React , { Component} from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faEnvelope,faLock } from '@fortawesome/free-solid-svg-icons';
//import CanvasApp from '../../css/CanvasApp.css';
//import * as services from '../../modules/services';
import axios from 'axios';

import { MDBContainer } from 'mdbreact';

import { Row, FormGroup, FormControl, ControlLabel, Button, HelpBlock } from 'react-bootstrap';




library.add(faEnvelope,faLock);


class LoginForm extends Component {


   constructor( props ) {
    super( props );

    this.state = {fields: {},emailerror:'', loginError: ''};
  }

  handleLoginFormData(e) {
    e.preventDefault();
    let fieldName = e.target.name, fieldValue = e.target.value, fields = this.state.fields;

    fields[fieldName] = fieldValue;
    this.setState({fields: fields});
 
  }


  handleValidation(){
        let fields = this.state.fields;
        let errors = {};
        let formIsValid = true;

        //Name
       

        //Email
        if(!fields["email"]){
           formIsValid = false;
           errors["email"] = "Cannot be empty";
        }

        if(typeof fields["email"] !== "undefined"){
           let lastAtPos = fields["email"].lastIndexOf('@');
           let lastDotPos = fields["email"].lastIndexOf('.');

           if (!(lastAtPos < lastDotPos && lastAtPos > 0 && fields["email"].indexOf('@@') == -1 && lastDotPos > 2 && (fields["email"].length - lastDotPos) > 2)) {
              formIsValid = false;
              errors["email"] = "Email is not valid";
            }
       }  

       this.setState({...this.state,emailerror: errors["email"]});
       console.log(this.state);
       return formIsValid;
   }

   


  handleLoginFormSubmit(e) {
    e.preventDefault();

    const err = this.handleValidation();


  /*  if(!err){

    let self = this, fields = this.state.fields;

    //console.log(fields.fullname);
    console.log(fields.email);
    console.log(fields.loginPassword);

    alert("You are here1...");

    

          let errors = this.validateLoginForm();

          const { formData } = this.state;

  


  }else{  */

    let self = this, fields = this.state.fields;

    //console.log(fields.fullname);
    console.log(fields.email);
    console.log(fields.loginPassword);


    alert(fields.email);
    alert(fields.loginPassword);


   //alert("You are here1...");

  

      axios.post('http://162.213.248.35:3003/api/login', {email: fields.email, password: fields.loginPassword}).then(function(res) {
        let resData = JSON.parse( JSON.stringify(res) );

        //alert(resData);

        //alert(resData.data);

        //alert(resData.status);

        if(resData.status===200){

        alert("Successfully Login");

        window.location.href="/templates";  

        }else{

          alert("Wrong Login");

        }

        ////services.setUserData( resData.data );
        ////window.location.href="/";  
      })
      .catch(function(error) {
        self.setState({loginError: 'Please provide correct login details.'});
      }); 

  ////    }
  

  }


   render(){

       
    return (
        <MDBContainer>
        <section className="login-bg">
        <div className="container">
            <div className="row">
            <div className="col-md-4">
              <div className="col-md-8"/>
                <div className="login-banner-text">
                <br />
                <br />
                <div style={{height:'40px'}}></div>
                <div style={{fontSize:'25px',fontWeight:'800'}}>Design Anything<br/> Publish Anywhere</div>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has </p>
                </div>
                <div class="login-form">
                
              <form name="loginForm" className="loginForm" onSubmit={(e) => this.handleLoginFormSubmit(e)}>

                    <h2>Customer Login</h2>
                    <span className="h2-line"></span>
                    
              <FontAwesomeIcon icon="envelope" />
               &nbsp;<input type="email" name= "email" placeholder="Email ID" onChange={(e) => this.handleLoginFormData(e)} value={this.state.email} errorText={this.state.emailerror} /><br/>
                  <FontAwesomeIcon icon="lock" />&nbsp; <input type="password"  name = "loginPassword" placeholder="Password" onChange={(e) => this.handleLoginFormData(e)} value={this.state.fields["loginPassword"]}/>
                    <br/>
                                
               <div className="login-forgot">
                        <input type="checkbox" style={{"width": "50px"}}/>Remember me
                       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <a href="/forgotPassword">Forgot Password ?</a>
                        </div>
                    <div className="site-login-btn">
                    

                  <Button type="submit" bsStyle="primary">LOGIN</Button>


                    </div>

                    </form>
                </div>

    
                    
                </div>

                
             </div>

      </div>
   
        </section>
        </MDBContainer>

        );



   }




}

export default LoginForm;






