import React, { Component } from "react";
import { render } from "react-dom";
import { Resize, ResizeVertical, ResizeHorizon } from "react-resize-layout";


class Templates extends Component {

// horizon and vertical
render() {
  return(
  <div>
    <Resize handleWidth="5px" handleColor="#777">
        <ResizeHorizon width="20%">
        <section class="profile section-padding">
            <div class="container">
                <div class="picture-resume-wrapper">
            <div class="picture-resume">
            <span><img src="128.jpg" alt="" /></span>
            
          </div>
             <div class="clearfix"></div>
     </div>
          <div class="name-wrapper">
            <h1>John <br/>Anderson</h1>
          </div>
          <div class="clearfix"></div>
          <div class="contact-info clearfix">
              <ul class="list-titles">
                  <li>Call</li>
                  <li>Mail</li>
                  <li>Web</li>
                  <li>Home</li>
              </ul>
            <ul class="list-content ">
                <li>+34 123 456 789</li> 
                <li>j.anderson@gmail.com</li> 
                <li><a href="#">janderson.com</a></li> 
                <li>Los Angeles, CA</li> 
            </ul>
          </div>
          <div class="contact-presentation"> 
              <p><span class="bold">Lorem</span> ipsum dolor sit amet, consectetur adipiscing elit. Vivamus euismod congue nisi, nec consequat quam. In consectetur faucibus turpis eget laoreet. Sed nec imperdiet purus. </p>
          </div>
          <div class="contact-social clearfix">
              <ul class="list-titles">
                  <li>Twitter</li>
                  <li>Dribbble</li>
                  <li>Codepen</li>
              </ul>
            <ul class="list-content"> 
                  <li><a href="">@janderson</a></li> 
                  <li><a href="">janderson</a></li> 
                  <li><a href="">janderson</a></li> 
              </ul>
          </div>
            </div>
        </section>
        </ResizeHorizon>
        <ResizeHorizon width="50%" minWidth="150px">Horizon 2</ResizeHorizon>
    </Resize>
    </div>
);
};
}

export default Templates;