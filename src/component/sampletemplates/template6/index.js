import React , { Component} from 'react';

import Constants from '../common/Constants';

import { MDBContainer, MDBTabPane, MDBTabContent, MDBNav, MDBNavItem, MDBNavLink } from "mdbreact";

import { MDBBtn, MDBModal, MDBModalBody, MDBModalHeader, MDBModalFooter } from 'mdbreact';

import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';

import Dropzone from 'react-dropzone';

import Chart from "chart.js";

//import './style/react-tabs.css';

import './css/main.css';

//import './css/main.css.map';
//import './css/main.scss';

import style from './css/main.scss';

import loremIpsum from 'react-lorem-ipsum';

import styles from './css/style.scss';

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCubes, faFileAlt } from "@fortawesome/free-solid-svg-icons";
import { faAppleAlt } from "@fortawesome/free-solid-svg-icons";
import { faCog } from "@fortawesome/free-solid-svg-icons";
import { faFile } from "@fortawesome/free-solid-svg-icons";
import { faClock } from "@fortawesome/free-solid-svg-icons";
import { faArrowDown } from "@fortawesome/free-solid-svg-icons";
import { faCrown } from "@fortawesome/free-solid-svg-icons";

import { faUpload } from "@fortawesome/free-solid-svg-icons";
import { faTextHeight } from "@fortawesome/free-solid-svg-icons";
import { faFolder } from "@fortawesome/free-solid-svg-icons";
import { faColumns } from "@fortawesome/free-solid-svg-icons";
import { faVideo } from "@fortawesome/free-solid-svg-icons";
import { faEnvelope } from "@fortawesome/free-solid-svg-icons";

import A4 from '../baseTemplate'
import { EditableText, List, RowTexts, HeaderPic } from '../core'

import Userpics from './128.jpg';

import cs from 'classnames';

import RaisedButton from 'material-ui/RaisedButton'
////import cs from 'classnames'
import ph from './placeholder.jpg'
import phimg from './128.jpg'
import FileReaderInput from 'react-file-reader-input'

//import Userpics1 from './images/500.jpg';

import phpie from './charts7.png';

//import Draggable from 'react-draggable';

import { Draggable, Droppable } from 'react-drag-and-drop';

import { Rnd } from "react-rnd";

import { DraggableEventHandler, default as DraggableRoot } from "react-draggable";
import { Enable, Resizable, ResizeDirection } from "re-resizable";

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import { TransformWrapper, TransformComponent } from "react-zoom-pan-pinch";

class Templates extends Component {
  
    state = {
      activeItem: "1"
    }

  constructor(props) {
      super(props);
      this.state = {
        showPopup: false,
        showDiv: true,
        show:true,
        isActive: true,
        isActive1: false,
        isActive2: false,
        isActive3: false,
        isActive4: false
      };

      this.state = {
        isOnHonver: false,
        imgSrc: phimg
      }

      this.state = {
        cv: {
          name: 'Wee',
          nickname: 'fi3ework',
        }
      }

      // Initializing states 
      this.state = {height:null, width:null} 
        
      // Bind context of 'this' 
      this.handleZoomIn = this.handleZoomIn.bind(this) 
      this.handleZoomOut = this.handleZoomOut.bind(this) 
        
      // Create reference of DOM object 
      this.imgRef = React.createRef() 


      this.ctx = React.createRef();

    }
  
    _arrayBufferToBase64(buffer) {
      let binary = ''
      let bytes = new Uint8Array(buffer)
      let len = bytes.byteLength
      for (let i = 0; i < len; i++) {
        binary += String.fromCharCode(bytes[i])
      }
      return window.btoa(binary)
    }
  
    upload = (e, results) => {
      let reader = results[0][0].target
      let that = this
      let base64 = 'data:image/jpg;base64, ' + this._arrayBufferToBase64(reader.result)
      that.img.setAttribute('src', base64)
    }

    uploads = (e, results) => {
      let reader = results[0][0].target
      let that = this
      let base64 = 'data:image/jpg;base64, ' + this._arrayBufferToBase64(reader.result)
      that.imgs.setAttribute('src', base64)
    }

    componentDidMount(){ 
      // Saving initial dimention of image as class properties 
      this.initialHeight = this.imgRef.current.clientHeight 
      this.initialWidth = this.imgRef.current.clientWidth 
    } 
    
    // Event handler callback for zoom in 
    handleZoomIn(){ 
      
      // Fetching current height and width 
      const height = this.imgRef.current.clientHeight 
      const width = this.imgRef.current.clientWidth 
        
      // Increase dimension(Zooming) 
      this.setState({ 
        height : height + 10, 
        width : width + 10, 
      })   
    } 
    
    // Event handler callback zoom out 
    handleZoomOut(){ 
      
      // Assigning original height and width 
      this.setState({ 
        height : this.initialHeight, 
        width : this.initialWidth, 
      }) 
    } 
    

    togglePopup() {
      this.setState({
        showPopup: !this.state.showPopup
      });
    }
    
    toggle = tab => () => {
      if (this.state.activeItem !== tab) {
      this.setState({
        activeItem: tab
      });
      }
    }

    handleShow = () => {
      this.setState({
        isActive: true,
        isActive1: false,
        isActive2: false,
        isActive3: false,
        isActive4: false,
        isActive5: false
      });
    };

    handleShows = () => {
      this.setState({
        isActive: false,
        isActive1: true,
        isActive2: false,
        isActive3: false,
        isActive5: false

      });
    };

    handleShows1 = () => {
      this.setState({
        isActive: false,
        isActive1: false,
        isActive2: true,
        isActive3: false,
        isActive4: false,
        isActive5: false
      });
    };

    handleShows2 = () => {
      this.setState({
        isActive: false,
        isActive1: false,
        isActive2: false,
        isActive3: true,
        isActive4: false,
        isActive5: false
      });
    };

    handleShows3 = () => {
      this.setState({
        isActive: false,
        isActive1: false,
        isActive2: false,
        isActive3: false,
        isActive4: true,
        isActive5: false
      });
    };
    handleShows4 = () => {
      this.setState({
        isActive: false,
        isActive1: false,
        isActive2: false,
        isActive3: false,
        isActive4: false,
        isActive5: true
      });
    };
  
    //state = { isOpen: false };
  
    handleShowDialog = () => {
      this.setState({ isOpen: !this.state.isOpen });
      console.log("clicked");
      
    };
  /*
    constructor(props) {
      super(props);
  
      this.state = { isOpen: false };
    }
  
    toggleModal = () => {
      this.setState({
        isOpen: !this.state.isOpen
      });
    }
    */

   state = {
    drag: false
  }

  
  dropRef = React.createRef()
  handleDrag = (e) => {
    e.preventDefault()
    e.stopPropagation()
  }
  handleDragIn = (e) => {
    e.preventDefault()
    e.stopPropagation()
    this.dragCounter++
    if (e.dataTransfer.items && e.dataTransfer.items.length > 0) {
      this.setState({drag: true})
    }
  }
  handleDragOut = (e) => {
    e.preventDefault()
    e.stopPropagation()
    this.dragCounter--
    if (this.dragCounter === 0) {
      this.setState({drag: false})
    }
  }
  handleDrop = (e) => {
    e.preventDefault()
    e.stopPropagation()
    this.setState({drag: false})
    if (e.dataTransfer.files && e.dataTransfer.files.length > 0) {
      this.props.handleDrop(e.dataTransfer.files)
      e.dataTransfer.clearData()
      this.dragCounter = 0    
      //let reader = results[0][0].target
      let that = this
      let base64 = 'data:image/jpg;base64, ' + this._arrayBufferToBase64(e.dataTransfer.files)
      that.img.setAttribute('src', base64)
    }
  }
  componentDidMount() {
    let div = this.dropRef.current
    div.addEventListener('dragenter', this.handleDragIn)
    div.addEventListener('dragleave', this.handleDragOut)
    div.addEventListener('dragover', this.handleDrag)
    div.addEventListener('drop', this.handleDrop)
  }
  componentWillUnmount() {
    let div = this.dropRef.current
    div.removeEventListener('dragenter', this.handleDragIn)
    div.removeEventListener('dragleave', this.handleDragOut)
    div.removeEventListener('dragover', this.handleDrag)
    div.removeEventListener('drop', this.handleDrop)
  }

  
  componentDidMount() {

    const retrievedDatas1 = sessionStorage.getItem("text1");
 /*   const retrievedDatas2="";
    const retrievedDatas3="";
    const retrievedDatas4="";
    const retrievedDatas5="";
    const retrievedDatas6="";
*/
    const retrievedDatas2 = sessionStorage.getItem("text2");
      //const text2 = JSON.parse(retrievedDatas2);

      const retrievedDatas3 = sessionStorage.getItem("text3");
      //const text3 = JSON.parse(retrievedDatas3);

      const retrievedDatas4 = sessionStorage.getItem("text4");
      //const text4 = JSON.parse(retrievedDatas4);

      const retrievedDatas5 = sessionStorage.getItem("text5");
      //const text5 = JSON.parse(retrievedDatas5);

      const retrievedDatas6 = sessionStorage.getItem("text6");
      //const text6 = JSON.parse(retrievedDatas6);


//const text6 = JSON.parse(retrievedDatas6);

      if(retrievedDatas1!=''){

        const retrievedDatas1 = sessionStorage.getItem("text1");
      //const text1 = JSON.parse(retrievedDatas1);

      ////alert("data : "+retrievedDatas1);

      const retrievedsess = sessionStorage.getItem("search");

      ////alert("retrievedsess .."+retrievedsess);

      //var retrievedDatas1 = sessionStorage.getItem("text1");

      const retrievedDatas2 = sessionStorage.getItem("text2");
      //const text2 = JSON.parse(retrievedDatas2);

      const retrievedDatas3 = sessionStorage.getItem("text3");
      //const text3 = JSON.parse(retrievedDatas3);

      const retrievedDatas4 = sessionStorage.getItem("text4");
      //const text4 = JSON.parse(retrievedDatas4);

      const retrievedDatas5 = sessionStorage.getItem("text5");
      //const text5 = JSON.parse(retrievedDatas5);

      const retrievedDatas6 = sessionStorage.getItem("text6");
      //const text6 = JSON.parse(retrievedDatas6);

      }else{

      const retrievedDatas1 = "10";

      const retrievedDatas2 = "20";
      //const text2 = JSON.parse(retrievedDatas2);

      const retrievedDatas3 = "20";
      //const text3 = JSON.parse(retrievedDatas3);

      const retrievedDatas4 = "300";
      //const text4 = JSON.parse(retrievedDatas4);

      const retrievedDatas5 = "10";
      //const text5 = JSON.parse(retrievedDatas5);

      const retrievedDatas6 = "10";
      //const text6 = JSON.parse(retrievedDatas6);

      }

  /*  const ctx = this.ctx;
    new Chart(ctx, {
      type: "doughnut",
      data: {
        labels: ["Summary", "Skills", "Profile", "Work Experience", "Education", "Text Section"],
        datasets: [
          {
            label: "# of CV",
            data: [retrievedDatas1, retrievedDatas2, retrievedDatas3, retrievedDatas4, retrievedDatas5, retrievedDatas6],
            backgroundColor: [
              "rgb(255,0,0)",
              "rgb(0,0,255)",
              "rgb(128,0,0)",
              "rgb(64, 0, 255)",
              "rgb(0, 255, 0)",
              "rgb(255, 255, 0)"
            ]
          }
        ]
      }
    }); */
  }

  
    render() {

      const ref = React.createRef();

      const retrievedDatas1 = sessionStorage.getItem("text1");
 /*   const retrievedDatas2="";
    const retrievedDatas3="";
    const retrievedDatas4="";
    const retrievedDatas5="";
    const retrievedDatas6="";
*/
    const retrievedDatas2 = sessionStorage.getItem("text2");
      //const text2 = JSON.parse(retrievedDatas2);

      const retrievedDatas3 = sessionStorage.getItem("text3");
      //const text3 = JSON.parse(retrievedDatas3);

      const retrievedDatas4 = sessionStorage.getItem("text4");
      //const text4 = JSON.parse(retrievedDatas4);

      const retrievedDatas5 = sessionStorage.getItem("text5");
      //const text5 = JSON.parse(retrievedDatas5);

      const retrievedDatas6 = sessionStorage.getItem("text6");
      //const text6 = JSON.parse(retrievedDatas6);


//const text6 = JSON.parse(retrievedDatas6);

      if(retrievedDatas1!=''){

        const retrievedDatas1 = sessionStorage.getItem("text1");
      //const text1 = JSON.parse(retrievedDatas1);

      ////alert("data : "+retrievedDatas1);

      const retrievedsess = sessionStorage.getItem("search");

      ////alert("retrievedsess .."+retrievedsess);

      //var retrievedDatas1 = sessionStorage.getItem("text1");

      const retrievedDatas2 = sessionStorage.getItem("text2");
      //const text2 = JSON.parse(retrievedDatas2);

      const retrievedDatas3 = sessionStorage.getItem("text3");
      //const text3 = JSON.parse(retrievedDatas3);

      const retrievedDatas4 = sessionStorage.getItem("text4");
      //const text4 = JSON.parse(retrievedDatas4);

      const retrievedDatas5 = sessionStorage.getItem("text5");
      //const text5 = JSON.parse(retrievedDatas5);

      const retrievedDatas6 = sessionStorage.getItem("text6");
      //const text6 = JSON.parse(retrievedDatas6);

      }else{

      const retrievedDatas1 = "10";

      const retrievedDatas2 = "20";
      //const text2 = JSON.parse(retrievedDatas2);

      const retrievedDatas3 = "20";
      //const text3 = JSON.parse(retrievedDatas3);

      const retrievedDatas4 = "300";
      //const text4 = JSON.parse(retrievedDatas4);

      const retrievedDatas5 = "10";
      //const text5 = JSON.parse(retrievedDatas5);

      const retrievedDatas6 = "10";
      //const text6 = JSON.parse(retrievedDatas6);

      }

      const ctx = this.ctx;
        var mychart = new Chart(ctx, {
          type: "doughnut",
          data: {
            labels: ["Summary", "Skills", "Profile", "Work Experience", "Education", "Text Section"],
            datasets: [
              {
                label: "# of CV",
                data: [retrievedDatas1, retrievedDatas2, retrievedDatas3, retrievedDatas4, retrievedDatas5, retrievedDatas6],
                //data: [100, 200, 100, 100, 200, 300],
                backgroundColor: [
                  "rgb(255,0,0)",
                  "rgb(0,0,255)",
                  "rgb(128,0,0)",
                  "rgb(64, 0, 255)",
                  "rgb(0, 255, 0)",
                  "rgb(255, 255, 0)"
                ]
              }
            ]
          }
        });

      const getUploadParams = () => {
        return { url: 'https://httpbin.org/post' }
      }
    
      const handleChangeStatus = ({ meta }, status) => {
        console.log(status, meta)
      }
    
      const handleSubmit = (files, allFiles) => {
        console.log(files.map(f => f.meta))
        allFiles.forEach(f => f.remove())
      }

      const imagesPath = Constants.imagespath;

      //const { showDiv } = this.state.showDiv;

      const template = () => {
        window.location.href = "/Template2";
        console.log("Template Page"); 
       }
  
      return (
        <div>
        <button onClick={this.handleZoomIn}>Zoom In</button> 
        <button onClick={this.handleZoomOut}>Zoom Out</button> 
        
        <div ref={this.imgRef}>

        <A4>
        <article className={cs({
          [style.cv]: true,
         exportRoot: true
        })}>
          <HeaderPic style={{
            padding: '0px 0px 0px 0px'
          }}>
        <header className={style.header}>
 
          
        <body id="top">
<div id="cv" class="instaFade">
	<div class="mainDetails">
		<div id="headshot" class="quickFade">
			<img src="headshot.jpg" alt="Alan Smith" />
		</div>
		
		<div id="name">
			<h1 class="quickFade delayTwo">Joe Bloggs</h1>
			<h2 class="quickFade delayThree">Job Title</h2>
		</div>
		
		<div id="contactDetails" class="quickFade delayFour">
			<ul>
				<li>e: <a href="mailto:joe@bloggs.com" target="_blank">joe@bloggs.com</a></li>
				<li>w: <a href="http://www.bloggs.com">www.bloggs.com</a></li>
				<li>m: 01234567890</li>
			</ul>
		</div>
		<div class="clear"></div>
	</div>
	
	<div id="mainArea" class="quickFade delayFive">
		<section>
			<article>
				<div class="sectionTitle">
					<h1>Personal Profile</h1>
				</div>
				
				<div class="sectionContent">
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer dolor metus, interdum at scelerisque in, porta at lacus. Maecenas dapibus luctus cursus. </p>
				</div>
			</article>
			<div class="clear"></div>
		</section>
		
		
		<section>
			<div class="sectionTitle">
				<h1>Work Experience</h1>
			</div>
			
			<div class="sectionContent">
				<article>
					<h2>Job Title at Company</h2>
					<p class="subDetails">April 2011 - Present</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ultricies massa et erat luctus hendrerit. </p>
				</article>
				
				<article>
					<h2>Job Title at Company</h2>
					<p class="subDetails">Janruary 2007 - March 2011</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ultricies massa et erat luctus hendrerit. </p>
				</article>
				
				<article>
					<h2>Job Title at Company</h2>
					<p class="subDetails">October 2004 - December 2006</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ultricies massa et erat luctus hendrerit. </p>
				</article>
			</div>
			<div class="clear"></div>
		</section>
		
		
		<section>
			<div class="sectionTitle">
				<h1>Key Skills</h1>
			</div>
			
			<div class="sectionContent">
				<ul class="keySkills">
					<li>A Key Skill</li>
					<li>A Key Skill</li>
					<li>A Key Skill</li>
					<li>A Key Skill</li>
					
				</ul>
			</div>
			<div class="clear"></div>
		</section>
		
		
		<section>
			<div class="sectionTitle">
				<h1>Education</h1>
			</div>
			
			<div class="sectionContent">
				<article>
					<h2>College/University</h2>
					<p class="subDetails">Qualification</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ultricies massa et erat luctus hendrerit. Curabitur non consequat enim.</p>
				</article>
				
				<article>
					<h2>College/University</h2>
					<p class="subDetails">Qualification</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ultricies massa et erat luctus hendrerit. Curabitur non consequat enim.</p>
				</article>
			</div>
			<div class="clear"></div>
		</section>
		
	</div>
</div>

</body>


    
    </header>
    </HeaderPic>
    </article>
    </A4>
          
    
    </div>
    </div>
    );

    
  }
  onDrop(data) {

    //this.upload(data);

    //alert("data : "+data);

    console.log(data)
    // => banana 
}
}

export default Templates;