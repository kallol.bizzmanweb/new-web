import React , { Component} from 'react';

import Constants from '../../common/Constants';

import { MDBContainer, MDBTabPane, MDBTabContent, MDBNav, MDBNavItem, MDBNavLink } from "mdbreact";

import { MDBBtn, MDBModal, MDBModalBody, MDBModalHeader, MDBModalFooter } from 'mdbreact';

import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import './style/react-tabs.css';

import DonutChart from "react-svg-donut-chart";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCubes, faFileAlt } from "@fortawesome/free-solid-svg-icons";
import { faAppleAlt } from "@fortawesome/free-solid-svg-icons";
import { faCog } from "@fortawesome/free-solid-svg-icons";
import { faFile } from "@fortawesome/free-solid-svg-icons";
import { faClock } from "@fortawesome/free-solid-svg-icons";
import { faArrowDown } from "@fortawesome/free-solid-svg-icons";
import { faCrown } from "@fortawesome/free-solid-svg-icons";

import { faUpload } from "@fortawesome/free-solid-svg-icons";
import { faTextHeight } from "@fortawesome/free-solid-svg-icons";
import { faFolder } from "@fortawesome/free-solid-svg-icons";
import { faColumns } from "@fortawesome/free-solid-svg-icons";
import { faVideo } from "@fortawesome/free-solid-svg-icons";
import { faEnvelope } from "@fortawesome/free-solid-svg-icons";

import Userpics from './images/300.png';

import Userpics1 from './images/500.jpg';

import Template1 from './images/image1.jpeg';
import Template2 from './images/image3.jpeg';
import Template3 from './images/image2.jpeg';

import pies from './images/piechart.png';
import bars from './images/barchart.jpg';
import lines from './images/linechart.png';

//import scenery from './images/scenery.jpg';
//import scenery2 from './images/scene2.jpg';

import image1 from './images/Icons/1.png';
import image2 from './images/Icons/2.png';
import image3 from './images/Icons/3.png';
import image4 from './images/Icons/4.png';
import image5 from './images/Icons/5.png';
import image6 from './images/Icons/6.png';
import image7 from './images/Icons/7.png';
import image8 from './images/Icons/8.png';
import image9 from './images/Icons/9.png';
import image10 from './images/Icons/10.png';
import image11 from './images/Icons/11.png';
import image12 from './images/Icons/12.png';
import image13 from './images/Icons/13.png';
import image14 from './images/Icons/14.png';
import image15 from './images/Icons/15.png';
import image16 from './images/Icons/16.png';
import image17 from './images/Icons/17.png';
import image18 from './images/Icons/18.png';
import image19 from './images/Icons/19.png';
import image20 from './images/Icons/20.png';
import image21 from './images/Icons/21.png';
import image22 from './images/Icons/22.png';
import image23 from './images/Icons/23.png';
import image24 from './images/Icons/24.png';
import image25 from './images/Icons/25.png';
import image26 from './images/Icons/26.png';
import image27 from './images/Icons/27.png';
import image28 from './images/Icons/28.png';
import image29 from './images/Icons/29.png';
import image30 from './images/Icons/30.png';
import image31 from './images/Icons/31.png';
import image32 from './images/Icons/32.png';
import image33 from './images/Icons/33.png';
import image34 from './images/Icons/34.png';
import image35 from './images/Icons/35.png';
import image36 from './images/Icons/36.png';
import image37 from './images/Icons/37.png';
import image38 from './images/Icons/38.png';
import image39 from './images/Icons/39.png';

import imagep1 from './picture/1.jpg';
import imagep2 from './picture/2.jpg';
import imagep3 from './picture/3.jpg';
import imagep4 from './picture/4.jpg';
import imagep5 from './picture/5.jpg';
import imagep6 from './picture/6.jpg';
import imagep7 from './picture/7.jpg';
import imagep8 from './picture/8.jpg';
import imagep9 from './picture/9.jpg';



import Chart from "chart.js";

import style from './css/main.scss';

////import CanvasImage from '@axetroy/react-canvas-image';

//import Draggable from 'react-draggable';

//import {Editor, Frame, Canvas, Selector} from "@craftjs/core";

import ResizeImage from 'react-resize-image';

import { DraggableEventHandler, default as DraggableRoot } from "react-draggable";
import { Enable, Resizable, ResizeDirection } from "re-resizable";

//import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import { Rnd } from "react-rnd";

import axios from 'axios';

import {Layer, Rect, Stage, Group} from 'react-konva';

import {Pie, Doughnut} from 'react-chartjs-2';

import A4 from './baseTemplate'
import { EditableText, List, RowTexts, HeaderPic } from './core'

import cs from 'classnames';

import RaisedButton from 'material-ui/RaisedButton'
////import cs from 'classnames'
import ph from './placeholder.jpg'
import phimg from './128.jpg'
import FileReaderInput from 'react-file-reader-input'

//import Userpics1 from './images/500.jpg';    

import phpie from './charts7.png';


import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

//import { Editor } from 'react-draft-wysiwyg';
//import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';


import { EditorState, ContentState, convertToRaw, convertFromRaw } from 'draft-js';
import htmlToDraft from 'html-to-draftjs';
import draftToHtml from 'draftjs-to-html';

import { Row, FormGroup, FormControl, ControlLabel, Button, HelpBlock } from 'react-bootstrap';

import styles from './css/style.scss'; 

import { Draggable, Droppable } from 'react-drag-and-drop';

import ReactToPdf from 'react-to-pdf';

import { PieChart } from 'react-minimal-pie-chart';

import ImageResize from 'quill-image-resize-vue';

import quillEmoji from 'quill-emoji';
import { render } from "react-dom";
import ReactQuill, { Quill } from "react-quill";
import "react-quill/dist/quill.snow.css";

/*
import Download from '@axetroy/react-download';

import DownloadLink from "react-download-link";


import { TransformWrapper, TransformComponent } from "react-zoom-pan-pinch";
*/

/*
const { EmojiBlot, ShortNameEmoji, ToolbarEmoji, TextAreaEmoji } = quillEmoji;


const Quills = ReactQuill.Quill;
const BlockEmbed = Quills.import('blots/block/embed');
class AudioBlot extends BlockEmbed {
  static create(url) {
    let node = super.create();
    node.setAttribute('src', url);
    node.setAttribute('controls', '');
    return node;
  }
  
  static value(node) {
    return node.getAttribute('src');
  }
}
AudioBlot.blotName = 'audio';
AudioBlot.tagName = 'audio';
Quills.register(AudioBlot);


class FontBlot extends BlockEmbed {
  static create(url) {
    let node = super.create();
    node.setAttribute('src', url);
    node.setAttribute('controls', '');
    node.setAttribute('style', 'color: rgb(230, 0, 0)');
    node.setAttribute('size', '14');

    return node;
  }
  
  static value(node) {
    return node.getAttribute('src');
  }
}
FontBlot.blotName = 'font';
FontBlot.tagName = 'font';
Quills.register(FontBlot);

class AppPanelEmbed extends BlockEmbed {
  static create(value) {
    const node = super.create(value);
    node.setAttribute('contenteditable', 'true');
    node.setAttribute('width', '100%');
    //Set custom HTML
    node.innerHTML = this.transformValue(value)
    return node;
  }

  static transformValue(value) {
    let handleArr = value.split('\n')
    handleArr = handleArr.map(e => e.replace(/^[\s]+/, '')
      .replace(/[\s]+$/, ''))
    return handleArr.join('')
  }

  //Returns the value of the node itself for undo operation
  static value(node) {
    return node.innerHTML
  }
}
// blotName
AppPanelEmbed.blotName = 'AppPanelEmbed';
//The class name will be used to match the blot name
AppPanelEmbed.className = 'embed-innerApp';
//Label type customization
AppPanelEmbed.tagName = 'div';
Quill.register(AppPanelEmbed, true);

Quill.register({
  //'formats/emoji': EmojiBlot,
  //'modules/emoji-shortname': ShortNameEmoji,
  //'modules/emoji-toolbar': ToolbarEmoji,
  //'modules/emoji-textarea': TextAreaEmoji
}, true);



const Font = ReactQuill.Quill.import('formats/font');
Font.whitelist = ['large', 'medium', "small", "regular", "bold", "pullquote"] ;
ReactQuill.Quill.register(Font, true);

let fonts = Quill.import("attributors/style/font");
fonts.whitelist = ["initial", "sans-serif", "serif", "monospace", 'Arial', 'Courier', 'Garamond', 'Tahoma', 'Times New Roman', 'Verdana'];
Quill.register(fonts, true);

var FontAttributor = Quill.import('attributors/class/font');
FontAttributor.whitelist = [
  'sofia', 'slabo', 'roboto', 'inconsolata', 'ubuntu'
];
Quill.register(FontAttributor, true)

*/

/*
var ColorClass = Quill.import('attributors/class/color');
var SizeStyle = Quill.import('attributors/style/size');
Quill.register(ColorClass, true);
Quill.register(SizeStyle, true);

const Font = ReactQuill.Quill.import('formats/font');
Font.whitelist = ['large', 'medium', "small", "regular", "bold", "pullquote"] ;
ReactQuill.Quill.register(Font, true);

*/

const { EmojiBlot, ShortNameEmoji, ToolbarEmoji, TextAreaEmoji } = quillEmoji;

Quill.register("modules/imageResize", ImageResize);

const modulesQuill = {
  toolbar: {
    container: [
      [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
      [{ 'font': [] }],
      [{ 'align': [] }],
      ['bold', 'italic', 'underline', 'strike'],
      [{ 'list': 'ordered' }, { 'list': 'bullet' }, { 'script': 'sub'}, { 'script': 'super' }, { 'indent': '-1'}, { 'indent': '+1' }, { 'direction': 'rtl' }, { 'size': ['small', false, 'large', 'huge'] }, { 'color': ['#000000', '#e60000', '#ff9900', '#ffff00', '#008a00', '#0066cc', '#9933ff', '#ffffff', '#facccc', '#ffebcc', '#ffffcc', '#cce8cc', '#cce0f5', '#ebd6ff', '#bbbbbb', '#f06666', '#ffc266', '#ffff66', '#66b966', '#66a3e0', '#c285ff', '#888888', '#a10000', '#b26b00', '#b2b200', '#006100', '#0047b2', '#6b24b2', '#444444', '#5c0000', '#663d00', '#666600', '#003700', '#002966', '#3d1466', 'custom-color'] }, { 'background': [] }, 'link', 'emoji'],          // dropdown with defaults from theme
      ['clean']      
    ],
    handlers: {
      'color': function (value) {
        if (value == 'custom-color') value = window.prompt('Enter Hex Color Code');
        this.quill.format('color', value);
      }
    }
  },
  keyboard: {
    bindings: {
      tab: false,
      custom: {
        key: 13,
        shiftKey: true,
        handler: function () { /** do nothing */ }
      },
      handleEnter: {
        key: 13,
        handler: function () { /** do nothing */ }
      }
    }
  },
  imageResize: {},
  //'emoji-toolbar': true,
  //'emoji-textarea': true,
  //'emoji-shortname': true,
  
};

const formatsQuill = [
  'header', 'font', 'size',
  'bold', 'italic', 'underline', 'strike', 'blockquote',
  'list', 'bullet', 'indent', 'align',
  'link', 'image', 'background', 'color', 'emoji'
];

const dataPie = [
  {value: 100, stroke: "#22594e", strokeWidth: 6},
  {value: 60, stroke: "#2f7d6d"},
  {value: 30, stroke: "#3da18d"},
  {value: 20, stroke: "#69c2b0"},
  {value: 10, stroke: "#a1d9ce"},
]

const CustomHeart = () => <span>♥</span>;

function insertHeart() {
  const cursorPosition = this.quill.getSelection().index;
  this.quill.insertText(cursorPosition, "♥");
  this.quill.setSelection(cursorPosition + 1);
}

/*
 * Custom toolbar component including the custom heart button and dropdowns
 */
const CustomToolbar = () => (
  <div id="toolbar">
    <select className="ql-font">
      <option value="arial" selected>
        Arial
      </option>
      <option value="comic-sans">Comic Sans</option>
      <option value="courier-new">Courier New</option>
      <option value="georgia">Georgia</option>
      <option value="helvetica" selected>Helvetica</option>
      <option value="lucida">Lucida</option>
    </select>
    <select className="ql-size">
      <option value="10px">Size 1</option>
      <option value="14px">Size 2</option>
      <option value="18px">Size 3</option>
      <option value="20px">Size 4</option>
      <option value="24px">Size 5</option>
      <option value="48px">Size 6</option>
      <option value="100px">
        Size 7
      </option>
      <option value="200px">Size 8</option>
      <option value="300px" selected>Size 9</option>
    </select>
    <button className="ql-bold" />
    <button className="ql-italic" />
    <button className="ql-strike" />
    <button className="ql-list" value="ordered" />
    <button className="ql-list" value="bullet" />
    <button className="ql-underline" />
    <select className="ql-align" />
    <select className="ql-color" />
    <select className="ql-background">      
    </select>
    <button className="ql-clean" />
    <button className="ql-insertHeart">
      <CustomHeart />
    </button>
  </div>
);

// Add sizes to whitelist and register them
/* const Size = Quill.import("formats/size");
Size.whitelist = ["extra-small", "small", "medium", "large","extra-large"];
Quill.register(Size, true);
*/

var Size = Quill.import('attributors/style/size');
Size.whitelist = ['10px','14px','18px','20px','24px', '48px', '100px', '200px', '300px'];
Quill.register(Size, true);

 const colorList = ['#001f3f', '#0074D9', '#7FDBFF', '#39CCCC', '#3D9970', '#2ECC40', '#01FF70', '#FFDC00', '#FF851B', '#FF4136', '#85144b', '#F012BE', '#B10DC9', '#111111', '#AAAAAA', '#DDDDDD', '#FFFFFF'];
   

// Add fonts to whitelist and register them
const Font = Quill.import("formats/font");
Font.whitelist = [
  "arial",
  "comic-sans",
  "courier-new",
  "georgia",
  "helvetica",
  "lucida"
];
Quill.register(Font, true);



/*
 * Editor component with custom toolbar and content containers
 */


  const modules = {
    toolbar: {
      container: "#toolbar",
      handlers: {
        insertHeart: insertHeart
      }
    },
    keyboard: {
      bindings: {
        tab: false,
        custom: {
          key: 13,
          shiftKey: true,
          handler: function () { /** do nothing */ }
        },
        handleEnter: {
          key: 13,
          handler: function () { /** do nothing */ }
        }
      }
    },
    imageResize: {},
    
  };

  
  const modulesnew = {
    toolbar: {
      container: "#toolbar",
      handlers: {
        insertHeart: insertHeart
      }
    },
    imageResize: {},
    
    
  };
  
  const formats = [
    "background",
    "header",
    "font",
    "size",
    "bold",
    "italic",
    "underline",
    "strike",
    "blockquote",
    "list",
    "bullet",
    "indent",
    "link",
    "image",
    "color"
  ];

class Templates extends Component {
  
    state = {
      activeItem: "1",
      
    }

  constructor() {
      super();
      ////this.state = {fields: {},emailerror:'', loginError: ''};

      const retrievedsess = sessionStorage.getItem("search");     

      this.myRef = React.createRef();

      this.ctx = React.createRef();

      this.ctxs = React.createRef();

      this.state = {
        showDiv: true,
        show:true}

      var html ='<div>';

      html = html + 'First Company, Inc., Los Angeles, CA January 1, 2007 - present';
      
      html = html + 'Customer Service Manager';
      html = html + '•	Oversee Customer Service Department';
      html = html + '•	Supervise Customer Service Representatives ';
      
      html = html + '•	Winner, First Company Excellence Award';
      
      html = html + 'Second Corporation, San Diego, CA';
      html = html + 'January 1, 2003 - December 31, 2006';
      
      html = html + 'Customer Service Representative';
      html = html + '•	Provide service to customers via telephone and email';
      html = html + '•	Respond to all inquiries within 24 hours';
      
      html = html + '•	Successfully reorganized call escalation protocol';

      html = html + '</div>';

      
     /* this.state = {
        editorState: EditorState.createEmpty(),
        hideToolbar: true,
        emailbody : "html here"
      };
      */

      var retrievedDatas6 = false;
      retrievedDatas6 = sessionStorage.getItem("chart");

      this.state = { editorHtml :'' }

      if(retrievedDatas6=='true'){

        this.state = {
          showPopup: false,
          showDiv: true,
          show:true,
          isActiveCreate: true,
          isActive: false,
          isActive1: false,
          isActive2: false,
          isActive3: false,
          isActive4: false,
          isActive5: false,
          isActive6: true,
          isActive7: false,
          isActive8: false,
          isActive9: false,
          fields: {},emailerror:'', loginError: '',
          editorHtml : html
        }

        this.setState({
            showPopup: false,
            showDiv: true,
            show:true,
            isActiveCreate: true,
            isActive: false,
            isActive1: false,
            isActive2: false,
            isActive3: false,
            isActive4: false,
            isActive5: false,
            isActive6: true,
            isActive7: false,
            isActive8: false,
            isActive9: false
        });

        sessionStorage.removeItem("chart");

        retrievedDatas6=false;

      }else{ 

        this.state = {
          showPopup: false,
          showDiv: true,
          show:true,
          isActiveCreate: true,
          isActive: false,
          isActive1: false,
          isActive2: true,
          isActive3: false,
          isActive4: false,
          isActive5: false,
          isActive6: false,
          isActive7: false,
          fields: {},emailerror:'', loginError: '',
          editorHtml : html
        };
      } 
      /*
      else{  

      if(retrievedsess=='true'){

      this.state = {
        showPopup: false,
        showDiv: true,
        show:true,
        isActiveCreate: true,
        isActive: false,
        isActive1: false,
        isActive2: true,
        isActive3: false,
        isActive4: false,
        isActive5: false,
        isActive6: false,
        isActive7: true,
        fields: {},emailerror:'', loginError: '',
        editorHtml : html
      };

    }else{

      this.state = {
        showPopup: false,
        showDiv: true,
        show:true,
        isActiveCreate: true,
        isActive: false,
        isActive1: false,
        isActive2: true,
        isActive3: false,
        isActive4: false,
        isActive5: false,
        isActive6: false,
        isActive7: false,
        isActive8: false,
        isActive9: true,
        fields: {},emailerror:'', loginError: '',
        editorHtml : html
      };
      
    }
    
    }

    //const html = 'Technical lead and senior developer with 19+ years of experience working on full lifecycle projects involving component based architectures. Experience includes implementing B2B and B2C omni-channel applications (web-11+(Php,Java), mobile(android, ios, ionic – 6+ years, Hadoop and Big Data(Map Reduce, Hive, Pig, Hbase)), client server applications and enabling of back office applications.';

    /* const reactStringReplace = require('react-string-replace');
      const backEndResponseString = "content here";
      const emailBody = reactStringReplace(backEndResponseString, () => (
        <span>Hello</span>
      ));
    */

    const contentBlock = htmlToDraft(html);
    if (contentBlock) {
      const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks);
      const newEditorState = EditorState.createWithContent(contentState);
      this.state.editorState = newEditorState;
    }

    this.states1={
      backEndResponseString : html
    }   

    /*  this.states1={
        
      }     
    */

    
   this.quillRef = null;
   this.reactQuillRef = null;
   this.handleChange = this.handleChange.bind(this)
   this.handleClick = this.handleClick.bind(this)
   this.handleClick1 = this.handleClick1.bind(this)
   this.handleClick2 = this.handleClick2.bind(this)
   this.handleClick3 = this.handleClick3.bind(this)
   this.handleClick4 = this.handleClick4.bind(this)
   this.attachQuillRefs = this.attachQuillRefs.bind(this);

   Templates.modules = {}
   Templates.modules.toolbar = [
  ['bold', 'italic', 'underline', 'strike'],       // toggled buttons
  ['blockquote', 'code-block'],                    // blocks
  [{ 'header': 1 }, { 'header': 2 }],              // custom button values
  [{ 'list': 'ordered'}, { 'list': 'bullet' }],    // lists
  [{ 'script': 'sub'}, { 'script': 'super' }],     // superscript/subscript
  [{ 'indent': '-1'}, { 'indent': '+1' }],         // outdent/indent
  [{ 'direction': 'rtl' }],                        // text direction
  [{ 'size': ['small', false, 'large', 'huge'] }], // custom dropdown
  [{ 'header': [1, 2, 3, 4, 5, 6, false] }],       // header dropdown
  [{ 'color': [] }, { 'background': [] }],         // dropdown with defaults
  [{ 'font': [] }],                                // font family
  [{ 'align': [] }],                               // text align
  ['clean'],                                       // remove formatting
]

/* 
 * Quill editor formats
 * See https://quilljs.com/docs/formats/
 */
Templates.formats = [
  'header', 'font', 'background', 'color', 'code', 'size',
  'bold', 'italic', 'underline', 'strike', 'blockquote',
  'list', 'bullet', 'indent', 'script', 'align', 'direction',
  'link', 'image', 'code-block', 'formula', 'audio'
]


  
  }

  
  
    
    /*  
    handleChange = (value)=>{
      this.setState({ emailbody: value });
      }
    */  

    handleChange = (html) => {
      this.setState({
        editorHtml : html
      })
    } 


    onEditorStateChange = editorState => {
    this.setState({
    editorState
    });
    };
    
    onContentStateChange = contentState => {
    this.setState({
    contentState
    });
    };
  

    togglePopup() {
      this.setState({
        showPopup: !this.state.showPopup
      });
    };
    
    handleLoginFormData(e) {
      e.preventDefault();
      let fieldName = e.target.name, fieldValue = e.target.value, fields = this.state.fields;
  
      fields[fieldName] = fieldValue;

      //alert(fields);

      this.setState({fields: fields});
   
    }
    
    toggle = tab => () => {
      if (this.state.activeItem !== tab) {
      this.setState({
        activeItem: tab
      });
      }
    }

    handleShow = () => {
      this.setState({
      /*  isActiveCreate: false, 
        isActive:  true,
        isActive1: false,
        isActive2: false,
        isActive3: false,
        isActive4: false,
        isActive5: false,
        isActive6: false,
        isActive7: false,
        isActive8: false,
        isActive9: true */

        isActiveCreate: true,
        isActive: true,
        isActive1: false,
        isActive2: false,
        isActive3: false,
        isActive4: false,
        isActive5: false,
        isActive6: false,
        isActive7: false,
        isActive8: false,
        isActive9: true
      });
    };

    handleShows = () => {
      this.setState({
       /* isActiveCreate: false,
        isActive: false,
        isActive1: true,
        isActive2: false,
        isActive3: false,
        isActive5: false,
        isActive6: false,
        isActive7: false,
        isActive8: false,
        isActive9: true */

        isActiveCreate: true,
        isActive: false,
        isActive1: true,
        isActive2: false,
        isActive3: false,
        isActive4: false,
        isActive5: false,
        isActive6: false,
        isActive7: false,
        isActive8: false,
        isActive9: false
      });
    };

    handleShowsCreate = () => {
      this.setState({
        isActiveCreate: true,
        isActive: false,
        isActive1: false,
        isActive2: true,
        isActive3: false,
        isActive4: false,
        isActive5: false,
        isActive6: false,
        isActive7: false,
        isActive8: false,
        isActive9: false
      });
    };

    handleShows1 = () => {
      this.setState({
       /* isActiveCreate: false,
        isActive: false,
        isActive1: false,
        isActive2: true,
        isActive3: false,
        isActive4: false,
        isActive5: false,
        isActive6: false,
        isActive7: false,
        isActive8: true,
        isActive9: true */

        isActiveCreate: true,
        isActive: false,
        isActive1: false,
        isActive2: true,
        isActive3: false,
        isActive4: false,
        isActive5: false,
        isActive6: false,
        isActive7: false,
        isActive8: false,
        isActive9: false
      });
    };

    handleShows2 = () => {
      this.setState({
      /*  isActiveCreate: false,
        isActive: false,
        isActive1: false,
        isActive2: false,
        isActive3: true,
        isActive4: false,
        isActive5: false,
        isActive6: false,
        isActive7: false,
        isActive8: false,
        isActive9: true */
        isActiveCreate: true,
        isActive: false,
        isActive1: false,
        isActive2: false,
        isActive3: true,
        isActive4: false,
        isActive5: false,
        isActive6: false,
        isActive7: false,
        isActive8: false,
        isActive9: false
      });
    };

    handleShows3 = () => {
      this.setState({
      /*  isActiveCreate: false,
        isActive: false,
        isActive1: false,
        isActive2: false,
        isActive3: false,
        isActive4: true,
        isActive5: false,
        isActive6: false,
        isActive7: false,
        isActive8: false,
        isActive9: true */

        isActiveCreate: true,
        isActive: false,
        isActive1: false,
        isActive2: false,
        isActive3: false,
        isActive4: true,
        isActive5: false,
        isActive6: false,
        isActive7: false,
        isActive8: false,
        isActive9: false
      });
    };
    handleShows4 = () => {
      this.setState({
     /*   isActiveCreate: false,
        isActive: false,
        isActive1: false,
        isActive2: false,
        isActive3: false,
        isActive4: false,
        isActive5: true,
        isActive6: false,
        isActive7: false,
        isActive8: false,
        isActive9: true */
        isActiveCreate: true,
        isActive: false,
        isActive1: false,
        isActive2: false,
        isActive3: false,
        isActive4: false,
        isActive5: true,
        isActive6: false,
        isActive7: false,
        isActive8: false,
        isActive9: false
      });
    };

    handleShows5 = () => {
      this.setState({
      /*  isActiveCreate: false,
        isActive: false,
        isActive1: false,
        isActive2: false,
        isActive3: false,
        isActive4: false,
        isActive5: false,
        isActive6: true,
        isActive7: false,
        isActive8: true,
        isActive9: true */
        isActiveCreate: true,
        isActive: false,
        isActive1: false,
        isActive2: false,
        isActive3: false,
        isActive4: false,
        isActive5: false,
        isActive6: true,
        isActive7: false,
        isActive8: false,
        isActive9: false
      });

      sessionStorage.removeItem("search");
    };

    handleShows6 = () => {
      this.setState({
       /* isActiveCreate: false,
        isActive: false,
        isActive1: false,
        isActive2: false,
        isActive3: false,
        isActive4: false,
        isActive5: false,
        isActive6: false,
        isActive7: true,
        isActive8: false,
        isActive9: true */

        isActiveCreate: true,
        isActive: false,
        isActive1: false,
        isActive2: false,
        isActive3: false,
        isActive4: false,
        isActive5: false,
        isActive6: true,
        isActive7: true,
        isActive8: false,
        isActive9: false
      });
    }

    handleShows7 = () => {
      this.setState({
        isActiveCreate: false,
        isActive: true,
        isActive1: false,
        isActive2: false,
        isActive3: false,
        isActive4: false,
        isActive5: false,
        isActive6: false,
        isActive7: false,
        isActive8: true,
        isActive9: true
      });
    }

    
  
    handleLoginFormSubmit(e) {
      e.preventDefault();

     //// const err = this.handleValidation();
  
     let self = this, fields = this.state.fields;

     sessionStorage.setItem('text1', fields.text1);
     sessionStorage.setItem('text2', fields.text2);
     sessionStorage.setItem('text3', fields.text3);
     sessionStorage.setItem('text4', fields.text4);
     sessionStorage.setItem('text5', fields.text5);
     sessionStorage.setItem('text6', fields.text6);
     sessionStorage.setItem('search', "true");


     sessionStorage.setItem("chart", "true");
  
     //console.log(fields.fullname);
     //console.log(fields.email);
     //console.log(fields.loginPassword);

     this.setState({
      isActiveCreate: true,
        isActive: false,
        isActive1: false,
        isActive2: true,
        isActive3: false,
        isActive4: false,
        isActive5: false,
        isActive6: true,
        isActive7: false,
        isActive8: false,
        isActive9: false
    });

     window.location.href="/visualseditor"; 

     

    this.forceUpdate();
    

    }

    //state = { isOpen: false };
  
    handleShowDialog = () => {
      this.setState({ isOpen: !this.state.isOpen });
      console.log("clicked");
      
    };

  /*
    constructor(props) {
      super(props);
  
      this.state = { isOpen: false };
    }
  
    toggleModal = () => {
      this.setState({
        isOpen: !this.state.isOpen
      });
    }
    */

   

   componentDidMount() {

    this.attachQuillRefs();

   /* const ctx = this.ctx;
    new Chart(this.ctx, {
      type: "bar",
      data: {
        labels: ["Red", "Blue", "Yellow"],
        datasets: [
          {
            label: "# of Likes",
            data: [12, 19, 3],
            backgroundColor: [
              "rgb(255,0,0)",
              "rgb(0,0,255)",
              "rgb(128,0,0)"
            ]
          },
          {
            label: "# of Likes",
            data: [-12, -19, -3],
            backgroundColor: [
              "rgb(255,0,0)",
              "rgb(0,0,255)",
              "rgb(128,0,0)"
            ]
          }
        ]
      }
    }); */
  }

  /*
  componentDidMount () {
    this.attachQuillRefs()
  }
  */
  
  componentDidUpdate () {
    this.attachQuillRefs()
  }

  attachQuillRefs() {

    //alert('here1....');

    // Ensure React-Quill reference is available:
    if (typeof this.reactQuillRef.getEditor !== 'function') return;
    // Skip if Quill reference is defined:
    if (this.quillRef != null) return;
    
    const quillRef = this.reactQuillRef.getEditor();
    if (quillRef != null) this.quillRef = quillRef;
  }
  
  ////};

  handleClick () {

    //alert('here2');

    var range = this.quillRef.getSelection();
    let position = range ? range.index : 0;
    
    /*
    this.quillRef.insertText(position, 'Hello, World!', {
      'color': '#000000',
      'italic': true
    });
    */

    ////this.quillRef.insertText(0, 'Hello', 'bold', true);

    this.quillRef.insertText(5, 'Quill', {
      'color': '#ffff00', 
      'italic': true
    });
    //this.quillRef.insertEmbed(position, 'audio', 'https://www.sample-videos.com/audio/mp3/crowd-cheering.mp3', 'user');
    //this.quillRef.insertEmbed(position, 'font', 'Hello Doing', 'user');

  /*  this.quillRef.insertEmbed(position, 'AppPanelEmbed', `
          <div class="app_card_header">     
              Custom panel title
          </div>
          <div class="app_card_content">     
              Custom panel content
          </div>
          <div class="app_card_footer">     
              footer
          </div>
      `);   */

  }

  /*    
  <h1 style={{colour:'powderblue'}} onClick={this.handleClick}>ACTION PLAY</h1><br />
  <p style={{color:'red'}} onClick={this.handleClick1}>Sparkle</p><br />
  <p style={{color:'green'}} onClick={this.handleClick2}>FREE DELIVERY</p>
  <p style={{color:'red'}} onClick={this.handleClick3}>HUGE SALE</p><br />
  <p style={{color:'green'}} onClick={this.handleClick4}>Game On</p>                     
  */                        


  handleClick () {

    alert('here2');

    var range = this.quillRef.getSelection();
    let position = range ? range.index : 0;
    
    this.quillRef.insertText(5, 'ACTION PLAY', {
      'color': 'powderblue', 
      'italic': true
    });
    
  }

  handleClick1 () {

    alert('here2');

    var range = this.quillRef.getSelection();
    let position = range ? range.index : 0;
    
    this.quillRef.insertText(5, 'Sparkle', {
      'color': 'red', 
      'italic': true
    });
    
  }

  handleClick2 () {

    alert('here2');

    var range = this.quillRef.getSelection();
    let position = range ? range.index : 0;
    
    this.quillRef.insertText(5, 'FREE DELIVERY', {
      'color': 'green', 
      'italic': true
    });
    
  }

  handleClick3 () {

    alert('here2');

    var range = this.quillRef.getSelection();
    let position = range ? range.index : 0;
    
    this.quillRef.insertText(5, 'HUGE SALE', {
      'color': 'red', 
      'italic': true
    });
    
  }

  handleClick4 () {

    alert('here2');

    var range = this.quillRef.getSelection();
    let position = range ? range.index : 0;
    
    this.quillRef.insertText(5, 'Game On', {
      'color': 'green', 
      'italic': true
    });
    
  }

   
  
  // Add sizes to whitelist and register them
  /* const Size = Quill.import("formats/size");
  Size.whitelist = ["extra-small", "small", "medium", "large","extra-large"];
  Quill.register(Size, true);
  */
  
 


  
    render() {

      const CustomButton = () => <span className="octicon octicon-star" />

/*
 * Event handler to be attached using Quill toolbar module (see line 73)
 * https://quilljs.com/docs/modules/toolbar/
 */
function insertStar () {
  const cursorPosition = this.quill.getSelection().index
  this.quill.insertText(cursorPosition, "★")
  this.quill.setSelection(cursorPosition + 1)
}

function insertCustomTags (args) {
  
  console.log("insertCustomTags", args);
  
  
  const value = args[0];    
  
  const cursorPosition = this.quill.getSelection().index
  this.quill.insertText(cursorPosition, value)
  this.quill.setSelection(cursorPosition + value.length)
}

/*
 * Custom toolbar component including insertStar button and dropdowns
 */
const CustomToolbars = () => (
  <div id="toolbar">
    <select className="ql-header">
      <option value="1"></option>
      <option value="2"></option>
      <option selected></option>
    </select>
    <button className="ql-bold"></button>
    <button className="ql-italic"></button>
    <select className="ql-color">
      <option value="red"></option>
      <option value="green"></option>
      <option value="blue"></option>
      <option value="orange"></option>
      <option value="violet"></option>
      <option value="#d0d1d2"></option>
      <option selected></option>
    </select>    
    <button className="ql-insertStar">
      <CustomButton />
    </button>
    
    <select className="ql-insertCustomTags">
      <option value="1">One</option>
      <option value="2">Two</option>
    </select>
  </div>
)

let module = { toolbar: { container: "#toolbar" } }

const CustomHeart = () => <span>♥</span>;

/*
insertHeart() {
    const cursorPosition = this.quill.getSelection().index;
    this.quill.insertText(cursorPosition, "♥");
    this.quill.setSelection(cursorPosition + 1);
  }
  */
  
  /*
   * Custom toolbar component including the custom heart button and dropdowns
   */
  


    /*
    const data02 = [
      { name: 'Summary', value: +retrievedDatas1 },
      { name: 'Skills', value: +retrievedDatas2 },
      { name: 'Profile', value: +retrievedDatas3 },
      { name: 'Work Experience', value: +retrievedDatas4 },
      { name: 'Education', value: +retrievedDatas5 },
      { name: 'Text Section', value: +retrievedDatas6 },
    ];
    */
    const data02 = [
      { name: 'Summary', value: 100 },
      { name: 'Skills', value: 200 },
      { name: 'Profile', value: 100 },
      { name: 'Work Experience', value: 200 },
      { name: 'Education', value: 300 },
      { name: 'Text Section', value: 100 },
    ];  

    
    const ref = React.createRef();

    var retrievedDatas1 = sessionStorage.getItem("text1");
    var retrievedDatas2 = sessionStorage.getItem("text2");
    var retrievedDatas3 = sessionStorage.getItem("text3");

    var retrievedDatas4 = sessionStorage.getItem("text4");
    var retrievedDatas5 = sessionStorage.getItem("text5");
    var retrievedDatas7 = sessionStorage.getItem("text6");

    if(retrievedDatas1==null)
    retrievedDatas1=200;
    if(retrievedDatas2==null)
    retrievedDatas1=100;
    if(retrievedDatas3==null)
    retrievedDatas1=100;
    if(retrievedDatas4==null)
    retrievedDatas1=200;
    if(retrievedDatas5==null)
    retrievedDatas1=300;
    if(retrievedDatas7==null)
    retrievedDatas7=100;

    var retrievedDatas6 = false;
      retrievedDatas6 = sessionStorage.getItem("chart");

      

      //if(retrievedDatas6==true){

    if(this.state.isActive6==true){

      const ctx = this.ctx;

        if(this.ctx!=null){

        var mychart = new Chart(ctx, {
          type: "doughnut",
          data: {
            labels: ["Summary", "Skills", "Profile", "Work Experience", "Education", "Text Section"],
            datasets: [
              {
                label: "# of CV",
                //data: [retrievedDatas1, retrievedDatas2, retrievedDatas3, retrievedDatas4, retrievedDatas5, retrievedDatas6],
                data: [retrievedDatas1, retrievedDatas2, retrievedDatas3, retrievedDatas4, retrievedDatas5, retrievedDatas7],
                backgroundColor: [
                  "rgb(255,0,0)",
                  "rgb(0,0,255)",
                  "rgb(128,0,0)",
                  "rgb(64, 0, 255)",
                  "rgb(0, 255, 0)",
                  "rgb(255, 255, 0)"
                ]
              }
            ]
          }
        });

      }

        

      }


      const isMobile = window.innerWidth <= 500;

      const onEditorStateChange = (newEditorState) => {
        this.state.editorState = newEditorState;
      }
  
      const { editorState } = this.state;
  
      const handleLoginFormSubmits = (e) => {
  
        e.preventDefault();
      
        this.state.hideToolbar=!this.state.hideToolbar;

        
      
      
      };


    /* const ctxs = this.ctxs;
      const config = {
        type: 'line',
        data: {
          labels: [new Date('2019-08-20'), new Date('2019-08-25'), new Date('2019-08-30')],
          datasets: [{
            label: 'Line',
            data: [2, 5, 3],
            borderColor: '#D4213D',
            fill: false,
          }, ],
        },
        options: {
          scales: {
            xAxes: [{
              type: 'time',
            }, ],
          },
          pan: {
            enabled: true,
            mode: 'xy',
          },
          zoom: {
            enabled: true,
            mode: 'x', // or 'x' for "drag" version
          },
        },
      };
      
      ////window.onload = function() {
      new Chart(this.ctxs, config);

      const state = {
        labels: ['January', 'February', 'March',
                 'April', 'May'],
        datasets: [
          {
            label: 'Rainfall',
            backgroundColor: [
              '#B21F00',
              '#C9DE00',
              '#2FDE00',
              '#00A6B4',
              '#6800B4'
            ],
            hoverBackgroundColor: [
            '#501800',
            '#4B5000',
            '#175000',
            '#003350',
            '#35014F'
            ],
            data: [65, 59, 80, 81, 56]
          }
        ]
      }
      */

   /*
   const ctx = this.ctx;
   var charts = new Chart(this.ctx, {
      type: "doughnut",
      data: {
        labels: ["Red", "Blue", "Yellow"],
        datasets: [
          {
            label: "# of Likes",
            data: [12, 19, 3],
            backgroundColor: [
              "rgb(255,0,0)",
              "rgb(0,0,255)",
              "rgb(128,0,0)"
            ]
          }
        ]
      }
    });
    */

    //const chart = charts;

     /* 
     
      alert(chart.toBase64Image(chart));

      alert(chart);

      const done = () => {

      let chart = this.ctx.current.charts;

      alert(chart.toBase64Image(chart));

      alert(chart);

      ////console.log(chart.toBase64Image(chart));
    }

    */


    ////const base64Image = this.refs.current.this.ctx.toBase64Image();

    ////alert(base64Image);

/*
    var chart = new Chart(ctx, {
      type: 'pie',
      data: {
         labels: ['Standing costs', 'Running costs'], // responsible for how many bars are gonna show on the chart
         // create 12 datasets, since we have 12 items
         // data[0] = labels[0] (data for first bar - 'Standing costs') | data[1] = labels[1] (data for second bar - 'Running costs')
         // put 0, if there is no data for the particular bar
         datasets: [{
            label: 'Washing and cleaning',
            data: [0, 8],
            backgroundColor: '#22aa99'
         }, {
            label: 'Traffic tickets',
            data: [0, 2],
            backgroundColor: '#994499'
         }, {
            label: 'Tolls',
            data: [0, 1],
            backgroundColor: '#316395'
         }, {
            label: 'Parking',
            data: [5, 2],
            backgroundColor: '#b82e2e'
         }, {
            label: 'Car tax',
            data: [0, 1],
            backgroundColor: '#66aa00'
         }, {
            label: 'Repairs and improvements',
            data: [0, 2],
            backgroundColor: '#dd4477'
         }, {
            label: 'Maintenance',
            data: [6, 1],
            backgroundColor: '#0099c6'
         }, {
            label: 'Inspection',
            data: [0, 2],
            backgroundColor: '#990099'
         }, {
            label: 'Loan interest',
            data: [0, 3],
            backgroundColor: '#109618'
         }, {
            label: 'Depreciation of the vehicle',
            data: [0, 2],
            backgroundColor: '#109618'
         }, {
            label: 'Fuel',
            data: [0, 1],
            backgroundColor: '#dc3912'
         }, {
            label: 'Insurance and Breakdown cover',
            data: [4, 0],
            backgroundColor: '#3366cc'
         }]
      },
      options: {
         responsive: false,
         legend: {
            position: 'right' // place legend on the right side of chart
         },
         scales: {
            xAxes: [{
               stacked: true // this should be set to make the bars stacked
            }],
            yAxes: [{
               stacked: true // this also..
            }]
         }  
      }
   });

   */
   
   ////var charts = chart!=null ? chart.toBase64Image():null;


   //alert( charts );

   const Size = Quill.import('attributors/style/size');
   Size.whitelist = ['10px','14px','18px','20px','24px', '48px', '100px', '200px', '300px'];
   Quill.register(Size, true);
   
    const colorList = ['#001f3f', '#0074D9', '#7FDBFF', '#39CCCC', '#3D9970', '#2ECC40', '#01FF70', '#FFDC00', '#FF851B', '#FF4136', '#85144b', '#F012BE', '#B10DC9', '#111111', '#AAAAAA', '#DDDDDD', '#FFFFFF'];
      
   
   // Add fonts to whitelist and register them
   const Font = Quill.import("formats/font");
   Font.whitelist = [
     "arial",
     "comic-sans",
     "courier-new",
     "georgia",
     "helvetica",
     "lucida"
   ];
   Quill.register(Font, true);
    
    
  
    



      const imagesPath = Constants.imagespath;

      ////const { showDiv } = this.state.showDiv;

      const template = () => {
        window.location.href = "/Template2";
        console.log("Template Page"); 
       }
  
       const template1 = () => {
        window.location.href = "/SampleTemplate2";
        console.log("Template Page"); 
       }

       const template2 = () => {
        window.location.href = "/SampleTemplate3";
        console.log("Template Page"); 
       }

       const template3 = () => {
        window.location.href = "/SampleTemplate4";
        console.log("Template Page"); 
       }

       const piecharts = () => {
        window.location.href = "/piechart";
        console.log("Template Page"); 
       }

       const barcharts = () => {
        window.location.href = "/barchart";
        console.log("Template Page"); 
       }

       const linecharts = () => {
        window.location.href = "/linechart";
        console.log("Template Page"); 
       }

    var title;   

    var banner_image;

    var banner_images55iour=[];

    const parsedTextimgour=[
    ];

    const iframe = '<iframe src="http://plnkr.co/" width="540" height="450"></iframe>'; 

    const apiUrl = 'http://162.213.248.35:3003/api/get-all-image';
 
    //alert("get all image .... : "); 
    
    axios(apiUrl, {
     method: 'GET',
     mode: 'no-cors',
     headers: {
       'Accept' : 'application/json',
       "Content-Type" : "application/json"
     },
    ////credentials: 'same-origin',
   }).then(response => {
     //return  response;
     //alert('products:'+JSON.stringify(response));
     
     const imagedata = JSON.stringify(response.data.result)
     var imagedatas = JSON.parse(imagedata);

      //alert("imagedatas : "+JSON.stringify(imagedatas));

      for (var i = 0; i < imagedatas.length; i++) {

        ////title = imagedatas[i].image;

        ////alert(title);

        //banner_image = "http://162.213.248.35:3004/images/uploads/" + imagedatas[i].image_slug;

        banner_image = imagedatas[i].image;
    
        banner_images55iour.push(banner_image);
        
      }

      localStorage.setItem('text5sarriour',  JSON.stringify(banner_images55iour));

      var retrievedDatas55our = localStorage.getItem("text5sarriour");
    
      var bannerimages12 = JSON.parse(retrievedDatas55our); 
      
      if(bannerimages12!=null){

      for (var i = 0; i < bannerimages12.length; i++) {
            
            const itemsimg = bannerimages12[i];

            //alert(itemsimg);
    
            parsedTextimgour.push({
                textimg: itemsimg
            });

            if(parsedTextimgour.length>1){
              localStorage.setItem('titlestxtimgour', JSON.stringify(parsedTextimgour));
  
              sessionStorage.setItem('titlestxtimgour', JSON.stringify(parsedTextimgour));
            }

      }  

    }

    })
     .catch((error) => {
         //return  error;
         //alert('ERROR1111111111:'+error);
    });

      var retrievedDatas59imgour = localStorage.getItem("titlestxtimgour");
    
      var bannertxt11imgour = JSON.parse(retrievedDatas59imgour);

      ////alert(bannertxt11imgour);

      return (

      <div class="site-inner-header">
      <div class="container" style={{minHeight:'50px'}}>
          <div class="row">
              <div class="col-md-6">
                  <div class="inner-nav">
                      <ul>
                          <li><a href="index.html" style={{color: '#fff'}}>Home</a></li>
                          <li>File</li>
                          <li><FontAwesomeIcon icon={faCrown} /> Resize</li>
                      </ul>
                  </div>
              </div>
              <div class="col-md-6">
                  <div class="left-inner-nav">
                      <ul>
                          
                      </ul>
                  </div>
              </div>
          </div>
      </div>
    <div class="grey-bg mg55" style={{height:'900px'}}>    
    <div class="container-fluid">
        <div class="row">
             
            <div style={{height:'900px',width:'500px',marginTop:'-35px'}} class="col-lg-4 col-md-4 col-sm-4 col-xs-4 bhoechie-tab-container">
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-3 bhoechie-tab-menu">
                    <div class="list-group">
                    {this.state.isActiveCreate ?(
                        <a  href="#" onClick={this.handleShowsCreate} class="list-group-item active text-center"> <FontAwesomeIcon icon={faColumns} />
                            <br />Create New </a>
                     ) : (    
                        <a  href="#" onClick={this.handleShowsCreate} class="list-group-item text-center"> <FontAwesomeIcon icon={faColumns} />
                            <br />Create New </a>      
                     )}                 
                     {this.state.isActive1 ?(
                        <a href="#" onClick={this.handleShows} class="list-group-item active text-center"> <FontAwesomeIcon icon={faVideo} />
                            <br />Photos </a>
                      ) : (    
                        <a href="#" onClick={this.handleShows} class="list-group-item text-center"> <FontAwesomeIcon icon={faVideo} />
                            <br />Photos </a>  
                      )} 
                      {this.state.isActive2 ?(       
                        <a href="#" onClick={this.handleShows1} class="list-group-item active text-center"> <FontAwesomeIcon icon={faEnvelope} />
                            <br />Elements </a>
                       ) : ( 
                        <a href="#" onClick={this.handleShows1} class="list-group-item text-center"> <FontAwesomeIcon icon={faEnvelope} />
                        <br />Elements </a>  
                      )}   
                      {this.state.isActive3 ?(  
                        <a href="#" onClick={this.handleShows2} class="list-group-item active text-center"> <FontAwesomeIcon icon={faTextHeight} />
                            <br /> Text </a>
                       ) : (
                        <a href="#" onClick={this.handleShows2} class="list-group-item text-center"> <FontAwesomeIcon icon={faTextHeight} />
                        <br /> Text </a>
                       )} 
                       {this.state.isActive4 ?(
                        <a href="#" onClick={this.handleShows3} class="list-group-item active text-center"> <FontAwesomeIcon icon={faUpload} />
                            <br /> Upload </a>
                        ) : (
                          <a href="#" onClick={this.handleShows3} class="list-group-item text-center"> <FontAwesomeIcon icon={faUpload} />
                          <br /> Upload </a>
                        )}   
                       {this.state.isActive6 ?(
                        <a href="#" onClick={this.handleShows5} class="list-group-item active text-center"> <FontAwesomeIcon icon={faFolder} />
                            <br /> Chart </a>
                        ) : (
                          <a href="#" onClick={this.handleShows5} class="list-group-item text-center"> <FontAwesomeIcon icon={faFolder} />
                          <br /> Chart </a>  
                       )}    
                    </div>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-9 col-xs-9 bhoechie-tab">
                {this.state.isActive2 &&
                    <div class="bhoechie-tab-content active">
                        <div class="bhoechie-tab-content active">
                        <div class="inner-search">
                            <input type="search" placeholder="Search Your Content"></input> </div>
                        <div class="inner-template">

            

                        <div class="row">

<div class="col-md-3 col-sm-3 col-xs-3"> 

<Draggable
handle=".handle"
defaultPosition={{x: 0, y: 0}}
position={null}
grid={[125, 125]}
scale={1}
onStart={this.handleStart}
onDrag={this.handleDrag}
onStop={this.handleStop}>
<Resizable
  ref={this.refResizable}
  defaultSize={20}
>

<img style={{width:'5px', height:'5px'}} alt="" src={image1} class="img-responsive" style={{backgroundColor:'white'}}></img>  
</Resizable>
</Draggable>   

</div>

<div class="col-md-3 col-sm-3 col-xs-3">

<Draggable
handle=".handle"
defaultPosition={{x: 0, y: 0}}
position={null}
grid={[125, 125]}
scale={1}
onStart={this.handleStart}
onDrag={this.handleDrag}
onStop={this.handleStop}>
<Resizable
  ref={this.refResizable}
  defaultSize={20}
>

<img style={{width:'5px', height:'5px'}} alt="" src={image2} class="img-responsive" style={{backgroundColor:'white'}}></img>  
</Resizable>
</Draggable>   

</div>

<div class="col-md-3 col-sm-3 col-xs-3">

<Draggable
handle=".handle"
defaultPosition={{x: 0, y: 0}}
position={null}
grid={[125, 125]}
scale={1}
onStart={this.handleStart}
onDrag={this.handleDrag}
onStop={this.handleStop}>
<Resizable
  ref={this.refResizable}
  defaultSize={20}
>

<img style={{width:'5px', height:'5px'}} alt="" src={image3} class="img-responsive" style={{backgroundColor:'white'}}></img>  
</Resizable>
</Draggable>   

</div>



<div class="col-md-3 col-sm-3 col-xs-3">

<Draggable
handle=".handle"
defaultPosition={{x: 0, y: 0}}
position={null}
grid={[125, 125]}
scale={1}
onStart={this.handleStart}
onDrag={this.handleDrag}
onStop={this.handleStop}>
<Resizable
  ref={this.refResizable}
  defaultSize={20}
>

<img style={{width:'15px', height:'15px'}} alt="" src={image4} class="img-responsive" style={{backgroundColor:'white'}}></img>  
</Resizable>
</Draggable>   

</div>

<div style={{ height:'5px' }} ></div>

<div class="col-md-3 col-sm-3 col-xs-3">

<Draggable
handle=".handle"
defaultPosition={{x: 0, y: 0}}
position={null}
grid={[125, 125]}
scale={1}
onStart={this.handleStart}
onDrag={this.handleDrag}
onStop={this.handleStop}>
<Resizable
  ref={this.refResizable}
  defaultSize={20}
>

<img style={{width:'15px', height:'15px'}} alt="" src={image5} class="img-responsive" style={{backgroundColor:'white'}}></img>  
</Resizable>
</Draggable>   

</div>
  

<div class="col-md-3 col-sm-3 col-xs-3">

<Draggable
handle=".handle"
defaultPosition={{x: 0, y: 0}}
position={null}
grid={[125, 125]}
scale={1}
onStart={this.handleStart}
onDrag={this.handleDrag}
onStop={this.handleStop}>
<Resizable
  ref={this.refResizable}
  defaultSize={20}
>

<img style={{width:'15px', height:'15px'}} alt="" src={image6} class="img-responsive" style={{backgroundColor:'white'}}></img>  
</Resizable>
</Draggable>   

</div>
  

<div class="col-md-3 col-sm-3 col-xs-3">

<Draggable
handle=".handle"
defaultPosition={{x: 0, y: 0}}
position={null}
grid={[125, 125]}
scale={1}
onStart={this.handleStart}
onDrag={this.handleDrag}
onStop={this.handleStop}>
<Resizable
  ref={this.refResizable}
  defaultSize={20}
>

<img style={{width:'15px', height:'15px'}} alt="" src={image7} class="img-responsive" style={{backgroundColor:'white'}}></img>  
</Resizable>
</Draggable>   

</div>  

<div class="col-md-3 col-sm-3 col-xs-3">

<Draggable
handle=".handle"
defaultPosition={{x: 0, y: 0}}
position={null}
grid={[125, 125]}
scale={1}
onStart={this.handleStart}
onDrag={this.handleDrag}
onStop={this.handleStop}>
<Resizable
  ref={this.refResizable}
  defaultSize={20}
>

<img style={{width:'15px', height:'15px'}} alt="" src={image8} class="img-responsive" style={{backgroundColor:'white'}}></img>  
</Resizable>
</Draggable>   

</div>

<div class="col-md-3 col-sm-3 col-xs-3">

<Draggable
handle=".handle"
defaultPosition={{x: 0, y: 0}}
position={null}
grid={[125, 125]}
scale={1}
onStart={this.handleStart}
onDrag={this.handleDrag}
onStop={this.handleStop}>
<Resizable
  ref={this.refResizable}
  defaultSize={20}
>

<img style={{width:'15px', height:'15px'}} alt="" src={image9} class="img-responsive" style={{backgroundColor:'white'}}></img>  
</Resizable>
</Draggable>   

</div>  

<div class="col-md-3 col-sm-3 col-xs-3">

<Draggable
handle=".handle"
defaultPosition={{x: 0, y: 0}}
position={null}
grid={[125, 125]}
scale={1}
onStart={this.handleStart}
onDrag={this.handleDrag}
onStop={this.handleStop}>
<Resizable
  ref={this.refResizable}
  defaultSize={20}
>

<img style={{width:'15px', height:'15px'}} alt="" src={image10} class="img-responsive" style={{backgroundColor:'white'}}></img>  
</Resizable>
</Draggable>   

</div>  

<div class="col-md-3 col-sm-3 col-xs-3">

  <Draggable
  handle=".handle"
  defaultPosition={{x: 0, y: 0}}
  position={null}
  grid={[125, 125]}
  scale={1}
  onStart={this.handleStart}
  onDrag={this.handleDrag}
  onStop={this.handleStop}>
  <Resizable
    ref={this.refResizable}
    defaultSize={20}
  >

  <img style={{width:'15px', height:'15px'}} alt="" src={image11} class="img-responsive" style={{backgroundColor:'white'}}></img>  
  </Resizable>
  </Draggable>   

  </div>  

  <div class="col-md-3 col-sm-3 col-xs-3">

  <Draggable
  handle=".handle"
  defaultPosition={{x: 0, y: 0}}
  position={null}
  grid={[125, 125]}
  scale={1}
  onStart={this.handleStart}
  onDrag={this.handleDrag}
  onStop={this.handleStop}>
  <Resizable
    ref={this.refResizable}
    defaultSize={20}
  >

  <img style={{width:'15px', height:'15px'}} alt="" src={image12} class="img-responsive" style={{backgroundColor:'white'}}></img>  
  </Resizable>
  </Draggable>   

  </div>  

  <div class="col-md-3 col-sm-3 col-xs-3">

  <Draggable
  handle=".handle"
  defaultPosition={{x: 0, y: 0}}
  position={null}
  grid={[125, 125]}
  scale={1}
  onStart={this.handleStart}
  onDrag={this.handleDrag}
  onStop={this.handleStop}>
  <Resizable
    ref={this.refResizable}
    defaultSize={20}
  >

  <img style={{width:'15px', height:'15px'}} alt="" src={image13} class="img-responsive" style={{backgroundColor:'white'}}></img>  
  </Resizable>
  </Draggable>   

  </div>  

  <div class="col-md-3 col-sm-3 col-xs-3">

    <Draggable
    handle=".handle"
    defaultPosition={{x: 0, y: 0}}
    position={null}
    grid={[125, 125]}
    scale={1}
    onStart={this.handleStart}
    onDrag={this.handleDrag}
    onStop={this.handleStop}>
    <Resizable
      ref={this.refResizable}
      defaultSize={20}
    >

    <img style={{width:'15px', height:'15px'}} alt="" src={image14} class="img-responsive" style={{backgroundColor:'white'}}></img>  
    </Resizable>
    </Draggable>   

    </div>  

    <div class="col-md-3 col-sm-3 col-xs-3">

    <Draggable
    handle=".handle"
    defaultPosition={{x: 0, y: 0}}
    position={null}
    grid={[125, 125]}
    scale={1}
    onStart={this.handleStart}
    onDrag={this.handleDrag}
    onStop={this.handleStop}>
    <Resizable
      ref={this.refResizable}
      defaultSize={20}
    >

    <img style={{width:'15px', height:'15px'}} alt="" src={image15} class="img-responsive" style={{backgroundColor:'white'}}></img>  
    </Resizable>
    </Draggable>   

    </div>  

    <div class="col-md-3 col-sm-3 col-xs-3">

      <Draggable
      handle=".handle"
      defaultPosition={{x: 0, y: 0}}
      position={null}
      grid={[125, 125]}
      scale={1}
      onStart={this.handleStart}
      onDrag={this.handleDrag}
      onStop={this.handleStop}>
      <Resizable
        ref={this.refResizable}
        defaultSize={20}
      >

      <img style={{width:'15px', height:'15px'}} alt="" src={image16} class="img-responsive" style={{backgroundColor:'white'}}></img>  
      </Resizable>
      </Draggable>   

      </div>  

      <div class="col-md-3 col-sm-3 col-xs-3">

      <Draggable
      handle=".handle"
      defaultPosition={{x: 0, y: 0}}
      position={null}
      grid={[125, 125]}
      scale={1}
      onStart={this.handleStart}
      onDrag={this.handleDrag}
      onStop={this.handleStop}>
      <Resizable
        ref={this.refResizable}
        defaultSize={20}
      >

      <img style={{width:'15px', height:'15px'}} alt="" src={image17} class="img-responsive" style={{backgroundColor:'white'}}></img>  
      </Resizable>
      </Draggable>   

      </div>  

      <div class="col-md-3 col-sm-3 col-xs-3">

      <Draggable
      handle=".handle"
      defaultPosition={{x: 0, y: 0}}
      position={null}
      grid={[125, 125]}
      scale={1}
      onStart={this.handleStart}
      onDrag={this.handleDrag}
      onStop={this.handleStop}>
      <Resizable
        ref={this.refResizable}
        defaultSize={20}
      >

      <img style={{width:'15px', height:'15px'}} alt="" src={image18} class="img-responsive" style={{backgroundColor:'white'}}></img>  
      </Resizable>
      </Draggable>   

      </div>  

      <div class="col-md-3 col-sm-3 col-xs-3">

      <Draggable
      handle=".handle"
      defaultPosition={{x: 0, y: 0}}
      position={null}
      grid={[125, 125]}
      scale={1}
      onStart={this.handleStart}
      onDrag={this.handleDrag}
      onStop={this.handleStop}>
      <Resizable
        ref={this.refResizable}
        defaultSize={20}
      >

      <img style={{width:'15px', height:'15px'}} alt="" src={image19} class="img-responsive" style={{backgroundColor:'white'}}></img>  
      </Resizable>
      </Draggable>   

      </div>  



<div class="col-md-3 col-sm-3 col-xs-3">

<Draggable
handle=".handle"
defaultPosition={{x: 0, y: 0}}
position={null}
grid={[125, 125]}
scale={1}
onStart={this.handleStart}
onDrag={this.handleDrag}
onStop={this.handleStop}>
<Resizable
  ref={this.refResizable}
  defaultSize={20}
>

<img style={{width:'15px', height:'15px'}} alt="" src={image20} class="img-responsive" style={{backgroundColor:'white'}}></img>  
</Resizable>
</Draggable>   

</div>  

<div class="col-md-3 col-sm-3 col-xs-3">

<Draggable
handle=".handle"
defaultPosition={{x: 0, y: 0}}
position={null}
grid={[125, 125]}
scale={1}
onStart={this.handleStart}
onDrag={this.handleDrag}
onStop={this.handleStop}>
<Resizable
  ref={this.refResizable}
  defaultSize={20}
>

<img style={{width:'15px', height:'15px'}} alt="" src={image21} class="img-responsive" style={{backgroundColor:'white'}}></img>  
</Resizable>
</Draggable>   

</div>  

<div class="col-md-3 col-sm-3 col-xs-3">

  <Draggable
  handle=".handle"
  defaultPosition={{x: 0, y: 0}}
  position={null}
  grid={[125, 125]}
  scale={1}
  onStart={this.handleStart}
  onDrag={this.handleDrag}
  onStop={this.handleStop}>
  <Resizable
    ref={this.refResizable}
    defaultSize={20}
  >

  <img style={{width:'15px', height:'15px'}} alt="" src={image22} class="img-responsive" style={{backgroundColor:'white'}}></img>  
  </Resizable>
  </Draggable>   

  </div>  

  <div class="col-md-3 col-sm-3 col-xs-3">

  <Draggable
  handle=".handle"
  defaultPosition={{x: 0, y: 0}}
  position={null}
  grid={[125, 125]}
  scale={1}
  onStart={this.handleStart}
  onDrag={this.handleDrag}
  onStop={this.handleStop}>
  <Resizable
    ref={this.refResizable}
    defaultSize={20}
  >

  <img style={{width:'15px', height:'15px'}} alt="" src={image23} class="img-responsive" style={{backgroundColor:'white'}}></img>  
  </Resizable>
  </Draggable>   

  </div>  

  <div class="col-md-3 col-sm-3 col-xs-3">

    <Draggable
    handle=".handle"
    defaultPosition={{x: 0, y: 0}}
    position={null}
    grid={[125, 125]}
    scale={1}
    onStart={this.handleStart}
    onDrag={this.handleDrag}
    onStop={this.handleStop}>
    <Resizable
      ref={this.refResizable}
      defaultSize={20}
    >

    <img style={{width:'15px', height:'15px'}} alt="" src={image24} class="img-responsive" style={{backgroundColor:'white'}}></img>  
    </Resizable>
    </Draggable>   

    </div>  

    <div class="col-md-3 col-sm-3 col-xs-3">

    <Draggable
    handle=".handle"
    defaultPosition={{x: 0, y: 0}}
    position={null}
    grid={[125, 125]}
    scale={1}
    onStart={this.handleStart}
    onDrag={this.handleDrag}
    onStop={this.handleStop}>
    <Resizable
      ref={this.refResizable}
      defaultSize={20}
    >

    <img style={{width:'15px', height:'15px'}} alt="" src={image25} class="img-responsive" style={{backgroundColor:'white'}}></img>  
    </Resizable>
    </Draggable>   

    </div>  

    <div class="col-md-3 col-sm-3 col-xs-3">

    <Draggable
    handle=".handle"
    defaultPosition={{x: 0, y: 0}}
    position={null}
    grid={[125, 125]}
    scale={1}
    onStart={this.handleStart}
    onDrag={this.handleDrag}
    onStop={this.handleStop}>
    <Resizable
      ref={this.refResizable}
      defaultSize={20}
    >

    <img style={{width:'15px', height:'15px'}} alt="" src={image26} class="img-responsive" style={{backgroundColor:'white'}}></img>  
    </Resizable>
    </Draggable>   

    </div>  

    <div class="col-md-3 col-sm-3 col-xs-3">

    <Draggable
    handle=".handle"
    defaultPosition={{x: 0, y: 0}}
    position={null}
    grid={[125, 125]}
    scale={1}
    onStart={this.handleStart}
    onDrag={this.handleDrag}
    onStop={this.handleStop}>
    <Resizable
      ref={this.refResizable}
      defaultSize={20}
    >

    <img style={{width:'15px', height:'15px'}} alt="" src={image27} class="img-responsive" style={{backgroundColor:'white'}}></img>  
    </Resizable>
    </Draggable>   

    </div>  

    



    <div class="col-md-3 col-sm-3 col-xs-3">

    <Draggable
    handle=".handle"
    defaultPosition={{x: 0, y: 0}}
    position={null}
    grid={[125, 125]}
    scale={1}
    onStart={this.handleStart}
    onDrag={this.handleDrag}
    onStop={this.handleStop}>
    <Resizable
      ref={this.refResizable}
      defaultSize={20}
    >

    <img style={{width:'15px', height:'15px'}} alt="" src={image28} class="img-responsive" style={{backgroundColor:'white'}}></img>  
    </Resizable>
    </Draggable>   

    </div>  

    <div class="col-md-3 col-sm-3 col-xs-3">

    <Draggable
    handle=".handle"
    defaultPosition={{x: 0, y: 0}}
    position={null}
    grid={[125, 125]}
    scale={1}
    onStart={this.handleStart}
    onDrag={this.handleDrag}
    onStop={this.handleStop}>
    <Resizable
      ref={this.refResizable}
      defaultSize={20}
    >

    <img style={{width:'15px', height:'15px'}} alt="" src={image29} class="img-responsive" style={{backgroundColor:'white'}}></img>  
    </Resizable>
    </Draggable>   

    </div>  

    <div class="col-md-3 col-sm-3 col-xs-3">

      <Draggable
      handle=".handle"
      defaultPosition={{x: 0, y: 0}}
      position={null}
      grid={[125, 125]}
      scale={1}
      onStart={this.handleStart}
      onDrag={this.handleDrag}
      onStop={this.handleStop}>
      <Resizable
        ref={this.refResizable}
        defaultSize={20}
      >

      <img style={{width:'15px', height:'15px'}} alt="" src={image30} class="img-responsive" style={{backgroundColor:'white'}}></img>  
      </Resizable>
      </Draggable>   

      </div>  

      <div class="col-md-3 col-sm-3 col-xs-3">

      <Draggable
      handle=".handle"
      defaultPosition={{x: 0, y: 0}}
      position={null}
      grid={[125, 125]}
      scale={1}
      onStart={this.handleStart}
      onDrag={this.handleDrag}
      onStop={this.handleStop}>
      <Resizable
        ref={this.refResizable}
        defaultSize={20}
      >

      <img style={{width:'15px', height:'15px'}} alt="" src={image31} class="img-responsive" style={{backgroundColor:'white'}}></img>  
      </Resizable>
      </Draggable>   

      </div>  

      <div class="col-md-3 col-sm-3 col-xs-3">

        <Draggable
        handle=".handle"
        defaultPosition={{x: 0, y: 0}}
        position={null}
        grid={[125, 125]}
        scale={1}
        onStart={this.handleStart}
        onDrag={this.handleDrag}
        onStop={this.handleStop}>
        <Resizable
          ref={this.refResizable}
          defaultSize={20}
        >

        <img style={{width:'15px', height:'15px'}} alt="" src={image32} class="img-responsive" style={{backgroundColor:'white'}}></img>  
        </Resizable>
        </Draggable>   

        </div>  

        <div class="col-md-3 col-sm-3 col-xs-3">

        <Draggable
        handle=".handle"
        defaultPosition={{x: 0, y: 0}}
        position={null}
        grid={[125, 125]}
        scale={1}
        onStart={this.handleStart}
        onDrag={this.handleDrag}
        onStop={this.handleStop}>
        <Resizable
          ref={this.refResizable}
          defaultSize={20}
        >

        <img style={{width:'15px', height:'15px'}} alt="" src={image33} class="img-responsive" style={{backgroundColor:'white'}}></img>  
        </Resizable>
        </Draggable>   

        </div>  

        <div class="col-md-3 col-sm-3 col-xs-3">

        <Draggable
        handle=".handle"
        defaultPosition={{x: 0, y: 0}}
        position={null}
        grid={[125, 125]}
        scale={1}
        onStart={this.handleStart}
        onDrag={this.handleDrag}
        onStop={this.handleStop}>
        <Resizable
          ref={this.refResizable}
          defaultSize={20}
        >

        <img style={{width:'15px', height:'15px'}} alt="" src={image34} class="img-responsive" style={{backgroundColor:'white'}}></img>  
        </Resizable>
        </Draggable>   

        </div>  

        <div class="col-md-3 col-sm-3 col-xs-3">

        <Draggable
        handle=".handle"
        defaultPosition={{x: 0, y: 0}}
        position={null}
        grid={[125, 125]}
        scale={1}
        onStart={this.handleStart}
        onDrag={this.handleDrag}
        onStop={this.handleStop}>
        <Resizable
          ref={this.refResizable}
          defaultSize={20}
        >

        <img style={{width:'15px', height:'15px'}} alt="" src={image35} class="img-responsive" style={{backgroundColor:'white'}}></img>  
        </Resizable>
        </Draggable>   

        </div>  

        <div class="col-md-3 col-sm-3 col-xs-3">

    <Draggable
    handle=".handle"
    defaultPosition={{x: 0, y: 0}}
    position={null}
    grid={[125, 125]}
    scale={1}
    onStart={this.handleStart}
    onDrag={this.handleDrag}
    onStop={this.handleStop}>
    <Resizable
      ref={this.refResizable}
      defaultSize={20}
    >

    <img style={{width:'15px', height:'15px'}} alt="" src={pies} class="img-responsive" style={{backgroundColor:'white'}}></img>  
    </Resizable>
    </Draggable>   

    </div>  


<div>
</div>   

<br />
<br />
<br />


<div class="col-md-12 col-sm-12 col-xs-12">      
{bannertxt11imgour!=null && bannertxt11imgour.map((person, index) => 
<div class="col-md-3 col-sm-3 col-xs-3">
<img style={{width:'5px', height:'5px'}} alt="" src={person.textimg} class="img-responsive" style={{backgroundColor:'coral'}} />
      </div>
)}
</div>
</div>


                            
                    </div>
                            
                    </div>
                    </div>
                    
                }  
                {this.state.isActive6 &&
                    <div class="bhoechie-tab-content active">
                        <div class="inner-search">
                            <input type="search" placeholder="Search Your Content"></input> </div>
                        <div class="inner-template">
                            <h4>Charts</h4>
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-6"> <img onClick={this.handleShows6} src={pies} class="img-responsive" alt="" style={{backgroundColor:'coral'}}></img> </div>
                                <div class="col-md-6 col-sm-6 col-xs-6"> <img onClick={barcharts} src={bars} class="img-responsive" alt="" style={{backgroundColor:'coral'}}></img> </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-6"> <img onClick={linecharts} alt="" src={lines} class="img-responsive" style={{backgroundColor:'coral'}}></img>  </div>
                                
                            </div>
                            
                        </div>
                    </div>
                }                
                {this.state.isActive &&
                    <div class="bhoechie-tab-content active">
                        <div class="inner-search">
                            <input type="search" placeholder="Search Your Content"></input> </div>
                        <div class="inner-template">
                            <h4>Sample Templates</h4>
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-6"> <img onClick={this.handleShows7} src={Template1} class="img-responsive" alt="" /> </div>
                                <div class="col-md-6 col-sm-6 col-xs-6"> <img onClick={template2} src={Template2} class="img-responsive" alt="" /> </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-6"> <img onClick={template3} src={Template3} class="img-responsive" alt="" /> </div>
                                <div class="col-md-6 col-sm-6 col-xs-6"> <img src={Userpics} class="img-responsive" alt="" /> </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-6"> <img src={Userpics} class="img-responsive" alt="" /> </div>
                                <div class="col-md-6 col-sm-6 col-xs-6"> <img src={Userpics} class="img-responsive" alt="" /> </div>
                            </div>
                        </div>
                    </div>
                }    
                {this.state.isActive7 &&
                    <div class="bhoechie-tab-content active">
                      <form name="chartForm" className="loginForm" onSubmit={(e) => this.handleLoginFormSubmit(e)}>
                        <div class="inner-search">
                            <input type="text" name= "text1" placeholder="Text1" onChange={(e) => this.handleLoginFormData(e)} value={this.state.text1}></input> </div>
                        <br /><br />    
                        <div class="inner-search">
                            <input type="text" name= "text2" placeholder="Text2" onChange={(e) => this.handleLoginFormData(e)} value={this.state.text2}></input> </div> 
                        <br /><br />    
                        <div class="inner-search">
                            <input type="text" name= "text3" placeholder="Text3" onChange={(e) => this.handleLoginFormData(e)} value={this.state.text3}></input> </div>
                        <br /><br />    
                        <div class="inner-search">
                            <input type="text" name= "text4" placeholder="Text4" onChange={(e) => this.handleLoginFormData(e)} value={this.state.text4}></input> </div> 
                        <br /><br />    
                        <div class="inner-search">
                        <input type="text" name= "text5" placeholder="Text5" onChange={(e) => this.handleLoginFormData(e)} value={this.state.text5}></input> </div>   
                        <br /><br />  
                        <div class="inner-search">
                        <input type="text" name= "text6" placeholder="Text6" onChange={(e) => this.handleLoginFormData(e)} value={this.state.text6}></input> </div>   
                        
                        <Button type="submit" bsStyle="primary">SUBMIT</Button>
                    </form>                                       
                    </div>
                    }
                    {this.state.isActive1 &&
                    <div class="bhoechie-tab-content active">
                      <p><div class="col-md-3 col-sm-3 col-xs-3">
          
                              <Draggable
                              handle=".handle"
                              defaultPosition={{x: 0, y: 0}}
                              position={null}
                              grid={[125, 125]}
                              scale={1}
                              onStart={this.handleStart}
                              onDrag={this.handleDrag}
                              onStop={this.handleStop}>
                              <Resizable
                                ref={this.refResizable}
                                defaultSize={20}
                              >
          
                              <img style={{width:'15px', height:'15px'}} alt="" src={imagep1} class="img-responsive" style={{backgroundColor:'white'}}></img>  
                              </Resizable>
                              </Draggable>   
          
                              </div>  
          
                              <div class="col-md-3 col-sm-3 col-xs-3">
          
                              <Draggable
                              handle=".handle"
                              defaultPosition={{x: 0, y: 0}}
                              position={null}
                              grid={[125, 125]}
                              scale={1}
                              onStart={this.handleStart}
                              onDrag={this.handleDrag}
                              onStop={this.handleStop}>
                              <Resizable
                                ref={this.refResizable}
                                defaultSize={20}
                              >
          
                              <img style={{width:'15px', height:'15px'}} alt="" src={imagep2} class="img-responsive" style={{backgroundColor:'white'}}></img>  
                              </Resizable>
                              </Draggable>   
          
                              </div>  
          
                              <div class="col-md-3 col-sm-3 col-xs-3">
          
                                <Draggable
                                handle=".handle"
                                defaultPosition={{x: 0, y: 0}}
                                position={null}
                                grid={[125, 125]}
                                scale={1}
                                onStart={this.handleStart}
                                onDrag={this.handleDrag}
                                onStop={this.handleStop}>
                                <Resizable
                                  ref={this.refResizable}
                                  defaultSize={20}
                                >
          
                                <img style={{width:'15px', height:'15px'}} alt="" src={imagep3} class="img-responsive" style={{backgroundColor:'white'}}></img>  
                                </Resizable>
                                </Draggable>   
          
                                </div>  
          
                                <div class="col-md-3 col-sm-3 col-xs-3">
          
                                <Draggable
                                handle=".handle"
                                defaultPosition={{x: 0, y: 0}}
                                position={null}
                                grid={[125, 125]}
                                scale={1}
                                onStart={this.handleStart}
                                onDrag={this.handleDrag}
                                onStop={this.handleStop}>
                                <Resizable
                                  ref={this.refResizable}
                                  defaultSize={20}
                                >
          
                                <img style={{width:'15px', height:'15px'}} alt="" src={imagep4} class="img-responsive" style={{backgroundColor:'white'}}></img>  
                                </Resizable>
                                </Draggable>   
          
                                </div>  
          
                                <div class="col-md-3 col-sm-3 col-xs-3">
          
                                  <Draggable
                                  handle=".handle"
                                  defaultPosition={{x: 0, y: 0}}
                                  position={null}
                                  grid={[125, 125]}
                                  scale={1}
                                  onStart={this.handleStart}
                                  onDrag={this.handleDrag}
                                  onStop={this.handleStop}>
                                  <Resizable
                                    ref={this.refResizable}
                                    defaultSize={20}
                                  >
          
                                  <img style={{width:'15px', height:'15px'}} alt="" src={imagep5} class="img-responsive" style={{backgroundColor:'white'}}></img>  
                                  </Resizable>
                                  </Draggable>   
          
                                  </div>  
          
                                  <div class="col-md-3 col-sm-3 col-xs-3">
          
                                  <Draggable
                                  handle=".handle"
                                  defaultPosition={{x: 0, y: 0}}
                                  position={null}
                                  grid={[125, 125]}
                                  scale={1}
                                  onStart={this.handleStart}
                                  onDrag={this.handleDrag}
                                  onStop={this.handleStop}>
                                  <Resizable
                                    ref={this.refResizable}
                                    defaultSize={20}
                                  >
          
                                  <img style={{width:'15px', height:'15px'}} alt="" src={imagep6} class="img-responsive" style={{backgroundColor:'white'}}></img>  
                                  </Resizable>
                                  </Draggable>   
          
                                  </div>  
          
                                  <div class="col-md-3 col-sm-3 col-xs-3">
          
                                  <Draggable
                                  handle=".handle"
                                  defaultPosition={{x: 0, y: 0}}
                                  position={null}
                                  grid={[125, 125]}
                                  scale={1}
                                  onStart={this.handleStart}
                                  onDrag={this.handleDrag}
                                  onStop={this.handleStop}>
                                  <Resizable
                                    ref={this.refResizable}
                                    defaultSize={20}
                                  >
          
                                  <img style={{width:'15px', height:'15px'}} alt="" src={imagep7} class="img-responsive" style={{backgroundColor:'white'}}></img>  
                                  </Resizable>
                                  </Draggable>   
          
                                  </div>  
          
                                  <div class="col-md-3 col-sm-3 col-xs-3">
          
                                  <Draggable
                                  handle=".handle"
                                  defaultPosition={{x: 0, y: 0}}
                                  position={null}
                                  grid={[125, 125]}
                                  scale={1}
                                  onStart={this.handleStart}
                                  onDrag={this.handleDrag}
                                  onStop={this.handleStop}>
                                  <Resizable
                                    ref={this.refResizable}
                                    defaultSize={20}
                                  >
          
                                  <img style={{width:'15px', height:'15px'}} alt="" src={imagep8} class="img-responsive" style={{backgroundColor:'white'}}></img>  
                                  </Resizable>
                                  </Draggable>   
          
                                  </div>  
                                  <div class="col-md-3 col-sm-3 col-xs-3">
          
                                  <Draggable
                                  handle=".handle"
                                  defaultPosition={{x: 0, y: 0}}
                                  position={null}
                                  grid={[125, 125]}
                                  scale={1}
                                  onStart={this.handleStart}
                                  onDrag={this.handleDrag}
                                  onStop={this.handleStop}>
                                  <Resizable
                                    ref={this.refResizable}
                                    defaultSize={20}
                                  >
          
                                  <img style={{width:'15px', height:'15px'}} alt="" src={imagep9} class="img-responsive" style={{backgroundColor:'white'}}></img>  
                                  </Resizable>
                                  </Draggable>   
          
                                  </div>  
          
                                  </p>
              
                    </div>
                    }   
                   
                    {this.state.isActive3 &&
                    <div class="bhoechie-tab-content active">
                        <p>
                        <div class="col-md-12 col-sm-12 col-xs-12">

                          <Draggable
                          handle=".handle"
                          defaultPosition={{x: 0, y: 0}}
                          position={null}
                          grid={[125, 125]}
                          scale={1}
                          onStart={this.handleStart}
                          onDrag={this.handleDrag}
                          onStop={this.handleStop}>
                          <Resizable
                            ref={this.refResizable}
                            defaultSize={20}
                          >
                          
                          <h1 style={{colour:'powderblue'}} onClick={this.handleClick}>ACTION PLAY</h1><br />
                          <p style={{color:'red'}} onClick={this.handleClick1}>Sparkle</p><br />
                          <p style={{color:'green'}} onClick={this.handleClick2}>FREE DELIVERY</p><br />
                          <p style={{color:'red'}} onClick={this.handleClick3}>HUGE SALE</p><br />
                          <p style={{color:'green'}} onClick={this.handleClick4}>Game On</p>
                          </Resizable>
                          </Draggable>   

                       </div>  
                        </p>
                    </div>
                    }  
                    {this.state.isActive4 &&   
                    <div class="bhoechie-tab-content active">
                        <p> <div class="inner-sidepage"> <iframe src={"http://162.213.248.35:3005/editorupload"} width="300" height="300" frameborder="0" scrollbars='no' allowfullscreen></iframe></div>
                       </p>
                    </div>
                    }
                    {this.state.isActive5 &&  
                    <div class="bhoechie-tab-content active">
                        <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32. The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32. </p>
                    </div>
                    }
                </div>
            </div>
            { isMobile ?  
          (<div class="col-lg-2 col-md-2 col-sm-3 col-xs-3">
          {
          this.state.isActiveCreate === true ?
          (<div style={{width:'400px', height:'400px', marginRight:'auto'}}>          
        
          <form name="contactForm" className="comment-form contact-form" onSubmit={(e) => handleLoginFormSubmits(e)}>    
          <Button type="submit" style={{marginTop:'30px', width:'110px'}} class="thm-btn" bsStyle="primary"><span>Toolbar</span></Button>
          </form>
      
          <A4 style={{width:'400px', height:'400px'}}>
          <article className={cs({
              [style.cv]: true,
              exportRoot: true
          })}>
              <HeaderPic style={{
                padding: '0px 0px 0px 0px'
          }}>
          <header className={style.header}>
                      
          <div class="clearfix"></div>

          <div mRef={ctx => (this.ctx = ctx)}>
            <canvas width='60' height='70' ref={ctx => (this.ctx = ctx)}/>
          </div>
             
          <div class="col-md-3 col-sm-3 col-xs-3">
              <Draggable
                handle=".handle"
                defaultPosition={{x: 0, y: 0}}
                position={null}
                grid={[35, 35]}
                scale={1}
                onStart={this.handleStart}
                onDrag={this.handleDrag}
                onStop={this.handleStop}>
                <Resizable
                  ref={this.refResizable}
                  defaultSize={20}
                >
                <div>
                  <div className="handle"> <canvas width='60' height='70' ref={ctx => (this.ctx = ctx)}/></div>
                  <div>Drag the Graph</div>
                </div>
                </Resizable>
            </Draggable>
          </div>
          
  
          <div id="editor" style={{height:'5000px', width:'400px'}}>
                          
          <ReactQuill
            modules={modulesQuill}
            formats={formatsQuill}
            onChange={this.handleChange.bind(this)}
            value={this.state.editorHtml}
            onChange={this.handleChange}
          />

          </div>

          </header>
          </HeaderPic>
          </article>
          </A4>
      
          </div>)
            :
            this.state.isActive7 === true ?
            (<div class="inner-sidepage"> <iframe src={"http://localhost:3000/piechart"} width="800" height="600" marginLeft="-500px" marginTop="-500px" frameborder="0" allowfullscreen></iframe></div>)
            :
            this.state.isActive8 === true ?
            (<div class="inner-sidepage"> <iframe src={"http://localhost:3000/SampleTemplate2"} width="1200" height="600" frameborder="0" allowfullscreen></iframe></div>)
            :          
            (<div class="inner-sidepage"> <img src={Userpics1} class="img-responsive" /> </div>)}
            
                
            </div>) : (<div class="col-lg-5 col-md-5 col-sm-4 col-xs-3">
          

          <Resizable
            ref={this.refResizable}
            defaultSize={20}
          >    
          
        <div style={{width:'1000px', height:'4000px', marginLeft:'-25px', marginTop:'-55px'}}>        
        
          <form name="contactForm" className="comment-form contact-form" onSubmit={(e) => handleLoginFormSubmits(e)}>    
          <Button type="submit" style={{width:'110px', marginLeft:'75%'}} class="thm-btn" bsStyle="primary"><span>Toolbar</span></Button>
          
          <ReactToPdf targetRef={ref} filename="resume.pdf" class="thm-btn" style={{width:'110px', marginLeft:'75%'}}>
          {({toPdf}) => (
              <button onClick={toPdf}>Download</button>
          )}
        </ReactToPdf>
          </form>
      
          <A4 style={{width:'1000px', height:'4000px',marginTop:'-90px'}}>
          <article className={cs({
              [style.cv]: true,
              exportRoot: true
          })}>
              <HeaderPic style={{
                padding: '0px 0px 0px 0px'
          }}>
          <header className={style.header}>
                      
          <div class="clearfix"></div>

          <div>
          

        

        <div style={{width:'1000px', height:'4000px', background: 'white'}} ref={ref}>

          

              
          <div id="editor" style={{height:'4000px', width:'1000px'}}>
            
          {this.state.isActive6 &&

          <div class="col-md-12 col-sm-12 col-xs-12">

          <div mRef={ctx => (this.ctx = ctx)}>
            <canvas width='60' height='70' ref={ctx => (this.ctx = ctx)}/>
          </div>
             
          <div class="col-md-3 col-sm-3 col-xs-3">
              <Draggable
                handle=".handle"
                defaultPosition={{x: 0, y: 0}}
                position={null}
                grid={[35, 35]}
                scale={1}
                onStart={this.handleStart}
                onDrag={this.handleDrag}
                onStop={this.handleStop}>
                <Resizable
                  ref={this.refResizable}
                  defaultSize={20}
                >
                <div>
                  <div className="handle"> <canvas width='60' height='70' ref={ctx => (this.ctx = ctx)}/></div>
                  <div>Drag the Graph</div>
                </div>
                </Resizable>
            </Draggable>
          </div>  
          </div>
          
          }

          <div className="text-editor">
              <CustomToolbar />
              <div style={{marginRight:'15%', width:'65%', height:'auto', marginLeft:'15%'}}>
              <ReactQuill
              ref={(el) => { this.reactQuillRef = el }}
              theme={'snow'}
              value={this.state.editorHtml}
              onChange={this.handleChange.bind(this)}
              onChange={this.handleChange}
              placeholder={this.props.placeholder}
              modules={modulesnew}
              formats={formats}
              />
              </div>
            </div>

          <div class="col-md-12 col-sm-12 col-xs-12">

          </div>

          </div>

          
          </div></div>  
          </header>
          </HeaderPic>
          </article>
          </A4>

          </div></Resizable>           
                
          </div>)}
            
        </div>
        </div>
        </div>
    </div>
    
    );
  }  onDrop(data) {

    //this.upload(data);

    //alert("data : "+data);

    console.log(data)
    // => banana 
}
}

export default Templates;