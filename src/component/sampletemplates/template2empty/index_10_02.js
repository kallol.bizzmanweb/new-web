import React , { Component} from 'react';

import Constants from '../common/Constants';

import { MDBContainer, MDBTabPane, MDBTabContent, MDBNav, MDBNavItem, MDBNavLink } from "mdbreact";

import { MDBBtn, MDBModal, MDBModalBody, MDBModalHeader, MDBModalFooter } from 'mdbreact';

import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';

import Dropzone from 'react-dropzone';

//import './style/react-tabs.css';

import './css/main.css';

//import './css/main.css.map';
//import './css/main.scss';

import style from './css/main.scss';

import loremIpsum from 'react-lorem-ipsum';

import styles from './css/style.scss';

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCubes, faFileAlt } from "@fortawesome/free-solid-svg-icons";
import { faAppleAlt } from "@fortawesome/free-solid-svg-icons";
import { faCog } from "@fortawesome/free-solid-svg-icons";
import { faFile } from "@fortawesome/free-solid-svg-icons";
import { faClock } from "@fortawesome/free-solid-svg-icons";
import { faArrowDown } from "@fortawesome/free-solid-svg-icons";
import { faCrown } from "@fortawesome/free-solid-svg-icons";

import { faUpload } from "@fortawesome/free-solid-svg-icons";
import { faTextHeight } from "@fortawesome/free-solid-svg-icons";
import { faFolder } from "@fortawesome/free-solid-svg-icons";
import { faColumns } from "@fortawesome/free-solid-svg-icons";
import { faVideo } from "@fortawesome/free-solid-svg-icons";
import { faEnvelope } from "@fortawesome/free-solid-svg-icons";

import A4 from '../baseTemplate'
import { EditableText, List, RowTexts, HeaderPic } from '../core'

import Userpics from './128.jpg';

import cs from 'classnames';

import RaisedButton from 'material-ui/RaisedButton'
////import cs from 'classnames'
import ph from './placeholder.jpg'
import phimg from './128.jpg'
import FileReaderInput from 'react-file-reader-input'

//import Userpics1 from './images/500.jpg';

import phpie from './charts7.png';

//import Draggable from 'react-draggable';

import { Draggable, Droppable } from 'react-drag-and-drop';

////import { EditorState } from "draft-js";

import { convertToRaw, convertFromRaw, EditorState } from 'draft-js';


import { Rnd } from "react-rnd";


import { DraggableEventHandler, default as DraggableRoot } from "react-draggable";
import { Enable, Resizable, ResizeDirection } from "re-resizable";

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import { Editor } from 'react-draft-wysiwyg';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';

const content = "<A4> <article className={cs({ [style.cv]: true, exportRoot: true})}><HeaderPic style={{ padding: '0px 0px 0px 0px'}}><header className={style.header}><div class='resume-wrapper'  style={{height:'1200px'}}></header></HeaderPic></article></A4>";

class Templates extends Component {
  
    state = {
      activeItem: "1"
    }

    

    constructor(props) {
      super(props);
      this.state = {
        showPopup: false,
        showDiv: true,
        show:true,
        isActive: true,
        isActive1: false,
        isActive2: false,
        isActive3: false,
        isActive4: false
      };

      ////const contentState = convertFromRaw(content);

      this.state = { content: "Resume "  + "\n"  + "Ranadip Chatterjee" + "\n" + "<h2>Introduction to Software Engineering</h2>" + "\n" + "<h3>Provide a course overview</h3></div>"};

      //const editorState = EditorState.createWithContent(this.state.content);

      


      this.state = {
        //'editorState: EditorState.createEmpty(),
        hideToolbar: false
      };

      this.state = {
        isOnHonver: false,
        imgSrc: phimg
      }

      this.state = {
        cv: {
          name: 'Wee',
          nickname: 'fi3ework',
        }
      }

      this.state = {
        editorState: EditorState.createEmpty(),
        hideToolbar: true
      };

      ////setEditorState("AQAAAAAAAA AAAAAAAA b");
      
      }
      
      onEditorStateChange = editorState => {
      this.setState({
      editorState
      });
      };
      
      onContentStateChange = contentState => {
      this.setState({
      contentState
      });
      };
    
  
    _arrayBufferToBase64(buffer) {
      let binary = ''
      let bytes = new Uint8Array(buffer)
      let len = bytes.byteLength
      for (let i = 0; i < len; i++) {
        binary += String.fromCharCode(bytes[i])
      }
      return window.btoa(binary)
    }
  
    upload = (e, results) => {
      let reader = results[0][0].target
      let that = this
      let base64 = 'data:image/jpg;base64, ' + this._arrayBufferToBase64(reader.result)
      that.img.setAttribute('src', base64)
    }

    uploads = (e, results) => {
      let reader = results[0][0].target
      let that = this
      let base64 = 'data:image/jpg;base64, ' + this._arrayBufferToBase64(reader.result)
      that.imgs.setAttribute('src', base64)
    }

    togglePopup() {
      this.setState({
        showPopup: !this.state.showPopup
      });
    }
    
    toggle = tab => () => {
      if (this.state.activeItem !== tab) {
      this.setState({
        activeItem: tab
      });
      }
    }

    handleShow = () => {
      this.setState({
        isActive: true,
        isActive1: false,
        isActive2: false,
        isActive3: false,
        isActive4: false,
        isActive5: false
      });
    };

    handleShows = () => {
      this.setState({
        isActive: false,
        isActive1: true,
        isActive2: false,
        isActive3: false,
        isActive5: false

      });
    };

    handleShows1 = () => {
      this.setState({
        isActive: false,
        isActive1: false,
        isActive2: true,
        isActive3: false,
        isActive4: false,
        isActive5: false
      });
    };

    handleShows2 = () => {
      this.setState({
        isActive: false,
        isActive1: false,
        isActive2: false,
        isActive3: true,
        isActive4: false,
        isActive5: false
      });
    };

    handleShows3 = () => {
      this.setState({
        isActive: false,
        isActive1: false,
        isActive2: false,
        isActive3: false,
        isActive4: true,
        isActive5: false
      });
    };
    handleShows4 = () => {
      this.setState({
        isActive: false,
        isActive1: false,
        isActive2: false,
        isActive3: false,
        isActive4: false,
        isActive5: true
      });
    };
  
    //state = { isOpen: false };
  
    handleShowDialog = () => {
      this.setState({ isOpen: !this.state.isOpen });
      console.log("clicked");
      
    };
  /*
    constructor(props) {
      super(props);
  
      this.state = { isOpen: false };
    }
  
    toggleModal = () => {
      this.setState({
        isOpen: !this.state.isOpen
      });
    }
    */

   
  
    render() {

      

      const { editorState } = this.state;

      const getUploadParams = () => {
        return { url: 'https://httpbin.org/post' }
      }
    
      const handleChangeStatus = ({ meta }, status) => {
        console.log(status, meta)
      }
    
      const handleSubmit = (files, allFiles) => {
        console.log(files.map(f => f.meta))
        allFiles.forEach(f => f.remove())
      }

      const imagesPath = Constants.imagespath;

      //const { showDiv } = this.state.showDiv;

      const template = () => {
        window.location.href = "/Template2";
        console.log("Template Page"); 
       }
  
      return (

        <A4>
        <article className={cs({
          [style.cv]: true,
         exportRoot: true
        })}>
          <HeaderPic style={{
            padding: '0px 0px 0px 0px'
          }}>
        <header className={style.header}>
          
        <div class="resume-wrapper"  style={{height:'1200px'}}>
      
        
        <Editor
        editorState={editorState}
        toolbarOnFocus={!this.state.hideToolbar}
        toolbarHidden={this.state.hideToolbar}
        wrapperClassName="demo-wrapper"
        editorClassName="demo-editor"
        toolbarClassName="toolbar-class"        
        onFocus={() => {
          this.setState({ hideToolbar: false });
        }}
        onBlur={() => {
          this.setState({ hideToolbar: true });
        }}
        onEditorStateChange={this.onEditorStateChange}
        onContentStateChange={this.onContentStateChange}
        placeholder={"  Write Here"}
        editorStyle={{
          backgroundColor: "#fff",
          padding: 0,
          borderWidth: 0,
          borderColor: "transparent",
          height: 60
        }}
      >
      </Editor>

    </div>
    </header>
    </HeaderPic>
    </article>
    </A4>
          
    
    );

    
  }
  onDrop(data) {

    //this.upload(data);

    //alert("data : "+data);

    console.log(data)
    // => banana 
}
}

export default Templates;