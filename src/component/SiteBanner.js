import React,{Component} from 'react';

import bannerVector from "../images/bannerVector.png";

import { MDBContainer } from 'mdbreact';


class SiteBanner extends Component {

 render(){

     return (


 <section className="site-banner">
        <div className="container">
            <div className="row">
                <div className="col-md-7">
                    <div className="banner-text">
                <div style={{height:'40px'}}></div>
                <div style={{fontSize:'35px',fontWeight:'500'}}>Create A Job-Ready Resume In Minutes<div style={{height:'30px'}}></div>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has </p>
                </div>                       
                </div>
                </div>
                <div className="col-md-5">
                    <div className="banner-vector"> <img src={bannerVector} alt="banner" className="img-responsive" /> </div>
                </div>
            </div>
        </div>
    </section>

     	);
 }

}

export default SiteBanner;

