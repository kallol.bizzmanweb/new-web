import React,{Component} from 'react';
import customerPic from '../images/customerPic.jpg';

class Reviews extends Component {

   render(){

    return(

        <section class="site-testimonial">
        <div class="container">
            <h2 class="template-header" style={{height:'140px'}}>
                   Join Over 3.1 Million Members
                 </h2>
            <center><span class="underline"></span></center>
            <div class="row">
                <div class="col-md-5">
                    <div class="site-tuts">
                        <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Distinctio quod dolore impedit praesentium quos veritatis dicta corrupti ratione laudantium, quia itaque blanditiis, nulla similique quidem, inventore aut natus molestiae! Sit?</p>
                        <hr/>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="rating rating2">
                                   <a href="#5" title="Give 5 stars">★</a>
                                   <a href="#4" title="Give 4 stars">★</a>
                                    <a href="#3" title="Give 3 stars">★</a>
                                  <a href="#2" title="Give 2 stars">★</a>
                                   <a href="#1" title="Give 1 star">★</a> </div>
                            </div>
                            <div class="col-md-6">
                                <div class="customer-img"> <img src={customerPic} alt ="customer" class="img-responsive" /> </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-5">
                    <div class="site-tuts">
                        <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Distinctio quod dolore impedit praesentium quos veritatis dicta corrupti ratione laudantium, quia itaque blanditiis, nulla similique quidem, inventore aut natus molestiae! Sit?</p>
                        <hr/>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="rating rating2">
                                   <a href="#5" title="Give 5 stars">★</a>
                                   <a href="#4" title="Give 4 stars">★</a>
                                    <a href="#3" title="Give 3 stars">★</a>
                                    <a href="#2" title="Give 2 stars">★</a>
                                  <a href="#1" title="Give 1 star">★</a> </div>
                            </div>
                            <div class="col-md-6">
                                <div class="customer-img"> <img src={customerPic} alt ="customer" class="img-responsive" /> </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br/>
            <div class="row">
                <div class="col-md-5">
                    <div class="site-tuts">
                        <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Distinctio quod dolore impedit praesentium quos veritatis dicta corrupti ratione laudantium, quia itaque blanditiis, nulla similique quidem, inventore aut natus molestiae! Sit?</p>
                        <hr/>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="rating rating2">
                                   <a href="#5" title="Give 5 stars">★</a>
                                   <a href="#4" title="Give 4 stars">★</a>
                                    <a href="#3" title="Give 3 stars">★</a>
                                   <a href="#2" title="Give 2 stars">★</a>
                                  <a href="#1" title="Give 1 star">★</a> </div>
                            </div>
                            <div class="col-md-6">
                                <div class="customer-img"> <img src={customerPic} alt ="customer" class="img-responsive" /> </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-5">
                    <div class="site-tuts">
                        <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Distinctio quod dolore impedit praesentium quos veritatis dicta corrupti ratione laudantium, quia itaque blanditiis, nulla similique quidem, inventore aut natus molestiae! Sit?</p>
                        <hr/>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="rating rating2">
                               <a href="#5" title="Give 5 stars">★</a>
                              <a href="#4" title="Give 4 stars">★</a>
                                  <a href="#3" title="Give 3 stars">★</a>
                                 <a href="#2" title="Give 2 stars">★</a>
                                 <a href="#1" title="Give 1 star">★</a> </div>
                            </div>
                            <div class="col-md-6">
                                <div class="customer-img"> <img src={customerPic} alt ="customer" class="img-responsive" /> </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>







    	);



   }

}

export default  Reviews ;