import React, { useRef, useState } from 'react';
import {Pie, Doughnut} from 'react-chartjs-2';

import { DraggableEventHandler, default as DraggableRoot } from "react-draggable";
import { Enable, Resizable, ResizeDirection } from "re-resizable";


import { Button } from 'react-bootstrap';

import Draggable from 'react-draggable';

var retrievedDatas1 = sessionStorage.getItem("text1");


//const text6 = JSON.parse(retrievedDatas6);

if(retrievedDatas1!=null){

  const retrievedDatas1 = sessionStorage.getItem("text1");
//const text1 = JSON.parse(retrievedDatas1);

////alert("data : "+retrievedDatas1);

const retrievedsess = sessionStorage.getItem("search");

////alert("retrievedsess .."+retrievedsess);

//var retrievedDatas1 = sessionStorage.getItem("text1");

var retrievedDatas2 = sessionStorage.getItem("text2");
//const text2 = JSON.parse(retrievedDatas2);

var retrievedDatas3 = sessionStorage.getItem("text3");
//const text3 = JSON.parse(retrievedDatas3);

var retrievedDatas4 = sessionStorage.getItem("text4");
//const text4 = JSON.parse(retrievedDatas4);

var retrievedDatas5 = sessionStorage.getItem("text5");
//const text5 = JSON.parse(retrievedDatas5);

var retrievedDatas6 = sessionStorage.getItem("text6");
//const text6 = JSON.parse(retrievedDatas6);

}else{

 const retrievedDatas1 = "100";

  var retrievedDatas2 = "200";
//const text2 = JSON.parse(retrievedDatas2);

var retrievedDatas3 = "100";
//const text3 = JSON.parse(retrievedDatas3);

var retrievedDatas4 = "400";
//const text4 = JSON.parse(retrievedDatas4);

var retrievedDatas5 = "800";
//const text5 = JSON.parse(retrievedDatas5);

var retrievedDatas6 = "100";
//const text6 = JSON.parse(retrievedDatas6);

}



var sum = retrievedDatas1 + retrievedDatas2 + retrievedDatas3 + retrievedDatas4 + retrievedDatas5 + retrievedDatas6;

var retrievedDatas11 = (retrievedDatas1/sum) * 100;
var retrievedDatas21 = (retrievedDatas2/sum) * 100;
var retrievedDatas31 = (retrievedDatas3/sum) * 100;
var retrievedDatas41 = (retrievedDatas4/sum) * 100;
var retrievedDatas51 = (retrievedDatas5/sum) * 100;
var retrievedDatas61 = (retrievedDatas6/sum) * 100;


const state = {

  labels: ['Summary', 'Skills', 'Profile', 'Work Experience', 'Education', 'Text Section'],
  datasets: [
    {
      label: 'Rainfall',
      backgroundColor: [
        '#B21F00',
        '#C9DE00',
        '#2FDE00',
        '#00A6B4',
        '#6800B4',
        '#008080'
      ],
      hoverBackgroundColor: [
      '#501800',
      '#4B5000',
      '#175000',
      '#003350',
      '#35014F',
      '#FF00FF'
      ],
      data: [retrievedDatas1, retrievedDatas2, retrievedDatas3, retrievedDatas4, retrievedDatas5, retrievedDatas6]
    }
  ]
}

var Templates =  function App() {

  

  const [copySuccess, setCopySuccess] = useState('');
  const textAreaRef = useRef(null);


  var copyToClipboardsDN = (e) => {
    textAreaRef.Doughnut.state;
    document.execCommand('copy');
    // This is just personal preference.
    // I prefer to not show the the whole text area selected.
    e.target.focus();
    setCopySuccess('Graph DN Copied!');
  };
 
    return (
      <div>

        {
         document.queryCommandSupported('copy') &&
          <div>
            <button onClick={copyToClipboardsDN}>Copy</button> 
            
          </div>
        }

        <form>
        <Doughnut
          data={state}
          options={{
            title:{
              display:true,
              text:'% of different captions in Resume',
              fontSize:20
            },
            legend:{
              display:true,
              position:'right'
            }
          }}
          ref={textAreaRef}
        />
        </form>
     
      </div>
    );
  }


export default Templates;